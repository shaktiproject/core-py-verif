import os
import re
import shutil
import subprocess
import shlex
import logging
import random
import string
from string import Template
import sys

import riscof.utils as utils
from riscof.pluginTemplate import pluginTemplate
import riscof.constants as constants

logger = logging.getLogger()

map = {
    'rv32i': 'rv32i',
    'rv32im': 'rv32im',
    'rv32ic': 'rv32ic',
    'rv32ia': 'rv32ia',
    'rv64i' : 'rv64i',
    'rv64im': 'rv64im'
}


class cclass(pluginTemplate):
    __model__ = "cclass"
    __version__ = "0.5.0"

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

        # error out if the config.ini does not have cclass node in it
        config = kwargs.get('config')
        if config is None:
            print("Please enter cclassbin path in configuration.")
            raise SystemExit
        else:
            # we collect the verilated cclass executable here. We expect the
            # user to have already built this before hand and the path of the
            # directory be mentioned in the config file. Please refer to the
            # README for directions on how to build the verilated executable.
            self.cclassbin = config['cclassbin']

        # collect path of current directory.
        path = os.path.abspath(os.path.dirname(__file__))

        # collect the isa yaml path from the config.ini node
        self.isa_spec = os.path.abspath(config['ispec'])
        
        # collect the platform yaml path from the config.ini node
        self.platform_spec = os.path.abspath(config['pspec'])

        # collect path of the plugin from the config.ini node
        self.pluginpath = os.path.abspath(config['pluginpath'])

        # misc variables not used 
        self.user_target = "cclass"
        self.user_sign = "sign"

        logger.debug('Name is : ' + self.name)

    # in this function we will set the compile and objdump commands we will be
    # using to compile the tests
    def initialise(self, suite, work_dir, compliance_env):

        # collect the suite directory path
        self.suite = suite

        # collect the work_dir where artifacts must be generated.
        self.work_dir = work_dir

        # set the compile command. The march args will come from the test-list.
        self.compile_cmd = 'riscv{1}-unknown-elf-gcc -march={0} \
         -static -mcmodel=medany -fvisibility=hidden -nostdlib -nostartfiles\
         -T '+self.pluginpath+'/env/link.ld\
         -I '+self.pluginpath+'/env/\
         -I ' + compliance_env
        self.objdump = "riscv{1}-unknown-elf-" + 'objdump -D rtl.elf > {0}.disass'

    # in this function we have the checked isa and platform yamls, which we will
    # use to capture the xlen, isa and mabi args.
    def build(self, isa_yaml, platform_yaml):
        
        # load the isa yaml as a python dictionary
        ispec = utils.load_yaml(isa_yaml)

        # set xlen based on variables in supported_xlen
        self.xlen = ('64' if 64 in ispec['hart0']['supported_xlen'] else '32')

        # set isa variable
        self.isa = 'rv' + self.xlen
        self.compile_cmd = self.compile_cmd+' -mabi='+('lp64 ' if 64 in ispec['hart0']['supported_xlen'] else 'ilp32 ')
        if "I" in ispec['hart0']["ISA"]:
            self.isa += 'i'
        if "M" in ispec['hart0']["ISA"]:
            self.isa += 'm'
        if "C" in ispec['hart0']["ISA"]:
            self.isa += 'c'
    
    # in this function we will be creating a makefile, where each target of the
    # makefile corresponds to compile a test, running it on the verilated
    # executable and format the signature. The following function will parse
    # through each entry in the the test-list to create a target in the makefile
    # At the end, the function runs the makefile in parallel.
    def runTests(self, testList):
        # setup up the make utility
        make = utils.makeUtil(makefilePath=os.path.join(self.work_dir, "Makefile." + self.name[:-1]))

        # set the make command to be used
        make.makeCommand = 'make -j10'

        # parse each entry in the testlist
        for file in testList:
            # name of the file
            testentry = testList[file]

            # path of the assembly test
            test = testentry['test_path']

            # working directory for this particular test
            test_dir = testentry['work_dir']
            
            # compile elf file name
            elf = 'rtl.elf'

            # The following are now bash commands that each target of the
            # Makefile will have.
            # first change directory to the test's working directory
            execute = "cd "+testentry['work_dir']+";"

            # In the compile command set the march variables based on the
            # test-entry
            cmd = self.compile_cmd.format(testentry['isa'].lower(),
                self.xlen) + ' ' + test + ' -o ' + elf

            # set the compile macros as well
            compile_cmd = cmd + ' -D' + " -D".join(testentry['macros'])

            # append the compile command to the execute steps
            execute+=compile_cmd+";"

            # append the objdump command to the execute steps
            execute+=self.objdump.format(test, self.xlen) + ';'
            
            test_dir = testentry['work_dir']
            d = dict(elf=elf,
                     testDir=test_dir,
                     cclassbin=self.cclassbin)

            # set the command to run the compiled test on the verilated
            # executable. This first creates a soft-link of the verilated
            # executable, converts it to hex using spike's elf2hex command.
            command = Template(
                '''ln -sf ${cclassbin}/* ${testDir}/. ; elf2hex 8 4194304 $elf 2147483648 > ${testDir}/code.mem'''
            ).safe_substitute(d)

            execute+=command+';'
    
            # initiate the simulation
            command = "cd " + testentry['work_dir'] + '; timeout 30s ./cclass_core +rtldump'
            execute += command+';'

            # rename the signature file
            sign_file = os.path.join(test_dir,
                                     self.name[:-1] + ".signature")
            execute += "cat " + os.path.join(test_dir,
                                       "signature") + " > " + sign_file + ';'

            # create a makefile target
            make.add_target(execute)
        # execute the makefile targets
        make.execute_all(self.work_dir)
