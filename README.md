# core-py-verif

## File Structure

```
├── cocotb_env.py       # should contain all the cocotb related routines used in testbench
├── conftest.py         # configuration file for pytest command line env setting
├── README.md           # this file
├── requirements.txt    # requirements file containing all python libraries required to run tests
├── test_cclass.py      # c-class test-bench
├── test_top.py         # top level pytest file which will collect tests, compile and call test_cclass
├── tcclibs             # this includes bluetcl files which are used to capture informatino about the bsv structs used in the design
├── user_config.py      # user defined configuration
├── river_config.ini    # river_core configuration file for generation of tests.
├── conftest.py         # includes python functions to enable pytest parser adoption
├── mycheckers          # this is where all the checkers (model + scoreboard) for submodules will be populated
├── mycoverage          # this is where all the coverage models will be populated
├── alias_signal.yaml   # this yaml file contains a dictionary of the signal aliases used in checkers and coverage models
└── utils.py            # miscellaneous utilities
```

## Install Python dependencies

The following command will install all python libraries required for testing activity

```
pip3 install -r requirements.txt
```

## Getting started

**ASSUMPTIONS**: You have installed python v3.7, bsc and verilator v4.106 (<-- exactly this version)

### Clone and compile C-Class

Replace `<jobs>` below by number of threads you want to use in parallel. This following assumes you
are cloning to the home directory.

```
cd ~/
git clone https://gitlab.com/shaktiproject/cores/c-class.git
cd ~/c-class
git checkout csrbox_integration
python3 -m configure.main -ispec sample_config/c64/rv64i_isa.yaml   -customspec sample_config/c64/rv64i_custom.yaml   -cspec sample_config/c64/core64.yaml   -gspec sample_config/c64/csr_grouping64.yaml   --verbose debug
make -j<jobs> generate_verilog
make link_verilator generate_boot_files
```

### Generate tests using `river_core` with aapg_plugin

We will be using river\_core to generate tests and use the test-list format adopted by river\_core as
a regression list to run simulations on the core. The `aapg_plugin` is already available in this
repo and thus can be used out of box itself. 

In `river_config.ini`, change the generator as `aapg` as given below.

```
# Generator options for the compiler
# Should ideally be aapg, microtesk, generic and if something else comes along
generator = aapg

```

From the root directory of the repo run the following command
```
cd core-py-verif/
river_core generate -c river_config.ini -v debug
```

This should generate a `tests` folder with multiple asm files and work directories. What is of
interest to us is the `test_list.yaml`. We will be using this file to run regressions on the c-class
in out cocotb framework

### Generate tests using `river_core` with riscof_plugin

We will be using river\_core to generate tests and use the test-list format adopted by river\_core as
a regression list to run simulations on the core. The `riscof_plugin` is already available in this
repo and thus can be used out of box itself. 

In `river_config.ini`, change the generator as `riscof` as given below.

```
# Generator options for the compiler
generator = riscof

```

Then from the root directory of the repo run the following command
```
cd core-py-verif/
river_core generate -c river_config.ini -v debug
```

This should generate a `tests` folder with multiple asm files and work directories. What is of
interest to us is the `test_list.yaml`. We will be using this file to run regressions on the c-class
in out cocotb framework

### Compiling the core verilog with verilator through cocotb

You can do the following once before you run tests in parallel. This will avoid all initial threads
from compiling the core themselves, and rather use this build as is

```
SIM=verilator pytest -k compile -o log_cli=True test_top.py --capture=tee-sys -v --log-cli-level=0 --html=compile-report.html --self-contained-html
```

The above command will create a `sim_build` directory which has the verilated binary. This will also
create a `bsvstruct_class.py` python file which has converted most of the bsv structs into python
dataclasses which will make it easy to extract subfields from a signals.

### Running tests with cocotb

**NOTE: THIS IS JUST A TRIVIAL SETUP. THE PASS AT THE END DOES NOT MEAN ANYTHING**

Open `user_config.py` and set paths to `cclass_path` and `bsclib` variables according to you system.
Leave everything else as is.

Make sure to enable the required checker and coverage files that you want to run in `enables.yaml`.

Running tests:

```
SIM=verilator pytest -k run -o log_cli=True test_top.py --capture=tee-sys -v --log-cli-level=0 --html=run-report.html --self-contained-html
```
This will start running the tests one by one on the cocotb based testbench. At the end you will find
a report.html which will capture pass/fail status.

To clean your setup run the following:
```
SIM=verilator pytest -k clean -o log_cli=True test_top.py --capture=tee-sys -v --log-cli-level=0
```

### Merging coverage with cocotb

```
SIM=verilator pytest -k merge -o log_cli=True test_top.py --capture=tee-sys -v --log-cli-level=0 --html=merge-report.html --self-contained-html
```


## Understanding the command line

- `SIM=verilator`: This indicates cocotb that we are going to use `verilator` as the simulator
- `pytest`: This is a python utility to initiate cocotb python test-bench in a parameterized fashion
- `-k`: select which set of tests to run
- `log_cli`: Indicates to capture log on screen
- `log-cli-level`: integer multiples of 10. Indicates verbosity level. 0- means print everything. 30-
  means print only critical messages
- --capture=tree-sys`: This enables capturing the log of each test in the html report as well.

## Artifacts of the run

The first artifact of the run will be compile-report.hmlt. This is will capture the pass/fail status
of each test and also provide detailed logs of each run

Within each tests working directory you will find:

- sim.log: this is the log file which will have all the log information that is printed from cocotb
- coverage.yml - this is the coverage file generated for each test

## Running a single test for debug

If you go to the `work_dir` of a test you can run the verilated binary on it using the following
command:

```
MODULE=test_cclass ./sim_build/mkTbSoc +rtldump
```

Other plusargs can also be provided in the above command
