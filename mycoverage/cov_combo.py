import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto

in1_bins = [0, 1]+walking_ones(64)+walking_zeros(64)+leading_ones(64)+leading_zeros(64)+alternate_ones(64)+alternate_zeros(64)+signed_special(64)

class fun3_enum(IntEnum):
    MUL = 0
    MULH = 1
    MULHSU = 2
    MULHU = 3

def compare_val(list_val, n):
    lst = []
    size = len(list_val)
    for i in range(size-2):
        if list_val[i] == list_val[i+(n-2)] and list_val[i+(n-2)] == list_val[i+(n-1)]:
            lst.append(list_val[i])
    if len(list_val)>2:
        if len(lst) == 0:
            return False
        else:
            return True

class InputSample:
    def __init__(self):
        self.in1 : int
        self.in2 : int
        self.funct3 : int
        self.wordop : int
        self.list_in1 : bool
        self.list_in2 : bool
        self.list_fun3 : bool
        self.list_wordop : bool
        self.oneinst_exec : bool
        self.twoinst_exec : bool
        self.threeinst_exec : bool
        self.fourinst_exec : bool
        self.null_cycles : bool
    def __str__(self):
        return 'in1 : '+str(hex(self.in1)) + \
                ' in2: ' +str(hex(self.in2)) +\
                ' funct3: ' + str(self.funct3) +\
                ' wordop: ' +str(self.wordop)

class Coverage(object):
    
    _signals = get_signals('combo','inputs')
    _signals.update(get_signals('combo', 'outputs'))
    _signals.update(get_signals('combo', 'wire'))

    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals_val())
        cocotb.fork(self.sample_signals_cycle())

    FnmulCov_val = coverage_section (
        CoverPoint("top.combo_Ops", xf = lambda sample: sample.funct3 , 
                                bins = [x.name for x in list(fun3_enum)]
                                ),
        CoverPoint("top.combo_In1", xf = lambda sample: sample.in1 , bins = in1_bins ),
        CoverPoint("top.combo_In2", xf = lambda sample: sample.in2 , bins = in1_bins ),
        CoverPoint("top.combo_wordop", xf = lambda sample: sample.wordop , bins = [1, 0]),
        CoverPoint("top.combo_list_in1", xf = lambda sample: sample.list_in1, bins = [True, False]),
        CoverPoint("top.combo_list_in2", xf = lambda sample: sample.list_in2, bins = [True, False]),
        CoverPoint("top.combo_list_fun3", xf = lambda sample: sample.list_fun3, bins = [True, False]),
        CoverPoint("top.combo_list_wordop", xf = lambda sample: sample.list_wordop, bins = [True, False]),
    )

    @FnmulCov_val
    def sample_coverage_val(self, sample):
        pass 

    @coroutine
    def sample_signals_val(self):
        list_in1 = []
        list_in2 = []
        list_fun3 = []
        list_wordop = []
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.input.value.integer == 1:
                sample = InputSample()
                sample.in1 = self.in1.value.signed_integer
                sample.in2 = self.in2.value.signed_integer
                sample.funct3 = fun3_enum(self.funct3.value.integer).name
                sample.wordop = self.wordop.value.integer
                list_in1.append(self.in1.value.integer)
                list_in2.append(self.in2.value.integer)
                list_fun3.append(self.funct3.value.integer)
                list_wordop.append(self.wordop.value.integer)
                sample.list_in1 = compare_val(list_in1,3)
                sample.list_in2 = compare_val(list_in2,3)
                sample.list_fun3 = compare_val(list_fun3,3)
                sample.list_wordop = compare_val(list_wordop,3) 
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage_val(sample)                          

    FnmulCov_val = coverage_section (
            CoverPoint("top.combo_oneinst_exec", xf = lambda sample: sample.oneinst_exec, bins = [True, False]),
            CoverPoint("top.combo_twoinst_exec", xf = lambda sample: sample.twoinst_exec, bins = [True, False]),
            CoverPoint("top.combo_threeinst_exec", xf = lambda sample: sample.threeinst_exec, bins = [True, False]),
            CoverPoint("top.combo_fourinst_exec", xf = lambda sample: sample.fourinst_exec, bins = [True, False]),
            CoverPoint("top.combo_null_cycles", xf = lambda sample: sample.null_cycles, bins = [True, False]),
        )

    @FnmulCov_val
    def sample_coverage_cycle(self, sample):
        pass

    @coroutine
    def sample_signals_cycle(self):
        count_input = 0
        null_cycle = 0
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.input.value.integer == 1 or self.mv_valid.value.integer == 1 or self.in_0.value.integer == 1 or self.out_0.value.integer == 1:
                null_cycle = null_cycle
            else: 
                null_cycle = null_cycle + 1
            if self.input.value.integer == 1:
                count_input = count_input + 1
            if self.mv_valid.value.integer == 1:            
                sample = InputSample()
                sample.oneinst_exec = True if count_input == 1 else False
                sample.twoinst_exec = True if count_input == 2 else False
                sample.threeinst_exec = True if count_input == 3 else False
                sample.fourinst_exec = True if count_input == 4 else False
                sample.null_cycles = True if ((null_cycle-1)>2) else False
                count_input = 0
                null_cycle = 0
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage_cycle(sample)


