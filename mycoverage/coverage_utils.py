from math import *
import itertools
import random
import copy

def twos(val,bits):
    '''
    Finds the twos complement of the number
    :param val: input to be complemented
    :param bits: size of the input

    :type val: str or int
    :type bits: int

    :result: two's complement version of the input

    '''
    if isinstance(val,str):
        if '0x' in val:
            val = int(val,16)
        else:
            val = int(val,2)
    if (val & (1 << (bits - 1))) != 0:
        val = val - (1 << bits)
    return val
    
def signed_special(size, signed=True):
    if signed:
        conv_func = lambda x: twos(x,size)
        sqrt_min = int(-sqrt(2**(size-1)))
        sqrt_max = int(sqrt((2**(size-1)-1)))
    else:
        sqrt_min = 0
        sqrt_max = int(sqrt((2**size)-1))
        conv_func = lambda x: (int(x,16) if '0x' in x else int(x,2)) if isinstance(x,str) else x

    dataset = [3, "0x"+"".join(["5"]*int(size/4)), "0x"+"".join(["a"]*int(size/4)), 5, "0x"+"".join(["3"]*int(size/4)), "0x"+"".join(["6"]*int(size/4))]
    dataset = list(map(conv_func,dataset)) + [int(sqrt(abs(conv_func("0x8"+"".join(["0"]*int((size/4)-1)))))*(-1 if signed else 1))] + [sqrt_min,sqrt_max]
    return dataset

def walking_ones(size, signed=True):
    mask=2**size-1
    if not signed:
        dataset = [1 << exp for exp in range(size)]
    else:
        dataset = [twos(1 << exp,size) for exp in range(size)]
    return dataset
    
def walking_zeros(size, signed=True):
    mask = 2**size -1 
    if not signed:
       dataset = [(1 << exp)^mask for exp in range(size)]
    else:
        dataset = [twos((1 << exp)^mask,size) for exp in range(size)]
    return dataset
    
def leading_ones(size):
    random.seed(10)
    coverpoints = []
    default = 2**size-1
    for sz in range(0,size+1):
        val = (default << sz) & default
        setval = (1 << sz-1) ^ default if sz!=0 else default
        val = (val | random.randrange(1,2**size)) & default & setval
        coverpoints.append(val)
    return coverpoints
    
def leading_zeros(size):
    random.seed(10)
    coverpoints = []
    default = 2**size-1
    for sz in range(0,size+1):
        val = (1 << sz)-1 & default
        setval = 1 << (sz-1) if sz!=0 else 0
        val = 1 << (sz-1) if sz!=0 else 0
        coverpoints.append(val)
    return coverpoints
    
def alternate_ones(size, signed=True):
    coverpoints=[]
    for s in range(2, size):
        t1 =( '' if s%2 == 0 else '1') + ''.join(['01']*int(s/2))
        if not signed:
             dataset = int(t1,2)
        else:
            dataset = twos(t1,s)
        coverpoints.append(dataset)
    return coverpoints
    
def alternate_zeros(size, signed=True):
    coverpoints=[]
    for s in range(2, size):
        t1 =( '' if s%2 == 0 else '0') + ''.join(['10']*int(s/2))
        if not signed:
             dataset = int(t1,2)
        else:
            dataset = twos(t1,s)
        coverpoints.append(dataset)
    return coverpoints
    pass


