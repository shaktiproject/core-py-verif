import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto
from bsvstruct_class import *

nzimm_6_bins = range(-32,32) #[1]+walking_ones(6)+walking_zeros(6) +leading_ones(6)+leading_zeros(6)+alternate_ones(6)+alternate_zeros(6)+signed_special(6)
imm_11=[0, 1]+walking_ones(11)+walking_zeros(11)+leading_ones(11)+leading_zeros(11)+alternate_ones(11)+alternate_zeros(11)+signed_special(11)
imm_8=[0, 1]+walking_ones(8)+walking_zeros(8)+leading_ones(8)+leading_zeros(8)+alternate_ones(8)+alternate_zeros(8)+signed_special(8)
nzuimm_6_bins=range(0,64)
rd_bins = [x for x in list(range(32))]
rd_3_bins = [x for x in list(range(8))]

rs2_bins=[x for x in list(range(8))]

class inst_enum(Enum):
    CNOP = auto()
    CADDI = auto()
    CLI = auto()
    CADDIW = auto()
    CADDI16SP = auto()
    CLUI = auto()
    CSRLI = auto()
    CSRAI = auto()
    CANDI = auto()
    CSUB = auto()
    CXOR = auto()
    COR = auto()
    CAND = auto()
    CSUBW = auto()
    CADDW = auto()
    CJ = auto()
    CBEQZ = auto()
    CBNEZ = auto()
    CSLLI = auto()
    CFLDSP = auto()
    CLWSP = auto()
    CLDSP = auto()
    CJR = auto()
    CMV = auto()
    CJALR = auto()
    CADD = auto()
    CFSDSP = auto()
    CSWSP = auto()
    CSDSP = auto()
    CADDI4SPN = auto()
    CFLD = auto()
    CLW = auto()
    CLD = auto()
    CFSD = auto()
    CSW = auto()
    CSD = auto()
    CEBREAK=auto()
    ILLEGAL = auto()
    RESERVED = auto()
    UKNOWN = auto()
    

class InputSample:
    def __init__(self):
        self.instruction: inst_enum
        self.nzimm: int
        self.nzuimm: int
        self.nzuimm_a4: int
        self.uimm: int
        self.uimm_5: int
        self.imm_j: int
        self.imm_bne_beq: int
        self.rs1_rd: int
        self.rs1_rd_3: int
        self.is_hint: bool
        self.rs2: int
        self.rs2_5: int
        self.rs1: int
        self.rd: int
        self.is_reserved: bool
        self.is_custom_32: bool
    def __str__ (self):
        return 'Instr: ' +str(self.instruction) + 'nzuimm: ' + str(self.nzuimm_a4)  +\
                'uimm: '+str(self.uimm_5) + 'rs1: ' + str(self.rs1) +\
                'rs2: ' +str(self.rs2_5) + 'rd:' +str(self.rd)

class Coverage(object):    
    
    _signals = get_signals('module_fn_decompress','inputs')
    _signals.update(get_signals('mkstage1','wires',['process_instruction_fire']))
    _signals.update(get_signals('mkstage1','inputs',['csr_misa_c']))
    _signals.update(get_signals('mkstage1','outputs',['tx_stage2_data']))

    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = True
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals())
    
    FnDecompressCov = coverage_section(
        CoverPoint("top.c_nzimm",xf = lambda sample: sample.nzimm, bins = nzimm_6_bins),
        CoverPoint("top.c_nzuimm",xf = lambda sample: sample.nzuimm, bins = nzuimm_6_bins),
        CoverPoint("top.c_nzuimm_a4",xf = lambda sample: sample.nzuimm_a4, bins = range(0, 256)),
        CoverPoint("top.c_uimm",xf = lambda sample: sample.uimm, bins = nzuimm_6_bins),
        CoverPoint("top.c_uimm_5",xf = lambda sample: sample.uimm_5, bins = rd_bins),
        CoverPoint("top.c_imm_j",xf = lambda sample: sample.imm_j, bins = imm_11),
        CoverPoint("top.c_imm_bne_beq",xf = lambda sample: sample.imm_bne_beq, bins = imm_8),
        CoverPoint("top.c_rs1_rd",xf = lambda sample: sample.rs1_rd, bins = rd_bins),
        CoverPoint("top.c_rs2_5",xf = lambda sample: sample.rs2_5, bins = rd_bins),
        CoverPoint("top.c_rs1_rd_3",xf = lambda sample: sample.rs1_rd_3, bins = rd_3_bins),
        CoverPoint("top.c_rs2",xf = lambda sample: sample.rs2, bins = rs2_bins),
        CoverPoint("top.c_rs1",xf = lambda sample: sample.rs1, bins = rs2_bins),
        CoverPoint("top.c_rd",xf = lambda sample: sample.rd, bins = rs2_bins),
        CoverPoint("top.c_instruction",xf = lambda sample: sample.instruction, bins = [x.name for x in list(inst_enum)]),
        CoverPoint("top.c_hints",xf = lambda sample: sample.is_hint, bins=[True]),
        CoverPoint("top.c_reserved",xf = lambda sample: sample.is_reserved, bins=[True]),
        CoverPoint("top.c_custom",xf = lambda sample: sample.is_custom_32, bins=[True]),
        CoverPoint("top.c_nop",xf = lambda sample: sample.instruction, bins = ["CNOP"]),
        CoverPoint("top.c_addi",xf = lambda sample: sample.instruction, bins = ["CADDI"]),
        CoverPoint("top.c_addiw",xf = lambda sample: sample.instruction, bins = ["CADDIW"]),
        CoverPoint("top.c_li",xf = lambda sample: sample.instruction, bins = ["CLI"]),
        CoverPoint("top.c_addi16sp",xf = lambda sample: sample.instruction, bins = ["CADDI16SP"]),
        CoverPoint("top.c_lui",xf = lambda sample: sample.instruction, bins = ["CLUI"]),
        CoverPoint("top.c_srli",xf = lambda sample: sample.instruction, bins = ["CSRLI"]),
        CoverPoint("top.c_srai",xf = lambda sample: sample.instruction, bins = ["CSRAI"]),
        CoverPoint("top.c_andi",xf = lambda sample: sample.instruction, bins = ["CANDI"]),
        CoverPoint("top.c_sub",xf = lambda sample: sample.instruction, bins = ["CSUB"]),
        CoverPoint("top.c_xor",xf = lambda sample: sample.instruction, bins = ["CXOR"]),
        CoverPoint("top.c_or",xf = lambda sample: sample.instruction, bins = ["COR"]),
        CoverPoint("top.c_and",xf = lambda sample: sample.instruction, bins = ["CAND"]),
        CoverPoint("top.c_subw",xf = lambda sample: sample.instruction, bins = ["CSUBW"]),
        CoverPoint("top.c_addw",xf = lambda sample: sample.instruction, bins = ["CADDW"]),
        CoverPoint("top.c_j",xf = lambda sample: sample.instruction, bins = ["CJ"]),
        CoverPoint("top.c_beqz",xf = lambda sample: sample.instruction, bins = ["CBEQZ"]),
        CoverPoint("top.c_bnez",xf = lambda sample: sample.instruction, bins = ["CBNEZ"]),
        
        CoverPoint("top.c_slli",xf = lambda sample: sample.instruction, bins = ["CSLLI"]),
        CoverPoint("top.c_fldsp",xf = lambda sample: sample.instruction, bins = ["CFLDSP"]),
        CoverPoint("top.c_lwsp",xf = lambda sample: sample.instruction, bins = ["CLWSP"]),
        CoverPoint("top.c_ldsp",xf = lambda sample: sample.instruction, bins = ["CLDSP"]),
        CoverPoint("top.c_jr",xf = lambda sample: sample.instruction, bins = ["CJR"]),
        CoverPoint("top.c_mv",xf = lambda sample: sample.instruction, bins = ["CMV"]),
        CoverPoint("top.c_jalr",xf = lambda sample: sample.instruction, bins = ["CJALR"]),
        CoverPoint("top.c_add",xf = lambda sample: sample.instruction, bins = ["CADD"]),
        CoverPoint("top.c_fsdsp",xf = lambda sample: sample.instruction, bins = ["CFSDSP"]),
        CoverPoint("top.c_swsp",xf = lambda sample: sample.instruction, bins = ["CSWSP"]),
        CoverPoint("top.c_sdsp",xf = lambda sample: sample.instruction, bins = ["CSDSP"]),
        
        CoverPoint("top.c_addi4spn",xf = lambda sample: sample.instruction, bins = ["CADDI4SPN"]),
        CoverPoint("top.c_fld",xf = lambda sample: sample.instruction, bins = ["CFLD"]),
        CoverPoint("top.c_lw",xf = lambda sample: sample.instruction, bins = ["CLW"]),
        CoverPoint("top.c_ld",xf = lambda sample: sample.instruction, bins = ["CLD"]),
        CoverPoint("top.c_fsd",xf = lambda sample: sample.instruction, bins = ["CFSD"]),
        CoverPoint("top.c_sw",xf = lambda sample: sample.instruction, bins = ["CSW"]),
        CoverPoint("top.c_sd",xf = lambda sample: sample.instruction, bins = ["CSD"]),
        #---------------------Q1-----------------------------------------------------------------------------------
        CoverCross("top.cnop_nzim", ["top.c_nzimm","top.c_nop"]),
        CoverCross("top.cnop_hint", ["top.c_hints","top.c_nop"]),
        CoverCross("top.caddi_nzim", ["top.c_nzimm","top.c_addi"]),
        CoverCross("top.caddi_rs1rd", ["top.c_rs1_rd","top.c_addi"]),
        CoverCross("top.caddi_hint", ["top.c_hints","top.c_addi"]),
        CoverCross("top.caddiw_nzim", ["top.c_nzimm","top.c_addiw"]),
        CoverCross("top.caddiw_rs1rd", ["top.c_rs1_rd","top.c_addiw"]),
        CoverCross("top.caddiw_reserved", ["top.c_hints","top.c_addiw"]),
        CoverCross("top.cli_nzim", ["top.c_nzimm","top.c_li"]),
        CoverCross("top.cli_rs1rd", ["top.c_rs1_rd","top.c_li"]),
        CoverCross("top.cli_hint", ["top.c_hints","top.c_li"]),
        CoverCross("top.caddi16sp_nzim", ["top.c_nzimm","top.c_addi16sp"]),
        CoverCross("top.caddi16sp_res", ["top.c_reserved","top.c_addi16sp"]),
        CoverCross("top.clui_nzim", ["top.c_nzimm","top.c_lui"]),
        CoverCross("top.clui_rs1rd", ["top.c_rs1_rd","top.c_lui"]),
        CoverCross("top.clui_hint", ["top.c_hints","top.c_lui"]),
        CoverCross("top.clui_reserved", ["top.c_reserved","top.c_lui"]),
        CoverCross("top.csrli_nzuim", ["top.c_nzuimm","top.c_srli"]),
        CoverCross("top.csrli_rs1rd", ["top.c_rs1_rd_3","top.c_srli"]),
        CoverCross("top.csrli_custom", ["top.c_custom","top.c_srli"]),
        CoverCross("top.csrai_nzuim", ["top.c_nzuimm","top.c_srai"]),
        CoverCross("top.csrai_rs1rd", ["top.c_rs1_rd_3","top.c_srai"]),
        CoverCross("top.csrai_custom", ["top.c_custom","top.c_srai"]),
        CoverCross("top.candi_nzim", ["top.c_nzimm","top.c_andi"]),
        CoverCross("top.candi_rs1rd", ["top.c_rs1_rd_3","top.c_andi"]),
        CoverCross("top.csub_rs1rd", ["top.c_rs1_rd_3","top.c_sub"]),
        CoverCross("top.cxor_rs1rd", ["top.c_rs1_rd_3","top.c_xor"]),
        CoverCross("top.cor_rs1rd", ["top.c_rs1_rd_3","top.c_or"]),
        CoverCross("top.cand_rs1rd", ["top.c_rs1_rd_3","top.c_and"]),
        CoverCross("top.csubw_rs1rd", ["top.c_rs1_rd_3","top.c_subw"]),
        CoverCross("top.caddw_rs1rd", ["top.c_rs1_rd_3","top.c_addw"]),
        CoverCross("top.csub_rs2", ["top.c_rs2","top.c_sub"]),
        CoverCross("top.cxor_rs2", ["top.c_rs2","top.c_xor"]),
        CoverCross("top.cor_rs2", ["top.c_rs2","top.c_or"]),
        CoverCross("top.cand_rs2", ["top.c_rs2","top.c_and"]),
        CoverCross("top.csubw_rs2", ["top.c_rs2","top.c_subw"]),
        CoverCross("top.caddw_rs2", ["top.c_rs2","top.c_addw"]),
        CoverCross("top.cj_nzim", ["top.c_imm_j","top.c_j"]),
        CoverCross("top.cbeqz_nzim", ["top.c_imm_bne_beq","top.c_beqz"]),
        CoverCross("top.cbeqz_rs1rd", ["top.c_rs1_rd_3","top.c_beqz"]),
        CoverCross("top.cbnez_nzim", ["top.c_imm_bne_beq","top.c_bnez"]),
        CoverCross("top.cbnez_rs1rd", ["top.c_rs1_rd_3","top.c_bnez"]),
        #------------------------q2----------------------------------------------------
        CoverCross("top.cslli_nzuim", ["top.c_nzuimm","top.c_slli"]),
        CoverCross("top.cslli_rs1rd", ["top.c_rs1_rd","top.c_slli"]),
        CoverCross("top.csllii_hint", ["top.c_hints","top.c_slli"]),
        CoverCross("top.cslli_custom", ["top.c_custom","top.c_slli"]),
        CoverCross("top.cfldsp_uim", ["top.c_uimm","top.c_fldsp"]),
        CoverCross("top.cfldsp_rs1rd", ["top.c_rs1_rd","top.c_fldsp"]),
        CoverCross("top.clwsp_uim", ["top.c_uimm","top.c_lwsp"]),
        CoverCross("top.clwsp_rs1rd", ["top.c_rs1_rd","top.c_lwsp"]),
        CoverCross("top.clwsp_reserved", ["top.c_reserved","top.c_lwsp"]),
        CoverCross("top.cldsp_uim", ["top.c_uimm","top.c_ldsp"]),
        CoverCross("top.cldsp_rs1rd", ["top.c_rs1_rd","top.c_ldsp"]),
        CoverCross("top.cldsp_reserved", ["top.c_reserved","top.c_ldsp"]),
        CoverCross("top.cjr_rs1rd", ["top.c_rs1_rd","top.c_jr"]),
        CoverCross("top.cjr_reserved", ["top.c_reserved","top.c_jr"]),
        CoverCross("top.cmv_rs2", ["top.c_rs2_5","top.c_mv"]),
        CoverCross("top.cmv_rs1rd", ["top.c_rs1_rd","top.c_mv"]),
        CoverCross("top.cmv_hints", ["top.c_hints","top.c_mv"]),
        CoverCross("top.cjalr_rs1rd", ["top.c_rs1_rd","top.c_jalr"]),
        CoverCross("top.cadd_rs2", ["top.c_rs2_5","top.c_add"]),
        CoverCross("top.cadd_rs1rd", ["top.c_rs1_rd","top.c_add"]),
        CoverCross("top.cadd_hints", ["top.c_hints","top.c_add"]),
        CoverCross("top.cfsdsp_uim", ["top.c_uimm","top.c_fsdsp"]),
        CoverCross("top.cfsdsp_rs2", ["top.c_rs2_5","top.c_fsdsp"]),
        CoverCross("top.cswsp_uim", ["top.c_uimm","top.c_swsp"]),
        CoverCross("top.cswsp_rs2", ["top.c_rs2_5","top.c_swsp"]),
        CoverCross("top.csdsp_uim", ["top.c_uimm","top.c_sdsp"]),
        CoverCross("top.csdsp_rs2", ["top.c_rs2_5","top.c_sdsp"]),
        #------------------------------q0---------------------------------------------------------------------
        CoverCross("top.caddi4spn_nzuim", ["top.c_nzuimm_a4","top.c_addi4spn"]),
        CoverCross("top.caddi4spn_rd", ["top.c_rd","top.c_addi4spn"]),
        CoverCross("top.caddi4spn_reserved", ["top.c_reserved","top.c_addi4spn"]),
        CoverCross("top.cfld_uim", ["top.c_uimm_5","top.c_fld"]),
        CoverCross("top.cfld_rd", ["top.c_rd","top.c_fld"]),
        CoverCross("top.cfld_rs1", ["top.c_rs1","top.c_fld"]),
        CoverCross("top.clw_uim", ["top.c_uimm_5","top.c_lw"]),
        CoverCross("top.clw_rd", ["top.c_rd","top.c_lw"]),
        CoverCross("top.clw_rs1", ["top.c_rs1","top.c_lw"]),
        CoverCross("top.cld_uim", ["top.c_uimm_5","top.c_ld"]),
        CoverCross("top.cld_rd", ["top.c_rd","top.c_ld"]),
        CoverCross("top.cld_rs1", ["top.c_rs1","top.c_ld"]),
        CoverCross("top.cfsd_uim", ["top.c_uimm_5","top.c_fsd"]),
        CoverCross("top.cfsd_rs2", ["top.c_rs2","top.c_fsd"]),
        CoverCross("top.cfsd_rs1", ["top.c_rs1","top.c_fsd"]),
        CoverCross("top.csw_uim", ["top.c_uimm_5","top.c_sw"]),
        CoverCross("top.csw_rs2", ["top.c_rs2","top.c_sw"]),
        CoverCross("top.csw_rs1", ["top.c_rs1","top.c_sw"]),
        CoverCross("top.csd_uim", ["top.c_uimm_5","top.c_sd"]),
        CoverCross("top.csd_rs2", ["top.c_rs2","top.c_sd"]),
        CoverCross("top.csd_rs1", ["top.c_rs1","top.c_sd"]),
    )
    @FnDecompressCov
    def sample_coverage(self, sample):
        pass
    @coroutine
    def sample_signals(self):
        tx_data = PIPE1()
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            tx_data.set(self.tx_stage2_data.value)
            c_inst = self.c_inst.value.integer

            if self.csr_misa_c.value.integer == 1 and \
                    tx_data.compressed == 1 and \
                    self.process_instruction_fire.value.integer == 1:
            
                self.log.info('CINSTR: ' + str(hex(c_inst)))
              #-----------------------------Quadrant 1-------------------------------------------------------------
                nzimm = BinaryValue(value=((c_inst& 0x7c) >> 2) + ((c_inst & 0x1000) >> 7), n_bits=7, binaryRepresentation=2, bigEndian=False)
                nzuimm = BinaryValue(value=((c_inst& 0x7c) >> 2) + ((c_inst & 0x1000) >> 7), n_bits=7, binaryRepresentation=2, bigEndian=False)
                imm_c_j=BinaryValue(value=((c_inst& 0x38) >> 3)+((c_inst& 0x800)>>8 )+((c_inst& 0x4) << 2)+((c_inst& 0x80) >> 2) + ((c_inst & 0x40)) + ((c_inst & 0x600)>>2)+ ((c_inst & 0x100)<<1)+ ((c_inst & 0x1000)>>2), n_bits=12, binaryRepresentation=2, bigEndian=False)
                imm_c_bne_beq=BinaryValue(value=((c_inst& 0x18) >> 3)+((c_inst& 0xC00)>>8 )+((c_inst& 0x4) << 2)+((c_inst& 0x60)) + ((c_inst & 0x1000)>>5), n_bits=9, binaryRepresentation=2, bigEndian=False)
                f3_op = (c_inst& 0xE000) >> 13
                quadrant = c_inst & 0x3
                rs1_rd = (c_inst& 0xF80) >> 7
                rs1_rd_3 = (c_inst& 0x380) >> 7
                rs2= (c_inst& 0x1C) >>2
                inst_11_10=(c_inst &0xC00) >>10
                inst_6_5=(c_inst&0x60)>>5
                instruction = inst_enum.UKNOWN.name
                is_hint = False
                is_reserved= False
                is_custom_32=False
                if quadrant == 1:
                    if f3_op == 0 and rs1_rd == 0:
                        instruction = inst_enum.CNOP.name
                        is_hint = True if nzimm != 0 else False
                    if f3_op == 0 and rs1_rd != 0:
                        is_hint = True if nzimm == 0 else False
                        instruction = inst_enum.CADDI.name
                    if f3_op==1:
                       instruction= inst_enum.CADDIW.name
                       is_reserved= True if rs1_rd==0 else False
                    if f3_op == 2:
                        instruction = inst_enum.CLI.name
                        is_hint = True if rs1_rd == 0 else False
                    if f3_op == 3 and rs1_rd==2:
                       instruction=inst_enum.CADDI16SP.name
                       nzimm=BinaryValue(value=((c_inst& 0x1000) >> 2)+((c_inst& 0x18) )+((c_inst& 0x20) >> 3)+((c_inst& 0x4) >> 1) + ((c_inst & 0x40) >> 6), n_bits=7, binaryRepresentation=2, bigEndian=False)
                       is_reserved= True if nzimm==0 else False
                    if f3_op == 3 and rs1_rd!=2 :
                       instruction=inst_enum.CLUI.name
                       is_reserved= True if nzimm==0 else False
                       is_hint= True if rs1_rd ==0 else False
                    if f3_op== 4 and inst_11_10==0:
                       instruction=inst_enum.CSRLI.name
                       is_custom_32=True if ((nzuimm& 0x20)>>5)==1 else False
                    if f3_op== 4 and inst_11_10==1:
                       instruction=inst_enum.CSRAI.name
                       is_custom_32=True if ((nzuimm& 0x20)>>5)==1 else False
                    if f3_op==4 and inst_11_10==2:
                       instruction=inst_enum.CANDI.name
                    if f3_op==4 and inst_11_10==3 :
                       if ((nzimm& 0x20)>>5)==0:
                          if inst_6_5==0 :
                             instruction=inst_enum.CSUB.name
                          if inst_6_5==1:
                             instruction=inst_enum.CXOR.name
                          if inst_6_5==2:
                             instruction=inst_enum.COR.name
                          if inst_6_5==3:
                             instruction=inst_enum.CAND.name
                       if ((nzimm& 0x20)>>5)==1:
                          if inst_6_5==0 :
                             instruction=inst_enum.CSUBW.name
                          if inst_6_5==1:
                             instruction=inst_enum.CADDW.name
                    if f3_op==5:
                        instruction=inst_enum.CJ.name
                    if f3_op==6:
                        instruction=inst_enum.CBEQZ.name
                    if f3_op==7:
                        instruction=inst_enum.CBNEZ.name
                 
            #---------------------------------------Quadrant 2----------------------------------------------------------------------------------------------------------------------------------
            
                uimm=BinaryValue(value=((c_inst& 0x60) >> 5) + ((c_inst & 0x1C)<<1) + ((c_inst & 0x1000)>>10), n_bits=7, binaryRepresentation=2, bigEndian=False)
                rs2_5=(c_inst& 0x7C) >>2
                if quadrant == 2 :
                   if f3_op==0:
                      instruction=inst_enum.CSLLI.name
                      is_hint= True if rs1_rd ==0 else False
                      is_custom_32=True if ((nzuimm& 0x20)>>5)==1 else False
                   if f3_op==1:
                      instruction=inst_enum.CFLDSP.name
                   if f3_op == 2:
                      instruction=inst_enum.CLWSP.name
                      uimm=BinaryValue(value=((c_inst& 0xC) << 2) + ((c_inst & 0x1C)>>4) + ((c_inst & 0x1000)>>9), n_bits=7, binaryRepresentation=2, bigEndian=False)
                   if f3_op==3:
                      instruction=inst_enum.CLDSP.name
                      is_reserved= True if rs1_rd==0 else False
                   if f3_op==4:
                      if ((nzimm& 0x20)>>5)==0:
                         if rs2_5 ==0:
                            instruction=inst_enum.CJR.name
                            is_reserved= True if rs1_rd==0 else False
                         else:
                            instruction=inst_enum.CMV.name   
                            is_hint= True if rs1_rd==0 else False
                      if ((nzimm& 0x20)>>5)==1:
                         if rs2_5 ==0 and rs1_rd==0:
                            instruction=inst_enum.CEBREAK.name 
                         if rs2_5 ==0 and rs1_rd!=0:
                            instruction=inst_enum.CJALR.name 
                         if rs2_5 !=0:
                            instruction=inst_enum.CADD.name 
                            is_hint= True if rs1_rd==0 else False
                   if f3_op==5:
                      instruction=inst_enum.CFSDSP.name
                      uimm= BinaryValue(value=((c_inst& 0x1C00) >> 10) + ((c_inst & 0x380)>>4) , n_bits=7, binaryRepresentation=2, bigEndian=False)
                   if f3_op==6:
                      instruction=inst_enum.CSWSP.name
                      uimm= BinaryValue(value=((c_inst& 0x1E00) >> 9) + ((c_inst & 0x180)>>3) , n_bits=7, binaryRepresentation=2, bigEndian=False)
                   if f3_op==7:
                      instruction=inst_enum.CSDSP.name
                      uimm= BinaryValue(value=((c_inst& 0x1C00) >> 10) + ((c_inst & 0x380)>>4) , n_bits=7, binaryRepresentation=2, bigEndian=False)
               #--------------------Quadrant 0------------------------------------------------------------------------------------------------------------------
                uimm_5=BinaryValue(value=((c_inst& 0x1c00) >> 10) + ((c_inst & 0x60)>>2) , n_bits=6, binaryRepresentation=2, bigEndian=False)
                nzuimm_addi4spn=BinaryValue(value=((c_inst& 0x40) >> 6) + ((c_inst & 0x20) >> 4)+((c_inst& 0x1800) >> 9)+((c_inst& 0x780) >> 3), n_bits=9, binaryRepresentation=2, bigEndian=False)
                rs2=((c_inst&0x1c)>>2)
                rs1=((c_inst&0x380)>>7)
                rd=((c_inst&0x1c)>>2)
                if quadrant==0:
                   if f3_op==0:
                      instruction=inst_enum.ILLEGAL.name
                   if f3_op==0 and c_inst!=0:
                      instruction=inst_enum.CADDI4SPN.name
                      is_reserved= True if nzimm==0 else False
                   if f3_op==1:
                      instruction=inst_enum.CFLD.name
                   if f3_op==2:
                      instruction=inst_enum.CLW.name
                      uimm_5=BinaryValue(value=((c_inst& 0x1c00) >> 9) + ((c_inst & 0x40)>>6) +((c_inst&0x20)>>1), n_bits=6, binaryRepresentation=2, bigEndian=False)
                   if f3_op==3:
                      instruction=inst_enum.CLD.name
                   if f3_op==4:
                      instruction=inst_enum.RESERVED.name
                   if f3_op==5:
                      instruction=inst_enum.CFSD.name
                   if f3_op==6:
                      instruction=inst_enum.CSW.name
                      uimm_5=BinaryValue(value=((c_inst& 0x1c00) >> 9) + ((c_inst & 0x40)>>6) +((c_inst&0x20)>>1), n_bits=6, binaryRepresentation=2, bigEndian=False)
                   if f3_op==7:
                      instruction=inst_enum.CSD.name
                sample = InputSample()
                sample.rs1_rd = rs1_rd
                sample.rs1_rd_3 = rs1_rd_3
                sample.instruction = instruction
                sample.nzimm = nzimm.signed_integer
                sample.nzuimm = nzuimm.integer
                sample.uimm= uimm.integer
                sample.uimm_5= uimm_5.integer
                sample.imm_j=imm_c_j.signed_integer
                sample.imm_bne_beq=imm_c_bne_beq.signed_integer
                sample.nzuimm_a4=nzuimm_addi4spn.integer
                sample.is_hint = is_hint
                sample.rs2=rs2
                sample.rs2_5=rs2_5
                sample.rs1=rs1
                sample.rd=rd
                sample.is_reserved=is_reserved
                sample.is_custom_32=is_custom_32
            #----------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage(sample)

