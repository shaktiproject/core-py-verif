import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto

op1_bins = [0, 1]+walking_ones(64)+walking_zeros(64)+leading_ones(64)+leading_zeros(64)+alternate_ones(64)+alternate_zeros(64)+signed_special(64)
op2_bins = list(range(0,64))
class fn_enum(IntEnum):
    FNSR  = 5
    FNSRA = 11
    FNSL = 1

def range_op(x):
    if x>0:
       return 'POS'
    elif x==0:
      return 'ZERO'
    else:
      return 'NEG'

       
class InputSample:
    def __init__(self):
        self.op1 : int
        self.fn : int
        self.op2: int
        self.op2_range: str
        self.wordop: int
        self.op1_eq_op2 : bool
        self.op1_range: str
    def __str__(self):
        return 'op1 : '+str(hex(self.op1)) + \
                ' op2: ' +str(hex(self.op2)) +\
                ' fn: ' + str(self.fn)

class Coverage(object):
    _signals = get_signals('module_fn_shift','inputs')
    _signals.update(get_signals('mkstage3','wires',['base_arith_fire', 'op1_avail', 'op2_avail' ]))
    
    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals())
    
    FnShiftCov = coverage_section (
        CoverPoint(name= "top.FnShift_Fn", xf = lambda sample: sample.fn , 
                                bins = [x.name for x in list(fn_enum)]
                                ),
        CoverPoint("top.FnShift_Op1", xf = lambda sample: sample.op1 , bins = op1_bins ),
        CoverPoint("top.FnShift_Op2", xf = lambda sample: sample.op2 , bins = op2_bins ),
        CoverPoint("top.FnShift_wordop",xf = lambda sample: sample.wordop, bins = [1, 0]), # This coverpoint exists only for RV64
        CoverPoint("top.FnShift_Op1Range",xf = lambda sample: sample.op1_range , bins = ['POS', 'ZERO', 'NEG'] ),
        CoverCross("top.FnShift_CrossFn_Op1", ["top.FnShift_Fn", "top.FnShift_Op1"]),
        CoverCross("top.FnShift_CrossFn_Op2", ["top.FnShift_Fn", "top.FnShift_Op2"]),
        #CoverCross("top.FnShift_CrossRange", ["top.FnShift_Fn", "top.FnShift_Op1", "top.FnShift_Op2"]),
    )

    
    
    @FnShiftCov
    def sample_coverage(self, sample):
        pass 

    @coroutine
    def sample_signals(self):
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.base_arith_fire.value.integer == 1 and \
                    self.fn.value.integer in [5, 11, 1] and \
                    self.op1_avail == 1 and self.op2_avail == 1:
                sample = InputSample()
                sample.op1 = self.op1.value.signed_integer
                sample.op2 = self.op2.value.integer
                sample.fn= fn_enum(self.fn.value.integer).name
                sample.wordop=self.wordop.value.integer
                sample.op1_range = range_op(self.op1.value.signed_integer)
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage(sample)


