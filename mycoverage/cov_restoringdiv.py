import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto
in1_bins = [0, 1]+walking_ones(64)+walking_zeros(64)+leading_ones(64)+leading_zeros(64)+alternate_ones(64)+alternate_zeros(64)+signed_special(64)
global in1_cyc, in2_cyc,f3_cyc, wordop_cyc, input_counter, next_input, assertion_count
in1_cyc=[]
in2_cyc=[]
f3_cyc=[]
wordop_cyc=[]
input_counter=0
next_input=0
assertion_count = 0

class fun3_enum(IntEnum):
    DIV  = 4
    DIVU = 5
    REM = 6
    REMU = 7

def range_op(x):
    if x>0:
       return 'POS'
    elif x==0:
      return 'ZERO'
    else:
      return 'NEG'

def conseq_3cycles(x, _input, n):
    global in1_cyc, in2_cyc,f3_cyc, wordop_cyc
    cov_pt='Null'
    eval(_input+'_cyc').append(x)
    len_cyc=len(eval(_input+'_cyc'))
    if len_cyc >2:
       if eval(_input+'_cyc')[len_cyc-(n-2)]==eval(_input+'_cyc')[len_cyc-(n-1)]==eval(_input+'_cyc')[len_cyc-n]:
          cov_pt= 'Consequtive same'
       else:
          cov_pt='3 cycles not same'
    return cov_pt     
    
def alt_zero(x):
    global in2_cyc
    cov_pt='Null'
    in2_cyc.append(x)
    len_cyc=len(in2_cyc)
    if len_cyc >2:
       if in2_cyc[len_cyc-1]==in2_cyc[len_cyc-3]:
          cov_pt= 'Alt zero detected'
       else:
          cov_pt='No Alt Zero'
    return cov_pt

class InputSample:
    def __init__(self):
        self.in1 : int
        self.in2 : int
        self.funct3 : int
        self.wordop : int
        self.in1_ls_in2 : bool
        self.in1_gr_in2 : bool
        self.in1_eq_in2 : bool
        self.in1_range: str
        self.in2_range: str
        self.in1_3cycle: str
        self.in2_3cycle: str
        self.funct3_3cycle: str
        self.wordop_3cycle: str
        self.oneinst_exec: bool
        self.twoinst_exec: bool
        self.threeinst_exec: bool
        self.fourinst_exec: bool
        self.null_cycles: bool
        self.in2_zero: str
    def __str__(self):
        return 'in1 : '+str(hex(self.in1)) + \
                ' in2: ' +str(hex(self.in2)) +\
                ' funct3: ' + str(self.funct3) +\
                ' wordop: ' +str(self.wordop)

class Coverage(object):
    
    _signals = get_signals('mkrestoring_div','inputs')
    _signals.update(get_signals('mkrestoring_div', 'wire',['RL_single_step']))

    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals_val())
        cocotb.fork(self.sample_signals_cycle())
        cocotb.fork(self.divstage())

    FnRestDivCov_val = coverage_section (
        CoverPoint("top.RestDiv_Ops", xf = lambda sample: sample.funct3 , 
                                bins = [x.name for x in list(fun3_enum)]
                                ),
        CoverPoint("top.RestDiv_In1", xf = lambda sample: sample.in1 , bins = in1_bins ),
        CoverPoint("top.RestDiv_In2", xf = lambda sample: sample.in2 , bins = in1_bins ),
        CoverPoint("top.RestDiv_wordop", xf = lambda sample: sample.wordop , bins = [1, 0]),
        CoverPoint("top.RestDiv_In1LIn2",xf = lambda sample: sample.in1_ls_in2, bins = [True, False]),
        CoverPoint("top.RestDiv_In1GIn2",xf = lambda sample: sample.in1_gr_in2, bins = [True, False]),
        CoverPoint("top.RestDiv_In1EIn2",xf = lambda sample: sample.in1_eq_in2, bins = [True, False]),
        CoverPoint("top.RestDiv_In1Range",xf = lambda sample: sample.in1_range , bins = ['POS', 'ZERO', 'NEG'] ),
        CoverPoint("top.RestDiv_In2Range",xf = lambda sample: sample.in2_range , bins = ['POS', 'ZERO', 'NEG'] ),
        CoverCross("top.RestDiv_CrossRange", ["top.RestDiv_In1Range","top.RestDiv_In2Range"]),
        CoverPoint("top.RestDiv_In1_3cycles", xf = lambda sample: sample.in1_3cycle , bins = ['Consequtive same', '3 cycles not same'] ),
        CoverPoint("top.RestDiv_In2_3cycles", xf = lambda sample: sample.in2_3cycle , bins = ['Consequtive same', '3 cycles not same'] ),
        CoverPoint("top.RestDiv_funct3_3cycles", xf = lambda sample: sample.funct3_3cycle , bins = ['Consequtive same', '3 cycles not same'] ),
        CoverPoint("top.RestDiv_wordop_3cycles", xf = lambda sample: sample.wordop_3cycle , bins = ['Consequtive same', '3 cycles not same'] ),
        CoverPoint("top.RestDiv_alt_zero", xf = lambda sample: sample.in2_zero , bins = ['Alt zero detected', 'No Alt Zero'] ),
        
    )

    @FnRestDivCov_val
    def sample_coverage_val(self, sample):
        pass 

    @coroutine
    def sample_signals_val(self):
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.input.value.integer == 1:
                sample = InputSample()
                sample.in1 = self.in1.value.signed_integer
                sample.in2 = self.in2.value.signed_integer
                sample.funct3= fun3_enum(self.funct3.value.integer).name
                sample.wordop=self.wordop.value.integer 
                sample.in1_eq_in2 = sample.in1 == sample.in2
                sample.in1_ls_in2 = sample.in1 < sample.in2
                sample.in1_gr_in2 = sample.in1 > sample.in2
                sample.in1_range = range_op(self.in1.value.signed_integer)
                sample.in2_range = range_op(self.in2.value.signed_integer)
                
                sample.in1_3cycle=conseq_3cycles(self.in1.value.signed_integer,'in1',3)
                sample.in2_3cycle=conseq_3cycles(self.in2.value.signed_integer,'in2',3)
                sample.funct3_3cycle=conseq_3cycles(self.funct3.value.signed_integer,'f3',3)
                sample.wordop_3cycle=conseq_3cycles(self.wordop.value.signed_integer,'wordop',3)
                sample.in2_zero=alt_zero(self.in2.value.integer)
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage_val(sample)
    
    FnRestDivCov_cycle = coverage_section (
        CoverPoint("top.RestDiv_one_inst_bfr_exec", xf = lambda sample: sample.oneinst_exec , bins = [True, False] ),
        CoverPoint("top.RestDiv_two_inst_bfr_exec", xf = lambda sample: sample.twoinst_exec , bins = [True, False] ),
        CoverPoint("top.RestDiv_three_inst_bfr_exec", xf = lambda sample: sample.threeinst_exec , bins = [True, False] ),
        CoverPoint("top.RestDiv_four_inst_bfr_exec", xf = lambda sample: sample.fourinst_exec , bins = [True, False] ),
        CoverPoint("top.RestDiv_null_cycles", xf = lambda sample: sample.null_cycles , bins = [True, False] ), )

    @FnRestDivCov_cycle
    def sample_coverage_cycle(self, sample):
        pass
    @coroutine
    def sample_signals_cycle(self):
        global input_counter, next_input
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.ready.value.integer==1:
                next_input=next_input+1
            else:
                next_input=0
            if self.output_valid.value.integer==1:
               input_counter=0
            if self.input.value.integer ==1:
               input_counter=input_counter+1
               sample = InputSample()          
               sample.oneinst_exec= True if input_counter==1 else False
               sample.twoinst_exec= True if input_counter==2 else False
               sample.threeinst_exec= True if input_counter==3 else False
               sample.fourinst_exec= True if input_counter==4 else False
               sample.null_cycles= True if (next_input-1)>2 else False
               if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
               self.sample_coverage_cycle(sample)           

    FnRestDivCov = coverage_section (
         CoverCheck("top.RestDiv_Assertion.check", f_fail = lambda sample_fail : False, f_pass = lambda sample_assert : True),
        )
    
    @FnRestDivCov 
    def sample_assert(self, assertion_count):
        pass

    @FnRestDivCov
    def sample_fail(self, assertion_count):
        False 
    
    @coroutine
    def divstage(self):
        prev_ = []
        global assertion_count 
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.input.value.integer == 1:
                assertion_count = 0
                if self.in2.value.integer == 0:
                    if self.RL_single_step.value.integer == 0:
                        assertion_count += 1
            elif self.RL_single_step.value.integer == 1 and self.input.value.integer == 0:
                    assertion_count+= 1
            elif self.output_valid.value.integer == 1:
                if assertion_count > 32 or assertion_count == 2:
                    self.sample_assert(assertion_count)
                else:
                    self.sample_fail(assertion_count)

coverage_db["top.RestDiv_Assertion.check"].add_bins_callback(lambda : TestFailure("Assertion Error"), "FAIL")
