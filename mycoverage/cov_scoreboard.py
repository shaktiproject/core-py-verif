import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto

in1_bins = [0, 1]+walking_ones(64)+walking_zeros(64)+leading_ones(64)+leading_zeros(64)+alternate_ones(64)+alternate_zeros(64)+signed_special(64)
global rg_rf_board

rg_rf_board={0: {'lock': 0, 'id': 0}, 1: {'lock': 0, 'id': 0}, 2: {'lock': 0, 'id': 0}, 3: {'lock': 0, 'id': 0}, 4: {'lock': 0, 'id': 0}, 5: {'lock': 0, 'id': 0}, 6: {'lock': 0, 'id': 0}, 7: {'lock': 0, 'id': 0}, 8: {'lock': 0, 'id': 0}, 9: {'lock': 0, 'id': 0}, 10: {'lock': 0, 'id': 0}, 11: {'lock': 0, 'id': 0}, 12: {'lock': 0, 'id': 0}, 13: {'lock': 0, 'id': 0}, 14: {'lock': 0, 'id': 0}, 15: {'lock': 0, 'id': 0}, 16: {'lock': 0, 'id': 0}, 17: {'lock': 0, 'id': 0}, 18: {'lock': 0, 'id': 0}, 19: {'lock': 0, 'id': 0}, 20: {'lock': 0, 'id': 0}, 21: {'lock': 0, 'id': 0}, 22: {'lock': 0, 'id': 0}, 23: {'lock': 0, 'id': 0}, 24: {'lock': 0, 'id': 0}, 25: {'lock': 0, 'id': 0}, 26: {'lock': 0, 'id': 0}, 27: {'lock': 0, 'id': 0}, 28: {'lock': 0, 'id': 0}, 29: {'lock': 0, 'id': 0}, 30: {'lock': 0, 'id': 0}, 31: {'lock': 0, 'id': 0}}

global prev_val
prev_val = 0

def transition_cov(new_val):
    global prev_val
    transition = (prev_val, new_val)
    prev_val = new_val 
    return transition


class InputSample:
    def __init__(self): 
        self.en_lock : int
        self.en_release : int
        self.lock_rd: int
        self.release_rd: int
        
    def __str__(self):
        return 'en_lock : '+str(hex(self.en_lock)) + \
                ' en_release: ' +str(hex(self.en_release)) +\
                'lock: ' +str(hex(self.lock)) +\
                'rls: ' +str(hex(self.rls))+\
                'rd:'+str(hex(self.rd))

class Coverage(object):
    
    _signals = get_signals('scoreboard','inputs')
    _signals.update(get_signals('scoreboard', 'outputs'))
    _signals.update(get_signals('scoreboard', 'register'))

    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals())
        cocotb.fork(self.sample_id_val()) 
        cocotb.fork(self.sample_toggle()) 
        
    FnSBDCov = coverage_section ( 
        CoverPoint("top.SBD.en_lock", xf = lambda sample : sample.en_lock , bins = [1, 0]) ,
        CoverPoint("top.SBD.en_release", xf = lambda sample : sample.en_release , bins = [1, 0]) ,
        CoverCross("top.SBD.en_lock_cross_en_release", ["top.SBD.en_lock","top.SBD.en_release"]),
        CoverPoint("top.SBD.lock_rd_1", xf = lambda sample : sample.lock_rd , bins = [1]) ,
        CoverPoint("top.SBD.lock_rd_2", xf = lambda sample : sample.lock_rd , bins = [2]) ,
        CoverPoint("top.SBD.lock_rd_3", xf = lambda sample : sample.lock_rd , bins = [3]) ,
        CoverPoint("top.SBD.lock_rd_4", xf = lambda sample : sample.lock_rd , bins = [4]) ,
        CoverPoint("top.SBD.lock_rd_5", xf = lambda sample : sample.lock_rd , bins = [5]) ,
        CoverPoint("top.SBD.lock_rd_6", xf = lambda sample : sample.lock_rd , bins = [6]) ,
        CoverPoint("top.SBD.lock_rd_7", xf = lambda sample : sample.lock_rd , bins = [7]) ,
        CoverPoint("top.SBD.lock_rd_8", xf = lambda sample : sample.lock_rd , bins = [8]) ,
        CoverPoint("top.SBD.lock_rd_9", xf = lambda sample : sample.lock_rd , bins = [9]) ,
        CoverPoint("top.SBD.lock_rd_10", xf = lambda sample : sample.lock_rd , bins = [10]) ,
        CoverPoint("top.SBD.lock_rd_11", xf = lambda sample : sample.lock_rd , bins = [11]) ,
        CoverPoint("top.SBD.lock_rd_12", xf = lambda sample : sample.lock_rd , bins = [12]) ,
        CoverPoint("top.SBD.lock_rd_13", xf = lambda sample : sample.lock_rd , bins = [13]) ,
        CoverPoint("top.SBD.lock_rd_14", xf = lambda sample : sample.lock_rd , bins = [14]) ,
        CoverPoint("top.SBD.lock_rd_15", xf = lambda sample : sample.lock_rd , bins = [15]) ,
        CoverPoint("top.SBD.lock_rd_16", xf = lambda sample : sample.lock_rd , bins = [16]) ,
        CoverPoint("top.SBD.lock_rd_17", xf = lambda sample : sample.lock_rd , bins = [17]) ,
        CoverPoint("top.SBD.lock_rd_18", xf = lambda sample : sample.lock_rd , bins = [18]) ,
        CoverPoint("top.SBD.lock_rd_19", xf = lambda sample : sample.lock_rd , bins = [19]) ,
        CoverPoint("top.SBD.lock_rd_20", xf = lambda sample : sample.lock_rd , bins = [20]) ,
        CoverPoint("top.SBD.lock_rd_21", xf = lambda sample : sample.lock_rd , bins = [21]) ,
        CoverPoint("top.SBD.lock_rd_22", xf = lambda sample : sample.lock_rd , bins = [22]) ,
        CoverPoint("top.SBD.lock_rd_23", xf = lambda sample : sample.lock_rd , bins = [23]) ,
        CoverPoint("top.SBD.lock_rd_24", xf = lambda sample : sample.lock_rd , bins = [24]) ,
        CoverPoint("top.SBD.lock_rd_25", xf = lambda sample : sample.lock_rd , bins = [25]) ,
        CoverPoint("top.SBD.lock_rd_26", xf = lambda sample : sample.lock_rd , bins = [26]) ,
        CoverPoint("top.SBD.lock_rd_27", xf = lambda sample : sample.lock_rd , bins = [27]) ,
        CoverPoint("top.SBD.lock_rd_28", xf = lambda sample : sample.lock_rd , bins = [28]) ,
        CoverPoint("top.SBD.lock_rd_29", xf = lambda sample : sample.lock_rd , bins = [29]) ,
        CoverPoint("top.SBD.lock_rd_30", xf = lambda sample : sample.lock_rd , bins = [30]) ,
        CoverPoint("top.SBD.lock_rd_31", xf = lambda sample : sample.lock_rd , bins = [31]) ,
        CoverPoint("top.SBD.release_rd_1", xf = lambda sample : sample.release_rd , bins = [1]) ,
        CoverPoint("top.SBD.release_rd_2", xf = lambda sample : sample.release_rd , bins = [2]) ,
        CoverPoint("top.SBD.release_rd_3", xf = lambda sample : sample.release_rd , bins = [3]) ,
        CoverPoint("top.SBD.release_rd_4", xf = lambda sample : sample.release_rd , bins = [4]) ,
        CoverPoint("top.SBD.release_rd_5", xf = lambda sample : sample.release_rd , bins = [5]) ,
        CoverPoint("top.SBD.release_rd_6", xf = lambda sample : sample.release_rd , bins = [6]) ,
        CoverPoint("top.SBD.release_rd_7", xf = lambda sample : sample.release_rd , bins = [7]) ,
        CoverPoint("top.SBD.release_rd_8", xf = lambda sample : sample.release_rd , bins = [8]) ,
        CoverPoint("top.SBD.release_rd_9", xf = lambda sample : sample.release_rd , bins = [9]) ,
        CoverPoint("top.SBD.release_rd_10", xf = lambda sample : sample.release_rd , bins = [10]) ,
        CoverPoint("top.SBD.release_rd_11", xf = lambda sample : sample.release_rd , bins = [11]) ,
        CoverPoint("top.SBD.release_rd_12", xf = lambda sample : sample.release_rd , bins = [12]) ,
        CoverPoint("top.SBD.release_rd_13", xf = lambda sample : sample.release_rd , bins = [13]) ,
        CoverPoint("top.SBD.release_rd_14", xf = lambda sample : sample.release_rd , bins = [14]) ,
        CoverPoint("top.SBD.release_rd_15", xf = lambda sample : sample.release_rd , bins = [15]) ,
        CoverPoint("top.SBD.release_rd_16", xf = lambda sample : sample.release_rd , bins = [16]) ,
        CoverPoint("top.SBD.release_rd_17", xf = lambda sample : sample.release_rd , bins = [17]) ,
        CoverPoint("top.SBD.release_rd_18", xf = lambda sample : sample.release_rd , bins = [18]) ,
        CoverPoint("top.SBD.release_rd_19", xf = lambda sample : sample.release_rd , bins = [19]) ,
        CoverPoint("top.SBD.release_rd_20", xf = lambda sample : sample.release_rd , bins = [20]) ,
        CoverPoint("top.SBD.release_rd_21", xf = lambda sample : sample.release_rd , bins = [21]) ,
        CoverPoint("top.SBD.release_rd_22", xf = lambda sample : sample.release_rd , bins = [22]) ,
        CoverPoint("top.SBD.release_rd_23", xf = lambda sample : sample.release_rd , bins = [23]) ,
        CoverPoint("top.SBD.release_rd_24", xf = lambda sample : sample.release_rd , bins = [24]) ,
        CoverPoint("top.SBD.release_rd_25", xf = lambda sample : sample.release_rd , bins = [25]) ,
        CoverPoint("top.SBD.release_rd_26", xf = lambda sample : sample.release_rd , bins = [26]) ,
        CoverPoint("top.SBD.release_rd_27", xf = lambda sample : sample.release_rd , bins = [27]) ,
        CoverPoint("top.SBD.release_rd_28", xf = lambda sample : sample.release_rd , bins = [28]) ,
        CoverPoint("top.SBD.release_rd_29", xf = lambda sample : sample.release_rd , bins = [29]) ,
        CoverPoint("top.SBD.release_rd_30", xf = lambda sample : sample.release_rd , bins = [30]) ,
        CoverPoint("top.SBD.release_rd_31", xf = lambda sample : sample.release_rd , bins = [31]) ,
        CoverPoint("top.SBD.en_lock_trans", xf = lambda sample : sample.en_lock_trans , bins = [(1, 0), (0, 1)]),
        CoverPoint("top.SBD.en_release_trans", xf = lambda sample : sample.en_release_trans , bins = [(0, 1), (1, 0)]),
    )

    @FnSBDCov
    def sample_coverage(self, sample):
        pass 

    @coroutine
    def sample_signals(self):
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns") 
            global rg_rf_board
            rd=0
            sample = InputSample()
            sample.en_lock = self.en_lock.value.integer
            sample.en_release = self.en_release.value.integer 
            index=((self.rls.value.integer)&0x1f)
            lock_rd=0
            release_id=0
            if (((self.rls.value.integer &0x1E0)>>5)==(((eval('self.rg_'+str(index)+'.value.integer'))&0x1E)>>1)) and self.en_release.value.integer==1:
               release_id=index
            if self.en_lock.value.integer==1 :
               lock_rd=((self.lock.value.integer)&0x1f)
            sample.lock_rd=lock_rd
            sample.release_rd=release_id
            sample.en_lock_trans = transition_cov(sample.en_lock)
            sample.en_release_trans = transition_cov(sample.en_release)
            if self.enlog:
                self.log.info('Coverage Sampling : ' +str(sample))
            self.sample_coverage(sample)
    
    
    FnSBDCov_id = coverage_section (
        CoverPoint("top.SBD.rg_id", xf = lambda sample : sample.rg_id , bins = [x for x in range(16)]),
        CoverCheck("top.SBD.rg_id_assertion.check", f_fail = lambda sample : sample.rg_id > 15 , f_pass = lambda sample : sample.rg_id <= 15),
    )

    @FnSBDCov_id
    def sample_id_cov(self, sample):
        pass

    @coroutine
    def sample_id_val(self):
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.en_lock.value.integer == 1:
                sample = InputSample()
                sample.rg_id = self.rg_id.value.integer
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_id_cov(sample)

    coverage_db["top.SBD.rg_id_assertion.check"].add_bins_callback(lambda : TestFailure("Assertion Error"), "FAIL")
    
    FnSBDCov_toggle = coverage_section (
        CoverPoint("top.reg_31_bit_0", xf = lambda sample : sample.reg_31_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_31_bit_1", xf = lambda sample : sample.reg_31_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_31_bit_2", xf = lambda sample : sample.reg_31_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_31_bit_3", xf = lambda sample : sample.reg_31_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_31_bit_4", xf = lambda sample : sample.reg_31_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_30_bit_0", xf = lambda sample : sample.reg_30_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_30_bit_1", xf = lambda sample : sample.reg_30_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_30_bit_2", xf = lambda sample : sample.reg_30_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_30_bit_3", xf = lambda sample : sample.reg_30_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_30_bit_4", xf = lambda sample : sample.reg_30_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_29_bit_0", xf = lambda sample : sample.reg_29_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_29_bit_1", xf = lambda sample : sample.reg_29_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_29_bit_2", xf = lambda sample : sample.reg_29_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_29_bit_3", xf = lambda sample : sample.reg_29_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_29_bit_4", xf = lambda sample : sample.reg_29_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_28_bit_0", xf = lambda sample : sample.reg_28_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_28_bit_1", xf = lambda sample : sample.reg_28_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_28_bit_2", xf = lambda sample : sample.reg_28_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_28_bit_3", xf = lambda sample : sample.reg_28_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_28_bit_4", xf = lambda sample : sample.reg_28_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_27_bit_0", xf = lambda sample : sample.reg_27_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_27_bit_1", xf = lambda sample : sample.reg_27_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_27_bit_2", xf = lambda sample : sample.reg_27_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_27_bit_3", xf = lambda sample : sample.reg_27_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_27_bit_4", xf = lambda sample : sample.reg_27_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_26_bit_0", xf = lambda sample : sample.reg_26_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_26_bit_1", xf = lambda sample : sample.reg_26_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_26_bit_2", xf = lambda sample : sample.reg_26_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_26_bit_3", xf = lambda sample : sample.reg_26_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_26_bit_4", xf = lambda sample : sample.reg_26_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_25_bit_0", xf = lambda sample : sample.reg_25_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_25_bit_1", xf = lambda sample : sample.reg_25_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_25_bit_2", xf = lambda sample : sample.reg_25_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_25_bit_3", xf = lambda sample : sample.reg_25_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_25_bit_4", xf = lambda sample : sample.reg_25_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_24_bit_0", xf = lambda sample : sample.reg_24_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_24_bit_1", xf = lambda sample : sample.reg_24_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_24_bit_2", xf = lambda sample : sample.reg_24_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_24_bit_3", xf = lambda sample : sample.reg_24_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_24_bit_4", xf = lambda sample : sample.reg_24_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_23_bit_0", xf = lambda sample : sample.reg_23_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_23_bit_1", xf = lambda sample : sample.reg_23_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_23_bit_2", xf = lambda sample : sample.reg_23_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_23_bit_3", xf = lambda sample : sample.reg_23_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_23_bit_4", xf = lambda sample : sample.reg_23_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_22_bit_0", xf = lambda sample : sample.reg_22_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_22_bit_1", xf = lambda sample : sample.reg_22_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_22_bit_2", xf = lambda sample : sample.reg_22_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_22_bit_3", xf = lambda sample : sample.reg_22_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_22_bit_4", xf = lambda sample : sample.reg_22_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_21_bit_0", xf = lambda sample : sample.reg_21_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_21_bit_1", xf = lambda sample : sample.reg_21_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_21_bit_2", xf = lambda sample : sample.reg_21_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_21_bit_3", xf = lambda sample : sample.reg_21_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_21_bit_4", xf = lambda sample : sample.reg_21_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_20_bit_0", xf = lambda sample : sample.reg_20_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_20_bit_1", xf = lambda sample : sample.reg_20_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_20_bit_2", xf = lambda sample : sample.reg_20_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_20_bit_3", xf = lambda sample : sample.reg_20_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_20_bit_4", xf = lambda sample : sample.reg_20_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_19_bit_0", xf = lambda sample : sample.reg_19_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_19_bit_1", xf = lambda sample : sample.reg_19_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_19_bit_2", xf = lambda sample : sample.reg_19_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_19_bit_3", xf = lambda sample : sample.reg_19_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_19_bit_4", xf = lambda sample : sample.reg_19_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_18_bit_0", xf = lambda sample : sample.reg_18_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_18_bit_1", xf = lambda sample : sample.reg_18_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_18_bit_2", xf = lambda sample : sample.reg_18_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_18_bit_3", xf = lambda sample : sample.reg_18_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_18_bit_4", xf = lambda sample : sample.reg_18_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_17_bit_0", xf = lambda sample : sample.reg_17_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_17_bit_1", xf = lambda sample : sample.reg_17_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_17_bit_2", xf = lambda sample : sample.reg_17_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_17_bit_3", xf = lambda sample : sample.reg_17_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_17_bit_4", xf = lambda sample : sample.reg_17_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_16_bit_0", xf = lambda sample : sample.reg_16_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_16_bit_1", xf = lambda sample : sample.reg_16_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_16_bit_2", xf = lambda sample : sample.reg_16_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_16_bit_3", xf = lambda sample : sample.reg_16_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_16_bit_4", xf = lambda sample : sample.reg_16_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_15_bit_0", xf = lambda sample : sample.reg_15_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_15_bit_1", xf = lambda sample : sample.reg_15_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_15_bit_2", xf = lambda sample : sample.reg_15_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_15_bit_3", xf = lambda sample : sample.reg_15_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_15_bit_4", xf = lambda sample : sample.reg_15_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_14_bit_0", xf = lambda sample : sample.reg_14_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_14_bit_1", xf = lambda sample : sample.reg_14_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_14_bit_2", xf = lambda sample : sample.reg_14_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_14_bit_3", xf = lambda sample : sample.reg_14_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_14_bit_4", xf = lambda sample : sample.reg_14_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_13_bit_0", xf = lambda sample : sample.reg_13_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_13_bit_1", xf = lambda sample : sample.reg_13_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_13_bit_2", xf = lambda sample : sample.reg_13_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_13_bit_3", xf = lambda sample : sample.reg_13_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_13_bit_4", xf = lambda sample : sample.reg_13_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_12_bit_0", xf = lambda sample : sample.reg_12_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_12_bit_1", xf = lambda sample : sample.reg_12_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_12_bit_2", xf = lambda sample : sample.reg_12_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_12_bit_3", xf = lambda sample : sample.reg_12_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_12_bit_4", xf = lambda sample : sample.reg_12_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_11_bit_0", xf = lambda sample : sample.reg_11_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_11_bit_1", xf = lambda sample : sample.reg_11_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_11_bit_2", xf = lambda sample : sample.reg_11_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_11_bit_3", xf = lambda sample : sample.reg_11_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_11_bit_4", xf = lambda sample : sample.reg_11_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_10_bit_0", xf = lambda sample : sample.reg_10_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_10_bit_1", xf = lambda sample : sample.reg_10_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_10_bit_2", xf = lambda sample : sample.reg_10_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_10_bit_3", xf = lambda sample : sample.reg_10_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_10_bit_4", xf = lambda sample : sample.reg_10_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_9_bit_0", xf = lambda sample : sample.reg_9_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_9_bit_1", xf = lambda sample : sample.reg_9_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_9_bit_2", xf = lambda sample : sample.reg_9_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_9_bit_3", xf = lambda sample : sample.reg_9_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_9_bit_4", xf = lambda sample : sample.reg_9_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_8_bit_0", xf = lambda sample : sample.reg_8_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_8_bit_1", xf = lambda sample : sample.reg_8_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_8_bit_2", xf = lambda sample : sample.reg_8_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_8_bit_3", xf = lambda sample : sample.reg_8_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_8_bit_4", xf = lambda sample : sample.reg_8_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_7_bit_0", xf = lambda sample : sample.reg_7_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_7_bit_1", xf = lambda sample : sample.reg_7_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_7_bit_2", xf = lambda sample : sample.reg_7_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_7_bit_3", xf = lambda sample : sample.reg_7_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_7_bit_4", xf = lambda sample : sample.reg_7_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_6_bit_0", xf = lambda sample : sample.reg_6_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_6_bit_1", xf = lambda sample : sample.reg_6_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_6_bit_2", xf = lambda sample : sample.reg_6_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_6_bit_3", xf = lambda sample : sample.reg_6_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_6_bit_4", xf = lambda sample : sample.reg_6_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_5_bit_0", xf = lambda sample : sample.reg_5_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_5_bit_1", xf = lambda sample : sample.reg_5_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_5_bit_2", xf = lambda sample : sample.reg_5_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_5_bit_3", xf = lambda sample : sample.reg_5_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_5_bit_4", xf = lambda sample : sample.reg_5_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_4_bit_0", xf = lambda sample : sample.reg_4_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_4_bit_1", xf = lambda sample : sample.reg_4_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_4_bit_2", xf = lambda sample : sample.reg_4_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_4_bit_3", xf = lambda sample : sample.reg_4_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_4_bit_4", xf = lambda sample : sample.reg_4_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_3_bit_0", xf = lambda sample : sample.reg_3_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_3_bit_1", xf = lambda sample : sample.reg_3_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_3_bit_2", xf = lambda sample : sample.reg_3_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_3_bit_3", xf = lambda sample : sample.reg_3_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_3_bit_4", xf = lambda sample : sample.reg_3_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_2_bit_0", xf = lambda sample : sample.reg_2_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_2_bit_1", xf = lambda sample : sample.reg_2_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_2_bit_2", xf = lambda sample : sample.reg_2_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_2_bit_3", xf = lambda sample : sample.reg_2_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_2_bit_4", xf = lambda sample : sample.reg_2_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_1_bit_0", xf = lambda sample : sample.reg_1_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_1_bit_1", xf = lambda sample : sample.reg_1_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_1_bit_2", xf = lambda sample : sample.reg_1_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_1_bit_3", xf = lambda sample : sample.reg_1_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_1_bit_4", xf = lambda sample : sample.reg_1_bit_4 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_0_bit_0", xf = lambda sample : sample.reg_0_bit_0 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_0_bit_1", xf = lambda sample : sample.reg_0_bit_1 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_0_bit_2", xf = lambda sample : sample.reg_0_bit_2 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_0_bit_3", xf = lambda sample : sample.reg_0_bit_3 , bins = [(0, 1), (1, 0)]),
        CoverPoint("top.reg_0_bit_4", xf = lambda sample : sample.reg_0_bit_4 , bins = [(0, 1), (1, 0)]),
    )

    @FnSBDCov_toggle
    def sample_cov_toggle(self, sample):
        pass

    @coroutine
    def sample_toggle(self):
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.en_lock.value.integer == 1 or self.en_release.value.integer == 1:
                sample = InputSample()
            #    print(rg_rf_board[31]['lock'],'snehaaaaaaaa')
                sample.reg_31_bit_0 = (rg_rf_board[31]['lock'], (self.rg_31.value.integer&0x1))
                sample.reg_31_bit_1 = ((rg_rf_board[31]['id']&0x1), ((self.rg_31.value.integer&0x2)>>1))
                sample.reg_31_bit_2 = (((rg_rf_board[31]['id']&0x2)>>1), ((self.rg_31.value.integer&0x4)>>2))
                sample.reg_31_bit_3 = (((rg_rf_board[31]['id']&0x4)>>2), ((self.rg_31.value.integer&0x8)>>3))
                sample.reg_31_bit_4 = (((rg_rf_board[31]['id']&0x8)>>3), ((self.rg_31.value.integer&0x10)>>4))
                sample.reg_30_bit_0 = (rg_rf_board[30]['lock'], (self.rg_30.value.integer&0x1))
                sample.reg_30_bit_1 = ((rg_rf_board[30]['id']&0x1), ((self.rg_30.value.integer&0x2)>>1))
                sample.reg_30_bit_2 = (((rg_rf_board[30]['id']&0x2)>>1), ((self.rg_30.value.integer&0x4)>>2))
                sample.reg_30_bit_3 = (((rg_rf_board[30]['id']&0x4)>>2), ((self.rg_30.value.integer&0x8)>>3))
                sample.reg_30_bit_4 = (((rg_rf_board[30]['id']&0x8)>>3), ((self.rg_30.value.integer&0x10)>>4))
                sample.reg_29_bit_0 = (rg_rf_board[29]['lock'], (self.rg_29.value.integer&0x1))
                sample.reg_29_bit_1 = ((rg_rf_board[29]['id']&0x1), ((self.rg_29.value.integer&0x2)>>1))
                sample.reg_29_bit_2 = (((rg_rf_board[29]['id']&0x2)>>1), ((self.rg_29.value.integer&0x4)>>2))
                sample.reg_29_bit_3 = (((rg_rf_board[29]['id']&0x4)>>2), ((self.rg_29.value.integer&0x8)>>3))
                sample.reg_29_bit_4 = (((rg_rf_board[29]['id']&0x8)>>3), ((self.rg_29.value.integer&0x10)>>4))
                sample.reg_28_bit_0 = (rg_rf_board[28]['lock'], (self.rg_28.value.integer&0x1))
                sample.reg_28_bit_1 = ((rg_rf_board[28]['id']&0x1), ((self.rg_28.value.integer&0x2)>>1))
                sample.reg_28_bit_2 = (((rg_rf_board[28]['id']&0x2)>>1), ((self.rg_28.value.integer&0x4)>>2))
                sample.reg_28_bit_3 = (((rg_rf_board[28]['id']&0x4)>>2), ((self.rg_28.value.integer&0x8)>>3))
                sample.reg_28_bit_4 = (((rg_rf_board[28]['id']&0x8)>>3), ((self.rg_28.value.integer&0x10)>>4))
                sample.reg_27_bit_0 = (rg_rf_board[27]['lock'], (self.rg_27.value.integer&0x1))
                sample.reg_27_bit_1 = ((rg_rf_board[27]['id']&0x1), ((self.rg_27.value.integer&0x2)>>1))
                sample.reg_27_bit_2 = (((rg_rf_board[27]['id']&0x2)>>1), ((self.rg_27.value.integer&0x4)>>2))
                sample.reg_27_bit_3 = (((rg_rf_board[27]['id']&0x4)>>2), ((self.rg_27.value.integer&0x8)>>3))
                sample.reg_27_bit_4 = (((rg_rf_board[27]['id']&0x8)>>3), ((self.rg_27.value.integer&0x10)>>4))
                sample.reg_26_bit_0 = (rg_rf_board[26]['lock'], (self.rg_26.value.integer&0x1))
                sample.reg_26_bit_1 = ((rg_rf_board[26]['id']&0x1), ((self.rg_26.value.integer&0x2)>>1))
                sample.reg_26_bit_2 = (((rg_rf_board[26]['id']&0x2)>>1), ((self.rg_26.value.integer&0x4)>>2))
                sample.reg_26_bit_3 = (((rg_rf_board[26]['id']&0x4)>>2), ((self.rg_26.value.integer&0x8)>>3))
                sample.reg_26_bit_4 = (((rg_rf_board[26]['id']&0x8)>>3), ((self.rg_26.value.integer&0x10)>>4))
                sample.reg_25_bit_0 = (rg_rf_board[25]['lock'], (self.rg_25.value.integer&0x1))
                sample.reg_25_bit_1 = ((rg_rf_board[25]['id']&0x1), ((self.rg_25.value.integer&0x2)>>1))
                sample.reg_25_bit_2 = (((rg_rf_board[25]['id']&0x2)>>1), ((self.rg_25.value.integer&0x4)>>2))
                sample.reg_25_bit_3 = (((rg_rf_board[25]['id']&0x4)>>2), ((self.rg_25.value.integer&0x8)>>3))
                sample.reg_25_bit_4 = (((rg_rf_board[25]['id']&0x8)>>3), ((self.rg_25.value.integer&0x10)>>4))
                sample.reg_24_bit_0 = (rg_rf_board[24]['lock'], (self.rg_24.value.integer&0x1))
                sample.reg_24_bit_1 = ((rg_rf_board[24]['id']&0x1), ((self.rg_24.value.integer&0x2)>>1))
                sample.reg_24_bit_2 = (((rg_rf_board[24]['id']&0x2)>>1), ((self.rg_24.value.integer&0x4)>>2))
                sample.reg_24_bit_3 = (((rg_rf_board[24]['id']&0x4)>>2), ((self.rg_24.value.integer&0x8)>>3))
                sample.reg_24_bit_4 = (((rg_rf_board[24]['id']&0x8)>>3), ((self.rg_24.value.integer&0x10)>>4))
                sample.reg_23_bit_0 = (rg_rf_board[23]['lock'], (self.rg_23.value.integer&0x1))
                sample.reg_23_bit_1 = ((rg_rf_board[23]['id']&0x1), ((self.rg_23.value.integer&0x2)>>1))
                sample.reg_23_bit_2 = (((rg_rf_board[23]['id']&0x2)>>1), ((self.rg_23.value.integer&0x4)>>2))
                sample.reg_23_bit_3 = (((rg_rf_board[23]['id']&0x4)>>2), ((self.rg_23.value.integer&0x8)>>3))
                sample.reg_23_bit_4 = (((rg_rf_board[23]['id']&0x8)>>3), ((self.rg_23.value.integer&0x10)>>4))
                sample.reg_22_bit_0 = (rg_rf_board[22]['lock'], (self.rg_22.value.integer&0x1))
                sample.reg_22_bit_1 = ((rg_rf_board[22]['id']&0x1), ((self.rg_22.value.integer&0x2)>>1))
                sample.reg_22_bit_2 = (((rg_rf_board[22]['id']&0x2)>>1), ((self.rg_22.value.integer&0x4)>>2))
                sample.reg_22_bit_3 = (((rg_rf_board[22]['id']&0x4)>>2), ((self.rg_22.value.integer&0x8)>>3))
                sample.reg_22_bit_4 = (((rg_rf_board[22]['id']&0x8)>>3), ((self.rg_22.value.integer&0x10)>>4))
                sample.reg_21_bit_0 = (rg_rf_board[21]['lock'], (self.rg_21.value.integer&0x1))
                sample.reg_21_bit_1 = ((rg_rf_board[21]['id']&0x1), ((self.rg_21.value.integer&0x2)>>1))
                sample.reg_21_bit_2 = (((rg_rf_board[21]['id']&0x2)>>1), ((self.rg_21.value.integer&0x4)>>2))
                sample.reg_21_bit_3 = (((rg_rf_board[21]['id']&0x4)>>2), ((self.rg_21.value.integer&0x8)>>3))
                sample.reg_21_bit_4 = (((rg_rf_board[21]['id']&0x8)>>3), ((self.rg_21.value.integer&0x10)>>4))
                sample.reg_20_bit_0 = (rg_rf_board[20]['lock'], (self.rg_20.value.integer&0x1))
                sample.reg_20_bit_1 = ((rg_rf_board[20]['id']&0x1), ((self.rg_20.value.integer&0x2)>>1))
                sample.reg_20_bit_2 = (((rg_rf_board[20]['id']&0x2)>>1), ((self.rg_20.value.integer&0x4)>>2))
                sample.reg_20_bit_3 = (((rg_rf_board[20]['id']&0x4)>>2), ((self.rg_20.value.integer&0x8)>>3))
                sample.reg_20_bit_4 = (((rg_rf_board[20]['id']&0x8)>>3), ((self.rg_20.value.integer&0x10)>>4))
                sample.reg_19_bit_0 = (rg_rf_board[19]['lock'], (self.rg_19.value.integer&0x1))
                sample.reg_19_bit_1 = ((rg_rf_board[19]['id']&0x1), ((self.rg_19.value.integer&0x2)>>1))
                sample.reg_19_bit_2 = (((rg_rf_board[19]['id']&0x2)>>1), ((self.rg_19.value.integer&0x4)>>2))
                sample.reg_19_bit_3 = (((rg_rf_board[19]['id']&0x4)>>2), ((self.rg_19.value.integer&0x8)>>3))
                sample.reg_19_bit_4 = (((rg_rf_board[19]['id']&0x8)>>3), ((self.rg_19.value.integer&0x10)>>4))
                sample.reg_18_bit_0 = (rg_rf_board[18]['lock'], (self.rg_18.value.integer&0x1))
                sample.reg_18_bit_1 = ((rg_rf_board[18]['id']&0x1), ((self.rg_18.value.integer&0x2)>>1))
                sample.reg_18_bit_2 = (((rg_rf_board[18]['id']&0x2)>>1), ((self.rg_18.value.integer&0x4)>>2))
                sample.reg_18_bit_3 = (((rg_rf_board[18]['id']&0x4)>>2), ((self.rg_18.value.integer&0x8)>>3))
                sample.reg_18_bit_4 = (((rg_rf_board[18]['id']&0x8)>>3), ((self.rg_18.value.integer&0x10)>>4))
                sample.reg_17_bit_0 = (rg_rf_board[17]['lock'], (self.rg_17.value.integer&0x1))
                sample.reg_17_bit_1 = ((rg_rf_board[17]['id']&0x1), ((self.rg_17.value.integer&0x2)>>1))
                sample.reg_17_bit_2 = (((rg_rf_board[17]['id']&0x2)>>1), ((self.rg_17.value.integer&0x4)>>2))
                sample.reg_17_bit_3 = (((rg_rf_board[17]['id']&0x4)>>2), ((self.rg_17.value.integer&0x8)>>3))
                sample.reg_17_bit_4 = (((rg_rf_board[17]['id']&0x8)>>3), ((self.rg_17.value.integer&0x10)>>4))
                sample.reg_16_bit_0 = (rg_rf_board[16]['lock'], (self.rg_16.value.integer&0x1))
                sample.reg_16_bit_1 = ((rg_rf_board[16]['id']&0x1), ((self.rg_16.value.integer&0x2)>>1))
                sample.reg_16_bit_2 = (((rg_rf_board[16]['id']&0x2)>>1), ((self.rg_16.value.integer&0x4)>>2))
                sample.reg_16_bit_3 = (((rg_rf_board[16]['id']&0x4)>>2), ((self.rg_16.value.integer&0x8)>>3))
                sample.reg_16_bit_4 = (((rg_rf_board[16]['id']&0x8)>>3), ((self.rg_16.value.integer&0x10)>>4))
                sample.reg_15_bit_0 = (rg_rf_board[15]['lock'], (self.rg_15.value.integer&0x1))
                sample.reg_15_bit_1 = ((rg_rf_board[15]['id']&0x1), ((self.rg_15.value.integer&0x2)>>1))
                sample.reg_15_bit_2 = (((rg_rf_board[15]['id']&0x2)>>1), ((self.rg_15.value.integer&0x4)>>2))
                sample.reg_15_bit_3 = (((rg_rf_board[15]['id']&0x4)>>2), ((self.rg_15.value.integer&0x8)>>3))
                sample.reg_15_bit_4 = (((rg_rf_board[15]['id']&0x8)>>3), ((self.rg_15.value.integer&0x10)>>4))
                sample.reg_14_bit_0 = (rg_rf_board[14]['lock'], (self.rg_14.value.integer&0x1))
                sample.reg_14_bit_1 = ((rg_rf_board[14]['id']&0x1), ((self.rg_14.value.integer&0x2)>>1))
                sample.reg_14_bit_2 = (((rg_rf_board[14]['id']&0x2)>>1), ((self.rg_14.value.integer&0x4)>>2))
                sample.reg_14_bit_3 = (((rg_rf_board[14]['id']&0x4)>>2), ((self.rg_14.value.integer&0x8)>>3))
                sample.reg_14_bit_4 = (((rg_rf_board[14]['id']&0x8)>>3), ((self.rg_14.value.integer&0x10)>>4))
                sample.reg_13_bit_0 = (rg_rf_board[13]['lock'], (self.rg_13.value.integer&0x1))
                sample.reg_13_bit_1 = ((rg_rf_board[13]['id']&0x1), ((self.rg_13.value.integer&0x2)>>1))
                sample.reg_13_bit_2 = (((rg_rf_board[13]['id']&0x2)>>1), ((self.rg_13.value.integer&0x4)>>2))
                sample.reg_13_bit_3 = (((rg_rf_board[13]['id']&0x4)>>2), ((self.rg_13.value.integer&0x8)>>3))
                sample.reg_13_bit_4 = (((rg_rf_board[13]['id']&0x8)>>3), ((self.rg_13.value.integer&0x10)>>4))
                sample.reg_12_bit_0 = (rg_rf_board[12]['lock'], (self.rg_12.value.integer&0x1))
                sample.reg_12_bit_1 = ((rg_rf_board[12]['id']&0x1), ((self.rg_12.value.integer&0x2)>>1))
                sample.reg_12_bit_2 = (((rg_rf_board[12]['id']&0x2)>>1), ((self.rg_12.value.integer&0x4)>>2))
                sample.reg_12_bit_3 = (((rg_rf_board[12]['id']&0x4)>>2), ((self.rg_12.value.integer&0x8)>>3))
                sample.reg_12_bit_4 = (((rg_rf_board[12]['id']&0x8)>>3), ((self.rg_12.value.integer&0x10)>>4))
                sample.reg_11_bit_0 = (rg_rf_board[11]['lock'], (self.rg_11.value.integer&0x1))
                sample.reg_11_bit_1 = ((rg_rf_board[11]['id']&0x1), ((self.rg_11.value.integer&0x2)>>1))
                sample.reg_11_bit_2 = (((rg_rf_board[11]['id']&0x2)>>1), ((self.rg_11.value.integer&0x4)>>2))
                sample.reg_11_bit_3 = (((rg_rf_board[11]['id']&0x4)>>2), ((self.rg_11.value.integer&0x8)>>3))
                sample.reg_11_bit_4 = (((rg_rf_board[11]['id']&0x8)>>3), ((self.rg_11.value.integer&0x10)>>4))
                sample.reg_10_bit_0 = (rg_rf_board[10]['lock'], (self.rg_10.value.integer&0x1))
                sample.reg_10_bit_1 = ((rg_rf_board[10]['id']&0x1), ((self.rg_10.value.integer&0x2)>>1))
                sample.reg_10_bit_2 = (((rg_rf_board[10]['id']&0x2)>>1), ((self.rg_10.value.integer&0x4)>>2))
                sample.reg_10_bit_3 = (((rg_rf_board[10]['id']&0x4)>>2), ((self.rg_10.value.integer&0x8)>>3))
                sample.reg_10_bit_4 = (((rg_rf_board[10]['id']&0x8)>>3), ((self.rg_10.value.integer&0x10)>>4))
                sample.reg_9_bit_0 = (rg_rf_board[9]['lock'], (self.rg_9.value.integer&0x1))
                sample.reg_9_bit_1 = ((rg_rf_board[9]['id']&0x1), ((self.rg_9.value.integer&0x2)>>1))
                sample.reg_9_bit_2 = (((rg_rf_board[9]['id']&0x2)>>1), ((self.rg_9.value.integer&0x4)>>2))
                sample.reg_9_bit_3 = (((rg_rf_board[9]['id']&0x4)>>2), ((self.rg_9.value.integer&0x8)>>3))
                sample.reg_9_bit_4 = (((rg_rf_board[9]['id']&0x8)>>3), ((self.rg_9.value.integer&0x10)>>4))
                sample.reg_8_bit_0 = (rg_rf_board[8]['lock'], (self.rg_8.value.integer&0x1))
                sample.reg_8_bit_1 = ((rg_rf_board[8]['id']&0x1), ((self.rg_8.value.integer&0x2)>>1))
                sample.reg_8_bit_2 = (((rg_rf_board[8]['id']&0x2)>>1), ((self.rg_8.value.integer&0x4)>>2))
                sample.reg_8_bit_3 = (((rg_rf_board[8]['id']&0x4)>>2), ((self.rg_8.value.integer&0x8)>>3))
                sample.reg_8_bit_4 = (((rg_rf_board[8]['id']&0x8)>>3), ((self.rg_8.value.integer&0x10)>>4))
                sample.reg_7_bit_0 = (rg_rf_board[7]['lock'], (self.rg_7.value.integer&0x1))
                sample.reg_7_bit_1 = ((rg_rf_board[7]['id']&0x1), ((self.rg_7.value.integer&0x2)>>1))
                sample.reg_7_bit_2 = (((rg_rf_board[7]['id']&0x2)>>1), ((self.rg_7.value.integer&0x4)>>2))
                sample.reg_7_bit_3 = (((rg_rf_board[7]['id']&0x4)>>2), ((self.rg_7.value.integer&0x8)>>3))
                sample.reg_7_bit_4 = (((rg_rf_board[7]['id']&0x8)>>3), ((self.rg_7.value.integer&0x10)>>4))
                sample.reg_6_bit_0 = (rg_rf_board[6]['lock'], (self.rg_6.value.integer&0x1))
                sample.reg_6_bit_1 = ((rg_rf_board[6]['id']&0x1), ((self.rg_6.value.integer&0x2)>>1))
                sample.reg_6_bit_2 = (((rg_rf_board[6]['id']&0x2)>>1), ((self.rg_6.value.integer&0x4)>>2))
                sample.reg_6_bit_3 = (((rg_rf_board[6]['id']&0x4)>>2), ((self.rg_6.value.integer&0x8)>>3))
                sample.reg_6_bit_4 = (((rg_rf_board[6]['id']&0x8)>>3), ((self.rg_6.value.integer&0x10)>>4))
                sample.reg_5_bit_0 = (rg_rf_board[5]['lock'], (self.rg_5.value.integer&0x1))
                sample.reg_5_bit_1 = ((rg_rf_board[5]['id']&0x1), ((self.rg_5.value.integer&0x2)>>1))
                sample.reg_5_bit_2 = (((rg_rf_board[5]['id']&0x2)>>1), ((self.rg_5.value.integer&0x4)>>2))
                sample.reg_5_bit_3 = (((rg_rf_board[5]['id']&0x4)>>2), ((self.rg_5.value.integer&0x8)>>3))
                sample.reg_5_bit_4 = (((rg_rf_board[5]['id']&0x8)>>3), ((self.rg_5.value.integer&0x10)>>4))
                sample.reg_4_bit_0 = (rg_rf_board[4]['lock'], (self.rg_4.value.integer&0x1))
                sample.reg_4_bit_1 = ((rg_rf_board[4]['id']&0x1), ((self.rg_4.value.integer&0x2)>>1))
                sample.reg_4_bit_2 = (((rg_rf_board[4]['id']&0x2)>>1), ((self.rg_4.value.integer&0x4)>>2))
                sample.reg_4_bit_3 = (((rg_rf_board[4]['id']&0x4)>>2), ((self.rg_4.value.integer&0x8)>>3))
                sample.reg_4_bit_4 = (((rg_rf_board[4]['id']&0x8)>>3), ((self.rg_4.value.integer&0x10)>>4))
                sample.reg_3_bit_0 = (rg_rf_board[3]['lock'], (self.rg_3.value.integer&0x1))
                sample.reg_3_bit_1 = ((rg_rf_board[3]['id']&0x1), ((self.rg_3.value.integer&0x2)>>1))
                sample.reg_3_bit_2 = (((rg_rf_board[3]['id']&0x2)>>1), ((self.rg_3.value.integer&0x4)>>2))
                sample.reg_3_bit_3 = (((rg_rf_board[3]['id']&0x4)>>2), ((self.rg_3.value.integer&0x8)>>3))
                sample.reg_3_bit_4 = (((rg_rf_board[3]['id']&0x8)>>3), ((self.rg_3.value.integer&0x10)>>4))
                sample.reg_2_bit_0 = (rg_rf_board[2]['lock'], (self.rg_2.value.integer&0x1))
                sample.reg_2_bit_1 = ((rg_rf_board[2]['id']&0x1), ((self.rg_2.value.integer&0x2)>>1))
                sample.reg_2_bit_2 = (((rg_rf_board[2]['id']&0x2)>>1), ((self.rg_2.value.integer&0x4)>>2))
                sample.reg_2_bit_3 = (((rg_rf_board[2]['id']&0x4)>>2), ((self.rg_2.value.integer&0x8)>>3))
                sample.reg_2_bit_4 = (((rg_rf_board[2]['id']&0x8)>>3), ((self.rg_2.value.integer&0x10)>>4))
                sample.reg_1_bit_0 = (rg_rf_board[1]['lock'], (self.rg_1.value.integer&0x1))
                sample.reg_1_bit_1 = ((rg_rf_board[1]['id']&0x1), ((self.rg_1.value.integer&0x2)>>1))
                sample.reg_1_bit_2 = (((rg_rf_board[1]['id']&0x2)>>1), ((self.rg_1.value.integer&0x4)>>2))
                sample.reg_1_bit_3 = (((rg_rf_board[1]['id']&0x4)>>2), ((self.rg_1.value.integer&0x8)>>3))
                sample.reg_1_bit_4 = (((rg_rf_board[1]['id']&0x8)>>3), ((self.rg_1.value.integer&0x10)>>4))
                sample.reg_0_bit_0 = (rg_rf_board[0]['lock'], (self.rg_0.value.integer&0x1))
                sample.reg_0_bit_1 = ((rg_rf_board[0]['id']&0x1), ((self.rg_0.value.integer&0x2)>>1))
                sample.reg_0_bit_2 = (((rg_rf_board[0]['id']&0x2)>>1), ((self.rg_0.value.integer&0x4)>>2))
                sample.reg_0_bit_3 = (((rg_rf_board[0]['id']&0x4)>>2), ((self.rg_0.value.integer&0x8)>>3))
                sample.reg_0_bit_4 = (((rg_rf_board[0]['id']&0x8)>>3), ((self.rg_0.value.integer&0x10)>>4))
                for i in range(0,32):
                    rg_rf_board[i]['lock']=(eval('self.rg_'+str(i)+'.value.integer')&0x1)
                    rg_rf_board[i]['id']=((eval('self.rg_'+str(i)+'.value.integer')&0x1E)>>1)
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_cov_toggle(sample)

