import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto
from bsvstruct_class import *

in1_bins = [0, 1]+walking_ones(133)+walking_zeros(133)+leading_ones(133)+leading_zeros(133)+alternate_ones(133)+alternate_zeros(133)+signed_special(133)
global input_in
input_in = 0

class fun3_enum(IntEnum):
    MUL = 0
    DIV = 1

def range_op(x):
    if x>0:
       return 'POS'
    elif x==0:
      return 'ZERO'
    else:
      return 'NEG'

def transition_cov(new_val):                
    prev_value=0                             
    transition = (prev_value, new_val)       
    prev_value = new_val                
    return transition

class InputSample:
    def __init__(self):
        self.in_in : int
        self.ff_full : int 
        self.ff_empty : int
        self.rl_fifo_full : int
        self.funct3 : fun3_enum
        self.full_trans : int
        self.empty_trans : int
        self.instr_stall : bool
    def __str__(self):
        return 'in_in : '+str(hex(self.in_in)) + \
                ' ff_full: ' +str(hex(self.ff_full)) +\
                ' ff_empty: ' + str(self.ff_empty)

class Coverage(object):
    
    _signals = get_signals('mbox','inputs')
    _signals.update(get_signals('mbox', 'outputs'))
    _signals.update(get_signals('mbox', 'wire'))
    _signals.update(get_signals('mkstage3', 'inputs'))
    _signals.update(get_signals('mkstage3', 'wires'))

    FnMBoxCov = coverage_section (
        CoverPoint("top.MBox_Funct3", xf = lambda sample: sample.funct3, 
                                bins = [x.name for x in list(fun3_enum)]
                                ),
        CoverPoint("top.MBox_In_In", xf = lambda sample: sample.in_in , bins = in1_bins),
        CoverPoint("top.MBox_ff_full", xf = lambda sample: sample.ff_full , bins = [1, 0]),
        CoverPoint("top.MBox_ff_empty", xf = lambda sample: sample.ff_empty , bins = [1, 0]),
        CoverPoint("top.MBox_rl_fifo_full", xf = lambda sample: sample.rl_fifo_full, bins = [1, 0]),
        CoverPoint("top.MBox_ff_full_trans", xf = lambda sample: sample.full_trans,  bins =  [(0, 1), (1, 0)]),
        CoverPoint("top.MBox_ff_empty_trans", xf = lambda sample: sample.empty_trans, bins = [(0, 1), (1, 0)]),
    )

    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals())
        cocotb.fork(self.pending_inst())

    @FnMBoxCov
    def sample_coverage(self, sample):
        pass 

    @coroutine
    def sample_signals(self):
        _input=MBoxIn()
        list_in_in = []
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            in_in = self.in_in.value.integer
            _input.set(BinaryValue(value=in_in, n_bits=133, binaryRepresentation=2, bigEndian=False))
            if self.input.value.integer == 1:
                sample = InputSample()
                sample.in_in = self.in_in.value.signed_integer
                sample.ff_full = int(not(self.ff_full.value.integer))
                sample.ff_empty = self.ff_empty.value.integer
                sample.in_in_range = range_op(self.in_in.value.signed_integer)
                sample.funct3 = fun3_enum((_input.funct3 & 0x4) >> 2).name
                sample.rl_fifo_full = self.rl_fifo_full.value.integer
                sample.full_trans = transition_cov(sample.ff_full)
                sample.empty_trans = transition_cov(self.ff_empty.value.integer)
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage(sample)

    FnMBoxCov_instr = coverage_section (
            CoverPoint("top.MBox_instr_stall", xf = lambda sample : sample.instr_stall , bins = [True, False]),
        )

    @FnMBoxCov_instr
    def sample_pending_instr(self, sample):
        pass

    @coroutine
    def pending_inst(self):
        global input_in
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            sample = InputSample()
            sample.trigger = self.stage2_deq_rdy.value.integer and self.fwding.value.integer and self.stage2_first.value.integer == 8 and self.cur_epochs.value.integer == self.stage2_first_6_to_5.value.integer
            sample.op1 = self.op1_avail.value.integer
            sample.op2 = self.op2_avail.value.integer
            if sample.trigger == 1 and sample.op1 == 1 and sample.op2 == 1:
                input_in += 1
            else:
                input_in = input_in
            if self.output.value.integer == 1:
                sample.instr_stall = True if input_in > 1 else False
                input_in = 0
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_pending_instr(sample)


