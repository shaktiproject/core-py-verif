import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto

in1_bins = [0, 1]+walking_ones(64)+walking_zeros(64)+leading_ones(64)+leading_zeros(64)+alternate_ones(64)+alternate_zeros(64)+signed_special(64)
global write_en_count, prev_val, addr_complement_count, addr_write_arr
write_en_count = 0
addr_complement_count = [0] * 32
prev_val = [0] * 32
addr_write_arr = []

class InputSample:
    def __init__(self): 
        self.write_enable : int
        self.addr_in : int
        self.rs1_addr : int
        self.rs2_addr : int
        self.arr_reg : list
    def __str__(self):
        return 'rs1 : '+str(hex(self.rs1_addr)) + \
                ' rs2: ' +str(hex(self.rs2_addr)) 

class Coverage(object):
    
    _signals = get_signals('mkregisterfile','inputs')
    _signals.update(get_signals('mkregisterfile', 'outputs'))
    _signals.update(get_signals('mkregisterfile', 'register'))
    _signals.update(get_signals('mkregisterfile', 'wires'))

    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals_val()) 
        cocotb.fork(self.bits_toggle())

    FnRegCov_val = coverage_section ( 
        CoverPoint("top.regfile.write_oneread_same_cycle", xf = lambda sample : sample.write_oneread_same_cycle , bins = [True, False]) ,
        CoverPoint("top.regfile.write_bothread_same_cycle", xf = lambda sample : sample.write_bothread_same_cycle , bins = [True, False]) ,
        CoverCheck("top.regfile.reg0_assertion.check", f_fail = lambda sample : sample.xrf[31] != 0, f_pass = lambda sample : sample.xrf[31] == 0),
        CoverPoint("top.regfile.all_addr_write", xf = lambda sample : sample.all_addr_write , bins = [True, False]),
    )

    @FnRegCov_val
    def sample_coverage_val(self, sample):
        pass 

    @coroutine
    def sample_signals_val(self):
        global addr_write_arr
        global write_en_count
        reg_address = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31]
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns") 
            sample = InputSample()
            sample.xrf = self.arr_reg.value
            sample.write_oneread_same_cycle = True if self.write_enable.value.integer == 1 and self.rs1_addr.value.integer == self.addr_in.value.integer or self.rs2_addr.value.integer == self.addr_in.value.integer else False
            sample.write_bothread_same_cycle = True if self.write_enable.value.integer ==1 and self.addr_in.value.integer == self.rs1_addr.value.integer == self.rs2_addr.value.integer else False
            if self.write_enable.value.integer == 1:
                write_en_count += 1
                addr_write_arr.append(self.addr_in.value.integer) 
            addr_write = [x for x in (reg_address) if x not in addr_write_arr] if write_en_count > 31 else []
            sample.all_addr_write = True if not addr_write else False 
            if self.enlog:
                self.log.info('Coverage Sampling : ' +str(sample))
            self.sample_coverage_val(sample)                          
 
    coverage_db["top.regfile.reg0_assertion.check"].add_bins_callback(lambda : TestFailure("Assertion Error"), "FAIL")
 
    FnRegCov_check = coverage_section (
            CoverPoint("top.reg_31_bit_0", xf = lambda sample : sample.reg_31_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_1", xf = lambda sample : sample.reg_31_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_2", xf = lambda sample : sample.reg_31_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_3", xf = lambda sample : sample.reg_31_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_4", xf = lambda sample : sample.reg_31_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_5", xf = lambda sample : sample.reg_31_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_6", xf = lambda sample : sample.reg_31_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_7", xf = lambda sample : sample.reg_31_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_8", xf = lambda sample : sample.reg_31_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_9", xf = lambda sample : sample.reg_31_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_10", xf = lambda sample : sample.reg_31_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_11", xf = lambda sample : sample.reg_31_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_12", xf = lambda sample : sample.reg_31_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_13", xf = lambda sample : sample.reg_31_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_14", xf = lambda sample : sample.reg_31_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_15", xf = lambda sample : sample.reg_31_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_16", xf = lambda sample : sample.reg_31_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_17", xf = lambda sample : sample.reg_31_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_18", xf = lambda sample : sample.reg_31_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_19", xf = lambda sample : sample.reg_31_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_20", xf = lambda sample : sample.reg_31_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_21", xf = lambda sample : sample.reg_31_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_22", xf = lambda sample : sample.reg_31_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_23", xf = lambda sample : sample.reg_31_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_24", xf = lambda sample : sample.reg_31_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_25", xf = lambda sample : sample.reg_31_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_26", xf = lambda sample : sample.reg_31_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_27", xf = lambda sample : sample.reg_31_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_28", xf = lambda sample : sample.reg_31_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_29", xf = lambda sample : sample.reg_31_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_30", xf = lambda sample : sample.reg_31_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_31", xf = lambda sample : sample.reg_31_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_32", xf = lambda sample : sample.reg_31_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_33", xf = lambda sample : sample.reg_31_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_34", xf = lambda sample : sample.reg_31_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_35", xf = lambda sample : sample.reg_31_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_36", xf = lambda sample : sample.reg_31_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_37", xf = lambda sample : sample.reg_31_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_38", xf = lambda sample : sample.reg_31_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_39", xf = lambda sample : sample.reg_31_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_40", xf = lambda sample : sample.reg_31_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_41", xf = lambda sample : sample.reg_31_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_42", xf = lambda sample : sample.reg_31_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_43", xf = lambda sample : sample.reg_31_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_44", xf = lambda sample : sample.reg_31_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_45", xf = lambda sample : sample.reg_31_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_46", xf = lambda sample : sample.reg_31_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_47", xf = lambda sample : sample.reg_31_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_48", xf = lambda sample : sample.reg_31_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_49", xf = lambda sample : sample.reg_31_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_50", xf = lambda sample : sample.reg_31_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_51", xf = lambda sample : sample.reg_31_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_52", xf = lambda sample : sample.reg_31_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_53", xf = lambda sample : sample.reg_31_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_54", xf = lambda sample : sample.reg_31_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_55", xf = lambda sample : sample.reg_31_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_56", xf = lambda sample : sample.reg_31_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_57", xf = lambda sample : sample.reg_31_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_58", xf = lambda sample : sample.reg_31_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_59", xf = lambda sample : sample.reg_31_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_60", xf = lambda sample : sample.reg_31_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_61", xf = lambda sample : sample.reg_31_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_62", xf = lambda sample : sample.reg_31_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_31_bit_63", xf = lambda sample : sample.reg_31_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_0", xf = lambda sample : sample.reg_30_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_1", xf = lambda sample : sample.reg_30_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_2", xf = lambda sample : sample.reg_30_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_3", xf = lambda sample : sample.reg_30_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_4", xf = lambda sample : sample.reg_30_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_5", xf = lambda sample : sample.reg_30_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_6", xf = lambda sample : sample.reg_30_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_7", xf = lambda sample : sample.reg_30_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_8", xf = lambda sample : sample.reg_30_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_9", xf = lambda sample : sample.reg_30_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_10", xf = lambda sample : sample.reg_30_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_11", xf = lambda sample : sample.reg_30_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_12", xf = lambda sample : sample.reg_30_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_13", xf = lambda sample : sample.reg_30_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_14", xf = lambda sample : sample.reg_30_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_15", xf = lambda sample : sample.reg_30_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_16", xf = lambda sample : sample.reg_30_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_17", xf = lambda sample : sample.reg_30_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_18", xf = lambda sample : sample.reg_30_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_19", xf = lambda sample : sample.reg_30_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_20", xf = lambda sample : sample.reg_30_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_21", xf = lambda sample : sample.reg_30_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_22", xf = lambda sample : sample.reg_30_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_23", xf = lambda sample : sample.reg_30_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_24", xf = lambda sample : sample.reg_30_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_25", xf = lambda sample : sample.reg_30_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_26", xf = lambda sample : sample.reg_30_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_27", xf = lambda sample : sample.reg_30_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_28", xf = lambda sample : sample.reg_30_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_29", xf = lambda sample : sample.reg_30_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_30", xf = lambda sample : sample.reg_30_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_31", xf = lambda sample : sample.reg_30_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_32", xf = lambda sample : sample.reg_30_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_33", xf = lambda sample : sample.reg_30_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_34", xf = lambda sample : sample.reg_30_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_35", xf = lambda sample : sample.reg_30_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_36", xf = lambda sample : sample.reg_30_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_37", xf = lambda sample : sample.reg_30_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_38", xf = lambda sample : sample.reg_30_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_39", xf = lambda sample : sample.reg_30_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_40", xf = lambda sample : sample.reg_30_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_41", xf = lambda sample : sample.reg_30_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_42", xf = lambda sample : sample.reg_30_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_43", xf = lambda sample : sample.reg_30_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_44", xf = lambda sample : sample.reg_30_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_45", xf = lambda sample : sample.reg_30_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_46", xf = lambda sample : sample.reg_30_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_47", xf = lambda sample : sample.reg_30_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_48", xf = lambda sample : sample.reg_30_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_49", xf = lambda sample : sample.reg_30_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_50", xf = lambda sample : sample.reg_30_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_51", xf = lambda sample : sample.reg_30_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_52", xf = lambda sample : sample.reg_30_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_53", xf = lambda sample : sample.reg_30_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_54", xf = lambda sample : sample.reg_30_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_55", xf = lambda sample : sample.reg_30_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_56", xf = lambda sample : sample.reg_30_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_57", xf = lambda sample : sample.reg_30_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_58", xf = lambda sample : sample.reg_30_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_59", xf = lambda sample : sample.reg_30_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_60", xf = lambda sample : sample.reg_30_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_61", xf = lambda sample : sample.reg_30_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_62", xf = lambda sample : sample.reg_30_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_30_bit_63", xf = lambda sample : sample.reg_30_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_0", xf = lambda sample : sample.reg_29_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_1", xf = lambda sample : sample.reg_29_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_2", xf = lambda sample : sample.reg_29_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_3", xf = lambda sample : sample.reg_29_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_4", xf = lambda sample : sample.reg_29_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_5", xf = lambda sample : sample.reg_29_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_6", xf = lambda sample : sample.reg_29_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_7", xf = lambda sample : sample.reg_29_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_8", xf = lambda sample : sample.reg_29_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_9", xf = lambda sample : sample.reg_29_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_10", xf = lambda sample : sample.reg_29_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_11", xf = lambda sample : sample.reg_29_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_12", xf = lambda sample : sample.reg_29_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_13", xf = lambda sample : sample.reg_29_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_14", xf = lambda sample : sample.reg_29_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_15", xf = lambda sample : sample.reg_29_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_16", xf = lambda sample : sample.reg_29_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_17", xf = lambda sample : sample.reg_29_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_18", xf = lambda sample : sample.reg_29_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_19", xf = lambda sample : sample.reg_29_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_20", xf = lambda sample : sample.reg_29_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_21", xf = lambda sample : sample.reg_29_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_22", xf = lambda sample : sample.reg_29_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_23", xf = lambda sample : sample.reg_29_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_24", xf = lambda sample : sample.reg_29_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_25", xf = lambda sample : sample.reg_29_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_26", xf = lambda sample : sample.reg_29_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_27", xf = lambda sample : sample.reg_29_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_28", xf = lambda sample : sample.reg_29_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_29", xf = lambda sample : sample.reg_29_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_30", xf = lambda sample : sample.reg_29_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_31", xf = lambda sample : sample.reg_29_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_32", xf = lambda sample : sample.reg_29_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_33", xf = lambda sample : sample.reg_29_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_34", xf = lambda sample : sample.reg_29_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_35", xf = lambda sample : sample.reg_29_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_36", xf = lambda sample : sample.reg_29_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_37", xf = lambda sample : sample.reg_29_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_38", xf = lambda sample : sample.reg_29_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_39", xf = lambda sample : sample.reg_29_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_40", xf = lambda sample : sample.reg_29_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_41", xf = lambda sample : sample.reg_29_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_42", xf = lambda sample : sample.reg_29_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_43", xf = lambda sample : sample.reg_29_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_44", xf = lambda sample : sample.reg_29_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_45", xf = lambda sample : sample.reg_29_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_46", xf = lambda sample : sample.reg_29_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_47", xf = lambda sample : sample.reg_29_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_48", xf = lambda sample : sample.reg_29_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_49", xf = lambda sample : sample.reg_29_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_50", xf = lambda sample : sample.reg_29_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_51", xf = lambda sample : sample.reg_29_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_52", xf = lambda sample : sample.reg_29_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_53", xf = lambda sample : sample.reg_29_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_54", xf = lambda sample : sample.reg_29_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_55", xf = lambda sample : sample.reg_29_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_56", xf = lambda sample : sample.reg_29_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_57", xf = lambda sample : sample.reg_29_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_58", xf = lambda sample : sample.reg_29_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_59", xf = lambda sample : sample.reg_29_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_60", xf = lambda sample : sample.reg_29_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_61", xf = lambda sample : sample.reg_29_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_62", xf = lambda sample : sample.reg_29_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_29_bit_63", xf = lambda sample : sample.reg_29_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_28_bit_0", xf = lambda sample : sample.reg_28_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_1", xf = lambda sample : sample.reg_28_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_2", xf = lambda sample : sample.reg_28_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_3", xf = lambda sample : sample.reg_28_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_4", xf = lambda sample : sample.reg_28_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_5", xf = lambda sample : sample.reg_28_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_6", xf = lambda sample : sample.reg_28_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_7", xf = lambda sample : sample.reg_28_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_8", xf = lambda sample : sample.reg_28_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_9", xf = lambda sample : sample.reg_28_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_10", xf = lambda sample : sample.reg_28_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_11", xf = lambda sample : sample.reg_28_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_12", xf = lambda sample : sample.reg_28_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_13", xf = lambda sample : sample.reg_28_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_14", xf = lambda sample : sample.reg_28_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_15", xf = lambda sample : sample.reg_28_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_16", xf = lambda sample : sample.reg_28_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_17", xf = lambda sample : sample.reg_28_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_18", xf = lambda sample : sample.reg_28_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_19", xf = lambda sample : sample.reg_28_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_20", xf = lambda sample : sample.reg_28_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_21", xf = lambda sample : sample.reg_28_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_22", xf = lambda sample : sample.reg_28_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_23", xf = lambda sample : sample.reg_28_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_24", xf = lambda sample : sample.reg_28_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_25", xf = lambda sample : sample.reg_28_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_26", xf = lambda sample : sample.reg_28_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_27", xf = lambda sample : sample.reg_28_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_28", xf = lambda sample : sample.reg_28_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_29", xf = lambda sample : sample.reg_28_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_30", xf = lambda sample : sample.reg_28_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_31", xf = lambda sample : sample.reg_28_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_32", xf = lambda sample : sample.reg_28_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_33", xf = lambda sample : sample.reg_28_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_34", xf = lambda sample : sample.reg_28_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_35", xf = lambda sample : sample.reg_28_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_36", xf = lambda sample : sample.reg_28_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_37", xf = lambda sample : sample.reg_28_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_38", xf = lambda sample : sample.reg_28_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_39", xf = lambda sample : sample.reg_28_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_40", xf = lambda sample : sample.reg_28_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_41", xf = lambda sample : sample.reg_28_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_42", xf = lambda sample : sample.reg_28_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_43", xf = lambda sample : sample.reg_28_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_44", xf = lambda sample : sample.reg_28_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_45", xf = lambda sample : sample.reg_28_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_46", xf = lambda sample : sample.reg_28_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_47", xf = lambda sample : sample.reg_28_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_48", xf = lambda sample : sample.reg_28_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_49", xf = lambda sample : sample.reg_28_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_50", xf = lambda sample : sample.reg_28_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_51", xf = lambda sample : sample.reg_28_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_52", xf = lambda sample : sample.reg_28_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_53", xf = lambda sample : sample.reg_28_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_54", xf = lambda sample : sample.reg_28_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_55", xf = lambda sample : sample.reg_28_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_56", xf = lambda sample : sample.reg_28_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_57", xf = lambda sample : sample.reg_28_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_58", xf = lambda sample : sample.reg_28_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_59", xf = lambda sample : sample.reg_28_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_60", xf = lambda sample : sample.reg_28_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_61", xf = lambda sample : sample.reg_28_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_62", xf = lambda sample : sample.reg_28_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_28_bit_63", xf = lambda sample : sample.reg_28_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_27_bit_0", xf = lambda sample : sample.reg_27_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_1", xf = lambda sample : sample.reg_27_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_2", xf = lambda sample : sample.reg_27_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_3", xf = lambda sample : sample.reg_27_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_4", xf = lambda sample : sample.reg_27_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_5", xf = lambda sample : sample.reg_27_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_6", xf = lambda sample : sample.reg_27_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_7", xf = lambda sample : sample.reg_27_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_8", xf = lambda sample : sample.reg_27_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_9", xf = lambda sample : sample.reg_27_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_10", xf = lambda sample : sample.reg_27_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_11", xf = lambda sample : sample.reg_27_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_12", xf = lambda sample : sample.reg_27_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_13", xf = lambda sample : sample.reg_27_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_14", xf = lambda sample : sample.reg_27_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_15", xf = lambda sample : sample.reg_27_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_16", xf = lambda sample : sample.reg_27_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_17", xf = lambda sample : sample.reg_27_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_18", xf = lambda sample : sample.reg_27_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_19", xf = lambda sample : sample.reg_27_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_20", xf = lambda sample : sample.reg_27_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_21", xf = lambda sample : sample.reg_27_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_22", xf = lambda sample : sample.reg_27_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_23", xf = lambda sample : sample.reg_27_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_24", xf = lambda sample : sample.reg_27_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_25", xf = lambda sample : sample.reg_27_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_26", xf = lambda sample : sample.reg_27_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_27", xf = lambda sample : sample.reg_27_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_28", xf = lambda sample : sample.reg_27_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_29", xf = lambda sample : sample.reg_27_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_30", xf = lambda sample : sample.reg_27_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_31", xf = lambda sample : sample.reg_27_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_32", xf = lambda sample : sample.reg_27_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_33", xf = lambda sample : sample.reg_27_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_34", xf = lambda sample : sample.reg_27_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_35", xf = lambda sample : sample.reg_27_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_36", xf = lambda sample : sample.reg_27_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_37", xf = lambda sample : sample.reg_27_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_38", xf = lambda sample : sample.reg_27_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_39", xf = lambda sample : sample.reg_27_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_40", xf = lambda sample : sample.reg_27_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_41", xf = lambda sample : sample.reg_27_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_42", xf = lambda sample : sample.reg_27_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_43", xf = lambda sample : sample.reg_27_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_44", xf = lambda sample : sample.reg_27_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_45", xf = lambda sample : sample.reg_27_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_46", xf = lambda sample : sample.reg_27_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_47", xf = lambda sample : sample.reg_27_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_48", xf = lambda sample : sample.reg_27_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_49", xf = lambda sample : sample.reg_27_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_50", xf = lambda sample : sample.reg_27_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_51", xf = lambda sample : sample.reg_27_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_52", xf = lambda sample : sample.reg_27_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_53", xf = lambda sample : sample.reg_27_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_54", xf = lambda sample : sample.reg_27_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_55", xf = lambda sample : sample.reg_27_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_56", xf = lambda sample : sample.reg_27_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_57", xf = lambda sample : sample.reg_27_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_58", xf = lambda sample : sample.reg_27_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_59", xf = lambda sample : sample.reg_27_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_60", xf = lambda sample : sample.reg_27_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_61", xf = lambda sample : sample.reg_27_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_62", xf = lambda sample : sample.reg_27_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_27_bit_63", xf = lambda sample : sample.reg_27_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_0", xf = lambda sample : sample.reg_26_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_1", xf = lambda sample : sample.reg_26_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_2", xf = lambda sample : sample.reg_26_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_3", xf = lambda sample : sample.reg_26_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_4", xf = lambda sample : sample.reg_26_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_5", xf = lambda sample : sample.reg_26_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_6", xf = lambda sample : sample.reg_26_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_7", xf = lambda sample : sample.reg_26_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_8", xf = lambda sample : sample.reg_26_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_9", xf = lambda sample : sample.reg_26_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_10", xf = lambda sample : sample.reg_26_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_11", xf = lambda sample : sample.reg_26_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_12", xf = lambda sample : sample.reg_26_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_13", xf = lambda sample : sample.reg_26_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_14", xf = lambda sample : sample.reg_26_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_15", xf = lambda sample : sample.reg_26_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_16", xf = lambda sample : sample.reg_26_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_17", xf = lambda sample : sample.reg_26_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_18", xf = lambda sample : sample.reg_26_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_19", xf = lambda sample : sample.reg_26_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_20", xf = lambda sample : sample.reg_26_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_21", xf = lambda sample : sample.reg_26_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_22", xf = lambda sample : sample.reg_26_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_23", xf = lambda sample : sample.reg_26_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_24", xf = lambda sample : sample.reg_26_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_25", xf = lambda sample : sample.reg_26_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_26", xf = lambda sample : sample.reg_26_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_27", xf = lambda sample : sample.reg_26_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_28", xf = lambda sample : sample.reg_26_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_29", xf = lambda sample : sample.reg_26_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_30", xf = lambda sample : sample.reg_26_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_31", xf = lambda sample : sample.reg_26_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_32", xf = lambda sample : sample.reg_26_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_33", xf = lambda sample : sample.reg_26_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_34", xf = lambda sample : sample.reg_26_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_35", xf = lambda sample : sample.reg_26_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_36", xf = lambda sample : sample.reg_26_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_37", xf = lambda sample : sample.reg_26_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_38", xf = lambda sample : sample.reg_26_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_39", xf = lambda sample : sample.reg_26_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_40", xf = lambda sample : sample.reg_26_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_41", xf = lambda sample : sample.reg_26_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_42", xf = lambda sample : sample.reg_26_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_43", xf = lambda sample : sample.reg_26_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_44", xf = lambda sample : sample.reg_26_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_45", xf = lambda sample : sample.reg_26_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_46", xf = lambda sample : sample.reg_26_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_47", xf = lambda sample : sample.reg_26_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_48", xf = lambda sample : sample.reg_26_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_49", xf = lambda sample : sample.reg_26_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_50", xf = lambda sample : sample.reg_26_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_51", xf = lambda sample : sample.reg_26_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_52", xf = lambda sample : sample.reg_26_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_53", xf = lambda sample : sample.reg_26_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_54", xf = lambda sample : sample.reg_26_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_55", xf = lambda sample : sample.reg_26_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_56", xf = lambda sample : sample.reg_26_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_57", xf = lambda sample : sample.reg_26_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_58", xf = lambda sample : sample.reg_26_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_59", xf = lambda sample : sample.reg_26_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_60", xf = lambda sample : sample.reg_26_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_61", xf = lambda sample : sample.reg_26_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_62", xf = lambda sample : sample.reg_26_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_26_bit_63", xf = lambda sample : sample.reg_26_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_25_bit_0", xf = lambda sample : sample.reg_25_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_1", xf = lambda sample : sample.reg_25_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_2", xf = lambda sample : sample.reg_25_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_3", xf = lambda sample : sample.reg_25_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_4", xf = lambda sample : sample.reg_25_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_5", xf = lambda sample : sample.reg_25_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_6", xf = lambda sample : sample.reg_25_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_7", xf = lambda sample : sample.reg_25_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_8", xf = lambda sample : sample.reg_25_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_9", xf = lambda sample : sample.reg_25_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_10", xf = lambda sample : sample.reg_25_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_11", xf = lambda sample : sample.reg_25_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_12", xf = lambda sample : sample.reg_25_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_13", xf = lambda sample : sample.reg_25_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_14", xf = lambda sample : sample.reg_25_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_15", xf = lambda sample : sample.reg_25_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_16", xf = lambda sample : sample.reg_25_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_17", xf = lambda sample : sample.reg_25_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_18", xf = lambda sample : sample.reg_25_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_19", xf = lambda sample : sample.reg_25_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_20", xf = lambda sample : sample.reg_25_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_21", xf = lambda sample : sample.reg_25_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_22", xf = lambda sample : sample.reg_25_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_23", xf = lambda sample : sample.reg_25_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_24", xf = lambda sample : sample.reg_25_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_25", xf = lambda sample : sample.reg_25_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_26", xf = lambda sample : sample.reg_25_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_27", xf = lambda sample : sample.reg_25_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_28", xf = lambda sample : sample.reg_25_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_29", xf = lambda sample : sample.reg_25_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_30", xf = lambda sample : sample.reg_25_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_31", xf = lambda sample : sample.reg_25_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_32", xf = lambda sample : sample.reg_25_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_33", xf = lambda sample : sample.reg_25_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_34", xf = lambda sample : sample.reg_25_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_35", xf = lambda sample : sample.reg_25_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_36", xf = lambda sample : sample.reg_25_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_37", xf = lambda sample : sample.reg_25_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_38", xf = lambda sample : sample.reg_25_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_39", xf = lambda sample : sample.reg_25_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_40", xf = lambda sample : sample.reg_25_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_41", xf = lambda sample : sample.reg_25_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_42", xf = lambda sample : sample.reg_25_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_43", xf = lambda sample : sample.reg_25_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_44", xf = lambda sample : sample.reg_25_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_45", xf = lambda sample : sample.reg_25_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_46", xf = lambda sample : sample.reg_25_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_47", xf = lambda sample : sample.reg_25_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_48", xf = lambda sample : sample.reg_25_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_49", xf = lambda sample : sample.reg_25_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_50", xf = lambda sample : sample.reg_25_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_51", xf = lambda sample : sample.reg_25_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_52", xf = lambda sample : sample.reg_25_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_53", xf = lambda sample : sample.reg_25_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_54", xf = lambda sample : sample.reg_25_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_55", xf = lambda sample : sample.reg_25_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_56", xf = lambda sample : sample.reg_25_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_57", xf = lambda sample : sample.reg_25_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_58", xf = lambda sample : sample.reg_25_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_59", xf = lambda sample : sample.reg_25_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_60", xf = lambda sample : sample.reg_25_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_61", xf = lambda sample : sample.reg_25_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_62", xf = lambda sample : sample.reg_25_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_25_bit_63", xf = lambda sample : sample.reg_25_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_24_bit_0", xf = lambda sample : sample.reg_24_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_1", xf = lambda sample : sample.reg_24_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_2", xf = lambda sample : sample.reg_24_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_3", xf = lambda sample : sample.reg_24_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_4", xf = lambda sample : sample.reg_24_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_5", xf = lambda sample : sample.reg_24_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_6", xf = lambda sample : sample.reg_24_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_7", xf = lambda sample : sample.reg_24_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_8", xf = lambda sample : sample.reg_24_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_9", xf = lambda sample : sample.reg_24_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_10", xf = lambda sample : sample.reg_24_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_11", xf = lambda sample : sample.reg_24_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_12", xf = lambda sample : sample.reg_24_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_13", xf = lambda sample : sample.reg_24_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_14", xf = lambda sample : sample.reg_24_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_15", xf = lambda sample : sample.reg_24_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_16", xf = lambda sample : sample.reg_24_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_17", xf = lambda sample : sample.reg_24_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_18", xf = lambda sample : sample.reg_24_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_19", xf = lambda sample : sample.reg_24_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_20", xf = lambda sample : sample.reg_24_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_21", xf = lambda sample : sample.reg_24_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_22", xf = lambda sample : sample.reg_24_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_23", xf = lambda sample : sample.reg_24_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_24", xf = lambda sample : sample.reg_24_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_25", xf = lambda sample : sample.reg_24_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_26", xf = lambda sample : sample.reg_24_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_27", xf = lambda sample : sample.reg_24_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_28", xf = lambda sample : sample.reg_24_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_29", xf = lambda sample : sample.reg_24_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_30", xf = lambda sample : sample.reg_24_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_31", xf = lambda sample : sample.reg_24_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_32", xf = lambda sample : sample.reg_24_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_33", xf = lambda sample : sample.reg_24_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_34", xf = lambda sample : sample.reg_24_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_35", xf = lambda sample : sample.reg_24_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_36", xf = lambda sample : sample.reg_24_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_37", xf = lambda sample : sample.reg_24_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_38", xf = lambda sample : sample.reg_24_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_39", xf = lambda sample : sample.reg_24_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_40", xf = lambda sample : sample.reg_24_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_41", xf = lambda sample : sample.reg_24_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_42", xf = lambda sample : sample.reg_24_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_43", xf = lambda sample : sample.reg_24_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_44", xf = lambda sample : sample.reg_24_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_45", xf = lambda sample : sample.reg_24_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_46", xf = lambda sample : sample.reg_24_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_47", xf = lambda sample : sample.reg_24_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_48", xf = lambda sample : sample.reg_24_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_49", xf = lambda sample : sample.reg_24_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_50", xf = lambda sample : sample.reg_24_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_51", xf = lambda sample : sample.reg_24_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_52", xf = lambda sample : sample.reg_24_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_53", xf = lambda sample : sample.reg_24_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_54", xf = lambda sample : sample.reg_24_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_55", xf = lambda sample : sample.reg_24_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_56", xf = lambda sample : sample.reg_24_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_57", xf = lambda sample : sample.reg_24_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_58", xf = lambda sample : sample.reg_24_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_59", xf = lambda sample : sample.reg_24_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_60", xf = lambda sample : sample.reg_24_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_61", xf = lambda sample : sample.reg_24_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_62", xf = lambda sample : sample.reg_24_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_24_bit_63", xf = lambda sample : sample.reg_24_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_23_bit_0", xf = lambda sample : sample.reg_23_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_1", xf = lambda sample : sample.reg_23_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_2", xf = lambda sample : sample.reg_23_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_3", xf = lambda sample : sample.reg_23_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_4", xf = lambda sample : sample.reg_23_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_5", xf = lambda sample : sample.reg_23_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_6", xf = lambda sample : sample.reg_23_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_7", xf = lambda sample : sample.reg_23_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_8", xf = lambda sample : sample.reg_23_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_9", xf = lambda sample : sample.reg_23_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_10", xf = lambda sample : sample.reg_23_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_11", xf = lambda sample : sample.reg_23_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_12", xf = lambda sample : sample.reg_23_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_13", xf = lambda sample : sample.reg_23_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_14", xf = lambda sample : sample.reg_23_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_15", xf = lambda sample : sample.reg_23_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_16", xf = lambda sample : sample.reg_23_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_17", xf = lambda sample : sample.reg_23_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_18", xf = lambda sample : sample.reg_23_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_19", xf = lambda sample : sample.reg_23_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_20", xf = lambda sample : sample.reg_23_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_21", xf = lambda sample : sample.reg_23_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_22", xf = lambda sample : sample.reg_23_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_23", xf = lambda sample : sample.reg_23_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_24", xf = lambda sample : sample.reg_23_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_25", xf = lambda sample : sample.reg_23_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_26", xf = lambda sample : sample.reg_23_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_27", xf = lambda sample : sample.reg_23_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_28", xf = lambda sample : sample.reg_23_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_29", xf = lambda sample : sample.reg_23_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_30", xf = lambda sample : sample.reg_23_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_31", xf = lambda sample : sample.reg_23_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_32", xf = lambda sample : sample.reg_23_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_33", xf = lambda sample : sample.reg_23_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_34", xf = lambda sample : sample.reg_23_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_35", xf = lambda sample : sample.reg_23_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_36", xf = lambda sample : sample.reg_23_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_37", xf = lambda sample : sample.reg_23_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_38", xf = lambda sample : sample.reg_23_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_39", xf = lambda sample : sample.reg_23_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_40", xf = lambda sample : sample.reg_23_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_41", xf = lambda sample : sample.reg_23_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_42", xf = lambda sample : sample.reg_23_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_43", xf = lambda sample : sample.reg_23_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_44", xf = lambda sample : sample.reg_23_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_45", xf = lambda sample : sample.reg_23_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_46", xf = lambda sample : sample.reg_23_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_47", xf = lambda sample : sample.reg_23_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_48", xf = lambda sample : sample.reg_23_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_49", xf = lambda sample : sample.reg_23_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_50", xf = lambda sample : sample.reg_23_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_51", xf = lambda sample : sample.reg_23_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_52", xf = lambda sample : sample.reg_23_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_53", xf = lambda sample : sample.reg_23_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_54", xf = lambda sample : sample.reg_23_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_55", xf = lambda sample : sample.reg_23_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_56", xf = lambda sample : sample.reg_23_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_57", xf = lambda sample : sample.reg_23_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_58", xf = lambda sample : sample.reg_23_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_59", xf = lambda sample : sample.reg_23_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_60", xf = lambda sample : sample.reg_23_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_61", xf = lambda sample : sample.reg_23_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_62", xf = lambda sample : sample.reg_23_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_23_bit_63", xf = lambda sample : sample.reg_23_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_0", xf = lambda sample : sample.reg_22_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_1", xf = lambda sample : sample.reg_22_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_2", xf = lambda sample : sample.reg_22_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_3", xf = lambda sample : sample.reg_22_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_4", xf = lambda sample : sample.reg_22_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_5", xf = lambda sample : sample.reg_22_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_6", xf = lambda sample : sample.reg_22_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_7", xf = lambda sample : sample.reg_22_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_8", xf = lambda sample : sample.reg_22_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_9", xf = lambda sample : sample.reg_22_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_10", xf = lambda sample : sample.reg_22_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_11", xf = lambda sample : sample.reg_22_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_12", xf = lambda sample : sample.reg_22_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_13", xf = lambda sample : sample.reg_22_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_14", xf = lambda sample : sample.reg_22_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_15", xf = lambda sample : sample.reg_22_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_16", xf = lambda sample : sample.reg_22_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_17", xf = lambda sample : sample.reg_22_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_18", xf = lambda sample : sample.reg_22_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_19", xf = lambda sample : sample.reg_22_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_20", xf = lambda sample : sample.reg_22_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_21", xf = lambda sample : sample.reg_22_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_22", xf = lambda sample : sample.reg_22_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_23", xf = lambda sample : sample.reg_22_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_24", xf = lambda sample : sample.reg_22_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_25", xf = lambda sample : sample.reg_22_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_26", xf = lambda sample : sample.reg_22_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_27", xf = lambda sample : sample.reg_22_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_28", xf = lambda sample : sample.reg_22_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_29", xf = lambda sample : sample.reg_22_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_30", xf = lambda sample : sample.reg_22_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_31", xf = lambda sample : sample.reg_22_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_32", xf = lambda sample : sample.reg_22_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_33", xf = lambda sample : sample.reg_22_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_34", xf = lambda sample : sample.reg_22_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_35", xf = lambda sample : sample.reg_22_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_36", xf = lambda sample : sample.reg_22_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_37", xf = lambda sample : sample.reg_22_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_38", xf = lambda sample : sample.reg_22_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_39", xf = lambda sample : sample.reg_22_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_40", xf = lambda sample : sample.reg_22_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_41", xf = lambda sample : sample.reg_22_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_42", xf = lambda sample : sample.reg_22_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_43", xf = lambda sample : sample.reg_22_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_44", xf = lambda sample : sample.reg_22_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_45", xf = lambda sample : sample.reg_22_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_46", xf = lambda sample : sample.reg_22_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_47", xf = lambda sample : sample.reg_22_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_48", xf = lambda sample : sample.reg_22_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_49", xf = lambda sample : sample.reg_22_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_50", xf = lambda sample : sample.reg_22_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_51", xf = lambda sample : sample.reg_22_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_52", xf = lambda sample : sample.reg_22_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_53", xf = lambda sample : sample.reg_22_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_54", xf = lambda sample : sample.reg_22_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_55", xf = lambda sample : sample.reg_22_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_56", xf = lambda sample : sample.reg_22_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_57", xf = lambda sample : sample.reg_22_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_58", xf = lambda sample : sample.reg_22_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_59", xf = lambda sample : sample.reg_22_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_60", xf = lambda sample : sample.reg_22_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_61", xf = lambda sample : sample.reg_22_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_62", xf = lambda sample : sample.reg_22_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_22_bit_63", xf = lambda sample : sample.reg_22_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_21_bit_0", xf = lambda sample : sample.reg_21_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_1", xf = lambda sample : sample.reg_21_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_2", xf = lambda sample : sample.reg_21_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_3", xf = lambda sample : sample.reg_21_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_4", xf = lambda sample : sample.reg_21_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_5", xf = lambda sample : sample.reg_21_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_6", xf = lambda sample : sample.reg_21_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_7", xf = lambda sample : sample.reg_21_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_8", xf = lambda sample : sample.reg_21_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_9", xf = lambda sample : sample.reg_21_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_10", xf = lambda sample : sample.reg_21_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_11", xf = lambda sample : sample.reg_21_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_12", xf = lambda sample : sample.reg_21_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_13", xf = lambda sample : sample.reg_21_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_14", xf = lambda sample : sample.reg_21_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_15", xf = lambda sample : sample.reg_21_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_16", xf = lambda sample : sample.reg_21_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_17", xf = lambda sample : sample.reg_21_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_18", xf = lambda sample : sample.reg_21_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_19", xf = lambda sample : sample.reg_21_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_20", xf = lambda sample : sample.reg_21_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_21", xf = lambda sample : sample.reg_21_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_22", xf = lambda sample : sample.reg_21_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_23", xf = lambda sample : sample.reg_21_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_24", xf = lambda sample : sample.reg_21_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_25", xf = lambda sample : sample.reg_21_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_26", xf = lambda sample : sample.reg_21_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_27", xf = lambda sample : sample.reg_21_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_28", xf = lambda sample : sample.reg_21_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_29", xf = lambda sample : sample.reg_21_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_30", xf = lambda sample : sample.reg_21_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_31", xf = lambda sample : sample.reg_21_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_32", xf = lambda sample : sample.reg_21_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_33", xf = lambda sample : sample.reg_21_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_34", xf = lambda sample : sample.reg_21_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_35", xf = lambda sample : sample.reg_21_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_36", xf = lambda sample : sample.reg_21_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_37", xf = lambda sample : sample.reg_21_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_38", xf = lambda sample : sample.reg_21_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_39", xf = lambda sample : sample.reg_21_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_40", xf = lambda sample : sample.reg_21_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_41", xf = lambda sample : sample.reg_21_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_42", xf = lambda sample : sample.reg_21_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_43", xf = lambda sample : sample.reg_21_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_44", xf = lambda sample : sample.reg_21_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_45", xf = lambda sample : sample.reg_21_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_46", xf = lambda sample : sample.reg_21_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_47", xf = lambda sample : sample.reg_21_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_48", xf = lambda sample : sample.reg_21_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_49", xf = lambda sample : sample.reg_21_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_50", xf = lambda sample : sample.reg_21_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_51", xf = lambda sample : sample.reg_21_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_52", xf = lambda sample : sample.reg_21_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_53", xf = lambda sample : sample.reg_21_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_54", xf = lambda sample : sample.reg_21_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_55", xf = lambda sample : sample.reg_21_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_56", xf = lambda sample : sample.reg_21_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_57", xf = lambda sample : sample.reg_21_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_58", xf = lambda sample : sample.reg_21_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_59", xf = lambda sample : sample.reg_21_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_60", xf = lambda sample : sample.reg_21_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_61", xf = lambda sample : sample.reg_21_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_62", xf = lambda sample : sample.reg_21_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_21_bit_63", xf = lambda sample : sample.reg_21_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_20_bit_0", xf = lambda sample : sample.reg_20_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_1", xf = lambda sample : sample.reg_20_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_2", xf = lambda sample : sample.reg_20_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_3", xf = lambda sample : sample.reg_20_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_4", xf = lambda sample : sample.reg_20_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_5", xf = lambda sample : sample.reg_20_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_6", xf = lambda sample : sample.reg_20_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_7", xf = lambda sample : sample.reg_20_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_8", xf = lambda sample : sample.reg_20_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_9", xf = lambda sample : sample.reg_20_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_10", xf = lambda sample : sample.reg_20_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_11", xf = lambda sample : sample.reg_20_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_12", xf = lambda sample : sample.reg_20_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_13", xf = lambda sample : sample.reg_20_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_14", xf = lambda sample : sample.reg_20_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_15", xf = lambda sample : sample.reg_20_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_16", xf = lambda sample : sample.reg_20_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_17", xf = lambda sample : sample.reg_20_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_18", xf = lambda sample : sample.reg_20_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_19", xf = lambda sample : sample.reg_20_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_20", xf = lambda sample : sample.reg_20_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_21", xf = lambda sample : sample.reg_20_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_22", xf = lambda sample : sample.reg_20_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_23", xf = lambda sample : sample.reg_20_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_24", xf = lambda sample : sample.reg_20_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_25", xf = lambda sample : sample.reg_20_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_26", xf = lambda sample : sample.reg_20_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_27", xf = lambda sample : sample.reg_20_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_28", xf = lambda sample : sample.reg_20_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_29", xf = lambda sample : sample.reg_20_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_30", xf = lambda sample : sample.reg_20_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_31", xf = lambda sample : sample.reg_20_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_32", xf = lambda sample : sample.reg_20_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_33", xf = lambda sample : sample.reg_20_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_34", xf = lambda sample : sample.reg_20_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_35", xf = lambda sample : sample.reg_20_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_36", xf = lambda sample : sample.reg_20_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_37", xf = lambda sample : sample.reg_20_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_38", xf = lambda sample : sample.reg_20_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_39", xf = lambda sample : sample.reg_20_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_40", xf = lambda sample : sample.reg_20_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_41", xf = lambda sample : sample.reg_20_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_42", xf = lambda sample : sample.reg_20_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_43", xf = lambda sample : sample.reg_20_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_44", xf = lambda sample : sample.reg_20_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_45", xf = lambda sample : sample.reg_20_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_46", xf = lambda sample : sample.reg_20_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_47", xf = lambda sample : sample.reg_20_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_48", xf = lambda sample : sample.reg_20_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_49", xf = lambda sample : sample.reg_20_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_50", xf = lambda sample : sample.reg_20_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_51", xf = lambda sample : sample.reg_20_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_52", xf = lambda sample : sample.reg_20_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_53", xf = lambda sample : sample.reg_20_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_54", xf = lambda sample : sample.reg_20_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_55", xf = lambda sample : sample.reg_20_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_56", xf = lambda sample : sample.reg_20_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_57", xf = lambda sample : sample.reg_20_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_58", xf = lambda sample : sample.reg_20_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_59", xf = lambda sample : sample.reg_20_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_60", xf = lambda sample : sample.reg_20_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_61", xf = lambda sample : sample.reg_20_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_20_bit_62", xf = lambda sample : sample.reg_20_bit_62 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_19_bit_0", xf = lambda sample : sample.reg_19_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_1", xf = lambda sample : sample.reg_19_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_2", xf = lambda sample : sample.reg_19_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_3", xf = lambda sample : sample.reg_19_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_4", xf = lambda sample : sample.reg_19_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_5", xf = lambda sample : sample.reg_19_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_6", xf = lambda sample : sample.reg_19_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_7", xf = lambda sample : sample.reg_19_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_8", xf = lambda sample : sample.reg_19_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_9", xf = lambda sample : sample.reg_19_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_10", xf = lambda sample : sample.reg_19_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_11", xf = lambda sample : sample.reg_19_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_12", xf = lambda sample : sample.reg_19_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_13", xf = lambda sample : sample.reg_19_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_14", xf = lambda sample : sample.reg_19_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_15", xf = lambda sample : sample.reg_19_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_16", xf = lambda sample : sample.reg_19_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_17", xf = lambda sample : sample.reg_19_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_18", xf = lambda sample : sample.reg_19_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_19", xf = lambda sample : sample.reg_19_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_20", xf = lambda sample : sample.reg_19_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_21", xf = lambda sample : sample.reg_19_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_22", xf = lambda sample : sample.reg_19_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_23", xf = lambda sample : sample.reg_19_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_24", xf = lambda sample : sample.reg_19_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_25", xf = lambda sample : sample.reg_19_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_26", xf = lambda sample : sample.reg_19_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_27", xf = lambda sample : sample.reg_19_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_28", xf = lambda sample : sample.reg_19_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_29", xf = lambda sample : sample.reg_19_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_30", xf = lambda sample : sample.reg_19_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_31", xf = lambda sample : sample.reg_19_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_32", xf = lambda sample : sample.reg_19_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_33", xf = lambda sample : sample.reg_19_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_34", xf = lambda sample : sample.reg_19_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_35", xf = lambda sample : sample.reg_19_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_36", xf = lambda sample : sample.reg_19_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_37", xf = lambda sample : sample.reg_19_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_38", xf = lambda sample : sample.reg_19_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_39", xf = lambda sample : sample.reg_19_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_40", xf = lambda sample : sample.reg_19_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_41", xf = lambda sample : sample.reg_19_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_42", xf = lambda sample : sample.reg_19_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_43", xf = lambda sample : sample.reg_19_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_44", xf = lambda sample : sample.reg_19_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_45", xf = lambda sample : sample.reg_19_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_46", xf = lambda sample : sample.reg_19_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_47", xf = lambda sample : sample.reg_19_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_48", xf = lambda sample : sample.reg_19_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_49", xf = lambda sample : sample.reg_19_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_50", xf = lambda sample : sample.reg_19_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_51", xf = lambda sample : sample.reg_19_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_52", xf = lambda sample : sample.reg_19_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_53", xf = lambda sample : sample.reg_19_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_54", xf = lambda sample : sample.reg_19_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_55", xf = lambda sample : sample.reg_19_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_56", xf = lambda sample : sample.reg_19_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_57", xf = lambda sample : sample.reg_19_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_58", xf = lambda sample : sample.reg_19_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_59", xf = lambda sample : sample.reg_19_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_60", xf = lambda sample : sample.reg_19_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_61", xf = lambda sample : sample.reg_19_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_62", xf = lambda sample : sample.reg_19_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_19_bit_63", xf = lambda sample : sample.reg_19_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_0", xf = lambda sample : sample.reg_18_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_1", xf = lambda sample : sample.reg_18_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_2", xf = lambda sample : sample.reg_18_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_3", xf = lambda sample : sample.reg_18_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_4", xf = lambda sample : sample.reg_18_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_5", xf = lambda sample : sample.reg_18_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_6", xf = lambda sample : sample.reg_18_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_7", xf = lambda sample : sample.reg_18_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_8", xf = lambda sample : sample.reg_18_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_9", xf = lambda sample : sample.reg_18_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_10", xf = lambda sample : sample.reg_18_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_11", xf = lambda sample : sample.reg_18_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_12", xf = lambda sample : sample.reg_18_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_13", xf = lambda sample : sample.reg_18_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_14", xf = lambda sample : sample.reg_18_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_15", xf = lambda sample : sample.reg_18_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_16", xf = lambda sample : sample.reg_18_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_17", xf = lambda sample : sample.reg_18_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_18", xf = lambda sample : sample.reg_18_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_19", xf = lambda sample : sample.reg_18_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_20", xf = lambda sample : sample.reg_18_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_21", xf = lambda sample : sample.reg_18_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_22", xf = lambda sample : sample.reg_18_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_23", xf = lambda sample : sample.reg_18_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_24", xf = lambda sample : sample.reg_18_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_25", xf = lambda sample : sample.reg_18_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_26", xf = lambda sample : sample.reg_18_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_27", xf = lambda sample : sample.reg_18_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_28", xf = lambda sample : sample.reg_18_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_29", xf = lambda sample : sample.reg_18_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_30", xf = lambda sample : sample.reg_18_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_31", xf = lambda sample : sample.reg_18_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_32", xf = lambda sample : sample.reg_18_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_33", xf = lambda sample : sample.reg_18_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_34", xf = lambda sample : sample.reg_18_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_35", xf = lambda sample : sample.reg_18_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_36", xf = lambda sample : sample.reg_18_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_37", xf = lambda sample : sample.reg_18_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_38", xf = lambda sample : sample.reg_18_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_39", xf = lambda sample : sample.reg_18_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_40", xf = lambda sample : sample.reg_18_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_41", xf = lambda sample : sample.reg_18_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_42", xf = lambda sample : sample.reg_18_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_43", xf = lambda sample : sample.reg_18_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_44", xf = lambda sample : sample.reg_18_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_45", xf = lambda sample : sample.reg_18_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_46", xf = lambda sample : sample.reg_18_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_47", xf = lambda sample : sample.reg_18_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_48", xf = lambda sample : sample.reg_18_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_49", xf = lambda sample : sample.reg_18_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_50", xf = lambda sample : sample.reg_18_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_51", xf = lambda sample : sample.reg_18_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_52", xf = lambda sample : sample.reg_18_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_53", xf = lambda sample : sample.reg_18_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_54", xf = lambda sample : sample.reg_18_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_55", xf = lambda sample : sample.reg_18_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_56", xf = lambda sample : sample.reg_18_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_57", xf = lambda sample : sample.reg_18_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_58", xf = lambda sample : sample.reg_18_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_59", xf = lambda sample : sample.reg_18_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_60", xf = lambda sample : sample.reg_18_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_61", xf = lambda sample : sample.reg_18_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_62", xf = lambda sample : sample.reg_18_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_18_bit_63", xf = lambda sample : sample.reg_18_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_17_bit_0", xf = lambda sample : sample.reg_17_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_1", xf = lambda sample : sample.reg_17_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_2", xf = lambda sample : sample.reg_17_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_3", xf = lambda sample : sample.reg_17_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_4", xf = lambda sample : sample.reg_17_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_5", xf = lambda sample : sample.reg_17_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_6", xf = lambda sample : sample.reg_17_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_7", xf = lambda sample : sample.reg_17_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_8", xf = lambda sample : sample.reg_17_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_9", xf = lambda sample : sample.reg_17_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_10", xf = lambda sample : sample.reg_17_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_11", xf = lambda sample : sample.reg_17_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_12", xf = lambda sample : sample.reg_17_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_13", xf = lambda sample : sample.reg_17_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_14", xf = lambda sample : sample.reg_17_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_15", xf = lambda sample : sample.reg_17_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_16", xf = lambda sample : sample.reg_17_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_17", xf = lambda sample : sample.reg_17_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_18", xf = lambda sample : sample.reg_17_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_19", xf = lambda sample : sample.reg_17_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_20", xf = lambda sample : sample.reg_17_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_21", xf = lambda sample : sample.reg_17_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_22", xf = lambda sample : sample.reg_17_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_23", xf = lambda sample : sample.reg_17_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_24", xf = lambda sample : sample.reg_17_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_25", xf = lambda sample : sample.reg_17_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_26", xf = lambda sample : sample.reg_17_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_27", xf = lambda sample : sample.reg_17_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_28", xf = lambda sample : sample.reg_17_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_29", xf = lambda sample : sample.reg_17_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_30", xf = lambda sample : sample.reg_17_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_31", xf = lambda sample : sample.reg_17_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_32", xf = lambda sample : sample.reg_17_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_33", xf = lambda sample : sample.reg_17_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_34", xf = lambda sample : sample.reg_17_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_35", xf = lambda sample : sample.reg_17_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_36", xf = lambda sample : sample.reg_17_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_37", xf = lambda sample : sample.reg_17_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_38", xf = lambda sample : sample.reg_17_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_39", xf = lambda sample : sample.reg_17_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_40", xf = lambda sample : sample.reg_17_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_41", xf = lambda sample : sample.reg_17_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_42", xf = lambda sample : sample.reg_17_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_43", xf = lambda sample : sample.reg_17_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_44", xf = lambda sample : sample.reg_17_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_45", xf = lambda sample : sample.reg_17_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_46", xf = lambda sample : sample.reg_17_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_47", xf = lambda sample : sample.reg_17_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_48", xf = lambda sample : sample.reg_17_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_49", xf = lambda sample : sample.reg_17_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_50", xf = lambda sample : sample.reg_17_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_51", xf = lambda sample : sample.reg_17_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_52", xf = lambda sample : sample.reg_17_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_53", xf = lambda sample : sample.reg_17_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_54", xf = lambda sample : sample.reg_17_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_55", xf = lambda sample : sample.reg_17_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_56", xf = lambda sample : sample.reg_17_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_57", xf = lambda sample : sample.reg_17_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_58", xf = lambda sample : sample.reg_17_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_59", xf = lambda sample : sample.reg_17_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_60", xf = lambda sample : sample.reg_17_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_61", xf = lambda sample : sample.reg_17_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_62", xf = lambda sample : sample.reg_17_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_17_bit_63", xf = lambda sample : sample.reg_17_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_16_bit_0", xf = lambda sample : sample.reg_16_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_1", xf = lambda sample : sample.reg_16_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_2", xf = lambda sample : sample.reg_16_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_3", xf = lambda sample : sample.reg_16_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_4", xf = lambda sample : sample.reg_16_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_5", xf = lambda sample : sample.reg_16_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_6", xf = lambda sample : sample.reg_16_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_7", xf = lambda sample : sample.reg_16_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_8", xf = lambda sample : sample.reg_16_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_9", xf = lambda sample : sample.reg_16_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_10", xf = lambda sample : sample.reg_16_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_11", xf = lambda sample : sample.reg_16_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_12", xf = lambda sample : sample.reg_16_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_13", xf = lambda sample : sample.reg_16_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_14", xf = lambda sample : sample.reg_16_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_15", xf = lambda sample : sample.reg_16_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_16", xf = lambda sample : sample.reg_16_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_17", xf = lambda sample : sample.reg_16_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_18", xf = lambda sample : sample.reg_16_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_19", xf = lambda sample : sample.reg_16_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_20", xf = lambda sample : sample.reg_16_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_21", xf = lambda sample : sample.reg_16_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_22", xf = lambda sample : sample.reg_16_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_23", xf = lambda sample : sample.reg_16_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_24", xf = lambda sample : sample.reg_16_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_25", xf = lambda sample : sample.reg_16_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_26", xf = lambda sample : sample.reg_16_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_27", xf = lambda sample : sample.reg_16_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_28", xf = lambda sample : sample.reg_16_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_29", xf = lambda sample : sample.reg_16_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_30", xf = lambda sample : sample.reg_16_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_31", xf = lambda sample : sample.reg_16_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_32", xf = lambda sample : sample.reg_16_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_33", xf = lambda sample : sample.reg_16_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_34", xf = lambda sample : sample.reg_16_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_35", xf = lambda sample : sample.reg_16_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_36", xf = lambda sample : sample.reg_16_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_37", xf = lambda sample : sample.reg_16_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_38", xf = lambda sample : sample.reg_16_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_39", xf = lambda sample : sample.reg_16_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_40", xf = lambda sample : sample.reg_16_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_41", xf = lambda sample : sample.reg_16_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_42", xf = lambda sample : sample.reg_16_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_43", xf = lambda sample : sample.reg_16_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_44", xf = lambda sample : sample.reg_16_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_45", xf = lambda sample : sample.reg_16_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_46", xf = lambda sample : sample.reg_16_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_47", xf = lambda sample : sample.reg_16_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_48", xf = lambda sample : sample.reg_16_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_49", xf = lambda sample : sample.reg_16_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_50", xf = lambda sample : sample.reg_16_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_51", xf = lambda sample : sample.reg_16_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_52", xf = lambda sample : sample.reg_16_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_53", xf = lambda sample : sample.reg_16_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_54", xf = lambda sample : sample.reg_16_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_55", xf = lambda sample : sample.reg_16_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_56", xf = lambda sample : sample.reg_16_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_57", xf = lambda sample : sample.reg_16_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_58", xf = lambda sample : sample.reg_16_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_59", xf = lambda sample : sample.reg_16_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_60", xf = lambda sample : sample.reg_16_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_61", xf = lambda sample : sample.reg_16_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_62", xf = lambda sample : sample.reg_16_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_16_bit_63", xf = lambda sample : sample.reg_16_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_15_bit_0", xf = lambda sample : sample.reg_15_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_1", xf = lambda sample : sample.reg_15_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_2", xf = lambda sample : sample.reg_15_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_3", xf = lambda sample : sample.reg_15_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_4", xf = lambda sample : sample.reg_15_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_5", xf = lambda sample : sample.reg_15_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_6", xf = lambda sample : sample.reg_15_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_7", xf = lambda sample : sample.reg_15_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_8", xf = lambda sample : sample.reg_15_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_9", xf = lambda sample : sample.reg_15_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_10", xf = lambda sample : sample.reg_15_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_11", xf = lambda sample : sample.reg_15_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_12", xf = lambda sample : sample.reg_15_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_13", xf = lambda sample : sample.reg_15_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_14", xf = lambda sample : sample.reg_15_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_15", xf = lambda sample : sample.reg_15_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_16", xf = lambda sample : sample.reg_15_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_17", xf = lambda sample : sample.reg_15_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_18", xf = lambda sample : sample.reg_15_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_19", xf = lambda sample : sample.reg_15_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_20", xf = lambda sample : sample.reg_15_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_21", xf = lambda sample : sample.reg_15_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_22", xf = lambda sample : sample.reg_15_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_23", xf = lambda sample : sample.reg_15_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_24", xf = lambda sample : sample.reg_15_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_25", xf = lambda sample : sample.reg_15_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_26", xf = lambda sample : sample.reg_15_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_27", xf = lambda sample : sample.reg_15_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_28", xf = lambda sample : sample.reg_15_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_29", xf = lambda sample : sample.reg_15_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_30", xf = lambda sample : sample.reg_15_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_31", xf = lambda sample : sample.reg_15_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_32", xf = lambda sample : sample.reg_15_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_33", xf = lambda sample : sample.reg_15_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_34", xf = lambda sample : sample.reg_15_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_35", xf = lambda sample : sample.reg_15_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_36", xf = lambda sample : sample.reg_15_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_37", xf = lambda sample : sample.reg_15_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_38", xf = lambda sample : sample.reg_15_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_39", xf = lambda sample : sample.reg_15_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_40", xf = lambda sample : sample.reg_15_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_41", xf = lambda sample : sample.reg_15_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_42", xf = lambda sample : sample.reg_15_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_43", xf = lambda sample : sample.reg_15_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_44", xf = lambda sample : sample.reg_15_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_45", xf = lambda sample : sample.reg_15_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_46", xf = lambda sample : sample.reg_15_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_47", xf = lambda sample : sample.reg_15_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_48", xf = lambda sample : sample.reg_15_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_49", xf = lambda sample : sample.reg_15_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_50", xf = lambda sample : sample.reg_15_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_51", xf = lambda sample : sample.reg_15_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_52", xf = lambda sample : sample.reg_15_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_53", xf = lambda sample : sample.reg_15_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_54", xf = lambda sample : sample.reg_15_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_55", xf = lambda sample : sample.reg_15_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_56", xf = lambda sample : sample.reg_15_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_57", xf = lambda sample : sample.reg_15_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_58", xf = lambda sample : sample.reg_15_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_59", xf = lambda sample : sample.reg_15_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_60", xf = lambda sample : sample.reg_15_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_61", xf = lambda sample : sample.reg_15_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_62", xf = lambda sample : sample.reg_15_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_15_bit_63", xf = lambda sample : sample.reg_15_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_0", xf = lambda sample : sample.reg_14_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_1", xf = lambda sample : sample.reg_14_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_2", xf = lambda sample : sample.reg_14_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_3", xf = lambda sample : sample.reg_14_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_4", xf = lambda sample : sample.reg_14_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_5", xf = lambda sample : sample.reg_14_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_6", xf = lambda sample : sample.reg_14_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_7", xf = lambda sample : sample.reg_14_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_8", xf = lambda sample : sample.reg_14_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_9", xf = lambda sample : sample.reg_14_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_10", xf = lambda sample : sample.reg_14_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_11", xf = lambda sample : sample.reg_14_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_12", xf = lambda sample : sample.reg_14_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_13", xf = lambda sample : sample.reg_14_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_14", xf = lambda sample : sample.reg_14_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_15", xf = lambda sample : sample.reg_14_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_16", xf = lambda sample : sample.reg_14_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_17", xf = lambda sample : sample.reg_14_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_18", xf = lambda sample : sample.reg_14_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_19", xf = lambda sample : sample.reg_14_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_20", xf = lambda sample : sample.reg_14_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_21", xf = lambda sample : sample.reg_14_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_22", xf = lambda sample : sample.reg_14_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_23", xf = lambda sample : sample.reg_14_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_24", xf = lambda sample : sample.reg_14_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_25", xf = lambda sample : sample.reg_14_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_26", xf = lambda sample : sample.reg_14_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_27", xf = lambda sample : sample.reg_14_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_28", xf = lambda sample : sample.reg_14_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_29", xf = lambda sample : sample.reg_14_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_30", xf = lambda sample : sample.reg_14_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_31", xf = lambda sample : sample.reg_14_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_32", xf = lambda sample : sample.reg_14_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_33", xf = lambda sample : sample.reg_14_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_34", xf = lambda sample : sample.reg_14_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_35", xf = lambda sample : sample.reg_14_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_36", xf = lambda sample : sample.reg_14_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_37", xf = lambda sample : sample.reg_14_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_38", xf = lambda sample : sample.reg_14_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_39", xf = lambda sample : sample.reg_14_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_40", xf = lambda sample : sample.reg_14_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_41", xf = lambda sample : sample.reg_14_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_42", xf = lambda sample : sample.reg_14_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_43", xf = lambda sample : sample.reg_14_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_44", xf = lambda sample : sample.reg_14_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_45", xf = lambda sample : sample.reg_14_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_46", xf = lambda sample : sample.reg_14_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_47", xf = lambda sample : sample.reg_14_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_48", xf = lambda sample : sample.reg_14_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_49", xf = lambda sample : sample.reg_14_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_50", xf = lambda sample : sample.reg_14_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_51", xf = lambda sample : sample.reg_14_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_52", xf = lambda sample : sample.reg_14_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_53", xf = lambda sample : sample.reg_14_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_54", xf = lambda sample : sample.reg_14_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_55", xf = lambda sample : sample.reg_14_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_56", xf = lambda sample : sample.reg_14_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_57", xf = lambda sample : sample.reg_14_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_58", xf = lambda sample : sample.reg_14_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_59", xf = lambda sample : sample.reg_14_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_60", xf = lambda sample : sample.reg_14_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_61", xf = lambda sample : sample.reg_14_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_62", xf = lambda sample : sample.reg_14_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_14_bit_63", xf = lambda sample : sample.reg_14_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_13_bit_0", xf = lambda sample : sample.reg_13_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_1", xf = lambda sample : sample.reg_13_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_2", xf = lambda sample : sample.reg_13_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_3", xf = lambda sample : sample.reg_13_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_4", xf = lambda sample : sample.reg_13_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_5", xf = lambda sample : sample.reg_13_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_6", xf = lambda sample : sample.reg_13_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_7", xf = lambda sample : sample.reg_13_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_8", xf = lambda sample : sample.reg_13_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_9", xf = lambda sample : sample.reg_13_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_10", xf = lambda sample : sample.reg_13_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_11", xf = lambda sample : sample.reg_13_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_12", xf = lambda sample : sample.reg_13_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_13", xf = lambda sample : sample.reg_13_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_14", xf = lambda sample : sample.reg_13_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_15", xf = lambda sample : sample.reg_13_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_16", xf = lambda sample : sample.reg_13_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_17", xf = lambda sample : sample.reg_13_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_18", xf = lambda sample : sample.reg_13_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_19", xf = lambda sample : sample.reg_13_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_20", xf = lambda sample : sample.reg_13_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_21", xf = lambda sample : sample.reg_13_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_22", xf = lambda sample : sample.reg_13_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_23", xf = lambda sample : sample.reg_13_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_24", xf = lambda sample : sample.reg_13_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_25", xf = lambda sample : sample.reg_13_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_26", xf = lambda sample : sample.reg_13_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_27", xf = lambda sample : sample.reg_13_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_28", xf = lambda sample : sample.reg_13_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_29", xf = lambda sample : sample.reg_13_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_30", xf = lambda sample : sample.reg_13_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_31", xf = lambda sample : sample.reg_13_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_32", xf = lambda sample : sample.reg_13_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_33", xf = lambda sample : sample.reg_13_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_34", xf = lambda sample : sample.reg_13_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_35", xf = lambda sample : sample.reg_13_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_36", xf = lambda sample : sample.reg_13_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_37", xf = lambda sample : sample.reg_13_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_38", xf = lambda sample : sample.reg_13_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_39", xf = lambda sample : sample.reg_13_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_40", xf = lambda sample : sample.reg_13_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_41", xf = lambda sample : sample.reg_13_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_42", xf = lambda sample : sample.reg_13_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_43", xf = lambda sample : sample.reg_13_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_44", xf = lambda sample : sample.reg_13_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_45", xf = lambda sample : sample.reg_13_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_46", xf = lambda sample : sample.reg_13_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_47", xf = lambda sample : sample.reg_13_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_48", xf = lambda sample : sample.reg_13_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_49", xf = lambda sample : sample.reg_13_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_50", xf = lambda sample : sample.reg_13_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_51", xf = lambda sample : sample.reg_13_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_52", xf = lambda sample : sample.reg_13_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_53", xf = lambda sample : sample.reg_13_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_54", xf = lambda sample : sample.reg_13_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_55", xf = lambda sample : sample.reg_13_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_56", xf = lambda sample : sample.reg_13_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_57", xf = lambda sample : sample.reg_13_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_58", xf = lambda sample : sample.reg_13_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_59", xf = lambda sample : sample.reg_13_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_60", xf = lambda sample : sample.reg_13_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_61", xf = lambda sample : sample.reg_13_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_62", xf = lambda sample : sample.reg_13_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_13_bit_63", xf = lambda sample : sample.reg_13_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_12_bit_0", xf = lambda sample : sample.reg_12_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_1", xf = lambda sample : sample.reg_12_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_2", xf = lambda sample : sample.reg_12_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_3", xf = lambda sample : sample.reg_12_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_4", xf = lambda sample : sample.reg_12_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_5", xf = lambda sample : sample.reg_12_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_6", xf = lambda sample : sample.reg_12_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_7", xf = lambda sample : sample.reg_12_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_8", xf = lambda sample : sample.reg_12_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_9", xf = lambda sample : sample.reg_12_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_10", xf = lambda sample : sample.reg_12_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_11", xf = lambda sample : sample.reg_12_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_12", xf = lambda sample : sample.reg_12_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_13", xf = lambda sample : sample.reg_12_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_14", xf = lambda sample : sample.reg_12_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_15", xf = lambda sample : sample.reg_12_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_16", xf = lambda sample : sample.reg_12_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_17", xf = lambda sample : sample.reg_12_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_18", xf = lambda sample : sample.reg_12_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_19", xf = lambda sample : sample.reg_12_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_20", xf = lambda sample : sample.reg_12_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_21", xf = lambda sample : sample.reg_12_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_22", xf = lambda sample : sample.reg_12_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_23", xf = lambda sample : sample.reg_12_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_24", xf = lambda sample : sample.reg_12_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_25", xf = lambda sample : sample.reg_12_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_26", xf = lambda sample : sample.reg_12_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_27", xf = lambda sample : sample.reg_12_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_28", xf = lambda sample : sample.reg_12_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_29", xf = lambda sample : sample.reg_12_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_30", xf = lambda sample : sample.reg_12_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_31", xf = lambda sample : sample.reg_12_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_32", xf = lambda sample : sample.reg_12_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_33", xf = lambda sample : sample.reg_12_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_34", xf = lambda sample : sample.reg_12_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_35", xf = lambda sample : sample.reg_12_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_36", xf = lambda sample : sample.reg_12_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_37", xf = lambda sample : sample.reg_12_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_38", xf = lambda sample : sample.reg_12_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_39", xf = lambda sample : sample.reg_12_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_40", xf = lambda sample : sample.reg_12_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_41", xf = lambda sample : sample.reg_12_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_42", xf = lambda sample : sample.reg_12_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_43", xf = lambda sample : sample.reg_12_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_44", xf = lambda sample : sample.reg_12_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_45", xf = lambda sample : sample.reg_12_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_46", xf = lambda sample : sample.reg_12_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_47", xf = lambda sample : sample.reg_12_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_48", xf = lambda sample : sample.reg_12_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_49", xf = lambda sample : sample.reg_12_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_50", xf = lambda sample : sample.reg_12_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_51", xf = lambda sample : sample.reg_12_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_52", xf = lambda sample : sample.reg_12_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_53", xf = lambda sample : sample.reg_12_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_54", xf = lambda sample : sample.reg_12_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_55", xf = lambda sample : sample.reg_12_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_56", xf = lambda sample : sample.reg_12_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_57", xf = lambda sample : sample.reg_12_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_58", xf = lambda sample : sample.reg_12_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_59", xf = lambda sample : sample.reg_12_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_60", xf = lambda sample : sample.reg_12_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_61", xf = lambda sample : sample.reg_12_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_62", xf = lambda sample : sample.reg_12_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_12_bit_63", xf = lambda sample : sample.reg_12_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_11_bit_0", xf = lambda sample : sample.reg_11_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_1", xf = lambda sample : sample.reg_11_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_2", xf = lambda sample : sample.reg_11_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_3", xf = lambda sample : sample.reg_11_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_4", xf = lambda sample : sample.reg_11_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_5", xf = lambda sample : sample.reg_11_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_6", xf = lambda sample : sample.reg_11_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_7", xf = lambda sample : sample.reg_11_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_8", xf = lambda sample : sample.reg_11_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_9", xf = lambda sample : sample.reg_11_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_10", xf = lambda sample : sample.reg_11_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_11", xf = lambda sample : sample.reg_11_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_12", xf = lambda sample : sample.reg_11_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_13", xf = lambda sample : sample.reg_11_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_14", xf = lambda sample : sample.reg_11_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_15", xf = lambda sample : sample.reg_11_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_16", xf = lambda sample : sample.reg_11_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_17", xf = lambda sample : sample.reg_11_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_18", xf = lambda sample : sample.reg_11_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_19", xf = lambda sample : sample.reg_11_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_20", xf = lambda sample : sample.reg_11_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_21", xf = lambda sample : sample.reg_11_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_22", xf = lambda sample : sample.reg_11_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_23", xf = lambda sample : sample.reg_11_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_24", xf = lambda sample : sample.reg_11_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_25", xf = lambda sample : sample.reg_11_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_26", xf = lambda sample : sample.reg_11_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_27", xf = lambda sample : sample.reg_11_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_28", xf = lambda sample : sample.reg_11_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_29", xf = lambda sample : sample.reg_11_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_30", xf = lambda sample : sample.reg_11_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_31", xf = lambda sample : sample.reg_11_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_32", xf = lambda sample : sample.reg_11_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_33", xf = lambda sample : sample.reg_11_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_34", xf = lambda sample : sample.reg_11_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_35", xf = lambda sample : sample.reg_11_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_36", xf = lambda sample : sample.reg_11_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_37", xf = lambda sample : sample.reg_11_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_38", xf = lambda sample : sample.reg_11_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_39", xf = lambda sample : sample.reg_11_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_40", xf = lambda sample : sample.reg_11_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_41", xf = lambda sample : sample.reg_11_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_42", xf = lambda sample : sample.reg_11_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_43", xf = lambda sample : sample.reg_11_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_44", xf = lambda sample : sample.reg_11_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_45", xf = lambda sample : sample.reg_11_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_46", xf = lambda sample : sample.reg_11_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_47", xf = lambda sample : sample.reg_11_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_48", xf = lambda sample : sample.reg_11_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_49", xf = lambda sample : sample.reg_11_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_50", xf = lambda sample : sample.reg_11_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_51", xf = lambda sample : sample.reg_11_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_52", xf = lambda sample : sample.reg_11_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_53", xf = lambda sample : sample.reg_11_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_54", xf = lambda sample : sample.reg_11_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_55", xf = lambda sample : sample.reg_11_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_56", xf = lambda sample : sample.reg_11_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_57", xf = lambda sample : sample.reg_11_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_58", xf = lambda sample : sample.reg_11_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_59", xf = lambda sample : sample.reg_11_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_60", xf = lambda sample : sample.reg_11_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_61", xf = lambda sample : sample.reg_11_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_62", xf = lambda sample : sample.reg_11_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_11_bit_63", xf = lambda sample : sample.reg_11_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_10_bit_0", xf = lambda sample : sample.reg_10_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_1", xf = lambda sample : sample.reg_10_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_2", xf = lambda sample : sample.reg_10_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_3", xf = lambda sample : sample.reg_10_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_4", xf = lambda sample : sample.reg_10_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_5", xf = lambda sample : sample.reg_10_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_6", xf = lambda sample : sample.reg_10_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_7", xf = lambda sample : sample.reg_10_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_8", xf = lambda sample : sample.reg_10_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_9", xf = lambda sample : sample.reg_10_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_10", xf = lambda sample : sample.reg_10_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_11", xf = lambda sample : sample.reg_10_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_12", xf = lambda sample : sample.reg_10_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_13", xf = lambda sample : sample.reg_10_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_14", xf = lambda sample : sample.reg_10_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_15", xf = lambda sample : sample.reg_10_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_16", xf = lambda sample : sample.reg_10_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_17", xf = lambda sample : sample.reg_10_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_18", xf = lambda sample : sample.reg_10_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_19", xf = lambda sample : sample.reg_10_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_20", xf = lambda sample : sample.reg_10_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_21", xf = lambda sample : sample.reg_10_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_22", xf = lambda sample : sample.reg_10_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_23", xf = lambda sample : sample.reg_10_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_24", xf = lambda sample : sample.reg_10_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_25", xf = lambda sample : sample.reg_10_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_26", xf = lambda sample : sample.reg_10_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_27", xf = lambda sample : sample.reg_10_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_28", xf = lambda sample : sample.reg_10_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_29", xf = lambda sample : sample.reg_10_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_30", xf = lambda sample : sample.reg_10_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_31", xf = lambda sample : sample.reg_10_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_32", xf = lambda sample : sample.reg_10_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_33", xf = lambda sample : sample.reg_10_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_34", xf = lambda sample : sample.reg_10_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_35", xf = lambda sample : sample.reg_10_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_36", xf = lambda sample : sample.reg_10_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_37", xf = lambda sample : sample.reg_10_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_38", xf = lambda sample : sample.reg_10_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_39", xf = lambda sample : sample.reg_10_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_40", xf = lambda sample : sample.reg_10_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_41", xf = lambda sample : sample.reg_10_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_42", xf = lambda sample : sample.reg_10_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_43", xf = lambda sample : sample.reg_10_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_44", xf = lambda sample : sample.reg_10_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_45", xf = lambda sample : sample.reg_10_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_46", xf = lambda sample : sample.reg_10_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_47", xf = lambda sample : sample.reg_10_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_48", xf = lambda sample : sample.reg_10_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_49", xf = lambda sample : sample.reg_10_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_50", xf = lambda sample : sample.reg_10_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_51", xf = lambda sample : sample.reg_10_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_52", xf = lambda sample : sample.reg_10_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_53", xf = lambda sample : sample.reg_10_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_54", xf = lambda sample : sample.reg_10_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_55", xf = lambda sample : sample.reg_10_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_56", xf = lambda sample : sample.reg_10_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_57", xf = lambda sample : sample.reg_10_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_58", xf = lambda sample : sample.reg_10_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_59", xf = lambda sample : sample.reg_10_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_60", xf = lambda sample : sample.reg_10_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_61", xf = lambda sample : sample.reg_10_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_62", xf = lambda sample : sample.reg_10_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_10_bit_63", xf = lambda sample : sample.reg_10_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_9_bit_0", xf = lambda sample : sample.reg_9_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_1", xf = lambda sample : sample.reg_9_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_2", xf = lambda sample : sample.reg_9_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_3", xf = lambda sample : sample.reg_9_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_4", xf = lambda sample : sample.reg_9_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_5", xf = lambda sample : sample.reg_9_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_6", xf = lambda sample : sample.reg_9_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_7", xf = lambda sample : sample.reg_9_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_8", xf = lambda sample : sample.reg_9_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_9", xf = lambda sample : sample.reg_9_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_10", xf = lambda sample : sample.reg_9_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_11", xf = lambda sample : sample.reg_9_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_12", xf = lambda sample : sample.reg_9_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_13", xf = lambda sample : sample.reg_9_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_14", xf = lambda sample : sample.reg_9_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_15", xf = lambda sample : sample.reg_9_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_16", xf = lambda sample : sample.reg_9_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_17", xf = lambda sample : sample.reg_9_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_18", xf = lambda sample : sample.reg_9_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_19", xf = lambda sample : sample.reg_9_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_20", xf = lambda sample : sample.reg_9_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_21", xf = lambda sample : sample.reg_9_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_22", xf = lambda sample : sample.reg_9_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_23", xf = lambda sample : sample.reg_9_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_24", xf = lambda sample : sample.reg_9_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_25", xf = lambda sample : sample.reg_9_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_26", xf = lambda sample : sample.reg_9_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_27", xf = lambda sample : sample.reg_9_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_28", xf = lambda sample : sample.reg_9_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_29", xf = lambda sample : sample.reg_9_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_30", xf = lambda sample : sample.reg_9_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_31", xf = lambda sample : sample.reg_9_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_32", xf = lambda sample : sample.reg_9_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_33", xf = lambda sample : sample.reg_9_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_34", xf = lambda sample : sample.reg_9_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_35", xf = lambda sample : sample.reg_9_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_36", xf = lambda sample : sample.reg_9_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_37", xf = lambda sample : sample.reg_9_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_38", xf = lambda sample : sample.reg_9_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_39", xf = lambda sample : sample.reg_9_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_40", xf = lambda sample : sample.reg_9_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_41", xf = lambda sample : sample.reg_9_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_42", xf = lambda sample : sample.reg_9_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_43", xf = lambda sample : sample.reg_9_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_44", xf = lambda sample : sample.reg_9_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_45", xf = lambda sample : sample.reg_9_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_46", xf = lambda sample : sample.reg_9_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_47", xf = lambda sample : sample.reg_9_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_48", xf = lambda sample : sample.reg_9_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_49", xf = lambda sample : sample.reg_9_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_50", xf = lambda sample : sample.reg_9_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_51", xf = lambda sample : sample.reg_9_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_52", xf = lambda sample : sample.reg_9_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_53", xf = lambda sample : sample.reg_9_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_54", xf = lambda sample : sample.reg_9_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_55", xf = lambda sample : sample.reg_9_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_56", xf = lambda sample : sample.reg_9_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_57", xf = lambda sample : sample.reg_9_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_58", xf = lambda sample : sample.reg_9_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_59", xf = lambda sample : sample.reg_9_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_60", xf = lambda sample : sample.reg_9_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_61", xf = lambda sample : sample.reg_9_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_62", xf = lambda sample : sample.reg_9_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_9_bit_63", xf = lambda sample : sample.reg_9_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_0", xf = lambda sample : sample.reg_8_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_1", xf = lambda sample : sample.reg_8_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_2", xf = lambda sample : sample.reg_8_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_3", xf = lambda sample : sample.reg_8_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_4", xf = lambda sample : sample.reg_8_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_5", xf = lambda sample : sample.reg_8_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_6", xf = lambda sample : sample.reg_8_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_7", xf = lambda sample : sample.reg_8_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_8", xf = lambda sample : sample.reg_8_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_9", xf = lambda sample : sample.reg_8_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_10", xf = lambda sample : sample.reg_8_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_11", xf = lambda sample : sample.reg_8_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_12", xf = lambda sample : sample.reg_8_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_13", xf = lambda sample : sample.reg_8_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_14", xf = lambda sample : sample.reg_8_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_15", xf = lambda sample : sample.reg_8_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_16", xf = lambda sample : sample.reg_8_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_17", xf = lambda sample : sample.reg_8_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_18", xf = lambda sample : sample.reg_8_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_19", xf = lambda sample : sample.reg_8_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_20", xf = lambda sample : sample.reg_8_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_21", xf = lambda sample : sample.reg_8_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_22", xf = lambda sample : sample.reg_8_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_23", xf = lambda sample : sample.reg_8_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_24", xf = lambda sample : sample.reg_8_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_25", xf = lambda sample : sample.reg_8_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_26", xf = lambda sample : sample.reg_8_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_27", xf = lambda sample : sample.reg_8_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_28", xf = lambda sample : sample.reg_8_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_29", xf = lambda sample : sample.reg_8_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_30", xf = lambda sample : sample.reg_8_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_31", xf = lambda sample : sample.reg_8_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_32", xf = lambda sample : sample.reg_8_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_33", xf = lambda sample : sample.reg_8_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_34", xf = lambda sample : sample.reg_8_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_35", xf = lambda sample : sample.reg_8_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_36", xf = lambda sample : sample.reg_8_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_37", xf = lambda sample : sample.reg_8_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_38", xf = lambda sample : sample.reg_8_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_39", xf = lambda sample : sample.reg_8_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_40", xf = lambda sample : sample.reg_8_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_41", xf = lambda sample : sample.reg_8_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_42", xf = lambda sample : sample.reg_8_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_43", xf = lambda sample : sample.reg_8_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_44", xf = lambda sample : sample.reg_8_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_45", xf = lambda sample : sample.reg_8_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_46", xf = lambda sample : sample.reg_8_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_47", xf = lambda sample : sample.reg_8_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_48", xf = lambda sample : sample.reg_8_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_49", xf = lambda sample : sample.reg_8_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_50", xf = lambda sample : sample.reg_8_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_51", xf = lambda sample : sample.reg_8_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_52", xf = lambda sample : sample.reg_8_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_53", xf = lambda sample : sample.reg_8_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_54", xf = lambda sample : sample.reg_8_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_55", xf = lambda sample : sample.reg_8_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_56", xf = lambda sample : sample.reg_8_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_57", xf = lambda sample : sample.reg_8_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_58", xf = lambda sample : sample.reg_8_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_59", xf = lambda sample : sample.reg_8_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_60", xf = lambda sample : sample.reg_8_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_61", xf = lambda sample : sample.reg_8_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_62", xf = lambda sample : sample.reg_8_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_8_bit_63", xf = lambda sample : sample.reg_8_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_7_bit_0", xf = lambda sample : sample.reg_7_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_1", xf = lambda sample : sample.reg_7_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_2", xf = lambda sample : sample.reg_7_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_3", xf = lambda sample : sample.reg_7_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_4", xf = lambda sample : sample.reg_7_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_5", xf = lambda sample : sample.reg_7_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_6", xf = lambda sample : sample.reg_7_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_7", xf = lambda sample : sample.reg_7_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_8", xf = lambda sample : sample.reg_7_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_9", xf = lambda sample : sample.reg_7_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_10", xf = lambda sample : sample.reg_7_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_11", xf = lambda sample : sample.reg_7_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_12", xf = lambda sample : sample.reg_7_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_13", xf = lambda sample : sample.reg_7_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_14", xf = lambda sample : sample.reg_7_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_15", xf = lambda sample : sample.reg_7_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_16", xf = lambda sample : sample.reg_7_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_17", xf = lambda sample : sample.reg_7_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_18", xf = lambda sample : sample.reg_7_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_19", xf = lambda sample : sample.reg_7_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_20", xf = lambda sample : sample.reg_7_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_21", xf = lambda sample : sample.reg_7_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_22", xf = lambda sample : sample.reg_7_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_23", xf = lambda sample : sample.reg_7_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_24", xf = lambda sample : sample.reg_7_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_25", xf = lambda sample : sample.reg_7_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_26", xf = lambda sample : sample.reg_7_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_27", xf = lambda sample : sample.reg_7_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_28", xf = lambda sample : sample.reg_7_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_29", xf = lambda sample : sample.reg_7_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_30", xf = lambda sample : sample.reg_7_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_31", xf = lambda sample : sample.reg_7_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_32", xf = lambda sample : sample.reg_7_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_33", xf = lambda sample : sample.reg_7_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_34", xf = lambda sample : sample.reg_7_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_35", xf = lambda sample : sample.reg_7_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_36", xf = lambda sample : sample.reg_7_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_37", xf = lambda sample : sample.reg_7_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_38", xf = lambda sample : sample.reg_7_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_39", xf = lambda sample : sample.reg_7_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_40", xf = lambda sample : sample.reg_7_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_41", xf = lambda sample : sample.reg_7_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_42", xf = lambda sample : sample.reg_7_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_43", xf = lambda sample : sample.reg_7_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_44", xf = lambda sample : sample.reg_7_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_45", xf = lambda sample : sample.reg_7_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_46", xf = lambda sample : sample.reg_7_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_47", xf = lambda sample : sample.reg_7_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_48", xf = lambda sample : sample.reg_7_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_49", xf = lambda sample : sample.reg_7_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_50", xf = lambda sample : sample.reg_7_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_51", xf = lambda sample : sample.reg_7_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_52", xf = lambda sample : sample.reg_7_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_53", xf = lambda sample : sample.reg_7_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_54", xf = lambda sample : sample.reg_7_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_55", xf = lambda sample : sample.reg_7_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_56", xf = lambda sample : sample.reg_7_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_57", xf = lambda sample : sample.reg_7_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_58", xf = lambda sample : sample.reg_7_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_59", xf = lambda sample : sample.reg_7_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_60", xf = lambda sample : sample.reg_7_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_61", xf = lambda sample : sample.reg_7_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_62", xf = lambda sample : sample.reg_7_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_7_bit_63", xf = lambda sample : sample.reg_7_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_6_bit_0", xf = lambda sample : sample.reg_6_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_1", xf = lambda sample : sample.reg_6_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_2", xf = lambda sample : sample.reg_6_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_3", xf = lambda sample : sample.reg_6_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_4", xf = lambda sample : sample.reg_6_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_5", xf = lambda sample : sample.reg_6_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_6", xf = lambda sample : sample.reg_6_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_7", xf = lambda sample : sample.reg_6_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_8", xf = lambda sample : sample.reg_6_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_9", xf = lambda sample : sample.reg_6_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_10", xf = lambda sample : sample.reg_6_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_11", xf = lambda sample : sample.reg_6_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_12", xf = lambda sample : sample.reg_6_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_13", xf = lambda sample : sample.reg_6_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_14", xf = lambda sample : sample.reg_6_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_15", xf = lambda sample : sample.reg_6_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_16", xf = lambda sample : sample.reg_6_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_17", xf = lambda sample : sample.reg_6_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_18", xf = lambda sample : sample.reg_6_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_19", xf = lambda sample : sample.reg_6_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_20", xf = lambda sample : sample.reg_6_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_21", xf = lambda sample : sample.reg_6_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_22", xf = lambda sample : sample.reg_6_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_23", xf = lambda sample : sample.reg_6_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_24", xf = lambda sample : sample.reg_6_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_25", xf = lambda sample : sample.reg_6_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_26", xf = lambda sample : sample.reg_6_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_27", xf = lambda sample : sample.reg_6_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_28", xf = lambda sample : sample.reg_6_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_29", xf = lambda sample : sample.reg_6_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_30", xf = lambda sample : sample.reg_6_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_31", xf = lambda sample : sample.reg_6_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_32", xf = lambda sample : sample.reg_6_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_33", xf = lambda sample : sample.reg_6_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_34", xf = lambda sample : sample.reg_6_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_35", xf = lambda sample : sample.reg_6_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_36", xf = lambda sample : sample.reg_6_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_37", xf = lambda sample : sample.reg_6_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_38", xf = lambda sample : sample.reg_6_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_39", xf = lambda sample : sample.reg_6_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_40", xf = lambda sample : sample.reg_6_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_41", xf = lambda sample : sample.reg_6_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_42", xf = lambda sample : sample.reg_6_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_43", xf = lambda sample : sample.reg_6_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_44", xf = lambda sample : sample.reg_6_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_45", xf = lambda sample : sample.reg_6_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_46", xf = lambda sample : sample.reg_6_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_47", xf = lambda sample : sample.reg_6_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_48", xf = lambda sample : sample.reg_6_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_49", xf = lambda sample : sample.reg_6_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_50", xf = lambda sample : sample.reg_6_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_51", xf = lambda sample : sample.reg_6_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_52", xf = lambda sample : sample.reg_6_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_53", xf = lambda sample : sample.reg_6_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_54", xf = lambda sample : sample.reg_6_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_55", xf = lambda sample : sample.reg_6_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_56", xf = lambda sample : sample.reg_6_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_57", xf = lambda sample : sample.reg_6_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_58", xf = lambda sample : sample.reg_6_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_59", xf = lambda sample : sample.reg_6_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_60", xf = lambda sample : sample.reg_6_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_61", xf = lambda sample : sample.reg_6_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_62", xf = lambda sample : sample.reg_6_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_6_bit_63", xf = lambda sample : sample.reg_6_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_5_bit_0", xf = lambda sample : sample.reg_5_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_1", xf = lambda sample : sample.reg_5_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_2", xf = lambda sample : sample.reg_5_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_3", xf = lambda sample : sample.reg_5_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_4", xf = lambda sample : sample.reg_5_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_5", xf = lambda sample : sample.reg_5_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_6", xf = lambda sample : sample.reg_5_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_7", xf = lambda sample : sample.reg_5_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_8", xf = lambda sample : sample.reg_5_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_9", xf = lambda sample : sample.reg_5_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_10", xf = lambda sample : sample.reg_5_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_11", xf = lambda sample : sample.reg_5_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_12", xf = lambda sample : sample.reg_5_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_13", xf = lambda sample : sample.reg_5_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_14", xf = lambda sample : sample.reg_5_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_15", xf = lambda sample : sample.reg_5_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_16", xf = lambda sample : sample.reg_5_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_17", xf = lambda sample : sample.reg_5_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_18", xf = lambda sample : sample.reg_5_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_19", xf = lambda sample : sample.reg_5_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_20", xf = lambda sample : sample.reg_5_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_21", xf = lambda sample : sample.reg_5_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_22", xf = lambda sample : sample.reg_5_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_23", xf = lambda sample : sample.reg_5_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_24", xf = lambda sample : sample.reg_5_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_25", xf = lambda sample : sample.reg_5_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_26", xf = lambda sample : sample.reg_5_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_27", xf = lambda sample : sample.reg_5_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_28", xf = lambda sample : sample.reg_5_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_29", xf = lambda sample : sample.reg_5_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_30", xf = lambda sample : sample.reg_5_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_31", xf = lambda sample : sample.reg_5_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_32", xf = lambda sample : sample.reg_5_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_33", xf = lambda sample : sample.reg_5_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_34", xf = lambda sample : sample.reg_5_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_35", xf = lambda sample : sample.reg_5_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_36", xf = lambda sample : sample.reg_5_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_37", xf = lambda sample : sample.reg_5_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_38", xf = lambda sample : sample.reg_5_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_39", xf = lambda sample : sample.reg_5_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_40", xf = lambda sample : sample.reg_5_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_41", xf = lambda sample : sample.reg_5_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_42", xf = lambda sample : sample.reg_5_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_43", xf = lambda sample : sample.reg_5_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_44", xf = lambda sample : sample.reg_5_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_45", xf = lambda sample : sample.reg_5_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_46", xf = lambda sample : sample.reg_5_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_47", xf = lambda sample : sample.reg_5_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_48", xf = lambda sample : sample.reg_5_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_49", xf = lambda sample : sample.reg_5_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_50", xf = lambda sample : sample.reg_5_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_51", xf = lambda sample : sample.reg_5_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_52", xf = lambda sample : sample.reg_5_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_53", xf = lambda sample : sample.reg_5_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_54", xf = lambda sample : sample.reg_5_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_55", xf = lambda sample : sample.reg_5_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_56", xf = lambda sample : sample.reg_5_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_57", xf = lambda sample : sample.reg_5_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_58", xf = lambda sample : sample.reg_5_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_59", xf = lambda sample : sample.reg_5_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_60", xf = lambda sample : sample.reg_5_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_61", xf = lambda sample : sample.reg_5_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_62", xf = lambda sample : sample.reg_5_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_5_bit_63", xf = lambda sample : sample.reg_5_bit_63 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_0", xf = lambda sample : sample.reg_4_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_1", xf = lambda sample : sample.reg_4_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_2", xf = lambda sample : sample.reg_4_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_3", xf = lambda sample : sample.reg_4_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_4", xf = lambda sample : sample.reg_4_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_5", xf = lambda sample : sample.reg_4_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_6", xf = lambda sample : sample.reg_4_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_7", xf = lambda sample : sample.reg_4_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_8", xf = lambda sample : sample.reg_4_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_9", xf = lambda sample : sample.reg_4_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_10", xf = lambda sample : sample.reg_4_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_11", xf = lambda sample : sample.reg_4_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_12", xf = lambda sample : sample.reg_4_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_13", xf = lambda sample : sample.reg_4_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_14", xf = lambda sample : sample.reg_4_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_15", xf = lambda sample : sample.reg_4_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_16", xf = lambda sample : sample.reg_4_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_17", xf = lambda sample : sample.reg_4_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_18", xf = lambda sample : sample.reg_4_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_19", xf = lambda sample : sample.reg_4_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_20", xf = lambda sample : sample.reg_4_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_21", xf = lambda sample : sample.reg_4_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_22", xf = lambda sample : sample.reg_4_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_23", xf = lambda sample : sample.reg_4_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_24", xf = lambda sample : sample.reg_4_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_25", xf = lambda sample : sample.reg_4_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_26", xf = lambda sample : sample.reg_4_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_27", xf = lambda sample : sample.reg_4_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_28", xf = lambda sample : sample.reg_4_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_29", xf = lambda sample : sample.reg_4_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_30", xf = lambda sample : sample.reg_4_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_31", xf = lambda sample : sample.reg_4_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_32", xf = lambda sample : sample.reg_4_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_33", xf = lambda sample : sample.reg_4_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_34", xf = lambda sample : sample.reg_4_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_35", xf = lambda sample : sample.reg_4_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_36", xf = lambda sample : sample.reg_4_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_37", xf = lambda sample : sample.reg_4_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_38", xf = lambda sample : sample.reg_4_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_39", xf = lambda sample : sample.reg_4_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_40", xf = lambda sample : sample.reg_4_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_41", xf = lambda sample : sample.reg_4_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_42", xf = lambda sample : sample.reg_4_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_43", xf = lambda sample : sample.reg_4_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_44", xf = lambda sample : sample.reg_4_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_45", xf = lambda sample : sample.reg_4_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_46", xf = lambda sample : sample.reg_4_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_47", xf = lambda sample : sample.reg_4_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_48", xf = lambda sample : sample.reg_4_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_49", xf = lambda sample : sample.reg_4_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_50", xf = lambda sample : sample.reg_4_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_51", xf = lambda sample : sample.reg_4_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_52", xf = lambda sample : sample.reg_4_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_53", xf = lambda sample : sample.reg_4_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_54", xf = lambda sample : sample.reg_4_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_55", xf = lambda sample : sample.reg_4_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_56", xf = lambda sample : sample.reg_4_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_57", xf = lambda sample : sample.reg_4_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_58", xf = lambda sample : sample.reg_4_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_59", xf = lambda sample : sample.reg_4_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_60", xf = lambda sample : sample.reg_4_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_61", xf = lambda sample : sample.reg_4_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_62", xf = lambda sample : sample.reg_4_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_4_bit_63", xf = lambda sample : sample.reg_4_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_3_bit_0", xf = lambda sample : sample.reg_3_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_1", xf = lambda sample : sample.reg_3_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_2", xf = lambda sample : sample.reg_3_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_3", xf = lambda sample : sample.reg_3_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_4", xf = lambda sample : sample.reg_3_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_5", xf = lambda sample : sample.reg_3_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_6", xf = lambda sample : sample.reg_3_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_7", xf = lambda sample : sample.reg_3_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_8", xf = lambda sample : sample.reg_3_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_9", xf = lambda sample : sample.reg_3_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_10", xf = lambda sample : sample.reg_3_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_11", xf = lambda sample : sample.reg_3_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_12", xf = lambda sample : sample.reg_3_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_13", xf = lambda sample : sample.reg_3_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_14", xf = lambda sample : sample.reg_3_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_15", xf = lambda sample : sample.reg_3_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_16", xf = lambda sample : sample.reg_3_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_17", xf = lambda sample : sample.reg_3_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_18", xf = lambda sample : sample.reg_3_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_19", xf = lambda sample : sample.reg_3_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_20", xf = lambda sample : sample.reg_3_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_21", xf = lambda sample : sample.reg_3_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_22", xf = lambda sample : sample.reg_3_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_23", xf = lambda sample : sample.reg_3_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_24", xf = lambda sample : sample.reg_3_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_25", xf = lambda sample : sample.reg_3_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_26", xf = lambda sample : sample.reg_3_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_27", xf = lambda sample : sample.reg_3_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_28", xf = lambda sample : sample.reg_3_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_29", xf = lambda sample : sample.reg_3_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_30", xf = lambda sample : sample.reg_3_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_31", xf = lambda sample : sample.reg_3_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_32", xf = lambda sample : sample.reg_3_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_33", xf = lambda sample : sample.reg_3_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_34", xf = lambda sample : sample.reg_3_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_35", xf = lambda sample : sample.reg_3_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_36", xf = lambda sample : sample.reg_3_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_37", xf = lambda sample : sample.reg_3_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_38", xf = lambda sample : sample.reg_3_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_39", xf = lambda sample : sample.reg_3_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_40", xf = lambda sample : sample.reg_3_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_41", xf = lambda sample : sample.reg_3_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_42", xf = lambda sample : sample.reg_3_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_43", xf = lambda sample : sample.reg_3_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_44", xf = lambda sample : sample.reg_3_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_45", xf = lambda sample : sample.reg_3_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_46", xf = lambda sample : sample.reg_3_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_47", xf = lambda sample : sample.reg_3_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_48", xf = lambda sample : sample.reg_3_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_49", xf = lambda sample : sample.reg_3_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_50", xf = lambda sample : sample.reg_3_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_51", xf = lambda sample : sample.reg_3_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_52", xf = lambda sample : sample.reg_3_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_53", xf = lambda sample : sample.reg_3_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_54", xf = lambda sample : sample.reg_3_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_55", xf = lambda sample : sample.reg_3_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_56", xf = lambda sample : sample.reg_3_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_57", xf = lambda sample : sample.reg_3_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_58", xf = lambda sample : sample.reg_3_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_59", xf = lambda sample : sample.reg_3_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_60", xf = lambda sample : sample.reg_3_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_61", xf = lambda sample : sample.reg_3_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_62", xf = lambda sample : sample.reg_3_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_3_bit_63", xf = lambda sample : sample.reg_3_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_2_bit_0", xf = lambda sample : sample.reg_2_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_1", xf = lambda sample : sample.reg_2_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_2", xf = lambda sample : sample.reg_2_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_3", xf = lambda sample : sample.reg_2_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_4", xf = lambda sample : sample.reg_2_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_5", xf = lambda sample : sample.reg_2_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_6", xf = lambda sample : sample.reg_2_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_7", xf = lambda sample : sample.reg_2_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_8", xf = lambda sample : sample.reg_2_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_9", xf = lambda sample : sample.reg_2_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_10", xf = lambda sample : sample.reg_2_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_11", xf = lambda sample : sample.reg_2_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_12", xf = lambda sample : sample.reg_2_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_13", xf = lambda sample : sample.reg_2_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_14", xf = lambda sample : sample.reg_2_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_15", xf = lambda sample : sample.reg_2_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_16", xf = lambda sample : sample.reg_2_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_17", xf = lambda sample : sample.reg_2_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_18", xf = lambda sample : sample.reg_2_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_19", xf = lambda sample : sample.reg_2_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_20", xf = lambda sample : sample.reg_2_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_21", xf = lambda sample : sample.reg_2_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_22", xf = lambda sample : sample.reg_2_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_23", xf = lambda sample : sample.reg_2_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_24", xf = lambda sample : sample.reg_2_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_25", xf = lambda sample : sample.reg_2_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_26", xf = lambda sample : sample.reg_2_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_27", xf = lambda sample : sample.reg_2_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_28", xf = lambda sample : sample.reg_2_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_29", xf = lambda sample : sample.reg_2_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_30", xf = lambda sample : sample.reg_2_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_31", xf = lambda sample : sample.reg_2_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_32", xf = lambda sample : sample.reg_2_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_33", xf = lambda sample : sample.reg_2_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_34", xf = lambda sample : sample.reg_2_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_35", xf = lambda sample : sample.reg_2_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_36", xf = lambda sample : sample.reg_2_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_37", xf = lambda sample : sample.reg_2_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_38", xf = lambda sample : sample.reg_2_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_39", xf = lambda sample : sample.reg_2_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_40", xf = lambda sample : sample.reg_2_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_41", xf = lambda sample : sample.reg_2_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_42", xf = lambda sample : sample.reg_2_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_43", xf = lambda sample : sample.reg_2_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_44", xf = lambda sample : sample.reg_2_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_45", xf = lambda sample : sample.reg_2_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_46", xf = lambda sample : sample.reg_2_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_47", xf = lambda sample : sample.reg_2_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_48", xf = lambda sample : sample.reg_2_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_49", xf = lambda sample : sample.reg_2_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_50", xf = lambda sample : sample.reg_2_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_51", xf = lambda sample : sample.reg_2_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_52", xf = lambda sample : sample.reg_2_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_53", xf = lambda sample : sample.reg_2_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_54", xf = lambda sample : sample.reg_2_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_55", xf = lambda sample : sample.reg_2_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_56", xf = lambda sample : sample.reg_2_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_57", xf = lambda sample : sample.reg_2_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_58", xf = lambda sample : sample.reg_2_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_59", xf = lambda sample : sample.reg_2_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_60", xf = lambda sample : sample.reg_2_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_61", xf = lambda sample : sample.reg_2_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_62", xf = lambda sample : sample.reg_2_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_2_bit_63", xf = lambda sample : sample.reg_2_bit_63 , bins = [('0', '1'), ('1', '0')]),
	    CoverPoint("top.reg_1_bit_0", xf = lambda sample : sample.reg_1_bit_0 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_1", xf = lambda sample : sample.reg_1_bit_1 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_2", xf = lambda sample : sample.reg_1_bit_2 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_3", xf = lambda sample : sample.reg_1_bit_3 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_4", xf = lambda sample : sample.reg_1_bit_4 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_5", xf = lambda sample : sample.reg_1_bit_5 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_6", xf = lambda sample : sample.reg_1_bit_6 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_7", xf = lambda sample : sample.reg_1_bit_7 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_8", xf = lambda sample : sample.reg_1_bit_8 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_9", xf = lambda sample : sample.reg_1_bit_9 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_10", xf = lambda sample : sample.reg_1_bit_10 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_11", xf = lambda sample : sample.reg_1_bit_11 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_12", xf = lambda sample : sample.reg_1_bit_12 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_13", xf = lambda sample : sample.reg_1_bit_13 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_14", xf = lambda sample : sample.reg_1_bit_14 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_15", xf = lambda sample : sample.reg_1_bit_15 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_16", xf = lambda sample : sample.reg_1_bit_16 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_17", xf = lambda sample : sample.reg_1_bit_17 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_18", xf = lambda sample : sample.reg_1_bit_18 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_19", xf = lambda sample : sample.reg_1_bit_19 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_20", xf = lambda sample : sample.reg_1_bit_20 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_21", xf = lambda sample : sample.reg_1_bit_21 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_22", xf = lambda sample : sample.reg_1_bit_22 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_23", xf = lambda sample : sample.reg_1_bit_23 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_24", xf = lambda sample : sample.reg_1_bit_24 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_25", xf = lambda sample : sample.reg_1_bit_25 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_26", xf = lambda sample : sample.reg_1_bit_26 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_27", xf = lambda sample : sample.reg_1_bit_27 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_28", xf = lambda sample : sample.reg_1_bit_28 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_29", xf = lambda sample : sample.reg_1_bit_29 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_30", xf = lambda sample : sample.reg_1_bit_30 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_31", xf = lambda sample : sample.reg_1_bit_31 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_32", xf = lambda sample : sample.reg_1_bit_32 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_33", xf = lambda sample : sample.reg_1_bit_33 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_34", xf = lambda sample : sample.reg_1_bit_34 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_35", xf = lambda sample : sample.reg_1_bit_35 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_36", xf = lambda sample : sample.reg_1_bit_36 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_37", xf = lambda sample : sample.reg_1_bit_37 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_38", xf = lambda sample : sample.reg_1_bit_38 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_39", xf = lambda sample : sample.reg_1_bit_39 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_40", xf = lambda sample : sample.reg_1_bit_40 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_41", xf = lambda sample : sample.reg_1_bit_41 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_42", xf = lambda sample : sample.reg_1_bit_42 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_43", xf = lambda sample : sample.reg_1_bit_43 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_44", xf = lambda sample : sample.reg_1_bit_44 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_45", xf = lambda sample : sample.reg_1_bit_45 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_46", xf = lambda sample : sample.reg_1_bit_46 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_47", xf = lambda sample : sample.reg_1_bit_47 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_48", xf = lambda sample : sample.reg_1_bit_48 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_49", xf = lambda sample : sample.reg_1_bit_49 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_50", xf = lambda sample : sample.reg_1_bit_50 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_51", xf = lambda sample : sample.reg_1_bit_51 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_52", xf = lambda sample : sample.reg_1_bit_52 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_53", xf = lambda sample : sample.reg_1_bit_53 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_54", xf = lambda sample : sample.reg_1_bit_54 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_55", xf = lambda sample : sample.reg_1_bit_55 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_56", xf = lambda sample : sample.reg_1_bit_56 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_57", xf = lambda sample : sample.reg_1_bit_57 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_58", xf = lambda sample : sample.reg_1_bit_58 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_59", xf = lambda sample : sample.reg_1_bit_59 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_60", xf = lambda sample : sample.reg_1_bit_60 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_61", xf = lambda sample : sample.reg_1_bit_61 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_62", xf = lambda sample : sample.reg_1_bit_62 , bins = [('0', '1'), ('1', '0')]),
            CoverPoint("top.reg_1_bit_63", xf = lambda sample : sample.reg_1_bit_63 , bins = [('0', '1'), ('1', '0')]),
           )
	
    @FnRegCov_check
    def sample_reg_toggle(self, sample):
        pass
		
    @coroutine
    def bits_toggle(self):
        global prev_val
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.write_enable.value.integer == 1:
                sample = InputSample()
                new_val = self.arr_reg.value
                for i in range (len(new_val)):
                    globals()[f"reg_{i}"] = bin(new_val[(len(new_val)-1)-i])[2:].zfill(64) 
                    globals()[f"prev_{i}"] = bin(prev_val[(len(new_val)-1)-i])[2:].zfill(64)
                sample.reg_31_bit_0 = (prev_31[0], reg_31[0])
                sample.reg_31_bit_1 = (prev_31[1], reg_31[1])
                sample.reg_31_bit_2 = (prev_31[2], reg_31[2])
                sample.reg_31_bit_3 = (prev_31[3], reg_31[3])
                sample.reg_31_bit_4 = (prev_31[4], reg_31[4])
                sample.reg_31_bit_5 = (prev_31[5], reg_31[5])
                sample.reg_31_bit_6 = (prev_31[6], reg_31[6])
                sample.reg_31_bit_7 = (prev_31[7], reg_31[7])
                sample.reg_31_bit_8 = (prev_31[8], reg_31[8])
                sample.reg_31_bit_9 = (prev_31[9], reg_31[9])
                sample.reg_31_bit_10 = (prev_31[10], reg_31[10])
                sample.reg_31_bit_11 = (prev_31[11], reg_31[11])
                sample.reg_31_bit_12 = (prev_31[12], reg_31[12])
                sample.reg_31_bit_13 = (prev_31[13], reg_31[13])
                sample.reg_31_bit_14 = (prev_31[14], reg_31[14])
                sample.reg_31_bit_15 = (prev_31[15], reg_31[15])
                sample.reg_31_bit_16 = (prev_31[16], reg_31[16])
                sample.reg_31_bit_17 = (prev_31[17], reg_31[17])
                sample.reg_31_bit_18 = (prev_31[18], reg_31[18])
                sample.reg_31_bit_19 = (prev_31[19], reg_31[19])
                sample.reg_31_bit_20 = (prev_31[20], reg_31[20])
                sample.reg_31_bit_21 = (prev_31[21], reg_31[21])
                sample.reg_31_bit_22 = (prev_31[22], reg_31[22])
                sample.reg_31_bit_23 = (prev_31[23], reg_31[23])
                sample.reg_31_bit_24 = (prev_31[24], reg_31[24])
                sample.reg_31_bit_25 = (prev_31[25], reg_31[25])
                sample.reg_31_bit_26 = (prev_31[26], reg_31[26])
                sample.reg_31_bit_27 = (prev_31[27], reg_31[27])
                sample.reg_31_bit_28 = (prev_31[28], reg_31[28])
                sample.reg_31_bit_29 = (prev_31[29], reg_31[29])
                sample.reg_31_bit_30 = (prev_31[30], reg_31[30])
                sample.reg_31_bit_31 = (prev_31[31], reg_31[31])
                sample.reg_31_bit_32 = (prev_31[32], reg_31[32])
                sample.reg_31_bit_33 = (prev_31[33], reg_31[33])
                sample.reg_31_bit_34 = (prev_31[34], reg_31[34])
                sample.reg_31_bit_35 = (prev_31[35], reg_31[35])
                sample.reg_31_bit_36 = (prev_31[36], reg_31[36])
                sample.reg_31_bit_37 = (prev_31[37], reg_31[37])
                sample.reg_31_bit_38 = (prev_31[38], reg_31[38])
                sample.reg_31_bit_39 = (prev_31[39], reg_31[39])
                sample.reg_31_bit_40 = (prev_31[40], reg_31[40])
                sample.reg_31_bit_41 = (prev_31[41], reg_31[41])
                sample.reg_31_bit_42 = (prev_31[42], reg_31[42])
                sample.reg_31_bit_43 = (prev_31[43], reg_31[43])
                sample.reg_31_bit_44 = (prev_31[44], reg_31[44])
                sample.reg_31_bit_45 = (prev_31[45], reg_31[45])
                sample.reg_31_bit_46 = (prev_31[46], reg_31[46])
                sample.reg_31_bit_47 = (prev_31[47], reg_31[47])
                sample.reg_31_bit_48 = (prev_31[48], reg_31[48])
                sample.reg_31_bit_49 = (prev_31[49], reg_31[49])
                sample.reg_31_bit_50 = (prev_31[50], reg_31[50])
                sample.reg_31_bit_51 = (prev_31[51], reg_31[51])
                sample.reg_31_bit_52 = (prev_31[52], reg_31[52])
                sample.reg_31_bit_53 = (prev_31[53], reg_31[53])
                sample.reg_31_bit_54 = (prev_31[54], reg_31[54])
                sample.reg_31_bit_55 = (prev_31[55], reg_31[55])
                sample.reg_31_bit_56 = (prev_31[56], reg_31[56])
                sample.reg_31_bit_57 = (prev_31[57], reg_31[57])
                sample.reg_31_bit_58 = (prev_31[58], reg_31[58])
                sample.reg_31_bit_59 = (prev_31[59], reg_31[59])
                sample.reg_31_bit_60 = (prev_31[60], reg_31[60])
                sample.reg_31_bit_61 = (prev_31[61], reg_31[61])
                sample.reg_31_bit_62 = (prev_31[62], reg_31[62])
                sample.reg_31_bit_63 = (prev_31[63], reg_31[63])
                sample.reg_30_bit_0 = (prev_30[0], reg_30[0])
                sample.reg_30_bit_1 = (prev_30[1], reg_30[1])
                sample.reg_30_bit_2 = (prev_30[2], reg_30[2])
                sample.reg_30_bit_3 = (prev_30[3], reg_30[3])
                sample.reg_30_bit_4 = (prev_30[4], reg_30[4])
                sample.reg_30_bit_5 = (prev_30[5], reg_30[5])
                sample.reg_30_bit_6 = (prev_30[6], reg_30[6])
                sample.reg_30_bit_7 = (prev_30[7], reg_30[7])
                sample.reg_30_bit_8 = (prev_30[8], reg_30[8])
                sample.reg_30_bit_9 = (prev_30[9], reg_30[9])
                sample.reg_30_bit_10 = (prev_30[10], reg_30[10])
                sample.reg_30_bit_11 = (prev_30[11], reg_30[11])
                sample.reg_30_bit_12 = (prev_30[12], reg_30[12])
                sample.reg_30_bit_13 = (prev_30[13], reg_30[13])
                sample.reg_30_bit_14 = (prev_30[14], reg_30[14])
                sample.reg_30_bit_15 = (prev_30[15], reg_30[15])
                sample.reg_30_bit_16 = (prev_30[16], reg_30[16])
                sample.reg_30_bit_17 = (prev_30[17], reg_30[17])
                sample.reg_30_bit_18 = (prev_30[18], reg_30[18])
                sample.reg_30_bit_19 = (prev_30[19], reg_30[19])
                sample.reg_30_bit_20 = (prev_30[20], reg_30[20])
                sample.reg_30_bit_21 = (prev_30[21], reg_30[21])
                sample.reg_30_bit_22 = (prev_30[22], reg_30[22])
                sample.reg_30_bit_23 = (prev_30[23], reg_30[23])
                sample.reg_30_bit_24 = (prev_30[24], reg_30[24])
                sample.reg_30_bit_25 = (prev_30[25], reg_30[25])
                sample.reg_30_bit_26 = (prev_30[26], reg_30[26])
                sample.reg_30_bit_27 = (prev_30[27], reg_30[27])
                sample.reg_30_bit_28 = (prev_30[28], reg_30[28])
                sample.reg_30_bit_29 = (prev_30[29], reg_30[29])
                sample.reg_30_bit_30 = (prev_30[30], reg_30[30])
                sample.reg_30_bit_31 = (prev_30[31], reg_30[31])
                sample.reg_30_bit_32 = (prev_30[32], reg_30[32])
                sample.reg_30_bit_33 = (prev_30[33], reg_30[33])
                sample.reg_30_bit_34 = (prev_30[34], reg_30[34])
                sample.reg_30_bit_35 = (prev_30[35], reg_30[35])
                sample.reg_30_bit_36 = (prev_30[36], reg_30[36])
                sample.reg_30_bit_37 = (prev_30[37], reg_30[37])
                sample.reg_30_bit_38 = (prev_30[38], reg_30[38])
                sample.reg_30_bit_39 = (prev_30[39], reg_30[39])
                sample.reg_30_bit_40 = (prev_30[40], reg_30[40])
                sample.reg_30_bit_41 = (prev_30[41], reg_30[41])
                sample.reg_30_bit_42 = (prev_30[42], reg_30[42])
                sample.reg_30_bit_43 = (prev_30[43], reg_30[43])
                sample.reg_30_bit_44 = (prev_30[44], reg_30[44])
                sample.reg_30_bit_45 = (prev_30[45], reg_30[45])
                sample.reg_30_bit_46 = (prev_30[46], reg_30[46])
                sample.reg_30_bit_47 = (prev_30[47], reg_30[47])
                sample.reg_30_bit_48 = (prev_30[48], reg_30[48])
                sample.reg_30_bit_49 = (prev_30[49], reg_30[49])
                sample.reg_30_bit_50 = (prev_30[50], reg_30[50])
                sample.reg_30_bit_51 = (prev_30[51], reg_30[51])
                sample.reg_30_bit_52 = (prev_30[52], reg_30[52])
                sample.reg_30_bit_53 = (prev_30[53], reg_30[53])
                sample.reg_30_bit_54 = (prev_30[54], reg_30[54])
                sample.reg_30_bit_55 = (prev_30[55], reg_30[55])
                sample.reg_30_bit_56 = (prev_30[56], reg_30[56])
                sample.reg_30_bit_57 = (prev_30[57], reg_30[57])
                sample.reg_30_bit_58 = (prev_30[58], reg_30[58])
                sample.reg_30_bit_59 = (prev_30[59], reg_30[59])
                sample.reg_30_bit_60 = (prev_30[60], reg_30[60])
                sample.reg_30_bit_61 = (prev_30[61], reg_30[61])
                sample.reg_30_bit_62 = (prev_30[62], reg_30[62])
                sample.reg_30_bit_63 = (prev_30[63], reg_30[63])
                sample.reg_29_bit_0 = (prev_29[0], reg_29[0])
                sample.reg_29_bit_1 = (prev_29[1], reg_29[1])
                sample.reg_29_bit_2 = (prev_29[2], reg_29[2])
                sample.reg_29_bit_3 = (prev_29[3], reg_29[3])
                sample.reg_29_bit_4 = (prev_29[4], reg_29[4])
                sample.reg_29_bit_5 = (prev_29[5], reg_29[5])
                sample.reg_29_bit_6 = (prev_29[6], reg_29[6])
                sample.reg_29_bit_7 = (prev_29[7], reg_29[7])
                sample.reg_29_bit_8 = (prev_29[8], reg_29[8])
                sample.reg_29_bit_9 = (prev_29[9], reg_29[9])
                sample.reg_29_bit_10 = (prev_29[10], reg_29[10])
                sample.reg_29_bit_11 = (prev_29[11], reg_29[11])
                sample.reg_29_bit_12 = (prev_29[12], reg_29[12])
                sample.reg_29_bit_13 = (prev_29[13], reg_29[13])
                sample.reg_29_bit_14 = (prev_29[14], reg_29[14])
                sample.reg_29_bit_15 = (prev_29[15], reg_29[15])
                sample.reg_29_bit_16 = (prev_29[16], reg_29[16])
                sample.reg_29_bit_17 = (prev_29[17], reg_29[17])
                sample.reg_29_bit_18 = (prev_29[18], reg_29[18])
                sample.reg_29_bit_19 = (prev_29[19], reg_29[19])
                sample.reg_29_bit_20 = (prev_29[20], reg_29[20])
                sample.reg_29_bit_21 = (prev_29[21], reg_29[21])
                sample.reg_29_bit_22 = (prev_29[22], reg_29[22])
                sample.reg_29_bit_23 = (prev_29[23], reg_29[23])
                sample.reg_29_bit_24 = (prev_29[24], reg_29[24])
                sample.reg_29_bit_25 = (prev_29[25], reg_29[25])
                sample.reg_29_bit_26 = (prev_29[26], reg_29[26])
                sample.reg_29_bit_27 = (prev_29[27], reg_29[27])
                sample.reg_29_bit_28 = (prev_29[28], reg_29[28])
                sample.reg_29_bit_29 = (prev_29[29], reg_29[29])
                sample.reg_29_bit_30 = (prev_29[30], reg_29[30])
                sample.reg_29_bit_31 = (prev_29[31], reg_29[31])
                sample.reg_29_bit_32 = (prev_29[32], reg_29[32])
                sample.reg_29_bit_33 = (prev_29[33], reg_29[33])
                sample.reg_29_bit_34 = (prev_29[34], reg_29[34])
                sample.reg_29_bit_35 = (prev_29[35], reg_29[35])
                sample.reg_29_bit_36 = (prev_29[36], reg_29[36])
                sample.reg_29_bit_37 = (prev_29[37], reg_29[37])
                sample.reg_29_bit_38 = (prev_29[38], reg_29[38])
                sample.reg_29_bit_39 = (prev_29[39], reg_29[39])
                sample.reg_29_bit_40 = (prev_29[40], reg_29[40])
                sample.reg_29_bit_41 = (prev_29[41], reg_29[41])
                sample.reg_29_bit_42 = (prev_29[42], reg_29[42])
                sample.reg_29_bit_43 = (prev_29[43], reg_29[43])
                sample.reg_29_bit_44 = (prev_29[44], reg_29[44])
                sample.reg_29_bit_45 = (prev_29[45], reg_29[45])
                sample.reg_29_bit_46 = (prev_29[46], reg_29[46])
                sample.reg_29_bit_47 = (prev_29[47], reg_29[47])
                sample.reg_29_bit_48 = (prev_29[48], reg_29[48])
                sample.reg_29_bit_49 = (prev_29[49], reg_29[49])
                sample.reg_29_bit_50 = (prev_29[50], reg_29[50])
                sample.reg_29_bit_51 = (prev_29[51], reg_29[51])
                sample.reg_29_bit_52 = (prev_29[52], reg_29[52])
                sample.reg_29_bit_53 = (prev_29[53], reg_29[53])
                sample.reg_29_bit_54 = (prev_29[54], reg_29[54])
                sample.reg_29_bit_55 = (prev_29[55], reg_29[55])
                sample.reg_29_bit_56 = (prev_29[56], reg_29[56])
                sample.reg_29_bit_57 = (prev_29[57], reg_29[57])
                sample.reg_29_bit_58 = (prev_29[58], reg_29[58])
                sample.reg_29_bit_59 = (prev_29[59], reg_29[59])
                sample.reg_29_bit_60 = (prev_29[60], reg_29[60])
                sample.reg_29_bit_61 = (prev_29[61], reg_29[61])
                sample.reg_29_bit_62 = (prev_29[62], reg_29[62])
                sample.reg_29_bit_63 = (prev_29[63], reg_29[63])
                sample.reg_28_bit_0 = (prev_28[0], reg_28[0])
                sample.reg_28_bit_1 = (prev_28[1], reg_28[1])
                sample.reg_28_bit_2 = (prev_28[2], reg_28[2])
                sample.reg_28_bit_3 = (prev_28[3], reg_28[3])
                sample.reg_28_bit_4 = (prev_28[4], reg_28[4])
                sample.reg_28_bit_5 = (prev_28[5], reg_28[5])
                sample.reg_28_bit_6 = (prev_28[6], reg_28[6])
                sample.reg_28_bit_7 = (prev_28[7], reg_28[7])
                sample.reg_28_bit_8 = (prev_28[8], reg_28[8])
                sample.reg_28_bit_9 = (prev_28[9], reg_28[9])
                sample.reg_28_bit_10 = (prev_28[10], reg_28[10])
                sample.reg_28_bit_11 = (prev_28[11], reg_28[11])
                sample.reg_28_bit_12 = (prev_28[12], reg_28[12])
                sample.reg_28_bit_13 = (prev_28[13], reg_28[13])
                sample.reg_28_bit_14 = (prev_28[14], reg_28[14])
                sample.reg_28_bit_15 = (prev_28[15], reg_28[15])
                sample.reg_28_bit_16 = (prev_28[16], reg_28[16])
                sample.reg_28_bit_17 = (prev_28[17], reg_28[17])
                sample.reg_28_bit_18 = (prev_28[18], reg_28[18])
                sample.reg_28_bit_19 = (prev_28[19], reg_28[19])
                sample.reg_28_bit_20 = (prev_28[20], reg_28[20])
                sample.reg_28_bit_21 = (prev_28[21], reg_28[21])
                sample.reg_28_bit_22 = (prev_28[22], reg_28[22])
                sample.reg_28_bit_23 = (prev_28[23], reg_28[23])
                sample.reg_28_bit_24 = (prev_28[24], reg_28[24])
                sample.reg_28_bit_25 = (prev_28[25], reg_28[25])
                sample.reg_28_bit_26 = (prev_28[26], reg_28[26])
                sample.reg_28_bit_27 = (prev_28[27], reg_28[27])
                sample.reg_28_bit_28 = (prev_28[28], reg_28[28])
                sample.reg_28_bit_29 = (prev_28[29], reg_28[29])
                sample.reg_28_bit_30 = (prev_28[30], reg_28[30])
                sample.reg_28_bit_31 = (prev_28[31], reg_28[31])
                sample.reg_28_bit_32 = (prev_28[32], reg_28[32])
                sample.reg_28_bit_33 = (prev_28[33], reg_28[33])
                sample.reg_28_bit_34 = (prev_28[34], reg_28[34])
                sample.reg_28_bit_35 = (prev_28[35], reg_28[35])
                sample.reg_28_bit_36 = (prev_28[36], reg_28[36])
                sample.reg_28_bit_37 = (prev_28[37], reg_28[37])
                sample.reg_28_bit_38 = (prev_28[38], reg_28[38])
                sample.reg_28_bit_39 = (prev_28[39], reg_28[39])
                sample.reg_28_bit_40 = (prev_28[40], reg_28[40])
                sample.reg_28_bit_41 = (prev_28[41], reg_28[41])
                sample.reg_28_bit_42 = (prev_28[42], reg_28[42])
                sample.reg_28_bit_43 = (prev_28[43], reg_28[43])
                sample.reg_28_bit_44 = (prev_28[44], reg_28[44])
                sample.reg_28_bit_45 = (prev_28[45], reg_28[45])
                sample.reg_28_bit_46 = (prev_28[46], reg_28[46])
                sample.reg_28_bit_47 = (prev_28[47], reg_28[47])
                sample.reg_28_bit_48 = (prev_28[48], reg_28[48])
                sample.reg_28_bit_49 = (prev_28[49], reg_28[49])
                sample.reg_28_bit_50 = (prev_28[50], reg_28[50])
                sample.reg_28_bit_51 = (prev_28[51], reg_28[51])
                sample.reg_28_bit_52 = (prev_28[52], reg_28[52])
                sample.reg_28_bit_53 = (prev_28[53], reg_28[53])
                sample.reg_28_bit_54 = (prev_28[54], reg_28[54])
                sample.reg_28_bit_55 = (prev_28[55], reg_28[55])
                sample.reg_28_bit_56 = (prev_28[56], reg_28[56])
                sample.reg_28_bit_57 = (prev_28[57], reg_28[57])
                sample.reg_28_bit_58 = (prev_28[58], reg_28[58])
                sample.reg_28_bit_59 = (prev_28[59], reg_28[59])
                sample.reg_28_bit_60 = (prev_28[60], reg_28[60])
                sample.reg_28_bit_61 = (prev_28[61], reg_28[61])
                sample.reg_28_bit_62 = (prev_28[62], reg_28[62])
                sample.reg_28_bit_63 = (prev_28[63], reg_28[63])
                sample.reg_27_bit_0 = (prev_27[0], reg_27[0])
                sample.reg_27_bit_1 = (prev_27[1], reg_27[1])
                sample.reg_27_bit_2 = (prev_27[2], reg_27[2])
                sample.reg_27_bit_3 = (prev_27[3], reg_27[3])
                sample.reg_27_bit_4 = (prev_27[4], reg_27[4])
                sample.reg_27_bit_5 = (prev_27[5], reg_27[5])
                sample.reg_27_bit_6 = (prev_27[6], reg_27[6])
                sample.reg_27_bit_7 = (prev_27[7], reg_27[7])
                sample.reg_27_bit_8 = (prev_27[8], reg_27[8])
                sample.reg_27_bit_9 = (prev_27[9], reg_27[9])
                sample.reg_27_bit_10 = (prev_27[10], reg_27[10])
                sample.reg_27_bit_11 = (prev_27[11], reg_27[11])
                sample.reg_27_bit_12 = (prev_27[12], reg_27[12])
                sample.reg_27_bit_13 = (prev_27[13], reg_27[13])
                sample.reg_27_bit_14 = (prev_27[14], reg_27[14])
                sample.reg_27_bit_15 = (prev_27[15], reg_27[15])
                sample.reg_27_bit_16 = (prev_27[16], reg_27[16])
                sample.reg_27_bit_17 = (prev_27[17], reg_27[17])
                sample.reg_27_bit_18 = (prev_27[18], reg_27[18])
                sample.reg_27_bit_19 = (prev_27[19], reg_27[19])
                sample.reg_27_bit_20 = (prev_27[20], reg_27[20])
                sample.reg_27_bit_21 = (prev_27[21], reg_27[21])
                sample.reg_27_bit_22 = (prev_27[22], reg_27[22])
                sample.reg_27_bit_23 = (prev_27[23], reg_27[23])
                sample.reg_27_bit_24 = (prev_27[24], reg_27[24])
                sample.reg_27_bit_25 = (prev_27[25], reg_27[25])
                sample.reg_27_bit_26 = (prev_27[26], reg_27[26])
                sample.reg_27_bit_27 = (prev_27[27], reg_27[27])
                sample.reg_27_bit_28 = (prev_27[28], reg_27[28])
                sample.reg_27_bit_29 = (prev_27[29], reg_27[29])
                sample.reg_27_bit_30 = (prev_27[30], reg_27[30])
                sample.reg_27_bit_31 = (prev_27[31], reg_27[31])
                sample.reg_27_bit_32 = (prev_27[32], reg_27[32])
                sample.reg_27_bit_33 = (prev_27[33], reg_27[33])
                sample.reg_27_bit_34 = (prev_27[34], reg_27[34])
                sample.reg_27_bit_35 = (prev_27[35], reg_27[35])
                sample.reg_27_bit_36 = (prev_27[36], reg_27[36])
                sample.reg_27_bit_37 = (prev_27[37], reg_27[37])
                sample.reg_27_bit_38 = (prev_27[38], reg_27[38])
                sample.reg_27_bit_39 = (prev_27[39], reg_27[39])
                sample.reg_27_bit_40 = (prev_27[40], reg_27[40])
                sample.reg_27_bit_41 = (prev_27[41], reg_27[41])
                sample.reg_27_bit_42 = (prev_27[42], reg_27[42])
                sample.reg_27_bit_43 = (prev_27[43], reg_27[43])
                sample.reg_27_bit_44 = (prev_27[44], reg_27[44])
                sample.reg_27_bit_45 = (prev_27[45], reg_27[45])
                sample.reg_27_bit_46 = (prev_27[46], reg_27[46])
                sample.reg_27_bit_47 = (prev_27[47], reg_27[47])
                sample.reg_27_bit_48 = (prev_27[48], reg_27[48])
                sample.reg_27_bit_49 = (prev_27[49], reg_27[49])
                sample.reg_27_bit_50 = (prev_27[50], reg_27[50])
                sample.reg_27_bit_51 = (prev_27[51], reg_27[51])
                sample.reg_27_bit_52 = (prev_27[52], reg_27[52])
                sample.reg_27_bit_53 = (prev_27[53], reg_27[53])
                sample.reg_27_bit_54 = (prev_27[54], reg_27[54])
                sample.reg_27_bit_55 = (prev_27[55], reg_27[55])
                sample.reg_27_bit_56 = (prev_27[56], reg_27[56])
                sample.reg_27_bit_57 = (prev_27[57], reg_27[57])
                sample.reg_27_bit_58 = (prev_27[58], reg_27[58])
                sample.reg_27_bit_59 = (prev_27[59], reg_27[59])
                sample.reg_27_bit_60 = (prev_27[60], reg_27[60])
                sample.reg_27_bit_61 = (prev_27[61], reg_27[61])
                sample.reg_27_bit_62 = (prev_27[62], reg_27[62])
                sample.reg_27_bit_63 = (prev_27[63], reg_27[63])
                sample.reg_26_bit_0 = (prev_26[0], reg_26[0])
                sample.reg_26_bit_1 = (prev_26[1], reg_26[1])
                sample.reg_26_bit_2 = (prev_26[2], reg_26[2])
                sample.reg_26_bit_3 = (prev_26[3], reg_26[3])
                sample.reg_26_bit_4 = (prev_26[4], reg_26[4])
                sample.reg_26_bit_5 = (prev_26[5], reg_26[5])
                sample.reg_26_bit_6 = (prev_26[6], reg_26[6])
                sample.reg_26_bit_7 = (prev_26[7], reg_26[7])
                sample.reg_26_bit_8 = (prev_26[8], reg_26[8])
                sample.reg_26_bit_9 = (prev_26[9], reg_26[9])
                sample.reg_26_bit_10 = (prev_26[10], reg_26[10])
                sample.reg_26_bit_11 = (prev_26[11], reg_26[11])
                sample.reg_26_bit_12 = (prev_26[12], reg_26[12])
                sample.reg_26_bit_13 = (prev_26[13], reg_26[13])
                sample.reg_26_bit_14 = (prev_26[14], reg_26[14])
                sample.reg_26_bit_15 = (prev_26[15], reg_26[15])
                sample.reg_26_bit_16 = (prev_26[16], reg_26[16])
                sample.reg_26_bit_17 = (prev_26[17], reg_26[17])
                sample.reg_26_bit_18 = (prev_26[18], reg_26[18])
                sample.reg_26_bit_19 = (prev_26[19], reg_26[19])
                sample.reg_26_bit_20 = (prev_26[20], reg_26[20])
                sample.reg_26_bit_21 = (prev_26[21], reg_26[21])
                sample.reg_26_bit_22 = (prev_26[22], reg_26[22])
                sample.reg_26_bit_23 = (prev_26[23], reg_26[23])
                sample.reg_26_bit_24 = (prev_26[24], reg_26[24])
                sample.reg_26_bit_25 = (prev_26[25], reg_26[25])
                sample.reg_26_bit_26 = (prev_26[26], reg_26[26])
                sample.reg_26_bit_27 = (prev_26[27], reg_26[27])
                sample.reg_26_bit_28 = (prev_26[28], reg_26[28])
                sample.reg_26_bit_29 = (prev_26[29], reg_26[29])
                sample.reg_26_bit_30 = (prev_26[30], reg_26[30])
                sample.reg_26_bit_31 = (prev_26[31], reg_26[31])
                sample.reg_26_bit_32 = (prev_26[32], reg_26[32])
                sample.reg_26_bit_33 = (prev_26[33], reg_26[33])
                sample.reg_26_bit_34 = (prev_26[34], reg_26[34])
                sample.reg_26_bit_35 = (prev_26[35], reg_26[35])
                sample.reg_26_bit_36 = (prev_26[36], reg_26[36])
                sample.reg_26_bit_37 = (prev_26[37], reg_26[37])
                sample.reg_26_bit_38 = (prev_26[38], reg_26[38])
                sample.reg_26_bit_39 = (prev_26[39], reg_26[39])
                sample.reg_26_bit_40 = (prev_26[40], reg_26[40])
                sample.reg_26_bit_41 = (prev_26[41], reg_26[41])
                sample.reg_26_bit_42 = (prev_26[42], reg_26[42])
                sample.reg_26_bit_43 = (prev_26[43], reg_26[43])
                sample.reg_26_bit_44 = (prev_26[44], reg_26[44])
                sample.reg_26_bit_45 = (prev_26[45], reg_26[45])
                sample.reg_26_bit_46 = (prev_26[46], reg_26[46])
                sample.reg_26_bit_47 = (prev_26[47], reg_26[47])
                sample.reg_26_bit_48 = (prev_26[48], reg_26[48])
                sample.reg_26_bit_49 = (prev_26[49], reg_26[49])
                sample.reg_26_bit_50 = (prev_26[50], reg_26[50])
                sample.reg_26_bit_51 = (prev_26[51], reg_26[51])
                sample.reg_26_bit_52 = (prev_26[52], reg_26[52])
                sample.reg_26_bit_53 = (prev_26[53], reg_26[53])
                sample.reg_26_bit_54 = (prev_26[54], reg_26[54])
                sample.reg_26_bit_55 = (prev_26[55], reg_26[55])
                sample.reg_26_bit_56 = (prev_26[56], reg_26[56])
                sample.reg_26_bit_57 = (prev_26[57], reg_26[57])
                sample.reg_26_bit_58 = (prev_26[58], reg_26[58])
                sample.reg_26_bit_59 = (prev_26[59], reg_26[59])
                sample.reg_26_bit_60 = (prev_26[60], reg_26[60])
                sample.reg_26_bit_61 = (prev_26[61], reg_26[61])
                sample.reg_26_bit_62 = (prev_26[62], reg_26[62])
                sample.reg_26_bit_63 = (prev_26[63], reg_26[63])
                sample.reg_25_bit_0 = (prev_25[0], reg_25[0])
                sample.reg_25_bit_1 = (prev_25[1], reg_25[1])
                sample.reg_25_bit_2 = (prev_25[2], reg_25[2])
                sample.reg_25_bit_3 = (prev_25[3], reg_25[3])
                sample.reg_25_bit_4 = (prev_25[4], reg_25[4])
                sample.reg_25_bit_5 = (prev_25[5], reg_25[5])
                sample.reg_25_bit_6 = (prev_25[6], reg_25[6])
                sample.reg_25_bit_7 = (prev_25[7], reg_25[7])
                sample.reg_25_bit_8 = (prev_25[8], reg_25[8])
                sample.reg_25_bit_9 = (prev_25[9], reg_25[9])
                sample.reg_25_bit_10 = (prev_25[10], reg_25[10])
                sample.reg_25_bit_11 = (prev_25[11], reg_25[11])
                sample.reg_25_bit_12 = (prev_25[12], reg_25[12])
                sample.reg_25_bit_13 = (prev_25[13], reg_25[13])
                sample.reg_25_bit_14 = (prev_25[14], reg_25[14])
                sample.reg_25_bit_15 = (prev_25[15], reg_25[15])
                sample.reg_25_bit_16 = (prev_25[16], reg_25[16])
                sample.reg_25_bit_17 = (prev_25[17], reg_25[17])
                sample.reg_25_bit_18 = (prev_25[18], reg_25[18])
                sample.reg_25_bit_19 = (prev_25[19], reg_25[19])
                sample.reg_25_bit_20 = (prev_25[20], reg_25[20])
                sample.reg_25_bit_21 = (prev_25[21], reg_25[21])
                sample.reg_25_bit_22 = (prev_25[22], reg_25[22])
                sample.reg_25_bit_23 = (prev_25[23], reg_25[23])
                sample.reg_25_bit_24 = (prev_25[24], reg_25[24])
                sample.reg_25_bit_25 = (prev_25[25], reg_25[25])
                sample.reg_25_bit_26 = (prev_25[26], reg_25[26])
                sample.reg_25_bit_27 = (prev_25[27], reg_25[27])
                sample.reg_25_bit_28 = (prev_25[28], reg_25[28])
                sample.reg_25_bit_29 = (prev_25[29], reg_25[29])
                sample.reg_25_bit_30 = (prev_25[30], reg_25[30])
                sample.reg_25_bit_31 = (prev_25[31], reg_25[31])
                sample.reg_25_bit_32 = (prev_25[32], reg_25[32])
                sample.reg_25_bit_33 = (prev_25[33], reg_25[33])
                sample.reg_25_bit_34 = (prev_25[34], reg_25[34])
                sample.reg_25_bit_35 = (prev_25[35], reg_25[35])
                sample.reg_25_bit_36 = (prev_25[36], reg_25[36])
                sample.reg_25_bit_37 = (prev_25[37], reg_25[37])
                sample.reg_25_bit_38 = (prev_25[38], reg_25[38])
                sample.reg_25_bit_39 = (prev_25[39], reg_25[39])
                sample.reg_25_bit_40 = (prev_25[40], reg_25[40])
                sample.reg_25_bit_41 = (prev_25[41], reg_25[41])
                sample.reg_25_bit_42 = (prev_25[42], reg_25[42])
                sample.reg_25_bit_43 = (prev_25[43], reg_25[43])
                sample.reg_25_bit_44 = (prev_25[44], reg_25[44])
                sample.reg_25_bit_45 = (prev_25[45], reg_25[45])
                sample.reg_25_bit_46 = (prev_25[46], reg_25[46])
                sample.reg_25_bit_47 = (prev_25[47], reg_25[47])
                sample.reg_25_bit_48 = (prev_25[48], reg_25[48])
                sample.reg_25_bit_49 = (prev_25[49], reg_25[49])
                sample.reg_25_bit_50 = (prev_25[50], reg_25[50])
                sample.reg_25_bit_51 = (prev_25[51], reg_25[51])
                sample.reg_25_bit_52 = (prev_25[52], reg_25[52])
                sample.reg_25_bit_53 = (prev_25[53], reg_25[53])
                sample.reg_25_bit_54 = (prev_25[54], reg_25[54])
                sample.reg_25_bit_55 = (prev_25[55], reg_25[55])
                sample.reg_25_bit_56 = (prev_25[56], reg_25[56])
                sample.reg_25_bit_57 = (prev_25[57], reg_25[57])
                sample.reg_25_bit_58 = (prev_25[58], reg_25[58])
                sample.reg_25_bit_59 = (prev_25[59], reg_25[59])
                sample.reg_25_bit_60 = (prev_25[60], reg_25[60])
                sample.reg_25_bit_61 = (prev_25[61], reg_25[61])
                sample.reg_25_bit_62 = (prev_25[62], reg_25[62])
                sample.reg_25_bit_63 = (prev_25[63], reg_25[63])
                sample.reg_24_bit_0 = (prev_24[0], reg_24[0])
                sample.reg_24_bit_1 = (prev_24[1], reg_24[1])
                sample.reg_24_bit_2 = (prev_24[2], reg_24[2])
                sample.reg_24_bit_3 = (prev_24[3], reg_24[3])
                sample.reg_24_bit_4 = (prev_24[4], reg_24[4])
                sample.reg_24_bit_5 = (prev_24[5], reg_24[5])
                sample.reg_24_bit_6 = (prev_24[6], reg_24[6])
                sample.reg_24_bit_7 = (prev_24[7], reg_24[7])
                sample.reg_24_bit_8 = (prev_24[8], reg_24[8])
                sample.reg_24_bit_9 = (prev_24[9], reg_24[9])
                sample.reg_24_bit_10 = (prev_24[10], reg_24[10])
                sample.reg_24_bit_11 = (prev_24[11], reg_24[11])
                sample.reg_24_bit_12 = (prev_24[12], reg_24[12])
                sample.reg_24_bit_13 = (prev_24[13], reg_24[13])
                sample.reg_24_bit_14 = (prev_24[14], reg_24[14])
                sample.reg_24_bit_15 = (prev_24[15], reg_24[15])
                sample.reg_24_bit_16 = (prev_24[16], reg_24[16])
                sample.reg_24_bit_17 = (prev_24[17], reg_24[17])
                sample.reg_24_bit_18 = (prev_24[18], reg_24[18])
                sample.reg_24_bit_19 = (prev_24[19], reg_24[19])
                sample.reg_24_bit_20 = (prev_24[20], reg_24[20])
                sample.reg_24_bit_21 = (prev_24[21], reg_24[21])
                sample.reg_24_bit_22 = (prev_24[22], reg_24[22])
                sample.reg_24_bit_23 = (prev_24[23], reg_24[23])
                sample.reg_24_bit_24 = (prev_24[24], reg_24[24])
                sample.reg_24_bit_25 = (prev_24[25], reg_24[25])
                sample.reg_24_bit_26 = (prev_24[26], reg_24[26])
                sample.reg_24_bit_27 = (prev_24[27], reg_24[27])
                sample.reg_24_bit_28 = (prev_24[28], reg_24[28])
                sample.reg_24_bit_29 = (prev_24[29], reg_24[29])
                sample.reg_24_bit_30 = (prev_24[30], reg_24[30])
                sample.reg_24_bit_31 = (prev_24[31], reg_24[31])
                sample.reg_24_bit_32 = (prev_24[32], reg_24[32])
                sample.reg_24_bit_33 = (prev_24[33], reg_24[33])
                sample.reg_24_bit_34 = (prev_24[34], reg_24[34])
                sample.reg_24_bit_35 = (prev_24[35], reg_24[35])
                sample.reg_24_bit_36 = (prev_24[36], reg_24[36])
                sample.reg_24_bit_37 = (prev_24[37], reg_24[37])
                sample.reg_24_bit_38 = (prev_24[38], reg_24[38])
                sample.reg_24_bit_39 = (prev_24[39], reg_24[39])
                sample.reg_24_bit_40 = (prev_24[40], reg_24[40])
                sample.reg_24_bit_41 = (prev_24[41], reg_24[41])
                sample.reg_24_bit_42 = (prev_24[42], reg_24[42])
                sample.reg_24_bit_43 = (prev_24[43], reg_24[43])
                sample.reg_24_bit_44 = (prev_24[44], reg_24[44])
                sample.reg_24_bit_45 = (prev_24[45], reg_24[45])
                sample.reg_24_bit_46 = (prev_24[46], reg_24[46])
                sample.reg_24_bit_47 = (prev_24[47], reg_24[47])
                sample.reg_24_bit_48 = (prev_24[48], reg_24[48])
                sample.reg_24_bit_49 = (prev_24[49], reg_24[49])
                sample.reg_24_bit_50 = (prev_24[50], reg_24[50])
                sample.reg_24_bit_51 = (prev_24[51], reg_24[51])
                sample.reg_24_bit_52 = (prev_24[52], reg_24[52])
                sample.reg_24_bit_53 = (prev_24[53], reg_24[53])
                sample.reg_24_bit_54 = (prev_24[54], reg_24[54])
                sample.reg_24_bit_55 = (prev_24[55], reg_24[55])
                sample.reg_24_bit_56 = (prev_24[56], reg_24[56])
                sample.reg_24_bit_57 = (prev_24[57], reg_24[57])
                sample.reg_24_bit_58 = (prev_24[58], reg_24[58])
                sample.reg_24_bit_59 = (prev_24[59], reg_24[59])
                sample.reg_24_bit_60 = (prev_24[60], reg_24[60])
                sample.reg_24_bit_61 = (prev_24[61], reg_24[61])
                sample.reg_24_bit_62 = (prev_24[62], reg_24[62])
                sample.reg_24_bit_63 = (prev_24[63], reg_24[63])
                sample.reg_23_bit_0 = (prev_23[0], reg_23[0])
                sample.reg_23_bit_1 = (prev_23[1], reg_23[1])
                sample.reg_23_bit_2 = (prev_23[2], reg_23[2])
                sample.reg_23_bit_3 = (prev_23[3], reg_23[3])
                sample.reg_23_bit_4 = (prev_23[4], reg_23[4])
                sample.reg_23_bit_5 = (prev_23[5], reg_23[5])
                sample.reg_23_bit_6 = (prev_23[6], reg_23[6])
                sample.reg_23_bit_7 = (prev_23[7], reg_23[7])
                sample.reg_23_bit_8 = (prev_23[8], reg_23[8])
                sample.reg_23_bit_9 = (prev_23[9], reg_23[9])
                sample.reg_23_bit_10 = (prev_23[10], reg_23[10])
                sample.reg_23_bit_11 = (prev_23[11], reg_23[11])
                sample.reg_23_bit_12 = (prev_23[12], reg_23[12])
                sample.reg_23_bit_13 = (prev_23[13], reg_23[13])
                sample.reg_23_bit_14 = (prev_23[14], reg_23[14])
                sample.reg_23_bit_15 = (prev_23[15], reg_23[15])
                sample.reg_23_bit_16 = (prev_23[16], reg_23[16])
                sample.reg_23_bit_17 = (prev_23[17], reg_23[17])
                sample.reg_23_bit_18 = (prev_23[18], reg_23[18])
                sample.reg_23_bit_19 = (prev_23[19], reg_23[19])
                sample.reg_23_bit_20 = (prev_23[20], reg_23[20])
                sample.reg_23_bit_21 = (prev_23[21], reg_23[21])
                sample.reg_23_bit_22 = (prev_23[22], reg_23[22])
                sample.reg_23_bit_23 = (prev_23[23], reg_23[23])
                sample.reg_23_bit_24 = (prev_23[24], reg_23[24])
                sample.reg_23_bit_25 = (prev_23[25], reg_23[25])
                sample.reg_23_bit_26 = (prev_23[26], reg_23[26])
                sample.reg_23_bit_27 = (prev_23[27], reg_23[27])
                sample.reg_23_bit_28 = (prev_23[28], reg_23[28])
                sample.reg_23_bit_29 = (prev_23[29], reg_23[29])
                sample.reg_23_bit_30 = (prev_23[30], reg_23[30])
                sample.reg_23_bit_31 = (prev_23[31], reg_23[31])
                sample.reg_23_bit_32 = (prev_23[32], reg_23[32])
                sample.reg_23_bit_33 = (prev_23[33], reg_23[33])
                sample.reg_23_bit_34 = (prev_23[34], reg_23[34])
                sample.reg_23_bit_35 = (prev_23[35], reg_23[35])
                sample.reg_23_bit_36 = (prev_23[36], reg_23[36])
                sample.reg_23_bit_37 = (prev_23[37], reg_23[37])
                sample.reg_23_bit_38 = (prev_23[38], reg_23[38])
                sample.reg_23_bit_39 = (prev_23[39], reg_23[39])
                sample.reg_23_bit_40 = (prev_23[40], reg_23[40])
                sample.reg_23_bit_41 = (prev_23[41], reg_23[41])
                sample.reg_23_bit_42 = (prev_23[42], reg_23[42])
                sample.reg_23_bit_43 = (prev_23[43], reg_23[43])
                sample.reg_23_bit_44 = (prev_23[44], reg_23[44])
                sample.reg_23_bit_45 = (prev_23[45], reg_23[45])
                sample.reg_23_bit_46 = (prev_23[46], reg_23[46])
                sample.reg_23_bit_47 = (prev_23[47], reg_23[47])
                sample.reg_23_bit_48 = (prev_23[48], reg_23[48])
                sample.reg_23_bit_49 = (prev_23[49], reg_23[49])
                sample.reg_23_bit_50 = (prev_23[50], reg_23[50])
                sample.reg_23_bit_51 = (prev_23[51], reg_23[51])
                sample.reg_23_bit_52 = (prev_23[52], reg_23[52])
                sample.reg_23_bit_53 = (prev_23[53], reg_23[53])
                sample.reg_23_bit_54 = (prev_23[54], reg_23[54])
                sample.reg_23_bit_55 = (prev_23[55], reg_23[55])
                sample.reg_23_bit_56 = (prev_23[56], reg_23[56])
                sample.reg_23_bit_57 = (prev_23[57], reg_23[57])
                sample.reg_23_bit_58 = (prev_23[58], reg_23[58])
                sample.reg_23_bit_59 = (prev_23[59], reg_23[59])
                sample.reg_23_bit_60 = (prev_23[60], reg_23[60])
                sample.reg_23_bit_61 = (prev_23[61], reg_23[61])
                sample.reg_23_bit_62 = (prev_23[62], reg_23[62])
                sample.reg_23_bit_63 = (prev_23[63], reg_23[63])
                sample.reg_22_bit_0 = (prev_22[0], reg_22[0])
                sample.reg_22_bit_1 = (prev_22[1], reg_22[1])
                sample.reg_22_bit_2 = (prev_22[2], reg_22[2])
                sample.reg_22_bit_3 = (prev_22[3], reg_22[3])
                sample.reg_22_bit_4 = (prev_22[4], reg_22[4])
                sample.reg_22_bit_5 = (prev_22[5], reg_22[5])
                sample.reg_22_bit_6 = (prev_22[6], reg_22[6])
                sample.reg_22_bit_7 = (prev_22[7], reg_22[7])
                sample.reg_22_bit_8 = (prev_22[8], reg_22[8])
                sample.reg_22_bit_9 = (prev_22[9], reg_22[9])
                sample.reg_22_bit_10 = (prev_22[10], reg_22[10])
                sample.reg_22_bit_11 = (prev_22[11], reg_22[11])
                sample.reg_22_bit_12 = (prev_22[12], reg_22[12])
                sample.reg_22_bit_13 = (prev_22[13], reg_22[13])
                sample.reg_22_bit_14 = (prev_22[14], reg_22[14])
                sample.reg_22_bit_15 = (prev_22[15], reg_22[15])
                sample.reg_22_bit_16 = (prev_22[16], reg_22[16])
                sample.reg_22_bit_17 = (prev_22[17], reg_22[17])
                sample.reg_22_bit_18 = (prev_22[18], reg_22[18])
                sample.reg_22_bit_19 = (prev_22[19], reg_22[19])
                sample.reg_22_bit_20 = (prev_22[20], reg_22[20])
                sample.reg_22_bit_21 = (prev_22[21], reg_22[21])
                sample.reg_22_bit_22 = (prev_22[22], reg_22[22])
                sample.reg_22_bit_23 = (prev_22[23], reg_22[23])
                sample.reg_22_bit_24 = (prev_22[24], reg_22[24])
                sample.reg_22_bit_25 = (prev_22[25], reg_22[25])
                sample.reg_22_bit_26 = (prev_22[26], reg_22[26])
                sample.reg_22_bit_27 = (prev_22[27], reg_22[27])
                sample.reg_22_bit_28 = (prev_22[28], reg_22[28])
                sample.reg_22_bit_29 = (prev_22[29], reg_22[29])
                sample.reg_22_bit_30 = (prev_22[30], reg_22[30])
                sample.reg_22_bit_31 = (prev_22[31], reg_22[31])
                sample.reg_22_bit_32 = (prev_22[32], reg_22[32])
                sample.reg_22_bit_33 = (prev_22[33], reg_22[33])
                sample.reg_22_bit_34 = (prev_22[34], reg_22[34])
                sample.reg_22_bit_35 = (prev_22[35], reg_22[35])
                sample.reg_22_bit_36 = (prev_22[36], reg_22[36])
                sample.reg_22_bit_37 = (prev_22[37], reg_22[37])
                sample.reg_22_bit_38 = (prev_22[38], reg_22[38])
                sample.reg_22_bit_39 = (prev_22[39], reg_22[39])
                sample.reg_22_bit_40 = (prev_22[40], reg_22[40])
                sample.reg_22_bit_41 = (prev_22[41], reg_22[41])
                sample.reg_22_bit_42 = (prev_22[42], reg_22[42])
                sample.reg_22_bit_43 = (prev_22[43], reg_22[43])
                sample.reg_22_bit_44 = (prev_22[44], reg_22[44])
                sample.reg_22_bit_45 = (prev_22[45], reg_22[45])
                sample.reg_22_bit_46 = (prev_22[46], reg_22[46])
                sample.reg_22_bit_47 = (prev_22[47], reg_22[47])
                sample.reg_22_bit_48 = (prev_22[48], reg_22[48])
                sample.reg_22_bit_49 = (prev_22[49], reg_22[49])
                sample.reg_22_bit_50 = (prev_22[50], reg_22[50])
                sample.reg_22_bit_51 = (prev_22[51], reg_22[51])
                sample.reg_22_bit_52 = (prev_22[52], reg_22[52])
                sample.reg_22_bit_53 = (prev_22[53], reg_22[53])
                sample.reg_22_bit_54 = (prev_22[54], reg_22[54])
                sample.reg_22_bit_55 = (prev_22[55], reg_22[55])
                sample.reg_22_bit_56 = (prev_22[56], reg_22[56])
                sample.reg_22_bit_57 = (prev_22[57], reg_22[57])
                sample.reg_22_bit_58 = (prev_22[58], reg_22[58])
                sample.reg_22_bit_59 = (prev_22[59], reg_22[59])
                sample.reg_22_bit_60 = (prev_22[60], reg_22[60])
                sample.reg_22_bit_61 = (prev_22[61], reg_22[61])
                sample.reg_22_bit_62 = (prev_22[62], reg_22[62])
                sample.reg_22_bit_63 = (prev_22[63], reg_22[63])
                sample.reg_21_bit_0 = (prev_21[0], reg_21[0])
                sample.reg_21_bit_1 = (prev_21[1], reg_21[1])
                sample.reg_21_bit_2 = (prev_21[2], reg_21[2])
                sample.reg_21_bit_3 = (prev_21[3], reg_21[3])
                sample.reg_21_bit_4 = (prev_21[4], reg_21[4])
                sample.reg_21_bit_5 = (prev_21[5], reg_21[5])
                sample.reg_21_bit_6 = (prev_21[6], reg_21[6])
                sample.reg_21_bit_7 = (prev_21[7], reg_21[7])
                sample.reg_21_bit_8 = (prev_21[8], reg_21[8])
                sample.reg_21_bit_9 = (prev_21[9], reg_21[9])
                sample.reg_21_bit_10 = (prev_21[10], reg_21[10])
                sample.reg_21_bit_11 = (prev_21[11], reg_21[11])
                sample.reg_21_bit_12 = (prev_21[12], reg_21[12])
                sample.reg_21_bit_13 = (prev_21[13], reg_21[13])
                sample.reg_21_bit_14 = (prev_21[14], reg_21[14])
                sample.reg_21_bit_15 = (prev_21[15], reg_21[15])
                sample.reg_21_bit_16 = (prev_21[16], reg_21[16])
                sample.reg_21_bit_17 = (prev_21[17], reg_21[17])
                sample.reg_21_bit_18 = (prev_21[18], reg_21[18])
                sample.reg_21_bit_19 = (prev_21[19], reg_21[19])
                sample.reg_21_bit_20 = (prev_21[20], reg_21[20])
                sample.reg_21_bit_21 = (prev_21[21], reg_21[21])
                sample.reg_21_bit_22 = (prev_21[22], reg_21[22])
                sample.reg_21_bit_23 = (prev_21[23], reg_21[23])
                sample.reg_21_bit_24 = (prev_21[24], reg_21[24])
                sample.reg_21_bit_25 = (prev_21[25], reg_21[25])
                sample.reg_21_bit_26 = (prev_21[26], reg_21[26])
                sample.reg_21_bit_27 = (prev_21[27], reg_21[27])
                sample.reg_21_bit_28 = (prev_21[28], reg_21[28])
                sample.reg_21_bit_29 = (prev_21[29], reg_21[29])
                sample.reg_21_bit_30 = (prev_21[30], reg_21[30])
                sample.reg_21_bit_31 = (prev_21[31], reg_21[31])
                sample.reg_21_bit_32 = (prev_21[32], reg_21[32])
                sample.reg_21_bit_33 = (prev_21[33], reg_21[33])
                sample.reg_21_bit_34 = (prev_21[34], reg_21[34])
                sample.reg_21_bit_35 = (prev_21[35], reg_21[35])
                sample.reg_21_bit_36 = (prev_21[36], reg_21[36])
                sample.reg_21_bit_37 = (prev_21[37], reg_21[37])
                sample.reg_21_bit_38 = (prev_21[38], reg_21[38])
                sample.reg_21_bit_39 = (prev_21[39], reg_21[39])
                sample.reg_21_bit_40 = (prev_21[40], reg_21[40])
                sample.reg_21_bit_41 = (prev_21[41], reg_21[41])
                sample.reg_21_bit_42 = (prev_21[42], reg_21[42])
                sample.reg_21_bit_43 = (prev_21[43], reg_21[43])
                sample.reg_21_bit_44 = (prev_21[44], reg_21[44])
                sample.reg_21_bit_45 = (prev_21[45], reg_21[45])
                sample.reg_21_bit_46 = (prev_21[46], reg_21[46])
                sample.reg_21_bit_47 = (prev_21[47], reg_21[47])
                sample.reg_21_bit_48 = (prev_21[48], reg_21[48])
                sample.reg_21_bit_49 = (prev_21[49], reg_21[49])
                sample.reg_21_bit_50 = (prev_21[50], reg_21[50])
                sample.reg_21_bit_51 = (prev_21[51], reg_21[51])
                sample.reg_21_bit_52 = (prev_21[52], reg_21[52])
                sample.reg_21_bit_53 = (prev_21[53], reg_21[53])
                sample.reg_21_bit_54 = (prev_21[54], reg_21[54])
                sample.reg_21_bit_55 = (prev_21[55], reg_21[55])
                sample.reg_21_bit_56 = (prev_21[56], reg_21[56])
                sample.reg_21_bit_57 = (prev_21[57], reg_21[57])
                sample.reg_21_bit_58 = (prev_21[58], reg_21[58])
                sample.reg_21_bit_59 = (prev_21[59], reg_21[59])
                sample.reg_21_bit_60 = (prev_21[60], reg_21[60])
                sample.reg_21_bit_61 = (prev_21[61], reg_21[61])
                sample.reg_21_bit_62 = (prev_21[62], reg_21[62])
                sample.reg_21_bit_63 = (prev_21[63], reg_21[63])
                sample.reg_20_bit_0 = (prev_20[0], reg_20[0])
                sample.reg_20_bit_1 = (prev_20[1], reg_20[1])
                sample.reg_20_bit_2 = (prev_20[2], reg_20[2])   
                sample.reg_20_bit_3 = (prev_20[3], reg_20[3])
                sample.reg_20_bit_4 = (prev_20[4], reg_20[4])               
                sample.reg_20_bit_5 = (prev_20[5], reg_20[5])
                sample.reg_20_bit_6 = (prev_20[6], reg_20[6])              
                sample.reg_20_bit_7 = (prev_20[7], reg_20[7])
                sample.reg_20_bit_8 = (prev_20[8], reg_20[8])
                sample.reg_20_bit_9 = (prev_20[9], reg_20[9])
                sample.reg_20_bit_10 = (prev_20[10], reg_20[10])
                sample.reg_20_bit_11 = (prev_20[11], reg_20[11])
                sample.reg_20_bit_12 = (prev_20[12], reg_20[12])
                sample.reg_20_bit_13 = (prev_20[13], reg_20[13])
                sample.reg_20_bit_14 = (prev_20[14], reg_20[14])
                sample.reg_20_bit_15 = (prev_20[15], reg_20[15])
                sample.reg_20_bit_16 = (prev_20[16], reg_20[16])
                sample.reg_20_bit_17 = (prev_20[17], reg_20[17])
                sample.reg_20_bit_18 = (prev_20[18], reg_20[18])
                sample.reg_20_bit_19 = (prev_20[19], reg_20[19])
                sample.reg_20_bit_20 = (prev_20[20], reg_20[20])
                sample.reg_20_bit_21 = (prev_20[21], reg_20[21])
                sample.reg_20_bit_22 = (prev_20[22], reg_20[22])
                sample.reg_20_bit_23 = (prev_20[23], reg_20[23])
                sample.reg_20_bit_24 = (prev_20[24], reg_20[24])
                sample.reg_20_bit_25 = (prev_20[25], reg_20[25])
                sample.reg_20_bit_26 = (prev_20[26], reg_20[26])
                sample.reg_20_bit_27 = (prev_20[27], reg_20[27])
                sample.reg_20_bit_28 = (prev_20[28], reg_20[28])
                sample.reg_20_bit_29 = (prev_20[29], reg_20[29])
                sample.reg_20_bit_30 = (prev_20[30], reg_20[30])
                sample.reg_20_bit_31 = (prev_20[31], reg_20[31])
                sample.reg_20_bit_32 = (prev_20[32], reg_20[32])
                sample.reg_20_bit_33 = (prev_20[33], reg_20[33])
                sample.reg_20_bit_34 = (prev_20[34], reg_20[34])
                sample.reg_20_bit_35 = (prev_20[35], reg_20[35])
                sample.reg_20_bit_36 = (prev_20[36], reg_20[36])
                sample.reg_20_bit_37 = (prev_20[37], reg_20[37])
                sample.reg_20_bit_38 = (prev_20[38], reg_20[38])
                sample.reg_20_bit_39 = (prev_20[39], reg_20[39])
                sample.reg_20_bit_40 = (prev_20[40], reg_20[40])
                sample.reg_20_bit_41 = (prev_20[41], reg_20[41])
                sample.reg_20_bit_42 = (prev_20[42], reg_20[42])
                sample.reg_20_bit_43 = (prev_20[43], reg_20[43])
                sample.reg_20_bit_44 = (prev_20[44], reg_20[44])
                sample.reg_20_bit_45 = (prev_20[45], reg_20[45])
                sample.reg_20_bit_46 = (prev_20[46], reg_20[46])
                sample.reg_20_bit_47 = (prev_20[47], reg_20[47])
                sample.reg_20_bit_48 = (prev_20[48], reg_20[48])
                sample.reg_20_bit_49 = (prev_20[49], reg_20[49])
                sample.reg_20_bit_50 = (prev_20[50], reg_20[50])
                sample.reg_20_bit_51 = (prev_20[51], reg_20[51])
                sample.reg_20_bit_52 = (prev_20[52], reg_20[52])
                sample.reg_20_bit_53 = (prev_20[53], reg_20[53])
                sample.reg_20_bit_54 = (prev_20[54], reg_20[54])
                sample.reg_20_bit_55 = (prev_20[55], reg_20[55])
                sample.reg_20_bit_56 = (prev_20[56], reg_20[56])
                sample.reg_20_bit_57 = (prev_20[57], reg_20[57])
                sample.reg_20_bit_58 = (prev_20[58], reg_20[58])
                sample.reg_20_bit_59 = (prev_20[59], reg_20[59])
                sample.reg_20_bit_60 = (prev_20[60], reg_20[60])
                sample.reg_20_bit_61 = (prev_20[61], reg_20[61])
                sample.reg_20_bit_62 = (prev_20[62], reg_20[62])
                sample.reg_20_bit_63 = (prev_20[63], reg_20[63])
                sample.reg_19_bit_0 = (prev_19[0], reg_19[0])
                sample.reg_19_bit_1 = (prev_19[1], reg_19[1])
                sample.reg_19_bit_2 = (prev_19[2], reg_19[2])
                sample.reg_19_bit_3 = (prev_19[3], reg_19[3])
                sample.reg_19_bit_4 = (prev_19[4], reg_19[4])
                sample.reg_19_bit_5 = (prev_19[5], reg_19[5])
                sample.reg_19_bit_6 = (prev_19[6], reg_19[6])
                sample.reg_19_bit_7 = (prev_19[7], reg_19[7])
                sample.reg_19_bit_8 = (prev_19[8], reg_19[8])
                sample.reg_19_bit_9 = (prev_19[9], reg_19[9])
                sample.reg_19_bit_10 = (prev_19[10], reg_19[10])
                sample.reg_19_bit_11 = (prev_19[11], reg_19[11])
                sample.reg_19_bit_12 = (prev_19[12], reg_19[12])
                sample.reg_19_bit_13 = (prev_19[13], reg_19[13])
                sample.reg_19_bit_14 = (prev_19[14], reg_19[14])
                sample.reg_19_bit_15 = (prev_19[15], reg_19[15])
                sample.reg_19_bit_16 = (prev_19[16], reg_19[16])
                sample.reg_19_bit_17 = (prev_19[17], reg_19[17])
                sample.reg_19_bit_18 = (prev_19[18], reg_19[18])
                sample.reg_19_bit_19 = (prev_19[19], reg_19[19])
                sample.reg_19_bit_20 = (prev_19[20], reg_19[20])
                sample.reg_19_bit_21 = (prev_19[21], reg_19[21])
                sample.reg_19_bit_22 = (prev_19[22], reg_19[22])
                sample.reg_19_bit_23 = (prev_19[23], reg_19[23])
                sample.reg_19_bit_24 = (prev_19[24], reg_19[24])
                sample.reg_19_bit_25 = (prev_19[25], reg_19[25])
                sample.reg_19_bit_26 = (prev_19[26], reg_19[26])
                sample.reg_19_bit_27 = (prev_19[27], reg_19[27])
                sample.reg_19_bit_28 = (prev_19[28], reg_19[28])
                sample.reg_19_bit_29 = (prev_19[29], reg_19[29])
                sample.reg_19_bit_30 = (prev_19[30], reg_19[30])
                sample.reg_19_bit_31 = (prev_19[31], reg_19[31])
                sample.reg_19_bit_32 = (prev_19[32], reg_19[32])
                sample.reg_19_bit_33 = (prev_19[33], reg_19[33])
                sample.reg_19_bit_34 = (prev_19[34], reg_19[34])
                sample.reg_19_bit_35 = (prev_19[35], reg_19[35])
                sample.reg_19_bit_36 = (prev_19[36], reg_19[36])
                sample.reg_19_bit_37 = (prev_19[37], reg_19[37])
                sample.reg_19_bit_38 = (prev_19[38], reg_19[38])
                sample.reg_19_bit_39 = (prev_19[39], reg_19[39])
                sample.reg_19_bit_40 = (prev_19[40], reg_19[40])
                sample.reg_19_bit_41 = (prev_19[41], reg_19[41])
                sample.reg_19_bit_42 = (prev_19[42], reg_19[42])
                sample.reg_19_bit_43 = (prev_19[43], reg_19[43])
                sample.reg_19_bit_44 = (prev_19[44], reg_19[44])
                sample.reg_19_bit_45 = (prev_19[45], reg_19[45])
                sample.reg_19_bit_46 = (prev_19[46], reg_19[46])
                sample.reg_19_bit_47 = (prev_19[47], reg_19[47])
                sample.reg_19_bit_48 = (prev_19[48], reg_19[48])
                sample.reg_19_bit_49 = (prev_19[49], reg_19[49])
                sample.reg_19_bit_50 = (prev_19[50], reg_19[50])
                sample.reg_19_bit_51 = (prev_19[51], reg_19[51])
                sample.reg_19_bit_52 = (prev_19[52], reg_19[52])
                sample.reg_19_bit_53 = (prev_19[53], reg_19[53])
                sample.reg_19_bit_54 = (prev_19[54], reg_19[54])
                sample.reg_19_bit_55 = (prev_19[55], reg_19[55])
                sample.reg_19_bit_56 = (prev_19[56], reg_19[56])
                sample.reg_19_bit_57 = (prev_19[57], reg_19[57])
                sample.reg_19_bit_58 = (prev_19[58], reg_19[58])
                sample.reg_19_bit_59 = (prev_19[59], reg_19[59])
                sample.reg_19_bit_60 = (prev_19[60], reg_19[60])
                sample.reg_19_bit_61 = (prev_19[61], reg_19[61])
                sample.reg_19_bit_62 = (prev_19[62], reg_19[62])
                sample.reg_19_bit_63 = (prev_19[63], reg_19[63])
                sample.reg_18_bit_0 = (prev_18[0], reg_18[0])
                sample.reg_18_bit_1 = (prev_18[1], reg_18[1])
                sample.reg_18_bit_2 = (prev_18[2], reg_18[2])
                sample.reg_18_bit_3 = (prev_18[3], reg_18[3])
                sample.reg_18_bit_4 = (prev_18[4], reg_18[4])
                sample.reg_18_bit_5 = (prev_18[5], reg_18[5])
                sample.reg_18_bit_6 = (prev_18[6], reg_18[6])
                sample.reg_18_bit_7 = (prev_18[7], reg_18[7])
                sample.reg_18_bit_8 = (prev_18[8], reg_18[8])
                sample.reg_18_bit_9 = (prev_18[9], reg_18[9])
                sample.reg_18_bit_10 = (prev_18[10], reg_18[10])
                sample.reg_18_bit_11 = (prev_18[11], reg_18[11])
                sample.reg_18_bit_12 = (prev_18[12], reg_18[12])
                sample.reg_18_bit_13 = (prev_18[13], reg_18[13])
                sample.reg_18_bit_14 = (prev_18[14], reg_18[14])
                sample.reg_18_bit_15 = (prev_18[15], reg_18[15])
                sample.reg_18_bit_16 = (prev_18[16], reg_18[16])
                sample.reg_18_bit_17 = (prev_18[17], reg_18[17])
                sample.reg_18_bit_18 = (prev_18[18], reg_18[18])
                sample.reg_18_bit_19 = (prev_18[19], reg_18[19])
                sample.reg_18_bit_20 = (prev_18[20], reg_18[20])
                sample.reg_18_bit_21 = (prev_18[21], reg_18[21])
                sample.reg_18_bit_22 = (prev_18[22], reg_18[22])
                sample.reg_18_bit_23 = (prev_18[23], reg_18[23])
                sample.reg_18_bit_24 = (prev_18[24], reg_18[24])
                sample.reg_18_bit_25 = (prev_18[25], reg_18[25])
                sample.reg_18_bit_26 = (prev_18[26], reg_18[26])
                sample.reg_18_bit_27 = (prev_18[27], reg_18[27])
                sample.reg_18_bit_28 = (prev_18[28], reg_18[28])
                sample.reg_18_bit_29 = (prev_18[29], reg_18[29])
                sample.reg_18_bit_30 = (prev_18[30], reg_18[30])
                sample.reg_18_bit_31 = (prev_18[31], reg_18[31])
                sample.reg_18_bit_32 = (prev_18[32], reg_18[32])
                sample.reg_18_bit_33 = (prev_18[33], reg_18[33])
                sample.reg_18_bit_34 = (prev_18[34], reg_18[34])
                sample.reg_18_bit_35 = (prev_18[35], reg_18[35])
                sample.reg_18_bit_36 = (prev_18[36], reg_18[36])
                sample.reg_18_bit_37 = (prev_18[37], reg_18[37])
                sample.reg_18_bit_38 = (prev_18[38], reg_18[38])
                sample.reg_18_bit_39 = (prev_18[39], reg_18[39])
                sample.reg_18_bit_40 = (prev_18[40], reg_18[40])
                sample.reg_18_bit_41 = (prev_18[41], reg_18[41])
                sample.reg_18_bit_42 = (prev_18[42], reg_18[42])
                sample.reg_18_bit_43 = (prev_18[43], reg_18[43])
                sample.reg_18_bit_44 = (prev_18[44], reg_18[44])
                sample.reg_18_bit_45 = (prev_18[45], reg_18[45])
                sample.reg_18_bit_46 = (prev_18[46], reg_18[46])
                sample.reg_18_bit_47 = (prev_18[47], reg_18[47])
                sample.reg_18_bit_48 = (prev_18[48], reg_18[48])
                sample.reg_18_bit_49 = (prev_18[49], reg_18[49])
                sample.reg_18_bit_50 = (prev_18[50], reg_18[50])
                sample.reg_18_bit_51 = (prev_18[51], reg_18[51])
                sample.reg_18_bit_52 = (prev_18[52], reg_18[52])
                sample.reg_18_bit_53 = (prev_18[53], reg_18[53])
                sample.reg_18_bit_54 = (prev_18[54], reg_18[54])
                sample.reg_18_bit_55 = (prev_18[55], reg_18[55])
                sample.reg_18_bit_56 = (prev_18[56], reg_18[56])
                sample.reg_18_bit_57 = (prev_18[57], reg_18[57])
                sample.reg_18_bit_58 = (prev_18[58], reg_18[58])
                sample.reg_18_bit_59 = (prev_18[59], reg_18[59])
                sample.reg_18_bit_60 = (prev_18[60], reg_18[60])
                sample.reg_18_bit_61 = (prev_18[61], reg_18[61])
                sample.reg_18_bit_62 = (prev_18[62], reg_18[62])
                sample.reg_18_bit_63 = (prev_18[63], reg_18[63])
                sample.reg_17_bit_0 = (prev_17[0], reg_17[0])
                sample.reg_17_bit_1 = (prev_17[1], reg_17[1])
                sample.reg_17_bit_2 = (prev_17[2], reg_17[2])
                sample.reg_17_bit_3 = (prev_17[3], reg_17[3])
                sample.reg_17_bit_4 = (prev_17[4], reg_17[4])
                sample.reg_17_bit_5 = (prev_17[5], reg_17[5])
                sample.reg_17_bit_6 = (prev_17[6], reg_17[6])
                sample.reg_17_bit_7 = (prev_17[7], reg_17[7])
                sample.reg_17_bit_8 = (prev_17[8], reg_17[8])
                sample.reg_17_bit_9 = (prev_17[9], reg_17[9])
                sample.reg_17_bit_10 = (prev_17[10], reg_17[10])
                sample.reg_17_bit_11 = (prev_17[11], reg_17[11])
                sample.reg_17_bit_12 = (prev_17[12], reg_17[12])
                sample.reg_17_bit_13 = (prev_17[13], reg_17[13])
                sample.reg_17_bit_14 = (prev_17[14], reg_17[14])
                sample.reg_17_bit_15 = (prev_17[15], reg_17[15])
                sample.reg_17_bit_16 = (prev_17[16], reg_17[16])
                sample.reg_17_bit_17 = (prev_17[17], reg_17[17])
                sample.reg_17_bit_18 = (prev_17[18], reg_17[18])
                sample.reg_17_bit_19 = (prev_17[19], reg_17[19])
                sample.reg_17_bit_20 = (prev_17[20], reg_17[20])
                sample.reg_17_bit_21 = (prev_17[21], reg_17[21])
                sample.reg_17_bit_22 = (prev_17[22], reg_17[22])
                sample.reg_17_bit_23 = (prev_17[23], reg_17[23])
                sample.reg_17_bit_24 = (prev_17[24], reg_17[24])
                sample.reg_17_bit_25 = (prev_17[25], reg_17[25])
                sample.reg_17_bit_26 = (prev_17[26], reg_17[26])
                sample.reg_17_bit_27 = (prev_17[27], reg_17[27])
                sample.reg_17_bit_28 = (prev_17[28], reg_17[28])
                sample.reg_17_bit_29 = (prev_17[29], reg_17[29])
                sample.reg_17_bit_30 = (prev_17[30], reg_17[30])
                sample.reg_17_bit_31 = (prev_17[31], reg_17[31])
                sample.reg_17_bit_32 = (prev_17[32], reg_17[32])
                sample.reg_17_bit_33 = (prev_17[33], reg_17[33])
                sample.reg_17_bit_34 = (prev_17[34], reg_17[34])
                sample.reg_17_bit_35 = (prev_17[35], reg_17[35])
                sample.reg_17_bit_36 = (prev_17[36], reg_17[36])
                sample.reg_17_bit_37 = (prev_17[37], reg_17[37])
                sample.reg_17_bit_38 = (prev_17[38], reg_17[38])
                sample.reg_17_bit_39 = (prev_17[39], reg_17[39])
                sample.reg_17_bit_40 = (prev_17[40], reg_17[40])
                sample.reg_17_bit_41 = (prev_17[41], reg_17[41])
                sample.reg_17_bit_42 = (prev_17[42], reg_17[42])
                sample.reg_17_bit_43 = (prev_17[43], reg_17[43])
                sample.reg_17_bit_44 = (prev_17[44], reg_17[44])
                sample.reg_17_bit_45 = (prev_17[45], reg_17[45])
                sample.reg_17_bit_46 = (prev_17[46], reg_17[46])
                sample.reg_17_bit_47 = (prev_17[47], reg_17[47])
                sample.reg_17_bit_48 = (prev_17[48], reg_17[48])
                sample.reg_17_bit_49 = (prev_17[49], reg_17[49])
                sample.reg_17_bit_50 = (prev_17[50], reg_17[50])
                sample.reg_17_bit_51 = (prev_17[51], reg_17[51])
                sample.reg_17_bit_52 = (prev_17[52], reg_17[52])
                sample.reg_17_bit_53 = (prev_17[53], reg_17[53])
                sample.reg_17_bit_54 = (prev_17[54], reg_17[54])
                sample.reg_17_bit_55 = (prev_17[55], reg_17[55])
                sample.reg_17_bit_56 = (prev_17[56], reg_17[56])
                sample.reg_17_bit_57 = (prev_17[57], reg_17[57])
                sample.reg_17_bit_58 = (prev_17[58], reg_17[58])
                sample.reg_17_bit_59 = (prev_17[59], reg_17[59])
                sample.reg_17_bit_60 = (prev_17[60], reg_17[60])
                sample.reg_17_bit_61 = (prev_17[61], reg_17[61])
                sample.reg_17_bit_62 = (prev_17[62], reg_17[62])
                sample.reg_17_bit_63 = (prev_17[63], reg_17[63])
                sample.reg_16_bit_0 = (prev_16[0], reg_16[0])
                sample.reg_16_bit_1 = (prev_16[1], reg_16[1])
                sample.reg_16_bit_2 = (prev_16[2], reg_16[2])
                sample.reg_16_bit_3 = (prev_16[3], reg_16[3])
                sample.reg_16_bit_4 = (prev_16[4], reg_16[4])
                sample.reg_16_bit_5 = (prev_16[5], reg_16[5])
                sample.reg_16_bit_6 = (prev_16[6], reg_16[6])
                sample.reg_16_bit_7 = (prev_16[7], reg_16[7])
                sample.reg_16_bit_8 = (prev_16[8], reg_16[8])
                sample.reg_16_bit_9 = (prev_16[9], reg_16[9])
                sample.reg_16_bit_10 = (prev_16[10], reg_16[10])
                sample.reg_16_bit_11 = (prev_16[11], reg_16[11])
                sample.reg_16_bit_12 = (prev_16[12], reg_16[12])
                sample.reg_16_bit_13 = (prev_16[13], reg_16[13])
                sample.reg_16_bit_14 = (prev_16[14], reg_16[14])
                sample.reg_16_bit_15 = (prev_16[15], reg_16[15])
                sample.reg_16_bit_16 = (prev_16[16], reg_16[16])
                sample.reg_16_bit_17 = (prev_16[17], reg_16[17])
                sample.reg_16_bit_18 = (prev_16[18], reg_16[18])
                sample.reg_16_bit_19 = (prev_16[19], reg_16[19])
                sample.reg_16_bit_20 = (prev_16[20], reg_16[20])
                sample.reg_16_bit_21 = (prev_16[21], reg_16[21])
                sample.reg_16_bit_22 = (prev_16[22], reg_16[22])
                sample.reg_16_bit_23 = (prev_16[23], reg_16[23])
                sample.reg_16_bit_24 = (prev_16[24], reg_16[24])
                sample.reg_16_bit_25 = (prev_16[25], reg_16[25])
                sample.reg_16_bit_26 = (prev_16[26], reg_16[26])
                sample.reg_16_bit_27 = (prev_16[27], reg_16[27])
                sample.reg_16_bit_28 = (prev_16[28], reg_16[28])
                sample.reg_16_bit_29 = (prev_16[29], reg_16[29])
                sample.reg_16_bit_30 = (prev_16[30], reg_16[30])
                sample.reg_16_bit_31 = (prev_16[31], reg_16[31])
                sample.reg_16_bit_32 = (prev_16[32], reg_16[32])
                sample.reg_16_bit_33 = (prev_16[33], reg_16[33])
                sample.reg_16_bit_34 = (prev_16[34], reg_16[34])
                sample.reg_16_bit_35 = (prev_16[35], reg_16[35])
                sample.reg_16_bit_36 = (prev_16[36], reg_16[36])
                sample.reg_16_bit_37 = (prev_16[37], reg_16[37])
                sample.reg_16_bit_38 = (prev_16[38], reg_16[38])
                sample.reg_16_bit_39 = (prev_16[39], reg_16[39])
                sample.reg_16_bit_40 = (prev_16[40], reg_16[40])
                sample.reg_16_bit_41 = (prev_16[41], reg_16[41])
                sample.reg_16_bit_42 = (prev_16[42], reg_16[42])
                sample.reg_16_bit_43 = (prev_16[43], reg_16[43])
                sample.reg_16_bit_44 = (prev_16[44], reg_16[44])
                sample.reg_16_bit_45 = (prev_16[45], reg_16[45])
                sample.reg_16_bit_46 = (prev_16[46], reg_16[46])
                sample.reg_16_bit_47 = (prev_16[47], reg_16[47])
                sample.reg_16_bit_48 = (prev_16[48], reg_16[48])
                sample.reg_16_bit_49 = (prev_16[49], reg_16[49])
                sample.reg_16_bit_50 = (prev_16[50], reg_16[50])
                sample.reg_16_bit_51 = (prev_16[51], reg_16[51])
                sample.reg_16_bit_52 = (prev_16[52], reg_16[52])
                sample.reg_16_bit_53 = (prev_16[53], reg_16[53])
                sample.reg_16_bit_54 = (prev_16[54], reg_16[54])
                sample.reg_16_bit_55 = (prev_16[55], reg_16[55])
                sample.reg_16_bit_56 = (prev_16[56], reg_16[56])
                sample.reg_16_bit_57 = (prev_16[57], reg_16[57])
                sample.reg_16_bit_58 = (prev_16[58], reg_16[58])
                sample.reg_16_bit_59 = (prev_16[59], reg_16[59])
                sample.reg_16_bit_60 = (prev_16[60], reg_16[60])
                sample.reg_16_bit_61 = (prev_16[61], reg_16[61])
                sample.reg_16_bit_62 = (prev_16[62], reg_16[62])
                sample.reg_16_bit_63 = (prev_16[63], reg_16[63])
                sample.reg_15_bit_0 = (prev_15[0], reg_15[0])
                sample.reg_15_bit_1 = (prev_15[1], reg_15[1])
                sample.reg_15_bit_2 = (prev_15[2], reg_15[2])
                sample.reg_15_bit_3 = (prev_15[3], reg_15[3])
                sample.reg_15_bit_4 = (prev_15[4], reg_15[4])
                sample.reg_15_bit_5 = (prev_15[5], reg_15[5])
                sample.reg_15_bit_6 = (prev_15[6], reg_15[6])
                sample.reg_15_bit_7 = (prev_15[7], reg_15[7])
                sample.reg_15_bit_8 = (prev_15[8], reg_15[8])
                sample.reg_15_bit_9 = (prev_15[9], reg_15[9])
                sample.reg_15_bit_10 = (prev_15[10], reg_15[10])
                sample.reg_15_bit_11 = (prev_15[11], reg_15[11])
                sample.reg_15_bit_12 = (prev_15[12], reg_15[12])
                sample.reg_15_bit_13 = (prev_15[13], reg_15[13])
                sample.reg_15_bit_14 = (prev_15[14], reg_15[14])
                sample.reg_15_bit_15 = (prev_15[15], reg_15[15])
                sample.reg_15_bit_16 = (prev_15[16], reg_15[16])
                sample.reg_15_bit_17 = (prev_15[17], reg_15[17])
                sample.reg_15_bit_18 = (prev_15[18], reg_15[18])
                sample.reg_15_bit_19 = (prev_15[19], reg_15[19])
                sample.reg_15_bit_20 = (prev_15[20], reg_15[20])
                sample.reg_15_bit_21 = (prev_15[21], reg_15[21])
                sample.reg_15_bit_22 = (prev_15[22], reg_15[22])
                sample.reg_15_bit_23 = (prev_15[23], reg_15[23])
                sample.reg_15_bit_24 = (prev_15[24], reg_15[24])
                sample.reg_15_bit_25 = (prev_15[25], reg_15[25])
                sample.reg_15_bit_26 = (prev_15[26], reg_15[26])
                sample.reg_15_bit_27 = (prev_15[27], reg_15[27])
                sample.reg_15_bit_28 = (prev_15[28], reg_15[28])
                sample.reg_15_bit_29 = (prev_15[29], reg_15[29])
                sample.reg_15_bit_30 = (prev_15[30], reg_15[30])
                sample.reg_15_bit_31 = (prev_15[31], reg_15[31])
                sample.reg_15_bit_32 = (prev_15[32], reg_15[32])
                sample.reg_15_bit_33 = (prev_15[33], reg_15[33])
                sample.reg_15_bit_34 = (prev_15[34], reg_15[34])
                sample.reg_15_bit_35 = (prev_15[35], reg_15[35])
                sample.reg_15_bit_36 = (prev_15[36], reg_15[36])
                sample.reg_15_bit_37 = (prev_15[37], reg_15[37])
                sample.reg_15_bit_38 = (prev_15[38], reg_15[38])
                sample.reg_15_bit_39 = (prev_15[39], reg_15[39])
                sample.reg_15_bit_40 = (prev_15[40], reg_15[40])
                sample.reg_15_bit_41 = (prev_15[41], reg_15[41])
                sample.reg_15_bit_42 = (prev_15[42], reg_15[42])
                sample.reg_15_bit_43 = (prev_15[43], reg_15[43])
                sample.reg_15_bit_44 = (prev_15[44], reg_15[44])
                sample.reg_15_bit_45 = (prev_15[45], reg_15[45])
                sample.reg_15_bit_46 = (prev_15[46], reg_15[46])
                sample.reg_15_bit_47 = (prev_15[47], reg_15[47])
                sample.reg_15_bit_48 = (prev_15[48], reg_15[48])
                sample.reg_15_bit_49 = (prev_15[49], reg_15[49])
                sample.reg_15_bit_50 = (prev_15[50], reg_15[50])
                sample.reg_15_bit_51 = (prev_15[51], reg_15[51])
                sample.reg_15_bit_52 = (prev_15[52], reg_15[52])
                sample.reg_15_bit_53 = (prev_15[53], reg_15[53])
                sample.reg_15_bit_54 = (prev_15[54], reg_15[54])
                sample.reg_15_bit_55 = (prev_15[55], reg_15[55])
                sample.reg_15_bit_56 = (prev_15[56], reg_15[56])
                sample.reg_15_bit_57 = (prev_15[57], reg_15[57])
                sample.reg_15_bit_58 = (prev_15[58], reg_15[58])
                sample.reg_15_bit_59 = (prev_15[59], reg_15[59])
                sample.reg_15_bit_60 = (prev_15[60], reg_15[60])
                sample.reg_15_bit_61 = (prev_15[61], reg_15[61])
                sample.reg_15_bit_62 = (prev_15[62], reg_15[62])
                sample.reg_15_bit_63 = (prev_15[63], reg_15[63])
                sample.reg_14_bit_0 = (prev_14[0], reg_14[0])
                sample.reg_14_bit_1 = (prev_14[1], reg_14[1])
                sample.reg_14_bit_2 = (prev_14[2], reg_14[2])
                sample.reg_14_bit_3 = (prev_14[3], reg_14[3])
                sample.reg_14_bit_4 = (prev_14[4], reg_14[4])
                sample.reg_14_bit_5 = (prev_14[5], reg_14[5])
                sample.reg_14_bit_6 = (prev_14[6], reg_14[6])
                sample.reg_14_bit_7 = (prev_14[7], reg_14[7])
                sample.reg_14_bit_8 = (prev_14[8], reg_14[8])
                sample.reg_14_bit_9 = (prev_14[9], reg_14[9])
                sample.reg_14_bit_10 = (prev_14[10], reg_14[10])
                sample.reg_14_bit_11 = (prev_14[11], reg_14[11])
                sample.reg_14_bit_12 = (prev_14[12], reg_14[12])
                sample.reg_14_bit_13 = (prev_14[13], reg_14[13])
                sample.reg_14_bit_14 = (prev_14[14], reg_14[14])
                sample.reg_14_bit_15 = (prev_14[15], reg_14[15])
                sample.reg_14_bit_16 = (prev_14[16], reg_14[16])
                sample.reg_14_bit_17 = (prev_14[17], reg_14[17])
                sample.reg_14_bit_18 = (prev_14[18], reg_14[18])
                sample.reg_14_bit_19 = (prev_14[19], reg_14[19])
                sample.reg_14_bit_20 = (prev_14[20], reg_14[20])
                sample.reg_14_bit_21 = (prev_14[21], reg_14[21])
                sample.reg_14_bit_22 = (prev_14[22], reg_14[22])
                sample.reg_14_bit_23 = (prev_14[23], reg_14[23])
                sample.reg_14_bit_24 = (prev_14[24], reg_14[24])
                sample.reg_14_bit_25 = (prev_14[25], reg_14[25])
                sample.reg_14_bit_26 = (prev_14[26], reg_14[26])
                sample.reg_14_bit_27 = (prev_14[27], reg_14[27])
                sample.reg_14_bit_28 = (prev_14[28], reg_14[28])
                sample.reg_14_bit_29 = (prev_14[29], reg_14[29])
                sample.reg_14_bit_30 = (prev_14[30], reg_14[30])
                sample.reg_14_bit_31 = (prev_14[31], reg_14[31])
                sample.reg_14_bit_32 = (prev_14[32], reg_14[32])
                sample.reg_14_bit_33 = (prev_14[33], reg_14[33])
                sample.reg_14_bit_34 = (prev_14[34], reg_14[34])
                sample.reg_14_bit_35 = (prev_14[35], reg_14[35])
                sample.reg_14_bit_36 = (prev_14[36], reg_14[36])
                sample.reg_14_bit_37 = (prev_14[37], reg_14[37])
                sample.reg_14_bit_38 = (prev_14[38], reg_14[38])
                sample.reg_14_bit_39 = (prev_14[39], reg_14[39])
                sample.reg_14_bit_40 = (prev_14[40], reg_14[40])
                sample.reg_14_bit_41 = (prev_14[41], reg_14[41])
                sample.reg_14_bit_42 = (prev_14[42], reg_14[42])
                sample.reg_14_bit_43 = (prev_14[43], reg_14[43])
                sample.reg_14_bit_44 = (prev_14[44], reg_14[44])
                sample.reg_14_bit_45 = (prev_14[45], reg_14[45])
                sample.reg_14_bit_46 = (prev_14[46], reg_14[46])
                sample.reg_14_bit_47 = (prev_14[47], reg_14[47])
                sample.reg_14_bit_48 = (prev_14[48], reg_14[48])
                sample.reg_14_bit_49 = (prev_14[49], reg_14[49])
                sample.reg_14_bit_50 = (prev_14[50], reg_14[50])
                sample.reg_14_bit_51 = (prev_14[51], reg_14[51])
                sample.reg_14_bit_52 = (prev_14[52], reg_14[52])
                sample.reg_14_bit_53 = (prev_14[53], reg_14[53])
                sample.reg_14_bit_54 = (prev_14[54], reg_14[54])
                sample.reg_14_bit_55 = (prev_14[55], reg_14[55])
                sample.reg_14_bit_56 = (prev_14[56], reg_14[56])
                sample.reg_14_bit_57 = (prev_14[57], reg_14[57])
                sample.reg_14_bit_58 = (prev_14[58], reg_14[58])
                sample.reg_14_bit_59 = (prev_14[59], reg_14[59])
                sample.reg_14_bit_60 = (prev_14[60], reg_14[60])
                sample.reg_14_bit_61 = (prev_14[61], reg_14[61])
                sample.reg_14_bit_62 = (prev_14[62], reg_14[62])
                sample.reg_14_bit_63 = (prev_14[63], reg_14[63])
                sample.reg_13_bit_0 = (prev_13[0], reg_13[0])
                sample.reg_13_bit_1 = (prev_13[1], reg_13[1])
                sample.reg_13_bit_2 = (prev_13[2], reg_13[2])
                sample.reg_13_bit_3 = (prev_13[3], reg_13[3])
                sample.reg_13_bit_4 = (prev_13[4], reg_13[4])
                sample.reg_13_bit_5 = (prev_13[5], reg_13[5])
                sample.reg_13_bit_6 = (prev_13[6], reg_13[6])
                sample.reg_13_bit_7 = (prev_13[7], reg_13[7])
                sample.reg_13_bit_8 = (prev_13[8], reg_13[8])
                sample.reg_13_bit_9 = (prev_13[9], reg_13[9])
                sample.reg_13_bit_10 = (prev_13[10], reg_13[10])
                sample.reg_13_bit_11 = (prev_13[11], reg_13[11])
                sample.reg_13_bit_12 = (prev_13[12], reg_13[12])
                sample.reg_13_bit_13 = (prev_13[13], reg_13[13])
                sample.reg_13_bit_14 = (prev_13[14], reg_13[14])
                sample.reg_13_bit_15 = (prev_13[15], reg_13[15])
                sample.reg_13_bit_16 = (prev_13[16], reg_13[16])
                sample.reg_13_bit_17 = (prev_13[17], reg_13[17])
                sample.reg_13_bit_18 = (prev_13[18], reg_13[18])
                sample.reg_13_bit_19 = (prev_13[19], reg_13[19])
                sample.reg_13_bit_20 = (prev_13[20], reg_13[20])
                sample.reg_13_bit_21 = (prev_13[21], reg_13[21])
                sample.reg_13_bit_22 = (prev_13[22], reg_13[22])
                sample.reg_13_bit_23 = (prev_13[23], reg_13[23])
                sample.reg_13_bit_24 = (prev_13[24], reg_13[24])
                sample.reg_13_bit_25 = (prev_13[25], reg_13[25])
                sample.reg_13_bit_26 = (prev_13[26], reg_13[26])
                sample.reg_13_bit_27 = (prev_13[27], reg_13[27])
                sample.reg_13_bit_28 = (prev_13[28], reg_13[28])
                sample.reg_13_bit_29 = (prev_13[29], reg_13[29])
                sample.reg_13_bit_30 = (prev_13[30], reg_13[30])
                sample.reg_13_bit_31 = (prev_13[31], reg_13[31])
                sample.reg_13_bit_32 = (prev_13[32], reg_13[32])
                sample.reg_13_bit_33 = (prev_13[33], reg_13[33])
                sample.reg_13_bit_34 = (prev_13[34], reg_13[34])
                sample.reg_13_bit_35 = (prev_13[35], reg_13[35])
                sample.reg_13_bit_36 = (prev_13[36], reg_13[36])
                sample.reg_13_bit_37 = (prev_13[37], reg_13[37])
                sample.reg_13_bit_38 = (prev_13[38], reg_13[38])
                sample.reg_13_bit_39 = (prev_13[39], reg_13[39])
                sample.reg_13_bit_40 = (prev_13[40], reg_13[40])
                sample.reg_13_bit_41 = (prev_13[41], reg_13[41])
                sample.reg_13_bit_42 = (prev_13[42], reg_13[42])
                sample.reg_13_bit_43 = (prev_13[43], reg_13[43])
                sample.reg_13_bit_44 = (prev_13[44], reg_13[44])
                sample.reg_13_bit_45 = (prev_13[45], reg_13[45])
                sample.reg_13_bit_46 = (prev_13[46], reg_13[46])
                sample.reg_13_bit_47 = (prev_13[47], reg_13[47])
                sample.reg_13_bit_48 = (prev_13[48], reg_13[48])
                sample.reg_13_bit_49 = (prev_13[49], reg_13[49])
                sample.reg_13_bit_50 = (prev_13[50], reg_13[50])
                sample.reg_13_bit_51 = (prev_13[51], reg_13[51])
                sample.reg_13_bit_52 = (prev_13[52], reg_13[52])
                sample.reg_13_bit_53 = (prev_13[53], reg_13[53])
                sample.reg_13_bit_54 = (prev_13[54], reg_13[54])
                sample.reg_13_bit_55 = (prev_13[55], reg_13[55])
                sample.reg_13_bit_56 = (prev_13[56], reg_13[56])
                sample.reg_13_bit_57 = (prev_13[57], reg_13[57])
                sample.reg_13_bit_58 = (prev_13[58], reg_13[58])
                sample.reg_13_bit_59 = (prev_13[59], reg_13[59])
                sample.reg_13_bit_60 = (prev_13[60], reg_13[60])
                sample.reg_13_bit_61 = (prev_13[61], reg_13[61])
                sample.reg_13_bit_62 = (prev_13[62], reg_13[62])
                sample.reg_13_bit_63 = (prev_13[63], reg_13[63])
                sample.reg_12_bit_0 = (prev_12[0], reg_12[0])
                sample.reg_12_bit_1 = (prev_12[1], reg_12[1])
                sample.reg_12_bit_2 = (prev_12[2], reg_12[2])
                sample.reg_12_bit_3 = (prev_12[3], reg_12[3])
                sample.reg_12_bit_4 = (prev_12[4], reg_12[4])
                sample.reg_12_bit_5 = (prev_12[5], reg_12[5])
                sample.reg_12_bit_6 = (prev_12[6], reg_12[6])
                sample.reg_12_bit_7 = (prev_12[7], reg_12[7])
                sample.reg_12_bit_8 = (prev_12[8], reg_12[8])
                sample.reg_12_bit_9 = (prev_12[9], reg_12[9])
                sample.reg_12_bit_10 = (prev_12[10], reg_12[10])
                sample.reg_12_bit_11 = (prev_12[11], reg_12[11])
                sample.reg_12_bit_12 = (prev_12[12], reg_12[12])
                sample.reg_12_bit_13 = (prev_12[13], reg_12[13])
                sample.reg_12_bit_14 = (prev_12[14], reg_12[14])
                sample.reg_12_bit_15 = (prev_12[15], reg_12[15])
                sample.reg_12_bit_16 = (prev_12[16], reg_12[16])
                sample.reg_12_bit_17 = (prev_12[17], reg_12[17])
                sample.reg_12_bit_18 = (prev_12[18], reg_12[18])
                sample.reg_12_bit_19 = (prev_12[19], reg_12[19])
                sample.reg_12_bit_20 = (prev_12[20], reg_12[20])
                sample.reg_12_bit_21 = (prev_12[21], reg_12[21])
                sample.reg_12_bit_22 = (prev_12[22], reg_12[22])
                sample.reg_12_bit_23 = (prev_12[23], reg_12[23])
                sample.reg_12_bit_24 = (prev_12[24], reg_12[24])
                sample.reg_12_bit_25 = (prev_12[25], reg_12[25])
                sample.reg_12_bit_26 = (prev_12[26], reg_12[26])
                sample.reg_12_bit_27 = (prev_12[27], reg_12[27])
                sample.reg_12_bit_28 = (prev_12[28], reg_12[28])
                sample.reg_12_bit_29 = (prev_12[29], reg_12[29])
                sample.reg_12_bit_30 = (prev_12[30], reg_12[30])
                sample.reg_12_bit_31 = (prev_12[31], reg_12[31])
                sample.reg_12_bit_32 = (prev_12[32], reg_12[32])
                sample.reg_12_bit_33 = (prev_12[33], reg_12[33])
                sample.reg_12_bit_34 = (prev_12[34], reg_12[34])
                sample.reg_12_bit_35 = (prev_12[35], reg_12[35])
                sample.reg_12_bit_36 = (prev_12[36], reg_12[36])
                sample.reg_12_bit_37 = (prev_12[37], reg_12[37])
                sample.reg_12_bit_38 = (prev_12[38], reg_12[38])
                sample.reg_12_bit_39 = (prev_12[39], reg_12[39])
                sample.reg_12_bit_40 = (prev_12[40], reg_12[40])
                sample.reg_12_bit_41 = (prev_12[41], reg_12[41])
                sample.reg_12_bit_42 = (prev_12[42], reg_12[42])
                sample.reg_12_bit_43 = (prev_12[43], reg_12[43])
                sample.reg_12_bit_44 = (prev_12[44], reg_12[44])
                sample.reg_12_bit_45 = (prev_12[45], reg_12[45])
                sample.reg_12_bit_46 = (prev_12[46], reg_12[46])
                sample.reg_12_bit_47 = (prev_12[47], reg_12[47])
                sample.reg_12_bit_48 = (prev_12[48], reg_12[48])
                sample.reg_12_bit_49 = (prev_12[49], reg_12[49])
                sample.reg_12_bit_50 = (prev_12[50], reg_12[50])
                sample.reg_12_bit_51 = (prev_12[51], reg_12[51])
                sample.reg_12_bit_52 = (prev_12[52], reg_12[52])
                sample.reg_12_bit_53 = (prev_12[53], reg_12[53])
                sample.reg_12_bit_54 = (prev_12[54], reg_12[54])
                sample.reg_12_bit_55 = (prev_12[55], reg_12[55])
                sample.reg_12_bit_56 = (prev_12[56], reg_12[56])
                sample.reg_12_bit_57 = (prev_12[57], reg_12[57])
                sample.reg_12_bit_58 = (prev_12[58], reg_12[58])
                sample.reg_12_bit_59 = (prev_12[59], reg_12[59])
                sample.reg_12_bit_60 = (prev_12[60], reg_12[60])
                sample.reg_12_bit_61 = (prev_12[61], reg_12[61])
                sample.reg_12_bit_62 = (prev_12[62], reg_12[62])
                sample.reg_12_bit_63 = (prev_12[63], reg_12[63])
                sample.reg_11_bit_0 = (prev_11[0], reg_11[0])
                sample.reg_11_bit_1 = (prev_11[1], reg_11[1])
                sample.reg_11_bit_2 = (prev_11[2], reg_11[2])
                sample.reg_11_bit_3 = (prev_11[3], reg_11[3])
                sample.reg_11_bit_4 = (prev_11[4], reg_11[4])
                sample.reg_11_bit_5 = (prev_11[5], reg_11[5])
                sample.reg_11_bit_6 = (prev_11[6], reg_11[6])
                sample.reg_11_bit_7 = (prev_11[7], reg_11[7])
                sample.reg_11_bit_8 = (prev_11[8], reg_11[8])
                sample.reg_11_bit_9 = (prev_11[9], reg_11[9])
                sample.reg_11_bit_10 = (prev_11[10], reg_11[10])
                sample.reg_11_bit_11 = (prev_11[11], reg_11[11])
                sample.reg_11_bit_12 = (prev_11[12], reg_11[12])
                sample.reg_11_bit_13 = (prev_11[13], reg_11[13])
                sample.reg_11_bit_14 = (prev_11[14], reg_11[14])
                sample.reg_11_bit_15 = (prev_11[15], reg_11[15])
                sample.reg_11_bit_16 = (prev_11[16], reg_11[16])
                sample.reg_11_bit_17 = (prev_11[17], reg_11[17])
                sample.reg_11_bit_18 = (prev_11[18], reg_11[18])
                sample.reg_11_bit_19 = (prev_11[19], reg_11[19])
                sample.reg_11_bit_20 = (prev_11[20], reg_11[20])
                sample.reg_11_bit_21 = (prev_11[21], reg_11[21])
                sample.reg_11_bit_22 = (prev_11[22], reg_11[22])
                sample.reg_11_bit_23 = (prev_11[23], reg_11[23])
                sample.reg_11_bit_24 = (prev_11[24], reg_11[24])
                sample.reg_11_bit_25 = (prev_11[25], reg_11[25])
                sample.reg_11_bit_26 = (prev_11[26], reg_11[26])
                sample.reg_11_bit_27 = (prev_11[27], reg_11[27])                
                sample.reg_11_bit_28 = (prev_11[28], reg_11[28])
                sample.reg_11_bit_29 = (prev_11[29], reg_11[29])
                sample.reg_11_bit_30 = (prev_11[30], reg_11[30])
                sample.reg_11_bit_31 = (prev_11[31], reg_11[31])
                sample.reg_11_bit_32 = (prev_11[32], reg_11[32])
                sample.reg_11_bit_33 = (prev_11[33], reg_11[33])
                sample.reg_11_bit_34 = (prev_11[34], reg_11[34])
                sample.reg_11_bit_35 = (prev_11[35], reg_11[35])
                sample.reg_11_bit_36 = (prev_11[36], reg_11[36])
                sample.reg_11_bit_37 = (prev_11[37], reg_11[37])
                sample.reg_11_bit_38 = (prev_11[38], reg_11[38])
                sample.reg_11_bit_39 = (prev_11[39], reg_11[39])
                sample.reg_11_bit_40 = (prev_11[40], reg_11[40])
                sample.reg_11_bit_41 = (prev_11[41], reg_11[41])
                sample.reg_11_bit_42 = (prev_11[42], reg_11[42])
                sample.reg_11_bit_43 = (prev_11[43], reg_11[43])
                sample.reg_11_bit_44 = (prev_11[44], reg_11[44])
                sample.reg_11_bit_45 = (prev_11[45], reg_11[45])
                sample.reg_11_bit_46 = (prev_11[46], reg_11[46])
                sample.reg_11_bit_47 = (prev_11[47], reg_11[47])
                sample.reg_11_bit_48 = (prev_11[48], reg_11[48])
                sample.reg_11_bit_49 = (prev_11[49], reg_11[49])
                sample.reg_11_bit_50 = (prev_11[50], reg_11[50])
                sample.reg_11_bit_51 = (prev_11[51], reg_11[51])
                sample.reg_11_bit_52 = (prev_11[52], reg_11[52])
                sample.reg_11_bit_53 = (prev_11[53], reg_11[53])
                sample.reg_11_bit_54 = (prev_11[54], reg_11[54])
                sample.reg_11_bit_55 = (prev_11[55], reg_11[55])
                sample.reg_11_bit_56 = (prev_11[56], reg_11[56])
                sample.reg_11_bit_57 = (prev_11[57], reg_11[57])
                sample.reg_11_bit_58 = (prev_11[58], reg_11[58])
                sample.reg_11_bit_59 = (prev_11[59], reg_11[59])
                sample.reg_11_bit_60 = (prev_11[60], reg_11[60])
                sample.reg_11_bit_61 = (prev_11[61], reg_11[61])
                sample.reg_11_bit_62 = (prev_11[62], reg_11[62])
                sample.reg_11_bit_63 = (prev_11[63], reg_11[63])     
                sample.reg_10_bit_0 = (prev_10[0], reg_10[0])
                sample.reg_10_bit_1 = (prev_10[1], reg_10[1])
                sample.reg_10_bit_2 = (prev_10[2], reg_10[2])
                sample.reg_10_bit_3 = (prev_10[3], reg_10[3])
                sample.reg_10_bit_4 = (prev_10[4], reg_10[4])
                sample.reg_10_bit_5 = (prev_10[5], reg_10[5])
                sample.reg_10_bit_6 = (prev_10[6], reg_10[6])
                sample.reg_10_bit_7 = (prev_10[7], reg_10[7])
                sample.reg_10_bit_8 = (prev_10[8], reg_10[8])
                sample.reg_10_bit_9 = (prev_10[9], reg_10[9])
                sample.reg_10_bit_10 = (prev_10[10], reg_10[10])
                sample.reg_10_bit_11 = (prev_10[11], reg_10[11])
                sample.reg_10_bit_12 = (prev_10[12], reg_10[12])
                sample.reg_10_bit_13 = (prev_10[13], reg_10[13])
                sample.reg_10_bit_14 = (prev_10[14], reg_10[14])
                sample.reg_10_bit_15 = (prev_10[15], reg_10[15])
                sample.reg_10_bit_16 = (prev_10[16], reg_10[16])
                sample.reg_10_bit_17 = (prev_10[17], reg_10[17])
                sample.reg_10_bit_18 = (prev_10[18], reg_10[18])
                sample.reg_10_bit_19 = (prev_10[19], reg_10[19])
                sample.reg_10_bit_20 = (prev_10[20], reg_10[20])
                sample.reg_10_bit_21 = (prev_10[21], reg_10[21])
                sample.reg_10_bit_22 = (prev_10[22], reg_10[22])
                sample.reg_10_bit_23 = (prev_10[23], reg_10[23])
                sample.reg_10_bit_24 = (prev_10[24], reg_10[24])
                sample.reg_10_bit_25 = (prev_10[25], reg_10[25])
                sample.reg_10_bit_26 = (prev_10[26], reg_10[26])
                sample.reg_10_bit_27 = (prev_10[27], reg_10[27])
                sample.reg_10_bit_28 = (prev_10[28], reg_10[28])
                sample.reg_10_bit_29 = (prev_10[29], reg_10[29])
                sample.reg_10_bit_30 = (prev_10[30], reg_10[30])
                sample.reg_10_bit_31 = (prev_10[31], reg_10[31])
                sample.reg_10_bit_32 = (prev_10[32], reg_10[32])
                sample.reg_10_bit_33 = (prev_10[33], reg_10[33])
                sample.reg_10_bit_34 = (prev_10[34], reg_10[34])
                sample.reg_10_bit_35 = (prev_10[35], reg_10[35])
                sample.reg_10_bit_36 = (prev_10[36], reg_10[36])
                sample.reg_10_bit_37 = (prev_10[37], reg_10[37])
                sample.reg_10_bit_38 = (prev_10[38], reg_10[38])
                sample.reg_10_bit_39 = (prev_10[39], reg_10[39])
                sample.reg_10_bit_40 = (prev_10[40], reg_10[40])
                sample.reg_10_bit_41 = (prev_10[41], reg_10[41])
                sample.reg_10_bit_42 = (prev_10[42], reg_10[42])
                sample.reg_10_bit_43 = (prev_10[43], reg_10[43])
                sample.reg_10_bit_44 = (prev_10[44], reg_10[44])
                sample.reg_10_bit_45 = (prev_10[45], reg_10[45])
                sample.reg_10_bit_46 = (prev_10[46], reg_10[46])
                sample.reg_10_bit_47 = (prev_10[47], reg_10[47])
                sample.reg_10_bit_48 = (prev_10[48], reg_10[48])
                sample.reg_10_bit_49 = (prev_10[49], reg_10[49])
                sample.reg_10_bit_50 = (prev_10[50], reg_10[50])
                sample.reg_10_bit_51 = (prev_10[51], reg_10[51])
                sample.reg_10_bit_52 = (prev_10[52], reg_10[52])
                sample.reg_10_bit_53 = (prev_10[53], reg_10[53])
                sample.reg_10_bit_54 = (prev_10[54], reg_10[54])
                sample.reg_10_bit_55 = (prev_10[55], reg_10[55])
                sample.reg_10_bit_56 = (prev_10[56], reg_10[56])
                sample.reg_10_bit_57 = (prev_10[57], reg_10[57])
                sample.reg_10_bit_58 = (prev_10[58], reg_10[58])
                sample.reg_10_bit_59 = (prev_10[59], reg_10[59])
                sample.reg_10_bit_60 = (prev_10[60], reg_10[60])
                sample.reg_10_bit_61 = (prev_10[61], reg_10[61])
                sample.reg_10_bit_62 = (prev_10[62], reg_10[62])
                sample.reg_10_bit_63 = (prev_10[63], reg_10[63])
                sample.reg_9_bit_0 = (prev_9[0], reg_9[0])
                sample.reg_9_bit_1 = (prev_9[1], reg_9[1])
                sample.reg_9_bit_2 = (prev_9[2], reg_9[2])
                sample.reg_9_bit_3 = (prev_9[3], reg_9[3])
                sample.reg_9_bit_4 = (prev_9[4], reg_9[4])
                sample.reg_9_bit_5 = (prev_9[5], reg_9[5])
                sample.reg_9_bit_6 = (prev_9[6], reg_9[6])
                sample.reg_9_bit_7 = (prev_9[7], reg_9[7])
                sample.reg_9_bit_8 = (prev_9[8], reg_9[8])
                sample.reg_9_bit_9 = (prev_9[9], reg_9[9])
                sample.reg_9_bit_10 = (prev_9[10], reg_9[10])
                sample.reg_9_bit_11 = (prev_9[11], reg_9[11])
                sample.reg_9_bit_12 = (prev_9[12], reg_9[12])
                sample.reg_9_bit_13 = (prev_9[13], reg_9[13])
                sample.reg_9_bit_14 = (prev_9[14], reg_9[14])
                sample.reg_9_bit_15 = (prev_9[15], reg_9[15])
                sample.reg_9_bit_16 = (prev_9[16], reg_9[16])
                sample.reg_9_bit_17 = (prev_9[17], reg_9[17])
                sample.reg_9_bit_18 = (prev_9[18], reg_9[18])
                sample.reg_9_bit_19 = (prev_9[19], reg_9[19])
                sample.reg_9_bit_20 = (prev_9[20], reg_9[20])
                sample.reg_9_bit_21 = (prev_9[21], reg_9[21])
                sample.reg_9_bit_22 = (prev_9[22], reg_9[22])
                sample.reg_9_bit_23 = (prev_9[23], reg_9[23])
                sample.reg_9_bit_24 = (prev_9[24], reg_9[24])
                sample.reg_9_bit_25 = (prev_9[25], reg_9[25])
                sample.reg_9_bit_26 = (prev_9[26], reg_9[26])
                sample.reg_9_bit_27 = (prev_9[27], reg_9[27])
                sample.reg_9_bit_28 = (prev_9[28], reg_9[28])
                sample.reg_9_bit_29 = (prev_9[29], reg_9[29])
                sample.reg_9_bit_30 = (prev_9[30], reg_9[30])
                sample.reg_9_bit_31 = (prev_9[31], reg_9[31])
                sample.reg_9_bit_32 = (prev_9[32], reg_9[32])
                sample.reg_9_bit_33 = (prev_9[33], reg_9[33])
                sample.reg_9_bit_34 = (prev_9[34], reg_9[34])
                sample.reg_9_bit_35 = (prev_9[35], reg_9[35])
                sample.reg_9_bit_36 = (prev_9[36], reg_9[36])
                sample.reg_9_bit_37 = (prev_9[37], reg_9[37])
                sample.reg_9_bit_38 = (prev_9[38], reg_9[38])
                sample.reg_9_bit_39 = (prev_9[39], reg_9[39])
                sample.reg_9_bit_40 = (prev_9[40], reg_9[40])
                sample.reg_9_bit_41 = (prev_9[41], reg_9[41])
                sample.reg_9_bit_42 = (prev_9[42], reg_9[42])
                sample.reg_9_bit_43 = (prev_9[43], reg_9[43])
                sample.reg_9_bit_44 = (prev_9[44], reg_9[44])
                sample.reg_9_bit_45 = (prev_9[45], reg_9[45])
                sample.reg_9_bit_46 = (prev_9[46], reg_9[46])
                sample.reg_9_bit_47 = (prev_9[47], reg_9[47])
                sample.reg_9_bit_48 = (prev_9[48], reg_9[48])
                sample.reg_9_bit_49 = (prev_9[49], reg_9[49])
                sample.reg_9_bit_50 = (prev_9[50], reg_9[50])
                sample.reg_9_bit_51 = (prev_9[51], reg_9[51])
                sample.reg_9_bit_52 = (prev_9[52], reg_9[52])
                sample.reg_9_bit_53 = (prev_9[53], reg_9[53])
                sample.reg_9_bit_54 = (prev_9[54], reg_9[54])
                sample.reg_9_bit_55 = (prev_9[55], reg_9[55])
                sample.reg_9_bit_56 = (prev_9[56], reg_9[56])
                sample.reg_9_bit_57 = (prev_9[57], reg_9[57])
                sample.reg_9_bit_58 = (prev_9[58], reg_9[58])
                sample.reg_9_bit_59 = (prev_9[59], reg_9[59])
                sample.reg_9_bit_60 = (prev_9[60], reg_9[60])
                sample.reg_9_bit_61 = (prev_9[61], reg_9[61])
                sample.reg_9_bit_62 = (prev_9[62], reg_9[62])
                sample.reg_9_bit_63 = (prev_9[63], reg_9[63])
                sample.reg_8_bit_0 = (prev_8[0], reg_8[0])
                sample.reg_8_bit_1 = (prev_8[1], reg_8[1])
                sample.reg_8_bit_2 = (prev_8[2], reg_8[2])
                sample.reg_8_bit_3 = (prev_8[3], reg_8[3])
                sample.reg_8_bit_4 = (prev_8[4], reg_8[4])
                sample.reg_8_bit_5 = (prev_8[5], reg_8[5])
                sample.reg_8_bit_6 = (prev_8[6], reg_8[6])
                sample.reg_8_bit_7 = (prev_8[7], reg_8[7])
                sample.reg_8_bit_8 = (prev_8[8], reg_8[8])
                sample.reg_8_bit_9 = (prev_8[9], reg_8[9])
                sample.reg_8_bit_10 = (prev_8[10], reg_8[10])
                sample.reg_8_bit_11 = (prev_8[11], reg_8[11])
                sample.reg_8_bit_12 = (prev_8[12], reg_8[12])
                sample.reg_8_bit_13 = (prev_8[13], reg_8[13])
                sample.reg_8_bit_14 = (prev_8[14], reg_8[14])
                sample.reg_8_bit_15 = (prev_8[15], reg_8[15])
                sample.reg_8_bit_16 = (prev_8[16], reg_8[16])
                sample.reg_8_bit_17 = (prev_8[17], reg_8[17])
                sample.reg_8_bit_18 = (prev_8[18], reg_8[18])
                sample.reg_8_bit_19 = (prev_8[19], reg_8[19])
                sample.reg_8_bit_20 = (prev_8[20], reg_8[20])
                sample.reg_8_bit_21 = (prev_8[21], reg_8[21])
                sample.reg_8_bit_22 = (prev_8[22], reg_8[22])
                sample.reg_8_bit_23 = (prev_8[23], reg_8[23])
                sample.reg_8_bit_24 = (prev_8[24], reg_8[24])
                sample.reg_8_bit_25 = (prev_8[25], reg_8[25])
                sample.reg_8_bit_26 = (prev_8[26], reg_8[26])
                sample.reg_8_bit_27 = (prev_8[27], reg_8[27])
                sample.reg_8_bit_28 = (prev_8[28], reg_8[28])
                sample.reg_8_bit_29 = (prev_8[29], reg_8[29])
                sample.reg_8_bit_30 = (prev_8[30], reg_8[30])
                sample.reg_8_bit_31 = (prev_8[31], reg_8[31])
                sample.reg_8_bit_32 = (prev_8[32], reg_8[32])
                sample.reg_8_bit_33 = (prev_8[33], reg_8[33])
                sample.reg_8_bit_34 = (prev_8[34], reg_8[34])
                sample.reg_8_bit_35 = (prev_8[35], reg_8[35])
                sample.reg_8_bit_36 = (prev_8[36], reg_8[36])
                sample.reg_8_bit_37 = (prev_8[37], reg_8[37])
                sample.reg_8_bit_38 = (prev_8[38], reg_8[38])
                sample.reg_8_bit_39 = (prev_8[39], reg_8[39])
                sample.reg_8_bit_40 = (prev_8[40], reg_8[40])
                sample.reg_8_bit_41 = (prev_8[41], reg_8[41])
                sample.reg_8_bit_42 = (prev_8[42], reg_8[42])
                sample.reg_8_bit_43 = (prev_8[43], reg_8[43])
                sample.reg_8_bit_44 = (prev_8[44], reg_8[44])
                sample.reg_8_bit_45 = (prev_8[45], reg_8[45])
                sample.reg_8_bit_46 = (prev_8[46], reg_8[46])
                sample.reg_8_bit_47 = (prev_8[47], reg_8[47])
                sample.reg_8_bit_48 = (prev_8[48], reg_8[48])
                sample.reg_8_bit_49 = (prev_8[49], reg_8[49])
                sample.reg_8_bit_50 = (prev_8[50], reg_8[50])
                sample.reg_8_bit_51 = (prev_8[51], reg_8[51])
                sample.reg_8_bit_52 = (prev_8[52], reg_8[52])
                sample.reg_8_bit_53 = (prev_8[53], reg_8[53])
                sample.reg_8_bit_54 = (prev_8[54], reg_8[54])
                sample.reg_8_bit_55 = (prev_8[55], reg_8[55])
                sample.reg_8_bit_56 = (prev_8[56], reg_8[56])
                sample.reg_8_bit_57 = (prev_8[57], reg_8[57])
                sample.reg_8_bit_58 = (prev_8[58], reg_8[58])
                sample.reg_8_bit_59 = (prev_8[59], reg_8[59])
                sample.reg_8_bit_60 = (prev_8[60], reg_8[60])
                sample.reg_8_bit_61 = (prev_8[61], reg_8[61])
                sample.reg_8_bit_62 = (prev_8[62], reg_8[62])
                sample.reg_8_bit_63 = (prev_8[63], reg_8[63])
                sample.reg_7_bit_0 = (prev_7[0], reg_7[0])
                sample.reg_7_bit_1 = (prev_7[1], reg_7[1])
                sample.reg_7_bit_2 = (prev_7[2], reg_7[2])
                sample.reg_7_bit_3 = (prev_7[3], reg_7[3])
                sample.reg_7_bit_4 = (prev_7[4], reg_7[4])
                sample.reg_7_bit_5 = (prev_7[5], reg_7[5])
                sample.reg_7_bit_6 = (prev_7[6], reg_7[6])
                sample.reg_7_bit_7 = (prev_7[7], reg_7[7])
                sample.reg_7_bit_8 = (prev_7[8], reg_7[8])
                sample.reg_7_bit_9 = (prev_7[9], reg_7[9])
                sample.reg_7_bit_10 = (prev_7[10], reg_7[10])
                sample.reg_7_bit_11 = (prev_7[11], reg_7[11])
                sample.reg_7_bit_12 = (prev_7[12], reg_7[12])
                sample.reg_7_bit_13 = (prev_7[13], reg_7[13])
                sample.reg_7_bit_14 = (prev_7[14], reg_7[14])
                sample.reg_7_bit_15 = (prev_7[15], reg_7[15])
                sample.reg_7_bit_16 = (prev_7[16], reg_7[16])
                sample.reg_7_bit_17 = (prev_7[17], reg_7[17])
                sample.reg_7_bit_18 = (prev_7[18], reg_7[18])
                sample.reg_7_bit_19 = (prev_7[19], reg_7[19])
                sample.reg_7_bit_20 = (prev_7[20], reg_7[20])
                sample.reg_7_bit_21 = (prev_7[21], reg_7[21])
                sample.reg_7_bit_22 = (prev_7[22], reg_7[22])
                sample.reg_7_bit_23 = (prev_7[23], reg_7[23])
                sample.reg_7_bit_24 = (prev_7[24], reg_7[24])
                sample.reg_7_bit_25 = (prev_7[25], reg_7[25])
                sample.reg_7_bit_26 = (prev_7[26], reg_7[26])
                sample.reg_7_bit_27 = (prev_7[27], reg_7[27])
                sample.reg_7_bit_28 = (prev_7[28], reg_7[28])
                sample.reg_7_bit_29 = (prev_7[29], reg_7[29])
                sample.reg_7_bit_30 = (prev_7[30], reg_7[30])
                sample.reg_7_bit_31 = (prev_7[31], reg_7[31])
                sample.reg_7_bit_32 = (prev_7[32], reg_7[32])
                sample.reg_7_bit_33 = (prev_7[33], reg_7[33])
                sample.reg_7_bit_34 = (prev_7[34], reg_7[34])
                sample.reg_7_bit_35 = (prev_7[35], reg_7[35])
                sample.reg_7_bit_36 = (prev_7[36], reg_7[36])
                sample.reg_7_bit_37 = (prev_7[37], reg_7[37])
                sample.reg_7_bit_38 = (prev_7[38], reg_7[38])
                sample.reg_7_bit_39 = (prev_7[39], reg_7[39])
                sample.reg_7_bit_40 = (prev_7[40], reg_7[40])
                sample.reg_7_bit_41 = (prev_7[41], reg_7[41])
                sample.reg_7_bit_42 = (prev_7[42], reg_7[42])
                sample.reg_7_bit_43 = (prev_7[43], reg_7[43])
                sample.reg_7_bit_44 = (prev_7[44], reg_7[44])
                sample.reg_7_bit_45 = (prev_7[45], reg_7[45])
                sample.reg_7_bit_46 = (prev_7[46], reg_7[46])
                sample.reg_7_bit_47 = (prev_7[47], reg_7[47])
                sample.reg_7_bit_48 = (prev_7[48], reg_7[48])
                sample.reg_7_bit_49 = (prev_7[49], reg_7[49])
                sample.reg_7_bit_50 = (prev_7[50], reg_7[50])
                sample.reg_7_bit_51 = (prev_7[51], reg_7[51])
                sample.reg_7_bit_52 = (prev_7[52], reg_7[52])
                sample.reg_7_bit_53 = (prev_7[53], reg_7[53])
                sample.reg_7_bit_54 = (prev_7[54], reg_7[54])
                sample.reg_7_bit_55 = (prev_7[55], reg_7[55])
                sample.reg_7_bit_56 = (prev_7[56], reg_7[56])
                sample.reg_7_bit_57 = (prev_7[57], reg_7[57])
                sample.reg_7_bit_58 = (prev_7[58], reg_7[58])
                sample.reg_7_bit_59 = (prev_7[59], reg_7[59])
                sample.reg_7_bit_60 = (prev_7[60], reg_7[60])
                sample.reg_7_bit_61 = (prev_7[61], reg_7[61])
                sample.reg_7_bit_62 = (prev_7[62], reg_7[62])
                sample.reg_7_bit_63 = (prev_7[63], reg_7[63])
                sample.reg_6_bit_0 = (prev_6[0], reg_6[0])
                sample.reg_6_bit_1 = (prev_6[1], reg_6[1])
                sample.reg_6_bit_2 = (prev_6[2], reg_6[2])
                sample.reg_6_bit_3 = (prev_6[3], reg_6[3])
                sample.reg_6_bit_4 = (prev_6[4], reg_6[4])
                sample.reg_6_bit_5 = (prev_6[5], reg_6[5])
                sample.reg_6_bit_6 = (prev_6[6], reg_6[6])
                sample.reg_6_bit_7 = (prev_6[7], reg_6[7])
                sample.reg_6_bit_8 = (prev_6[8], reg_6[8])
                sample.reg_6_bit_9 = (prev_6[9], reg_6[9])
                sample.reg_6_bit_10 = (prev_6[10], reg_6[10])
                sample.reg_6_bit_11 = (prev_6[11], reg_6[11])
                sample.reg_6_bit_12 = (prev_6[12], reg_6[12])
                sample.reg_6_bit_13 = (prev_6[13], reg_6[13])
                sample.reg_6_bit_14 = (prev_6[14], reg_6[14])
                sample.reg_6_bit_15 = (prev_6[15], reg_6[15])
                sample.reg_6_bit_16 = (prev_6[16], reg_6[16])
                sample.reg_6_bit_17 = (prev_6[17], reg_6[17])
                sample.reg_6_bit_18 = (prev_6[18], reg_6[18])
                sample.reg_6_bit_19 = (prev_6[19], reg_6[19])
                sample.reg_6_bit_20 = (prev_6[20], reg_6[20])
                sample.reg_6_bit_21 = (prev_6[21], reg_6[21])
                sample.reg_6_bit_22 = (prev_6[22], reg_6[22])
                sample.reg_6_bit_23 = (prev_6[23], reg_6[23])
                sample.reg_6_bit_24 = (prev_6[24], reg_6[24])
                sample.reg_6_bit_25 = (prev_6[25], reg_6[25])
                sample.reg_6_bit_26 = (prev_6[26], reg_6[26])
                sample.reg_6_bit_27 = (prev_6[27], reg_6[27])
                sample.reg_6_bit_28 = (prev_6[28], reg_6[28])
                sample.reg_6_bit_29 = (prev_6[29], reg_6[29])
                sample.reg_6_bit_30 = (prev_6[30], reg_6[30])
                sample.reg_6_bit_31 = (prev_6[31], reg_6[31])
                sample.reg_6_bit_32 = (prev_6[32], reg_6[32])
                sample.reg_6_bit_33 = (prev_6[33], reg_6[33])
                sample.reg_6_bit_34 = (prev_6[34], reg_6[34])
                sample.reg_6_bit_35 = (prev_6[35], reg_6[35])
                sample.reg_6_bit_36 = (prev_6[36], reg_6[36])
                sample.reg_6_bit_37 = (prev_6[37], reg_6[37])
                sample.reg_6_bit_38 = (prev_6[38], reg_6[38])
                sample.reg_6_bit_39 = (prev_6[39], reg_6[39])
                sample.reg_6_bit_40 = (prev_6[40], reg_6[40])
                sample.reg_6_bit_41 = (prev_6[41], reg_6[41])
                sample.reg_6_bit_42 = (prev_6[42], reg_6[42])
                sample.reg_6_bit_43 = (prev_6[43], reg_6[43])
                sample.reg_6_bit_44 = (prev_6[44], reg_6[44])
                sample.reg_6_bit_45 = (prev_6[45], reg_6[45])
                sample.reg_6_bit_46 = (prev_6[46], reg_6[46])
                sample.reg_6_bit_47 = (prev_6[47], reg_6[47])
                sample.reg_6_bit_48 = (prev_6[48], reg_6[48])
                sample.reg_6_bit_49 = (prev_6[49], reg_6[49])
                sample.reg_6_bit_50 = (prev_6[50], reg_6[50])
                sample.reg_6_bit_51 = (prev_6[51], reg_6[51])
                sample.reg_6_bit_52 = (prev_6[52], reg_6[52])
                sample.reg_6_bit_53 = (prev_6[53], reg_6[53])
                sample.reg_6_bit_54 = (prev_6[54], reg_6[54])
                sample.reg_6_bit_55 = (prev_6[55], reg_6[55])
                sample.reg_6_bit_56 = (prev_6[56], reg_6[56])
                sample.reg_6_bit_57 = (prev_6[57], reg_6[57])
                sample.reg_6_bit_58 = (prev_6[58], reg_6[58])
                sample.reg_6_bit_59 = (prev_6[59], reg_6[59])
                sample.reg_6_bit_60 = (prev_6[60], reg_6[60])
                sample.reg_6_bit_61 = (prev_6[61], reg_6[61])
                sample.reg_6_bit_62 = (prev_6[62], reg_6[62])
                sample.reg_6_bit_63 = (prev_6[63], reg_6[63])
                sample.reg_5_bit_0 = (prev_5[0], reg_5[0])
                sample.reg_5_bit_1 = (prev_5[1], reg_5[1])
                sample.reg_5_bit_2 = (prev_5[2], reg_5[2])
                sample.reg_5_bit_3 = (prev_5[3], reg_5[3])
                sample.reg_5_bit_4 = (prev_5[4], reg_5[4])
                sample.reg_5_bit_5 = (prev_5[5], reg_5[5])
                sample.reg_5_bit_6 = (prev_5[6], reg_5[6])
                sample.reg_5_bit_7 = (prev_5[7], reg_5[7])
                sample.reg_5_bit_8 = (prev_5[8], reg_5[8])
                sample.reg_5_bit_9 = (prev_5[9], reg_5[9])
                sample.reg_5_bit_10 = (prev_5[10], reg_5[10])
                sample.reg_5_bit_11 = (prev_5[11], reg_5[11])
                sample.reg_5_bit_12 = (prev_5[12], reg_5[12])
                sample.reg_5_bit_13 = (prev_5[13], reg_5[13])
                sample.reg_5_bit_14 = (prev_5[14], reg_5[14])
                sample.reg_5_bit_15 = (prev_5[15], reg_5[15])
                sample.reg_5_bit_16 = (prev_5[16], reg_5[16])
                sample.reg_5_bit_17 = (prev_5[17], reg_5[17])
                sample.reg_5_bit_18 = (prev_5[18], reg_5[18])
                sample.reg_5_bit_19 = (prev_5[19], reg_5[19])
                sample.reg_5_bit_20 = (prev_5[20], reg_5[20])
                sample.reg_5_bit_21 = (prev_5[21], reg_5[21])
                sample.reg_5_bit_22 = (prev_5[22], reg_5[22])
                sample.reg_5_bit_23 = (prev_5[23], reg_5[23])
                sample.reg_5_bit_24 = (prev_5[24], reg_5[24])
                sample.reg_5_bit_25 = (prev_5[25], reg_5[25])
                sample.reg_5_bit_26 = (prev_5[26], reg_5[26])
                sample.reg_5_bit_27 = (prev_5[27], reg_5[27])
                sample.reg_5_bit_28 = (prev_5[28], reg_5[28])
                sample.reg_5_bit_29 = (prev_5[29], reg_5[29])
                sample.reg_5_bit_30 = (prev_5[30], reg_5[30])
                sample.reg_5_bit_31 = (prev_5[31], reg_5[31])
                sample.reg_5_bit_32 = (prev_5[32], reg_5[32])
                sample.reg_5_bit_33 = (prev_5[33], reg_5[33])
                sample.reg_5_bit_34 = (prev_5[34], reg_5[34])
                sample.reg_5_bit_35 = (prev_5[35], reg_5[35])
                sample.reg_5_bit_36 = (prev_5[36], reg_5[36])
                sample.reg_5_bit_37 = (prev_5[37], reg_5[37])
                sample.reg_5_bit_38 = (prev_5[38], reg_5[38])
                sample.reg_5_bit_39 = (prev_5[39], reg_5[39])
                sample.reg_5_bit_40 = (prev_5[40], reg_5[40])
                sample.reg_5_bit_41 = (prev_5[41], reg_5[41])
                sample.reg_5_bit_42 = (prev_5[42], reg_5[42])
                sample.reg_5_bit_43 = (prev_5[43], reg_5[43])
                sample.reg_5_bit_44 = (prev_5[44], reg_5[44])
                sample.reg_5_bit_45 = (prev_5[45], reg_5[45])
                sample.reg_5_bit_46 = (prev_5[46], reg_5[46])
                sample.reg_5_bit_47 = (prev_5[47], reg_5[47])
                sample.reg_5_bit_48 = (prev_5[48], reg_5[48])
                sample.reg_5_bit_49 = (prev_5[49], reg_5[49])
                sample.reg_5_bit_50 = (prev_5[50], reg_5[50])
                sample.reg_5_bit_51 = (prev_5[51], reg_5[51])
                sample.reg_5_bit_52 = (prev_5[52], reg_5[52])
                sample.reg_5_bit_53 = (prev_5[53], reg_5[53])
                sample.reg_5_bit_54 = (prev_5[54], reg_5[54])
                sample.reg_5_bit_55 = (prev_5[55], reg_5[55])
                sample.reg_5_bit_56 = (prev_5[56], reg_5[56])
                sample.reg_5_bit_57 = (prev_5[57], reg_5[57])
                sample.reg_5_bit_58 = (prev_5[58], reg_5[58])
                sample.reg_5_bit_59 = (prev_5[59], reg_5[59])
                sample.reg_5_bit_60 = (prev_5[60], reg_5[60])
                sample.reg_5_bit_61 = (prev_5[61], reg_5[61])
                sample.reg_5_bit_62 = (prev_5[62], reg_5[62])
                sample.reg_5_bit_63 = (prev_5[63], reg_5[63])
                sample.reg_4_bit_0 = (prev_4[0], reg_4[0])
                sample.reg_4_bit_1 = (prev_4[1], reg_4[1])
                sample.reg_4_bit_2 = (prev_4[2], reg_4[2])
                sample.reg_4_bit_3 = (prev_4[3], reg_4[3])
                sample.reg_4_bit_4 = (prev_4[4], reg_4[4])
                sample.reg_4_bit_5 = (prev_4[5], reg_4[5])
                sample.reg_4_bit_6 = (prev_4[6], reg_4[6])
                sample.reg_4_bit_7 = (prev_4[7], reg_4[7])
                sample.reg_4_bit_8 = (prev_4[8], reg_4[8])
                sample.reg_4_bit_9 = (prev_4[9], reg_4[9])
                sample.reg_4_bit_10 = (prev_4[10], reg_4[10])
                sample.reg_4_bit_11 = (prev_4[11], reg_4[11])
                sample.reg_4_bit_12 = (prev_4[12], reg_4[12])
                sample.reg_4_bit_13 = (prev_4[13], reg_4[13])
                sample.reg_4_bit_14 = (prev_4[14], reg_4[14])
                sample.reg_4_bit_15 = (prev_4[15], reg_4[15])
                sample.reg_4_bit_16 = (prev_4[16], reg_4[16])
                sample.reg_4_bit_17 = (prev_4[17], reg_4[17])
                sample.reg_4_bit_18 = (prev_4[18], reg_4[18])
                sample.reg_4_bit_19 = (prev_4[19], reg_4[19])
                sample.reg_4_bit_20 = (prev_4[20], reg_4[20])
                sample.reg_4_bit_21 = (prev_4[21], reg_4[21])
                sample.reg_4_bit_22 = (prev_4[22], reg_4[22])
                sample.reg_4_bit_23 = (prev_4[23], reg_4[23])
                sample.reg_4_bit_24 = (prev_4[24], reg_4[24])
                sample.reg_4_bit_25 = (prev_4[25], reg_4[25])
                sample.reg_4_bit_26 = (prev_4[26], reg_4[26])
                sample.reg_4_bit_27 = (prev_4[27], reg_4[27])
                sample.reg_4_bit_28 = (prev_4[28], reg_4[28])
                sample.reg_4_bit_29 = (prev_4[29], reg_4[29])
                sample.reg_4_bit_30 = (prev_4[30], reg_4[30])
                sample.reg_4_bit_31 = (prev_4[31], reg_4[31])
                sample.reg_4_bit_32 = (prev_4[32], reg_4[32])
                sample.reg_4_bit_33 = (prev_4[33], reg_4[33])
                sample.reg_4_bit_34 = (prev_4[34], reg_4[34])
                sample.reg_4_bit_35 = (prev_4[35], reg_4[35])
                sample.reg_4_bit_36 = (prev_4[36], reg_4[36])
                sample.reg_4_bit_37 = (prev_4[37], reg_4[37])
                sample.reg_4_bit_38 = (prev_4[38], reg_4[38])
                sample.reg_4_bit_39 = (prev_4[39], reg_4[39])
                sample.reg_4_bit_40 = (prev_4[40], reg_4[40])
                sample.reg_4_bit_41 = (prev_4[41], reg_4[41])
                sample.reg_4_bit_42 = (prev_4[42], reg_4[42])
                sample.reg_4_bit_43 = (prev_4[43], reg_4[43])
                sample.reg_4_bit_44 = (prev_4[44], reg_4[44])
                sample.reg_4_bit_45 = (prev_4[45], reg_4[45])
                sample.reg_4_bit_46 = (prev_4[46], reg_4[46])
                sample.reg_4_bit_47 = (prev_4[47], reg_4[47])
                sample.reg_4_bit_48 = (prev_4[48], reg_4[48])
                sample.reg_4_bit_49 = (prev_4[49], reg_4[49])
                sample.reg_4_bit_50 = (prev_4[50], reg_4[50])
                sample.reg_4_bit_51 = (prev_4[51], reg_4[51])
                sample.reg_4_bit_52 = (prev_4[52], reg_4[52])
                sample.reg_4_bit_53 = (prev_4[53], reg_4[53])
                sample.reg_4_bit_54 = (prev_4[54], reg_4[54])
                sample.reg_4_bit_55 = (prev_4[55], reg_4[55])
                sample.reg_4_bit_56 = (prev_4[56], reg_4[56])
                sample.reg_4_bit_57 = (prev_4[57], reg_4[57])
                sample.reg_4_bit_58 = (prev_4[58], reg_4[58])
                sample.reg_4_bit_59 = (prev_4[59], reg_4[59])
                sample.reg_4_bit_60 = (prev_4[60], reg_4[60])
                sample.reg_4_bit_61 = (prev_4[61], reg_4[61])
                sample.reg_4_bit_62 = (prev_4[62], reg_4[62])
                sample.reg_4_bit_63 = (prev_4[63], reg_4[63])
                sample.reg_3_bit_0 = (prev_3[0], reg_3[0])
                sample.reg_3_bit_1 = (prev_3[1], reg_3[1])
                sample.reg_3_bit_2 = (prev_3[2], reg_3[2])
                sample.reg_3_bit_3 = (prev_3[3], reg_3[3])
                sample.reg_3_bit_4 = (prev_3[4], reg_3[4])
                sample.reg_3_bit_5 = (prev_3[5], reg_3[5])
                sample.reg_3_bit_6 = (prev_3[6], reg_3[6])
                sample.reg_3_bit_7 = (prev_3[7], reg_3[7])
                sample.reg_3_bit_8 = (prev_3[8], reg_3[8])
                sample.reg_3_bit_9 = (prev_3[9], reg_3[9])
                sample.reg_3_bit_10 = (prev_3[10], reg_3[10])
                sample.reg_3_bit_11 = (prev_3[11], reg_3[11])
                sample.reg_3_bit_12 = (prev_3[12], reg_3[12])
                sample.reg_3_bit_13 = (prev_3[13], reg_3[13])
                sample.reg_3_bit_14 = (prev_3[14], reg_3[14])
                sample.reg_3_bit_15 = (prev_3[15], reg_3[15])
                sample.reg_3_bit_16 = (prev_3[16], reg_3[16])
                sample.reg_3_bit_17 = (prev_3[17], reg_3[17])
                sample.reg_3_bit_18 = (prev_3[18], reg_3[18])
                sample.reg_3_bit_19 = (prev_3[19], reg_3[19])
                sample.reg_3_bit_20 = (prev_3[20], reg_3[20])
                sample.reg_3_bit_21 = (prev_3[21], reg_3[21])
                sample.reg_3_bit_22 = (prev_3[22], reg_3[22])
                sample.reg_3_bit_23 = (prev_3[23], reg_3[23])
                sample.reg_3_bit_24 = (prev_3[24], reg_3[24])
                sample.reg_3_bit_25 = (prev_3[25], reg_3[25])
                sample.reg_3_bit_26 = (prev_3[26], reg_3[26])
                sample.reg_3_bit_27 = (prev_3[27], reg_3[27])
                sample.reg_3_bit_28 = (prev_3[28], reg_3[28])
                sample.reg_3_bit_29 = (prev_3[29], reg_3[29])
                sample.reg_3_bit_30 = (prev_3[30], reg_3[30])
                sample.reg_3_bit_31 = (prev_3[31], reg_3[31])
                sample.reg_3_bit_32 = (prev_3[32], reg_3[32])
                sample.reg_3_bit_33 = (prev_3[33], reg_3[33])
                sample.reg_3_bit_34 = (prev_3[34], reg_3[34])
                sample.reg_3_bit_35 = (prev_3[35], reg_3[35])
                sample.reg_3_bit_36 = (prev_3[36], reg_3[36])
                sample.reg_3_bit_37 = (prev_3[37], reg_3[37])
                sample.reg_3_bit_38 = (prev_3[38], reg_3[38])
                sample.reg_3_bit_39 = (prev_3[39], reg_3[39])
                sample.reg_3_bit_40 = (prev_3[40], reg_3[40])
                sample.reg_3_bit_41 = (prev_3[41], reg_3[41])
                sample.reg_3_bit_42 = (prev_3[42], reg_3[42])
                sample.reg_3_bit_43 = (prev_3[43], reg_3[43])
                sample.reg_3_bit_44 = (prev_3[44], reg_3[44])
                sample.reg_3_bit_45 = (prev_3[45], reg_3[45])
                sample.reg_3_bit_46 = (prev_3[46], reg_3[46])
                sample.reg_3_bit_47 = (prev_3[47], reg_3[47])
                sample.reg_3_bit_48 = (prev_3[48], reg_3[48])
                sample.reg_3_bit_49 = (prev_3[49], reg_3[49])
                sample.reg_3_bit_50 = (prev_3[50], reg_3[50])
                sample.reg_3_bit_51 = (prev_3[51], reg_3[51])
                sample.reg_3_bit_52 = (prev_3[52], reg_3[52])
                sample.reg_3_bit_53 = (prev_3[53], reg_3[53])
                sample.reg_3_bit_54 = (prev_3[54], reg_3[54])
                sample.reg_3_bit_55 = (prev_3[55], reg_3[55])
                sample.reg_3_bit_56 = (prev_3[56], reg_3[56])
                sample.reg_3_bit_57 = (prev_3[57], reg_3[57])
                sample.reg_3_bit_58 = (prev_3[58], reg_3[58])
                sample.reg_3_bit_59 = (prev_3[59], reg_3[59])
                sample.reg_3_bit_60 = (prev_3[60], reg_3[60])
                sample.reg_3_bit_61 = (prev_3[61], reg_3[61])
                sample.reg_3_bit_62 = (prev_3[62], reg_3[62])
                sample.reg_3_bit_63 = (prev_3[63], reg_3[63])
                sample.reg_2_bit_0 = (prev_2[0], reg_2[0])
                sample.reg_2_bit_1 = (prev_2[1], reg_2[1])
                sample.reg_2_bit_2 = (prev_2[2], reg_2[2])
                sample.reg_2_bit_3 = (prev_2[3], reg_2[3])
                sample.reg_2_bit_4 = (prev_2[4], reg_2[4])
                sample.reg_2_bit_5 = (prev_2[5], reg_2[5])
                sample.reg_2_bit_6 = (prev_2[6], reg_2[6])
                sample.reg_2_bit_7 = (prev_2[7], reg_2[7])
                sample.reg_2_bit_8 = (prev_2[8], reg_2[8])
                sample.reg_2_bit_9 = (prev_2[9], reg_2[9])
                sample.reg_2_bit_10 = (prev_2[10], reg_2[10])
                sample.reg_2_bit_11 = (prev_2[11], reg_2[11])
                sample.reg_2_bit_12 = (prev_2[12], reg_2[12])
                sample.reg_2_bit_13 = (prev_2[13], reg_2[13])
                sample.reg_2_bit_14 = (prev_2[14], reg_2[14])
                sample.reg_2_bit_15 = (prev_2[15], reg_2[15])
                sample.reg_2_bit_16 = (prev_2[16], reg_2[16])
                sample.reg_2_bit_17 = (prev_2[17], reg_2[17])
                sample.reg_2_bit_18 = (prev_2[18], reg_2[18])
                sample.reg_2_bit_19 = (prev_2[19], reg_2[19])
                sample.reg_2_bit_20 = (prev_2[20], reg_2[20])
                sample.reg_2_bit_21 = (prev_2[21], reg_2[21])
                sample.reg_2_bit_22 = (prev_2[22], reg_2[22])
                sample.reg_2_bit_23 = (prev_2[23], reg_2[23])
                sample.reg_2_bit_24 = (prev_2[24], reg_2[24])
                sample.reg_2_bit_25 = (prev_2[25], reg_2[25])
                sample.reg_2_bit_26 = (prev_2[26], reg_2[26])
                sample.reg_2_bit_27 = (prev_2[27], reg_2[27])
                sample.reg_2_bit_28 = (prev_2[28], reg_2[28])
                sample.reg_2_bit_29 = (prev_2[29], reg_2[29])
                sample.reg_2_bit_30 = (prev_2[30], reg_2[30])
                sample.reg_2_bit_31 = (prev_2[31], reg_2[31])
                sample.reg_2_bit_32 = (prev_2[32], reg_2[32])
                sample.reg_2_bit_33 = (prev_2[33], reg_2[33])
                sample.reg_2_bit_34 = (prev_2[34], reg_2[34])
                sample.reg_2_bit_35 = (prev_2[35], reg_2[35])
                sample.reg_2_bit_36 = (prev_2[36], reg_2[36])
                sample.reg_2_bit_37 = (prev_2[37], reg_2[37])
                sample.reg_2_bit_38 = (prev_2[38], reg_2[38])
                sample.reg_2_bit_39 = (prev_2[39], reg_2[39])
                sample.reg_2_bit_40 = (prev_2[40], reg_2[40])
                sample.reg_2_bit_41 = (prev_2[41], reg_2[41])
                sample.reg_2_bit_42 = (prev_2[42], reg_2[42])
                sample.reg_2_bit_43 = (prev_2[43], reg_2[43])
                sample.reg_2_bit_44 = (prev_2[44], reg_2[44])
                sample.reg_2_bit_45 = (prev_2[45], reg_2[45])
                sample.reg_2_bit_46 = (prev_2[46], reg_2[46])
                sample.reg_2_bit_47 = (prev_2[47], reg_2[47])
                sample.reg_2_bit_48 = (prev_2[48], reg_2[48])
                sample.reg_2_bit_49 = (prev_2[49], reg_2[49])
                sample.reg_2_bit_50 = (prev_2[50], reg_2[50])
                sample.reg_2_bit_51 = (prev_2[51], reg_2[51])
                sample.reg_2_bit_52 = (prev_2[52], reg_2[52])
                sample.reg_2_bit_53 = (prev_2[53], reg_2[53])
                sample.reg_2_bit_54 = (prev_2[54], reg_2[54])
                sample.reg_2_bit_55 = (prev_2[55], reg_2[55])
                sample.reg_2_bit_56 = (prev_2[56], reg_2[56])
                sample.reg_2_bit_57 = (prev_2[57], reg_2[57])
                sample.reg_2_bit_58 = (prev_2[58], reg_2[58])
                sample.reg_2_bit_59 = (prev_2[59], reg_2[59])
                sample.reg_2_bit_60 = (prev_2[60], reg_2[60])
                sample.reg_2_bit_61 = (prev_2[61], reg_2[61])
                sample.reg_2_bit_62 = (prev_2[62], reg_2[62])
                sample.reg_2_bit_63 = (prev_2[63], reg_2[63])
                sample.reg_1_bit_0 = (prev_1[0], reg_1[0])
                sample.reg_1_bit_1 = (prev_1[1], reg_1[1])
                sample.reg_1_bit_2 = (prev_1[2], reg_1[2])
                sample.reg_1_bit_3 = (prev_1[3], reg_1[3])
                sample.reg_1_bit_4 = (prev_1[4], reg_1[4])
                sample.reg_1_bit_5 = (prev_1[5], reg_1[5])
                sample.reg_1_bit_6 = (prev_1[6], reg_1[6])
                sample.reg_1_bit_7 = (prev_1[7], reg_1[7])
                sample.reg_1_bit_8 = (prev_1[8], reg_1[8])
                sample.reg_1_bit_9 = (prev_1[9], reg_1[9])
                sample.reg_1_bit_10 = (prev_1[10], reg_1[10])
                sample.reg_1_bit_11 = (prev_1[11], reg_1[11])
                sample.reg_1_bit_12 = (prev_1[12], reg_1[12])
                sample.reg_1_bit_13 = (prev_1[13], reg_1[13])
                sample.reg_1_bit_14 = (prev_1[14], reg_1[14])
                sample.reg_1_bit_15 = (prev_1[15], reg_1[15])
                sample.reg_1_bit_16 = (prev_1[16], reg_1[16])
                sample.reg_1_bit_17 = (prev_1[17], reg_1[17])
                sample.reg_1_bit_18 = (prev_1[18], reg_1[18])
                sample.reg_1_bit_19 = (prev_1[19], reg_1[19])
                sample.reg_1_bit_20 = (prev_1[20], reg_1[20])
                sample.reg_1_bit_21 = (prev_1[21], reg_1[21])
                sample.reg_1_bit_22 = (prev_1[22], reg_1[22])
                sample.reg_1_bit_23 = (prev_1[23], reg_1[23])
                sample.reg_1_bit_24 = (prev_1[24], reg_1[24])
                sample.reg_1_bit_25 = (prev_1[25], reg_1[25])
                sample.reg_1_bit_26 = (prev_1[26], reg_1[26])
                sample.reg_1_bit_27 = (prev_1[27], reg_1[27])
                sample.reg_1_bit_28 = (prev_1[28], reg_1[28])
                sample.reg_1_bit_29 = (prev_1[29], reg_1[29])
                sample.reg_1_bit_30 = (prev_1[30], reg_1[30])
                sample.reg_1_bit_31 = (prev_1[31], reg_1[31])
                sample.reg_1_bit_32 = (prev_1[32], reg_1[32])
                sample.reg_1_bit_33 = (prev_1[33], reg_1[33])
                sample.reg_1_bit_34 = (prev_1[34], reg_1[34])
                sample.reg_1_bit_35 = (prev_1[35], reg_1[35])
                sample.reg_1_bit_36 = (prev_1[36], reg_1[36])
                sample.reg_1_bit_37 = (prev_1[37], reg_1[37])
                sample.reg_1_bit_38 = (prev_1[38], reg_1[38])
                sample.reg_1_bit_39 = (prev_1[39], reg_1[39])
                sample.reg_1_bit_40 = (prev_1[40], reg_1[40])
                sample.reg_1_bit_41 = (prev_1[41], reg_1[41])
                sample.reg_1_bit_42 = (prev_1[42], reg_1[42])
                sample.reg_1_bit_43 = (prev_1[43], reg_1[43])
                sample.reg_1_bit_44 = (prev_1[44], reg_1[44])
                sample.reg_1_bit_45 = (prev_1[45], reg_1[45])
                sample.reg_1_bit_46 = (prev_1[46], reg_1[46])
                sample.reg_1_bit_47 = (prev_1[47], reg_1[47])
                sample.reg_1_bit_48 = (prev_1[48], reg_1[48])
                sample.reg_1_bit_49 = (prev_1[49], reg_1[49])
                sample.reg_1_bit_50 = (prev_1[50], reg_1[50])
                sample.reg_1_bit_51 = (prev_1[51], reg_1[51])
                sample.reg_1_bit_52 = (prev_1[52], reg_1[52])
                sample.reg_1_bit_53 = (prev_1[53], reg_1[53])
                sample.reg_1_bit_54 = (prev_1[54], reg_1[54])
                sample.reg_1_bit_55 = (prev_1[55], reg_1[55])
                sample.reg_1_bit_56 = (prev_1[56], reg_1[56])
                sample.reg_1_bit_57 = (prev_1[57], reg_1[57])
                sample.reg_1_bit_58 = (prev_1[58], reg_1[58])
                sample.reg_1_bit_59 = (prev_1[59], reg_1[59])
                sample.reg_1_bit_60 = (prev_1[60], reg_1[60])
                sample.reg_1_bit_61 = (prev_1[61], reg_1[61])
                sample.reg_1_bit_62 = (prev_1[62], reg_1[62])
                sample.reg_1_bit_63 = (prev_1[63], reg_1[63])                
                prev_val = new_val
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_reg_toggle(sample)

sys.setrecursionlimit(10000)

   
