import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
from mycheckers.decoder_struct import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto

class opcode(Enum):
      BRANCH=auto()
      IMM_ARITH=auto()
      LOAD=auto()
      STORE=auto()
      ARITH=auto()
      MULDIV=auto()
      ARITHIW=auto()
      ARITHW=auto()
      MULDIVW=auto()
      ATOMIC=auto()
      FLOAT=auto()
      ECALL=auto()
      EBREAK=auto()
      PAUSE=auto()
      FENCE_TSO=auto()
      CSROP=auto()
      FENCE=auto()
      WFI=auto()
      SRET=auto()
      MRET=auto()
      SFENCE_VMA=auto()
      LUI=auto()
      AUIPC=auto()
      JAL=auto()
      JALR=auto()
      UNKNOWN=auto()
      
class InputSample:
    def __init__(self):
        self.inst : int
    def __str__(self):
        return 'inst : '+str(hex(self.inst)) 

class Coverage(object):
    
    _signals = get_signals('module_fn_decode','inputs')
    _signals.update(get_signals('mkstage2','wires',['decode_opfetch']))
    
    FnDecoder_Cov = coverage_section (
        CoverPoint("top.FnDecoder_opcode", xf = lambda sample: sample.instruction , bins = [x.name for x in list(opcode)] ),
        CoverPoint("top.branch",xf = lambda sample: sample.instruction, bins = ["BRANCH"]),
        CoverPoint("top.branch_f3_illegal",xf = lambda sample: sample.f3, bins = [2,3]),
        CoverCross("top.branch_illegal", ["top.branch","top.branch_f3_illegal"]),
        CoverPoint("top.imm_arith",xf = lambda sample: sample.instruction, bins = ["IMM_ARITH"]),
        CoverPoint("top.imm_arith_slli",xf = lambda sample: sample.f3, bins = [1]),
        CoverPoint("top.imm_arith_sr_i",xf = lambda sample: sample.f3, bins = [5]),
        CoverPoint("top.imm_arith_slli_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0]]),
        CoverPoint("top.imm_arith_sr_i_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0,16]]),
        CoverCross("top.imm_arith_slli_illegal", ["top.imm_arith","top.imm_arith_slli","top.imm_arith_slli_f6_illegal"]),
        CoverCross("top.imm_arith_sr_i_illegal", ["top.imm_arith","top.imm_arith_sr_i","top.imm_arith_sr_i_f6_illegal"]),
        CoverPoint("top.load",xf = lambda sample: sample.instruction, bins = ["LOAD"]),
        CoverPoint("top.load_f3_illegal",xf = lambda sample: sample.f3, bins = [7]),
        CoverCross("top.load_illegal", ["top.load","top.load_f3_illegal"]),
        CoverPoint("top.store",xf = lambda sample: sample.instruction, bins = ["STORE"]),
        CoverPoint("top.store_f3_illegal",xf = lambda sample: sample.f3, bins = [4,5,6,7]),
        CoverCross("top.store_illegal", ["top.store","top.store_f3_illegal"]),
        CoverPoint("top.arith",xf = lambda sample: sample.instruction, bins = ["ARITH"]),
        CoverPoint("top.arith_add",xf = lambda sample: sample.f3, bins = [0]),
        CoverPoint("top.arith_sr",xf = lambda sample: sample.f3, bins = [5]),
        CoverPoint("top.arith_add_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0,16]]),
        CoverPoint("top.arith_sr_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0,16]]),
        CoverCross("top.arith_add_illegal", ["top.arith","top.arith_add","top.arith_add_f6_illegal"]),
        CoverCross("top.arith_sr_illegal", ["top.arith","top.arith_sr","top.arith_sr_f6_illegal"]),
        CoverPoint("top.muldiv",xf = lambda sample: sample.instruction, bins = ["MULDIV"]),
        CoverPoint("top.muldiv_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0]]),
        CoverCross("top.muldiv_illegal", ["top.muldiv","top.muldiv_f6_illegal"]),
        CoverPoint("top.arithiw",xf = lambda sample: sample.instruction, bins = ["ARITHIW"]),
        CoverPoint("top.arithiw_f3_illegal",xf = lambda sample: sample.f3, bins = [2,3,4,6,7]),
        CoverCross("top.arithiw_illegal", ["top.arithiw","top.arithiw_f3_illegal"]),
        CoverPoint("top.arith_sr_iw",xf = lambda sample: sample.f3, bins = [5]),
        CoverPoint("top.arith_sr_iw_f7_illegal",xf = lambda sample: sample.f7, bins = [x for x in list(range(128)) if x not in [0,32]]),
        CoverCross("top.arith_sr_iw_illegal", ["top.arithiw","top.arith_sr_iw","top.arith_sr_iw_f7_illegal"]),
        CoverPoint("top.muldivw",xf = lambda sample: sample.instruction, bins = ["MULDIVW"]),
        CoverPoint("top.muldivw_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0]]),
        CoverCross("top.f6_muldivw_illegal", ["top.muldivw","top.muldivw_f6_illegal"]),
        CoverPoint("top.muldivw_f3_illegal",xf = lambda sample: sample.f3, bins = [1,2,3]),
        CoverCross("top.muldivw_illegal", ["top.muldivw","top.muldivw_f3_illegal"]),
        CoverPoint("top.arithw",xf = lambda sample: sample.instruction, bins = ["ARITHW"]),
        CoverPoint("top.arithw_f3_illegal",xf = lambda sample: sample.f3, bins = [2,3,4,6,7]),
        CoverCross("top.arithw_illegal", ["top.arithw","top.arithw_f3_illegal"]),
        CoverPoint("top.arithw_add",xf = lambda sample: sample.f3, bins = [0]),
        CoverPoint("top.arith_sr_w",xf = lambda sample: sample.f3, bins = [5]),
        CoverPoint("top.arithw_add_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0,16]]),
        CoverPoint("top.arithw_sr_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0,16]]),
        CoverCross("top.arithw_add_illegal", ["top.arithw","top.arithw_add","top.arithw_add_f6_illegal"]),
        CoverCross("top.arithw_sr_illegal", ["top.arithw","top.arith_sr_w","top.arithw_sr_f6_illegal"]),
        CoverPoint("top.atomic",xf = lambda sample: sample.instruction, bins = ["ATOMIC"]),
        CoverPoint("top.atomic_f3_illegal",xf = lambda sample: sample.f3, bins = [0,1,4,5,6,7]),
        CoverCross("top.atomic_illegal", ["top.arithw","top.arithw_f3_illegal"]),
        CoverPoint("top.atomic_w",xf = lambda sample: sample.f3, bins = [2]),
        CoverPoint("top.atomic_d",xf = lambda sample: sample.f3, bins = [3]),
        CoverPoint("top.atomic_f5_illegal",xf = lambda sample: sample.f5, bins = [x for x in list(range(32)) if x not in [2,3,1,0,4,12,8,16,20,24,28]]),
        CoverCross("top.atomic_w_illegal", ["top.atomic","top.atomic_w","top.atomic_f5_illegal"]),
        CoverCross("top.atomic_d_illegal", ["top.atomic","top.atomic_d","top.atomic_f5_illegal"]),
        CoverPoint("top.float",xf = lambda sample: sample.instruction, bins = ["FLOAT"]),
        CoverPoint("top.float_f6_illegal",xf = lambda sample: sample.f6, bins = [x for x in list(range(64)) if x not in [0,2,4,6,22,8,10,16,40,56,48,52,60]]),
        CoverCross("top.float_illegal",["top.float","top.float_f6_illegal"]),
        CoverPoint("top.float_fcvt_s_d",xf = lambda sample: sample.f6, bins = [16]),
        CoverPoint("top.float_fcvt_s_d_24_20_illegal",xf = lambda sample: sample.inst24_20, bins = [x for x in list(range(32)) if x not in [0,1]]),
        CoverCross("top.float_fcvt_s_d_illegal",["top.float","top.float_fcvt_s_d","top.float_fcvt_s_d_24_20_illegal"]),
        CoverPoint("top.float_sqrt",xf = lambda sample: sample.f6, bins = [22]),
        CoverPoint("top.float_sqrt_24_20_illegal",xf = lambda sample: sample.inst24_20, bins = [x for x in list(range(32)) if x not in [0]]),
        CoverCross("top.float_sqrt_illegal",["top.float","top.float_sqrt","top.float_sqrt_24_20_illegal"]),
        CoverPoint("top.float_sgnj",xf = lambda sample: sample.f6, bins = [8]),
        CoverPoint("top.float_sgnj_f3_illegal",xf = lambda sample: sample.f3, bins = [3,4,5,6,7]),
        CoverCross("top.float_sgnj_illegal",["top.float","top.float_sgnj","top.float_sgnj_f3_illegal"]),
        CoverPoint("top.float_min",xf = lambda sample: sample.f6, bins = [10]),
        CoverPoint("top.float_min_f3_illegal",xf = lambda sample: sample.f3, bins = [2,3,4,5,6,7]),
        CoverCross("top.float_min_illegal",["top.float","top.float_min","top.float_min_f3_illegal"]),
        CoverPoint("top.float_fcvt",xf = lambda sample: sample.f6, bins = [48]),
        CoverPoint("top.float_fcvt_24_20_illegal",xf = lambda sample: sample.inst24_20, bins = [x for x in list(range(32)) if x not in [0,1,2,3]]),
        CoverCross("top.float_fcvt_illegal",["top.float","top.float_fcvt","top.float_fcvt_24_20_illegal"]),
        CoverPoint("top.float_fmv_class",xf = lambda sample: sample.f6, bins = [56]),
        CoverPoint("top.float_fmv_class_24_20_illegal",xf = lambda sample: sample.inst24_20, bins = [x for x in list(range(32)) if x not in [0]]),
        CoverPoint("top.float_fmv_class_f3_illegal",xf = lambda sample: sample.f3, bins = [2,3,4,5,6,7]),
        CoverPoint("top.float_fmv_class_24_20_legal",xf = lambda sample: sample.inst24_20, bins = [0]),
        CoverCross("top.float_fmv_class_illegal",["top.float","top.float_fmv_class","top.float_fmv_class_24_20_illegal"]),
        CoverCross("top.float_fmv_class_illegal",["top.float","top.float_fmv_class","top.float_fmv_class_f3_legal","top.float_fmv_class_f3_illegal"]),
        CoverPoint("top.float_flogic",xf = lambda sample: sample.f6, bins = [40]),
        CoverPoint("top.float_flogic_f3_illegal",xf = lambda sample: sample.f3, bins = [3]),
        CoverCross("top.float_flogic_illegal",["top.float","top.float_flogic","top.float_flogic_f3_illegal"]),
        CoverPoint("top.float_fcvt_s",xf = lambda sample: sample.f6, bins = [52]),
        CoverPoint("top.float_fcvt_s_24_20_illegal",xf = lambda sample: sample.inst24_20, bins = [x for x in list(range(32)) if x not in [0,1,2,3]]),
        CoverCross("top.float_fcvt_illegal",["top.float","top.float_fcvt_s","top.float_fcvt_s_24_20_illegal"]),
        CoverPoint("top.float_fmv_w",xf = lambda sample: sample.f6, bins = [60]),
        CoverPoint("top.float_fmv_w_24_20_illegal",xf = lambda sample: sample.inst24_20, bins = [x for x in list(range(32)) if x not in [0]]),
        CoverPoint("top.float_fmv_w_f3_illegal",xf = lambda sample: sample.f3, bins = [1,2,3,4,5,6,7]),
        CoverPoint("top.float_fmv_w_24_20_legal",xf = lambda sample: sample.inst24_20, bins = [0]),
        CoverCross("top.float_fmv_w_illegal",["top.float","top.float_fmv_w","top.float_fmv_w_24_20_illegal"]),
        CoverCross("top.float_fmv_class_illegal",["top.float","top.float_fmv_w","top.float_fmv_w_f3_legal","top.float_fmv_w_f3_illegal"]),
        CoverPoint("top.csrop",xf = lambda sample: sample.instruction, bins = ["CSROP"]),
        CoverPoint("top.csrop_f3_illegal",xf = lambda sample: sample.f3, bins = [0,4]),
        CoverCross("top.csrop_illegal",["top.csrop","top.csrop_f3_illegal"]),
        CoverPoint("top.fence",xf = lambda sample: sample.instruction, bins = ["FENCE"]),
        CoverPoint("top.fence_f3_illegal",xf = lambda sample: sample.f3, bins = [2,3,4,5,6,7]),
        CoverCross("top.fence_illegal",["top.fence","top.fence_f3_illegal"]),
        CoverPoint("top.fload",xf = lambda sample: sample.instruction, bins = ["FLOAD"]),
        CoverPoint("top.fload_f3_illegal",xf = lambda sample: sample.f3, bins = [0,1,3,4,5,6,7]),
        CoverCross("top.fload_illegal",["top.fload","top.fload_f3_illegal"]),
        CoverPoint("top.fstore",xf = lambda sample: sample.instruction, bins = ["FSTORE"]),
        CoverPoint("top.fstore_f3_illegal",xf = lambda sample: sample.f3, bins = [0,1,3,4,5,6,7]),
        CoverCross("top.fstore_illegal",["top.fstore","top.fstore_f3_illegal"]),
        CoverPoint("top.fmadd",xf = lambda sample: sample.instruction, bins = ["FMADD"]),
        CoverPoint("top.fmadd_f3_illegal",xf = lambda sample: (sample.f7&0X3), bins = [2,3]),
        CoverCross("top.fmadd_illegal",["top.fmadd","top.fmadd_f3_illegal"]),
        CoverPoint("top.fnmadd",xf = lambda sample: sample.instruction, bins = ["FNMADD"]),
        CoverPoint("top.fnmadd_f3_illegal",xf = lambda sample: (sample.f7&0X3), bins = [2,3]),
        CoverCross("top.fnmadd_illegal",["top.fnmadd","top.fnmadd_f3_illegal"]),
        CoverPoint("top.fmsub",xf = lambda sample: sample.instruction, bins = ["FMSUB"]),
        CoverPoint("top.fmsub_f3_illegal",xf = lambda sample: (sample.f7&0X3), bins = [2,3]),
        CoverCross("top.fmsub_illegal",["top.fmsub","top.fmsub_f3_illegal"]),
        CoverPoint("top.fnmsub",xf = lambda sample: sample.instruction, bins = ["FNMSUB"]),
        CoverPoint("top.fnmsub_f3_illegal",xf = lambda sample: (sample.f7&0X3), bins = [2,3]),
        CoverCross("top.fnmsub_illegal",["top.fnmsub","top.fnmsub_f3_illegal"]),
    )

    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        cocotb.fork(self.sample_signals())

    @FnDecoder_Cov
    def sample_coverage(self, sample):
        pass 

    @coroutine
    def sample_signals(self):
        while True:
            yield RisingEdge(self.dut.CLK)
            yield Timer(1, units="ns")
            if self.decode_opfetch.value.integer == 1 :
                sample = InputSample()
                inst=self.inst.value.integer
                instruction=opcode.UNKNOWN.name
                op=(inst&0x7f)
                f6=(inst&0xFC000000)>>26
                f5=(inst&0xF8000000)>>27
                inst_25=(inst&0x2000000)>>25
                f3=(inst&0x7000)>>12
                f7=(inst&0xFE000000)>>25
                inst24_20=(inst&0x1F00000)>>20
                if op==99:
                   instruction=opcode.BRANCH.name
                if op==19:
                   instruction=opcode.IMM_ARITH.name
                if op==3:
                   instruction=opcode.LOAD.name
                if op==35:
                   instruction=opcode.STORE.name 
                if op==51 and inst_25==0:
                   instruction=opcode.ARITH.name
                if op==51 and inst_25==1:
                   instruction=opcode.MULDIV.name
                if op==27:
                   instruction=opcode.ARITHIW.name
                if op==59 and inst_25==0:
                   instruction=opcode.ARITHW.name
                if op==59 and inst_25==1:
                   instruction=opcode.MULDIVW.name
                if op==47:
                   instruction=opcode.ATOMIC.name
                if op==83 :
                   instruction=opcode.FLOAT.name
                if inst==115:
                   instruction=opcode.ECALL.name
                if inst==1048691:
                   instruction=opcode.EBREAK.name
                if inst==16777231:
                   instruction=opcode.PAUSE.name
                if inst==2200961039:
                   instruction=opcode.FENCE_TSO.name
                if op==15 and inst not in [2200961039, 16777231]:
                   instruction=opcode.FENCE.name
                if inst==273678451:
                   instruction=opcode.WFI.name
                if inst==270532723:
                   instruction=opcode.SRET.name
                if inst== 807403635:
                   instruction=opcode.MRET.name
                if (inst&0x7fff)==115 and f7==9:
                   instruction=opcode.SFENCE_VMA.name
                if op==115 and inst not in [115, 1048691, 273678451, 270532723, 807403635] and instruction!=opcode.SFENCE_VMA.name :
                   instruction=opcode.CSROP.name
                if op==23:
                   instruction=opcode.AUIPC.name
                if op==55:
                   instruction=opcode.LUI.name
                if op==111:
                   instruction=opcode.JAL.name
                if op==103:
                   instruction=opcode.JALR.name
                if op==7:
                   instruction=opcode.FLOAD.name
                if op==39:
                   instruction=opcode.FSTORE.name
                if op==67:
                   instruction=opcode.FMADD.name
                if op==71:
                   instruction=opcode.FMSUB.name
                if op==79:
                   instruction=opcode.FNMADD.name
                if op==75:
                   instruction=opcode.FNMSUB.name
                
                sample.instruction=instruction
                sample.f3=f3
                sample.f6=f6
                sample.f7=f7
                sample.f5=f5
                sample.inst24_20=inst24_20
                if self.enlog:
                    self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage(sample)



