import random
import cocotb
from cocotb.clock import Clock
from cocotb.regression import TestFactory
from cocotb.triggers import FallingEdge, RisingEdge, Timer, ReadOnly, ClockCycles
import cocotb.log

from bsvstruct_class import *

from cocotb_env import *
from utils import *

from cocotb_coverage.coverage import *
@cocotb.test()
def run_testfactory(dut):

    tb = Tb(dut)
    
    tb.logger.info('Starting Simulation')

    # start the simulation by setting clock and asserting reset
    yield tb.start_sim(clk_delay=5, rst_delay=200)

    # initiate forks which will check on termination conditions.
    cocotb.fork(tb.terminate_on_selfloop())
    cocotb.fork(tb.terminate_on_nocommit())
    cocotb.fork(tb.terminate_on_memwrite())
        
    test_map=load_yaml(enables)
    
    for node, val in test_map['checkers'].items():
        try:
            if val:
                tb.logger.info('Enabling Checker for : '+str(node))
                exec('from mycheckers import test_{0}'.format(node))
                exec('test_{0}.Checker(dut)'.format(node))
        except:
            raise SystemError('invalid checker {0} name enabled'.format(node))
    
    cov_enabled = False
    for node, val in test_map['coverage'].items():
        try:
            if val:
                tb.logger.info('Enabling Coverage for : '+str(node))
                exec('from mycoverage import cov_{0}'.format(node))
                exec('cov_{0}.Coverage(dut)'.format(node))
                cov_enabled = True
        except:
            raise SystemError('invalid coverage {0} name enabled'.format(node))

    #yield Timer(50000, units="ns")
    # endlessly wait for one of the above to exist simulation
    yield tb.wait_for_end()
    if cov_enabled:
        tb.logger.info('Dumping out coverage')
        coverage_db.report_coverage(log.info, bins=True)
        coverage_db.export_to_yaml(filename="coverage.yml")
        coverage_db.export_to_xml(filename="coverage.xml")

    tb.logger.info('Ending Simulation')
