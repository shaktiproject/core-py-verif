# set this path to where you have cloned c-class
cclass_path='/scratch/git-repo/shaktiproject/cores/c-class/'

# provide absolute path of the alias signal file
alias_file='/scratch/git-repo/shaktiproject/core-py-verif/alias_signal.yaml'

# provide the absolute path of the enables signal file
enables='/scratch/git-repo/shaktiproject/core-py-verif/enables.yaml'
# set this path the lib directory of your bsc installation
bsclib='/software/experimental/open-bsc/lib/'

# provide the absolute path of the patterns yaml file
pattern_decompress_yaml='/scratch/git-repo/shaktiproject/core-py-verif/mycheckers/pattern_decompress.yaml'


# these are cocotb environment settings. Refer to
# https://docs.cocotb.org/en/stable/building.html for details on what can be set
# here
myenv = {
#        "LIBPYTHON_LOC": "/usr/lib/x86_64-linux-gnu/libpython3.8.so", 
        "COCOTB_REDUCED_LOG_FMT": "TRUE",
#        "COCOTB_LOG_LEVEL": "ERROR"
        }

# there are parameters of the mkTbSoc testbench. Mostly should never change.
#parameters= {"rtldumpfile": '\"rtl.dump\"', 
#             "applogfile": '\"app.log\"', 
#             "inputcodemem": '\"code.mem\"'
#             }

# path to the testlist
testlist_path = './tests/test_list.yaml'

