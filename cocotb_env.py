import cocotb
from cocotb.clock import Clock
from cocotb.triggers import RisingEdge, FallingEdge, Timer,  ReadOnly, ClockCycles
from cocotb.log import SimLogFormatter, SimColourLogFormatter, SimLog, SimTimeContextFilter
from cocotb.result import TestFailure, TestSuccess, SimFailure, SimTimeoutError
from datetime import datetime
from utils import *
import os 
import logging
from logging.handlers import RotatingFileHandler
import time
from dataclasses import dataclass

from bsvstruct_class import *

timeout_cycles = 1000000

class CommitLogPacketCust(CommitLogPacket):
    def __str__(self):
        return 'CommitLog : Instr:{0} PC:{1}'.format( hex(self.instruction) , \
                hex(self.pc))


class Tb:
    """

    Base class for C-Class Test-bench

    """

    def __init__(self, dut):

        self.dut = dut

        # set up the logger
        self.logger = logging.getLogger("cocotb")
#        self.logger.setLevel(logging.INFO)
        self.log_file_name = 'sim.log'
        fh = logging.FileHandler(self.log_file_name, mode='w')
        fh.setLevel(logging.INFO)
        formatter = logging.Formatter('%(asctime)s | %(levelname)s | %(message)s')
        fh.setFormatter(formatter)
        self.logger.addHandler(fh)
        self.stop_sim = False
        
        
    @cocotb.coroutine
    def start_sim(self, clk_delay, rst_delay):
        """
        This will set the clock and assert the reset based on the arguments.
        Unit is forced to ns
        """
        cocotb.fork(clock_gen(self.dut.CLK, clk_delay))
        yield assert_reset(self.dut.RST_N, 0, 1, rst_delay)

    @cocotb.coroutine
    def terminate_on_selfloop(self):
        """
        This routine will wait untill a self-loop instruction is observed and
        then raise a TestSuccess exception. Until then it will continously print
        the instructions that are committed.
        """
        commitstruct = CommitLogPacketCust()
        while True:
            yield RisingEdge(self.dut.CLK)
            commitpacket = self.dut.soc_commitlog.value
            commitstruct.set(commitpacket)
            commitvalid = commitpacket.integer >> (commitpacket.n_bits-1)
            willfire = self.dut.WILL_FIRE_RL_write_dump_file.value.integer

            if (willfire == 1 and commitvalid == 1):
                if commitstruct.instruction == 0x0000006F or commitstruct.instruction == 0x0000A001:
                    self.stop_sim = True
                    self.logger.debug("Self-Loop reached")
                else:
                    self.logger.debug('Instruction: ' + str(commitstruct))

    @cocotb.coroutine
    def wait_for_end(self):
        """docstring for wait_for_end
        This routine will wait endlessly until an exception is raised
        """
        cycle_count = 0
        while self.stop_sim == False:
            yield RisingEdge(self.dut.CLK)    
    
    @cocotb.coroutine
    def terminate_on_nocommit(self):
        """
        This routine will terminate with a failure if there have been no
        instruction commits within a timeout_cycles number of clock cycles. The
        counter is reset each time an instruction commits
        """
        last_commit_timer = 0
        self.logger.info('Will terminate sim if no commit for {0} cycles'.format(str(timeout_cycles))) 
        while True:
            yield RisingEdge(self.dut.CLK)
            commitpacket = self.dut.soc_commitlog.value
            commitvalid = commitpacket.integer >> (commitpacket.n_bits-1)
            willfire = self.dut.WILL_FIRE_RL_write_dump_file.value.integer
            if (willfire == 1 and commitvalid == 1):
                last_commit_timer = 0
            else:
                last_commit_timer += 1
            if last_commit_timer > timeout_cycles:
                self.logger.error('Instruction Commit Timeout')
                raise TestFailure('Instruction Commit Timeout')

    @cocotb.coroutine
    def terminate_on_memwrite(self):
        """
        This routine will wait for a write to a memory location to end the
        simulation successfully
        """

        while True:
            yield RisingEdge(self.dut.CLK)
            willfire = self.dut.soc.signature.WILL_FIRE_RL_configure_registers.value.integer
            wrpack = self.dut.soc.signature.s_xactor_f_wr_addr_D_OUT.value.integer

            awaddr = (wrpack >> 20 ) & 0xF
            if willfire == 1 and awaddr == 0xC:
                self.logger.info('Program write to signature module detected')
                self.stop_sim = True
            if willfire == 1 and awaddr == 0:
                self.logger.info('Initiating signature dump')

    @cocotb.coroutine
    def stop(self):	
        """
        Stop generation of expected output transactions.
        One more clock cycle must be executed afterwards, so that, output of
        D-FF can be checked.
        """
        self.stopped = True  


@cocotb.coroutine
def clock_gen(clk_signal, clk_delay):
    '''
      This coroutine can be used to initialize a clock signal in the DUT.
      Inputs
      clk_signal: The signal corresponding to the clock of the DUT should be
                  passed here
      clk_delay : The delay required for the clock signal - (Time period / 2)
    '''
    while True:
       clk_signal <= 0
       yield Timer(clk_delay, units="ns")
       clk_signal <= 1
       yield Timer(clk_delay, units="ns")

@cocotb.coroutine
def assert_reset(rst_signal, rst_val, rstn_val, rst_delay):
    '''
    This coroutine can be used to reset DUT.
    Inputs
    rst_signal: The signal corresponding to the reset of the DUT should be
                passed here
    rst_val   : The value which needs to be assigned to the signal to assert
                 reset
    rstn_val  : The value which needs to be assigned to the signal after the
                 reset is done
    rst_delay : The delay required before the rstn_val is assigned to the
                 signal
    '''
    rst_signal <= rst_val
    yield Timer(rst_delay, units="ns")
    rst_signal <= rstn_val

