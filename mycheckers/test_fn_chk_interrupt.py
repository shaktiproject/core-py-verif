import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from bitstring import BitArray
from cocotb_bus.scoreboard import Scoreboard
from bsvstruct_class import *
from enum import Enum, IntEnum, unique, auto
import logging
from utils import *
import random
import sys
class Checker:

    def __init__(self, dut, enlog=False):
        
        self.name = "fnchk_interrupt"
        self.log = logging.getLogger("cocotb")

        if enlog:
            self.log.info('Setting up Output Monitor')
        self.output_mon = OMonitor(dut)

        if enlog:
            self.log.info('Setting up Input Monitor')
        self.input_mon = IMonitor(dut, callback=self.model)
        
        self.log.info('Setting up Score-board')
        self.expected_output = []
        self.scoreboard = MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)

    
    def model(self, transaction, flag=False):
        """Model """
        # This function acts as the model for the dut under test
        prv, mstatus, mie, mip, mideleg = transaction
        m_enabled= (prv!=3) or (((mstatus&0x8)>>3)==1)
        s_enabled=(prv==0) or (((mstatus&0x2)>>2)==1 and prv==1)
        m_interrupts=mie & mip & m_enabled & (~ mideleg)
        s_interrupts=mie & mip & mideleg & s_enabled
        pending_interrupts=m_interrupts | s_interrupts
        pending=bin(pending_interrupts)[2:].zfill(17)
        taketrap=int(pending[0])
        for x in range(1,16):
            taketrap=taketrap|int(pending[x])
        int_cause=63
        mode=1
        if pending[6]=='1':
           int_cause=11
        elif pending[14]=='1':
          int_cause=3
        elif pending[10]=='1':
          int_cause=7
        elif pending[8]=='1':
          int_cause=9
        elif pending[15]=='1':
          int_cause=1
        elif pending[12]=='1':
          int_cause=5
        elif pending[9]=='1':
          int_cause=8
        elif pending[16]=='1':
          int_cause=0
        elif pending[12]=='1':
          int_cause=4
        chk_interrupt=(mode<<7)|(int_cause<<1)|taketrap
        if not flag:
           self.expected_output.append( chk_interrupt )
        else:
           return chk_interrupt
     
    
class MyScoreboard(Scoreboard):
    # This is the scoreboard class. You need to define how the comparison needs
    # to be happen between data received from dut and that received from the
    # model
    def chk_interrupt(self, got, exp, log, **_):
        got_output=got
        exp_output=exp
        if got_output != exp_output:
           log.info(__file__ + ": Output differs Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
           exit(1)
            

class IMonitor(Monitor):
    """Observes inputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('module_fn_chk_interrupt','inputs')
    _signals.update(get_signals('mkstage2','wires',['decode_opfetch']))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))

        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "fn_chk_interrupt.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)

    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 1 more second
            yield Timer(1,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            if self.decode_opfetch.value.integer == 1 :
                # sample the inputs and send to _recv method. All samples must
                # be sent to _recv. This is what is available to the model. IT
                # could be a tuple, dictionary, list, etc. The same must be
                # assumed in the model as well
                vec = (self.prv.value.integer,
                       self.mstatus.value.integer,
                       self.mie.value.integer,
                       self.mip.value.integer,
                       self.mideleg.value.integer)
                self._recv(vec)
                if self.enlog:
                    for x in vec:
                        self.log.debug('fn_chk_interrupt.imon: '+str(x))

class OMonitor(Monitor):
    """Observes outputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('module_fn_chk_interrupt','outputs')
    _signals.update(get_signals('mkstage2','wires',['decode_opfetch']))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        """tb must be an instance of the Testbench class."""
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "fn_chk_interrupt.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)
   
    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 5 more nano-second. This is so that the output from the
            # combo is available. Doing it at 1ns (same as inputs) might not
            # work.
            yield Timer(5,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            if self.decode_opfetch.value.integer == 1 :
                # check if the value is not don't care. Then send the value
                # to _recv method. This _recv will act as the actual output for
                # comparison in scoreboard.
                if (self.chk_interrupt.value is not None):
                    self._recv(self.chk_interrupt.value.integer)
                    if self.enlog:
                        self.log.debug('fn_chk_interrupt.omon: ' + str(self.chk_interrupt.value.integer))

