import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from bitstring import BitArray
from cocotb_bus.scoreboard import Scoreboard
from bsvstruct_class import *
import logging
from utils import *
import random
import sys
import mycheckers.test_fn_decoder_rs1 as decoder_rs1
import mycheckers.test_fn_decoder_rs2 as decoder_rs2
import mycheckers.test_fn_decoder_rs2type as decoder_rs2type
import mycheckers.test_fn_decoder_rs1type as decoder_rs1type

import mycheckers.test_fn_decoder_rd as decoder_rd
#import mycheckers.test_fn_decoder_rdtype as decoder_rdtype
import mycheckers.test_fn_decoder_fn as decoder_fn
import mycheckers.test_fn_decoder_immediate as decoder_immediate

#import mycheckers.test_fn_decoder_rs3 as decoder_rs3
#import mycheckers.test_fn_decoder_rs3type as decoder_rs3type
import mycheckers.test_fn_decoder_insttype as decoder_insttype
import mycheckers.test_fn_decoder_trapcause as decoder_trapcause
import mycheckers.test_fn_decoder_mem_access as decoder_mem_access
class Checker:

    def __init__(self, dut, enlog=False):
        
        self.name = "fndecode"
        self.log = logging.getLogger("cocotb")

        if enlog:
            self.log.info('Setting up Output Monitor')
        self.output_mon = OMonitor(dut)

        if enlog:
            self.log.info('Setting up Input Monitor')
        self.input_mon = IMonitor(dut, callback=self.model)
        
        self.log.info('Setting up Score-board')
        self.expected_output = []
        self.scoreboard = MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)

    
    def model(self, transaction, flag=False):
        """Model """
        # This function acts as the model for the dut under test
        inst, csrs, compressed = transaction
        word32=False
        rs1=decoder_rs1.Checker.model(self, transaction=(inst), flag=True)
        rs2=decoder_rs2.Checker.model(self, transaction=(inst, csrs), flag=True)
        rd=decoder_rd.Checker.model(self, transaction=(inst), flag=True)
        rs1type=decoder_rs1type.Checker.model(self, transaction=(inst), flag=True)
        rs2type=decoder_rs2type.Checker.model(self, transaction=(inst, compressed), flag=True)
        #rs3=decoder_rs3.Checker.model(self, transaction=(inst), flag=True)
        #rs3type=decoder_rs3type.Checker.model(self, transaction=(inst), flag=True)
        #rdtype=decoder_rdtype.Checker.model(self, transaction=(inst), flag=True)
        
        immediate=decoder_immediate.Checker.model(self, transaction=(inst, csrs), flag=True)
        mem_access=decoder_mem_access.Checker.model(self, transaction=(inst), flag=True)
        trapcause=decoder_trapcause.Checker.model(self, transaction=(inst, csrs), flag=True)
        inst_type=decoder_insttype.Checker.model(self, transaction=(inst, csrs), flag=True)
        
        fn=decoder_fn.Checker.model(self, transaction=(inst, csrs), flag=True)
        funct3=((inst&0x7000)>>12)
        temp1=(fn<<3)|(funct3)
        if inst_type==6:
           temp1=trapcause
        microtrap=(mem_access in [3,4,5] ) or ( inst_type==5) 
        
        
        decode_out=DecodeOut()
        decode_out.op_addr_rs1addr=rs1
        decode_out.op_addr_rs2addr=rs2
        decode_out.op_addr_rd=rd
        decode_out.op_type_rs1type=rs1type
        decode_out.op_type_rs2type=rs2type
        decode_out.meta_inst_type=inst_type
        decode_out.meta_memaccess=mem_access
        decode_out.meta_funct_cause=temp1
        decode_out.meta_microtrap=microtrap
        decode_out.meta_immediate=immediate
        decode_out.compressed=False
        decode=decode_out.get()
        if not flag:
           self.expected_output.append( decode )
        else:
           return decode
     
    
class MyScoreboard(Scoreboard):
    # This is the scoreboard class. You need to define how the comparison needs
    # to be happen between data received from dut and that received from the
    # model
    def decode(self, got, exp, log, **_):
        got_output=got
        exp_output=exp
        if got_output != exp_output:
           log.info(__file__ + ": Output differs Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
           exit(1)
            

class IMonitor(Monitor):
    """Observes inputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('module_fn_decode','inputs')
    _signals.update(get_signals('mkstage2','wires',['decode_opfetch']))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))

        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "fn_decode.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)

    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 1 more second
            yield Timer(1,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            if self.decode_opfetch.value.integer == 1 :
                # sample the inputs and send to _recv method. All samples must
                # be sent to _recv. This is what is available to the model. IT
                # could be a tuple, dictionary, list, etc. The same must be
                # assumed in the model as well
                vec = (self.inst.value.integer,
                       self.csrs.value.integer,
                       self.compressed.value.integer)
                self._recv(vec)
                if self.enlog:
                    for x in vec:
                        self.log.debug('fn_decode.imon: '+str(x))

class OMonitor(Monitor):
    """Observes outputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('module_fn_decode','outputs')
    _signals.update(get_signals('mkstage2','wires',['decode_opfetch']))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        """tb must be an instance of the Testbench class."""
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "fn_decode.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)
   
    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 5 more nano-second. This is so that the output from the
            # combo is available. Doing it at 1ns (same as inputs) might not
            # work.
            yield Timer(5,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            if self.decode_opfetch.value.integer == 1 :
                # check if the value is not don't care. Then send the value
                # to _recv method. This _recv will act as the actual output for
                # comparison in scoreboard.
                if (self.decode.value is not None):
                    self._recv(self.decode.value.integer)
                    if self.enlog:
                        self.log.debug('fn_decode.omon: ' + str(self.decode.value.integer))

