from dataclasses import dataclass
import re
from enum import Enum, IntEnum, unique, auto
import mycheckers.test_fn_valid_csr_access as test_fn_valid_csr_access
from mycheckers.decoder_defines  import *
from mycheckers.decoder_inst_defines  import *

@dataclass(init=False)
class Decode:

  rs1: int = 0
  rs2: int = 0
  rd : int=0
  rdtype: int=0
  rs1type: int=0
  rs2type: int=0
  insttype: int=0
  rs3: int=0
  rs3type: int=0
  mem_access: int=0
  fn: int=0
  trapcause: int=0
  imm: int=0
  word32: int=0
  
  
  def set(self, inst, compressed, csrs):
      self.rs1=(inst & 0xF8000)>>15
      self.rs2=(inst&0x1F00000)>>20
      self.rs1type=0
      self.rs2type=0
      self.insttype=inst_type.TRAP.value
      self.rd=(inst &0xF80)>>7
      self.rdtype=0
      self.rs3=0
      self.rs3type=0
      self.mem_access=0
      self.fn=0
      self.trapcause=2
      self.word32=0
      bit31=((inst&0x80000000)>>31)
      self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
      fs=((csrs.csr_mstatus&0x4000)>>14)|((csrs.csr_mstatus&0x2000)>>13)
      inst_bin=bin(inst)[2:].zfill(32)
      funct3=(inst &0x7000)>>12
      address_is_valid=[x.value for x in list(csrs_enum)]
      s_address_is_valid=[x.value for x in list(supervisor_csrs_enum)]
      access_is_valid=bool(test_fn_valid_csr_access.Checker.model(self, transaction=(((inst&0xFFF00000)>>20), ((inst&0xF8000)>>15), ((inst&0x3000)>>12), ((csrs.csr_mstatus&0x100000)>>20) , csrs.prv), flag=True)==1)
      #frm=csrs.frm
      frm=0 #change this to the above comment once floating point is enabled
      valid_rounding = (frm not in [5,6,7]) if funct3==7 else (funct3 not in [5,6])
      if re.match(rf'\b{rv64.LUI.value}\b', inst_bin) :
         self.rs1=0
         self.rs2=0
         self.rs2type=1
         self.insttype=inst_type.ALU.value
         self.imm=(inst&0xFFFFF000)
      if re.match(rf'\b{rv64.AUIPC.value}\b', inst_bin) :
         self.rs1=0
         self.rs1type=1
         self.rs2=0
         self.rs2type=1
         self.insttype=inst_type.ALU.value
         self.imm=(inst&0xFFFFF000)
      if re.match(rf'\b{rv64.JAL.value}\b', inst_bin) :
         self.rs1=0        
         self.rs1type=1
         self.rs2=0 
         self.rs2type= 3 if compressed else 2
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|((inst&0xFF000))|((inst&0x100000)>>9)|((inst&0x7FE00000)>>20)
         self.insttype=inst_type.JAL.value
      if re.match(rf'\b{rv64.JALR.value}\b', inst_bin) :
         self.rs1type=1
         self.rs2=0
         self.rs2type= 3 if compressed else 2
         self.insttype=inst_type.JALR.value
      if re.match(rf'\b{rv64.CSRRWI.value}\b', inst_bin) :
         self.rs1=0
         self.rs2=0 
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         if ( ((inst & 0xFFF00000)>>20) in address_is_valid or (((inst & 0xFFF00000)>>20) in s_address_is_valid and ((csrs.csr_misa&0x40000)>>18)==1) ) and access_is_valid:
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.CSRRSI.value}\b', inst_bin) :
         self.rs1=0
         self.rs2=0 
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         if ( ((inst & 0xFFF00000)>>20) in address_is_valid or (((inst & 0xFFF00000)>>20) in s_address_is_valid and ((csrs.csr_misa&0x40000)>>18)==1) ) and access_is_valid:
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.CSRRCI.value}\b', inst_bin) :  
         self.rs1=0
         self.rs2=0 
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         if ( ((inst & 0xFFF00000)>>20) in address_is_valid or (((inst & 0xFFF00000)>>20) in s_address_is_valid and ((csrs.csr_misa&0x40000)>>18)==1) ) and access_is_valid:
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.CSRRW.value}\b', inst_bin) :
         self.rs2=0 
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         if ( ((inst & 0xFFF00000)>>20) in address_is_valid or (((inst & 0xFFF00000)>>20) in s_address_is_valid and ((csrs.csr_misa&0x40000)>>18)==1) ) and access_is_valid:
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.CSRRS.value}\b', inst_bin) :
         self.rs2=0 
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         if ( ((inst & 0xFFF00000)>>20) in address_is_valid or (((inst & 0xFFF00000)>>20) in s_address_is_valid and ((csrs.csr_misa&0x40000)>>18)==1) ) and access_is_valid:
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.CSRRC.value}\b', inst_bin) :  
         self.rs2=0
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         if ( ((inst & 0xFFF00000)>>20) in address_is_valid or (((inst & 0xFFF00000)>>20) in s_address_is_valid and ((csrs.csr_misa&0x40000)>>18)==1) ) and access_is_valid:
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.SFENCE_VMA.value}\b', inst_bin) :  
         self.mem_access=5
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
      if re.match(rf'\b{rv64.LWU.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
      if re.match(rf'\b{rv64.LD.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
      if re.match(rf'\b{rv64.LB.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
      if re.match(rf'\b{rv64.LH.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
      if re.match(rf'\b{rv64.LW.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
      if re.match(rf'\b{rv64.LBU.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
      if re.match(rf'\b{rv64.LHU.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
      if re.match(rf'\b{rv64.FENCE.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
         self.mem_access=3
         self.rs2type= 3 if compressed else 2
      if re.match(rf'\b{rv64.FENCE_TSO.value}\b', inst_bin) :  
         self.rs2=0
         self.mem_access=3
         self.insttype=inst_type.MEMORY.value
         self.rs2type= 3 if compressed else 2
      if re.match(rf'\b{rv64.PAUSE.value}\b', inst_bin) :  
         self.rs2=0
         self.mem_access=3 #?? 
         self.insttype=inst_type.MEMORY.value
         self.rs2type= 3 if compressed else 2
      if re.match(rf'\b{rv64.FENCE_I.value}\b', inst_bin) :  
         self.rs2=0
         self.insttype=inst_type.MEMORY.value
         self.mem_access=4
         self.rs2type= 3 if compressed else 2
      if re.match(rf'\b{rv64.FLW.value}\b', inst_bin) :  
         self.rs2=0
         self.rdtype=1
         self.insttype= inst_type.MEMORY.value if ((csrs.csr_misa&0x20)>>5)==1 and fs!=0 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.FSW.value}\b', inst_bin) :  
         self.insttype= inst_type.MEMORY.value if ((csrs.csr_misa&0x20)>>5)==1 and fs!=0 else inst_type.TRAP.value
         self.rs2type=4
         self.rd=0
         self.mem_access= 1
         if (csrs.csr_misa&0x20)>>5==1:
            self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|((inst&0x80)>>7)
      if re.match(rf'\b{rv64.FLD.value}\b', inst_bin) :  
         self.rs2=0
         self.rdtype=1
         self.insttype= inst_type.MEMORY.value if ((csrs.csr_misa&0x20)>>5)==1 and  ((csrs.csr_misa&0x8)>>3)==1 and fs!=0 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.FSD.value}\b', inst_bin) :  
         self.insttype= inst_type.MEMORY.value if ((csrs.csr_misa&0x20)>>5)==1 and  ((csrs.csr_misa&0x8)>>3)==1 and fs!=0 else inst_type.TRAP.value
         self.rd=0
         self.rs2type=4
         self.mem_access=1
         if (csrs.csr_misa&0x20)>>5==1:
            self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|((inst&0x80)>>7)
      if re.match(rf'\b{rv64.FLQ.value}\b', inst_bin) :  
         self.rs2=0
         self.rdtype=1
      if re.match(rf'\b{rv64.FLH.value}\b', inst_bin) :  
         self.rs2=0
         self.rdtype=1
      if re.match(rf'\b{rv64.ADDI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=funct3
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SLTI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=12
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SLTIU.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=14
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.XORI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=funct3
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.ORI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=funct3
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.ANDI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=funct3
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SLLI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=funct3
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SRLI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=5
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SRAI.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=11
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.ADDIW.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=funct3
         self.insttype=inst_type.ALU.value
         self.word32=1
      if re.match(rf'\b{rv64.SLLIW.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=funct3
         self.word32=1
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SRLIW.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1
         self.fn=5
         self.word32=1
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SRAIW.value}\b', inst_bin) : 
         self.rs2=0
         self.rs2type=1     
         self.fn=11
         self.word32=1
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.BEQ.value}\b', inst_bin) : 
         self.rd=0
         self.fn=(0x2)|(funct3&0x1)
         self.insttype=inst_type.BRANCH.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80)<<4)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|0
      if re.match(rf'\b{rv64.BNE.value}\b', inst_bin) : 
         self.rd=0
         self.fn=(0x2)|(funct3&0x1)
         self.insttype=inst_type.BRANCH.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80)<<4)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|0
      if re.match(rf'\b{rv64.BLT.value}\b', inst_bin) : 
         self.rd=0
         self.fn=((0x8)|(funct3))
         self.insttype=inst_type.BRANCH.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80)<<4)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|0
      if re.match(rf'\b{rv64.BGE.value}\b', inst_bin) : 
         self.rd=0
         self.fn=((0x8)|(funct3))
         self.insttype=inst_type.BRANCH.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80)<<4)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|0
      if re.match(rf'\b{rv64.BLTU.value}\b', inst_bin) : 
         self.rd=0
         self.fn=((0x8)|(funct3))
         self.insttype=inst_type.BRANCH.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80)<<4)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|0
      if re.match(rf'\b{rv64.BGEU.value}\b', inst_bin) : 
         self.rd=0
         self.fn=((0x8)|(funct3))
         self.insttype=inst_type.BRANCH.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80)<<4)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|0
      if re.match(rf'\b{rv64.SB.value}\b', inst_bin) :
         self.rd=0
         self.mem_access=1
         self.insttype=inst_type.MEMORY.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|((inst&0x80)>>7)
      if re.match(rf'\b{rv64.SH.value}\b', inst_bin) :
         self.rd=0   
         self.mem_access=1
         self.insttype=inst_type.MEMORY.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|((inst&0x80)>>7)
      if re.match(rf'\b{rv64.SW.value}\b', inst_bin) :
         self.rd=0   
         self.mem_access=1
         self.insttype=inst_type.MEMORY.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|((inst&0x80)>>7)
      if re.match(rf'\b{rv64.SD.value}\b', inst_bin) :
         self.rd=0   
         self.mem_access=1
         self.insttype=inst_type.MEMORY.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(( 0xFF if bit31==1 else 0)<<12)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0xF00)>>7)|((inst&0x80)>>7)
      if re.match(rf'\b{rv64.LR_W.value}\b', inst_bin) :
         self.mem_access=2
         self.word32=1
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
         self.fn=((inst&0x38000000)>>26)|(0x1)
      if re.match(rf'\b{rv64.SC_W.value}\b', inst_bin) :
         self.mem_access=2
         self.word32=1
         self.fn=((inst&0x38000000)>>26)|(0x1)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOSWAP_W.value}\b', inst_bin) :
         self.mem_access=2
         self.fn=((inst&0x38000000)>>26)|(0x1)
         self.word32=1
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
         self.imm=0
      if re.match(rf'\b{rv64.AMOADD_W.value}\b', inst_bin) :
         self.mem_access=2
         self.word32=1
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
         self.imm=0
      if re.match(rf'\b{rv64.AMOXOR_W.value}\b', inst_bin) :
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.imm=0
         self.word32=1
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOAND_W.value}\b', inst_bin) :
         self.imm=0
         self.word32=1
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOOR_W.value}\b', inst_bin) :
         self.word32=1
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMIN_W.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.word32=1
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMAX_W.value}\b', inst_bin) :
         self.word32=1
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMINU_W.value}\b', inst_bin) :
         self.imm=0
         self.word32=1
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMAXU_W.value}\b', inst_bin) :
         self.word32=1
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.LR_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst&0x38000000)>>26)|(0x1)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.SC_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst&0x38000000)>>26)|(0x1)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOSWAP_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst&0x38000000)>>26)|(0x1)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOADD_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOXOR_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOAND_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOOR_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMIN_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMAX_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMINU_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.AMOMAXU_D.value}\b', inst_bin) :
         self.imm=0
         self.mem_access=2
         self.fn=((inst & 0xE0000000)>>28)|((inst&0x8000000)>>27)
         self.insttype=inst_type.MEMORY.value if (csrs.csr_misa&0x1)==1 else inst_type.TRAP.value
      if re.match(rf'\b{rv64.ADD.value}\b', inst_bin):
         self.fn=0  
         self.insttype=inst_type.ALU.value 
      if re.match(rf'\b{rv64.SUB.value}\b', inst_bin):
         self.fn=10   
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SLL.value}\b', inst_bin):
         self.fn=funct3   
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SLT.value}\b', inst_bin):
         self.fn=12   
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SLTU.value}\b', inst_bin):
         self.fn=14  
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SRA.value}\b', inst_bin):
         self.fn=11  
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.XOR.value}\b', inst_bin):
         self.fn=funct3  
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.SRL.value}\b', inst_bin):
         self.fn=5
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.OR.value}\b', inst_bin):
         self.fn=funct3   
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.AND.value}\b', inst_bin):
         self.fn=funct3
         self.insttype=inst_type.ALU.value
      if re.match(rf'\b{rv64.ADDW.value}\b', inst_bin):
         self.fn=0   
         self.insttype=inst_type.ALU.value
         self.word32=1
      if re.match(rf'\b{rv64.SUBW.value}\b', inst_bin):
         self.fn=10   
         self.insttype=inst_type.ALU.value
         self.word32=1
      if re.match(rf'\b{rv64.SLLW.value}\b', inst_bin):
         self.fn=funct3   
         self.insttype=inst_type.ALU.value
         self.word32=1
      if re.match(rf'\b{rv64.SRLW.value}\b', inst_bin):
         self.fn=5   
         self.insttype=inst_type.ALU.value
         self.word32=1
      if re.match(rf'\b{rv64.SRAW.value}\b', inst_bin):
         self.fn=11  
         self.insttype=inst_type.ALU.value
         self.word32=1
      if re.match(rf'\b{rv64.ECALL.value}\b', inst_bin):   
         self.insttype=inst_type.TRAP.value
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         if (((csrs.csr_misa&0x100000)>>20)==1 and csrs.prv==0):
            self.trapcause=8
         elif (((csrs.csr_misa&0x40000)>>18)==1 and csrs.prv==1):
            self.trapcause=9
         else:
            self.trapcause=11
         self.rs2=0
      if re.match(rf'\b{rv64.EBREAK.value}\b', inst_bin):   
         self.insttype=inst_type.TRAP.value
         self.trapcause=3
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         self.rs2=0
      if re.match(rf'\b{rv64.SRET.value}\b', inst_bin):
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)   
         if (csrs.csr_misa & 0x40000)>>18==1 and (csrs.prv == 3 or (csrs.prv == 1 and (csrs.csr_mstatus&0x400000)>>22==0)):
             self.insttype=inst_type.SYSTEM_INSTR.value
         self.rs2=0
      if re.match(rf'\b{rv64.MRET.value}\b', inst_bin):  
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         self.rs2=0 
         if csrs.prv == 3 :
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.WFI.value}\b', inst_bin):   
         self.imm=(bit31<<31)|(( 0x7FF if bit31==1 else 0)<<20)|(inst&0xFF000)|((inst&0x80000000)>>20)|((inst&0x7E000000)>>20)|((inst&0x1E00000)>>20)|((inst&0x100000)>>20)
         self.rs2=0
         if csrs.prv == 3  or (csrs.csr_mstatus&0x200000)>>21==0:
             self.insttype=inst_type.SYSTEM_INSTR.value
      if re.match(rf'\b{rv64.MUL.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.MULH.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.MULHSU.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.MULHU.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.DIV.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.DIVU.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.REM.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.REMU.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
      if re.match(rf'\b{rv64.MULW.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
         self.word32=1
      if re.match(rf'\b{rv64.DIVW.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
         self.word32=1
      if re.match(rf'\b{rv64.DIVUW.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
         self.word32=1
      if re.match(rf'\b{rv64.REMW.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
         self.word32=1
      if re.match(rf'\b{rv64.REMUW.value}\b', inst_bin):
         self.insttype=inst_type.MULDIV.value
         self.word32=1
      if re.match(rf'\b{rv64.FMADD_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2	
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMSUB_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FNMADD_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FNMSUB_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMADD_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMSUB_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FNMADD_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FNMSUB_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.rs3=(inst&0xF8000000)>>27
         self.rs3type=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FADD_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSUB_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMUL_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FDIV_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSQRT_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSGNJ_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSGNJN_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSGNJX_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMIN_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMAX_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_W_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_WU_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMV_X_W.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if  ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FEQ_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FLT_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FLE_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCLASS_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_S_W.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_S_WU.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMV_W_X.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if  ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_L_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_LU_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_S_L.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_S_LU.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         self.word32=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value      
      if re.match(rf'\b{rv64.FADD_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSUB_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMUL_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FDIV_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSQRT_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSGNJ_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSGNJN_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FSGNJX_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMIN_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMAX_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_S_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_D_S.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FEQ_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if  ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FLT_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if  ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FLE_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2type=4
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if  ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCLASS_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_W_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_WU_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_D_W.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_D_WU.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_L_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_LU_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMV_X_D.value}\b', inst_bin):
         self.rs1type=2
         self.rs2=0
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if  ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_D_L.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FCVT_D_LU.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if valid_rounding and ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{rv64.FMV_D_X.value}\b', inst_bin):
         self.rs2=0
         self.rdtype=1
         if ((csrs.csr_misa&0x20)>>5)==1 or ((csrs.csr_misa&0x8)>>3)==1:
            self.fn=(inst &0x3c)>>2
         if  ((csrs.csr_misa&0x20)>>5)==1  and fs!=0:
            self.insttype=inst_type.FLOAT.value
      if re.match(rf'\b{inst_op.AUIPC_OP.value}\b', inst_bin[25:30]):
         self.rs1type=1
         self.imm=(inst&0xFFFFF000)
         self.rs1=0
      return
