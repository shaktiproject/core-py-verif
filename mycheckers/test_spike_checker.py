import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard

import logging
from utils import *
import random
import sys

import spike_intf

from cocotb_env import Tb
class Checker:

    def __init__(self, dut, enlog=False):
        
        self.name = "spike_model"
        self.log = logging.getLogger("cocotb")

        if enlog:
            self.log.info('Setting up Output Monitor')
        self.output_mon = OMonitor(dut)

        if enlog:
            self.log.info('Setting up Input Monitor')
        self.input_mon = IMonitor(dut, callback=self.model)
        
        self.log.info('Setting up Score-board')
        self.expected_output = []
        self.scoreboard = MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)       
        self.spike_ifc = spike_intf.spike_intf('dut.elf')
        self.spike_ifc.initialise('RV64I')

    
    def model(self, transaction, flag=False):                            
        """Model """
        
        stop, step = transaction

        a = 0x1000
        b = 0x1020
        spike_reg = []    
        if stop:
            self.log.info("Calling destroy sim")
            spike_reg.clear()
            self.spike_ifc.destroy_sim

        elif step == True:
            spike_reg.clear()
            self.spike_ifc.single_step()
            for n in range (a, b):
                spike_reg.append(self.spike_ifc.get_variable(n))
            if not flag:                              
                self.expected_output.append( spike_reg )
            else:
                return spike_reg
		
class MyScoreboard(Scoreboard):
    # This is the scoreboard class. You need to define how the comparison needs
    # to be happen between data received from dut and that received from the
    # model
    def compare(self, got, exp, log, **_):
        if got != exp:
            log.info(__file__ + ": Expected and Got Output Differs")
            self.log.info('Expected Output')
            for x in range(len(exp)):
                self.log.info('[{0}]:{1}'.format(x,hex(exp[x])))
            self.log.info('DUT Output')
            for x in range(len(got)):
                self.log.info('[{0}]:{1}'.format(x,hex(got[x])))
            exit(1)
            
                    

class IMonitor(Monitor):
    """Observes inputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('mkTbSoc', 'wire')
    _signals.update(get_signals('mkTbSoc','wire',['soc_log', 'write_dump']))
    def __init__(self, dut, callback=None, event=None, enlog=False):
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))

        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "soc.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)


    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        #spike_intf.initialise(ifc, sys.argv[2])
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 1 more second
            yield Timer(1,units="ns")
 
            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            commitpacket = self.soc_log.value                                 
            commitvalid = commitpacket.integer >> (commitpacket.n_bits-1)
            willfire = self.write_dump.value.integer

            if willfire == 1 and commitvalid == 1:
                # sample the inputs and send to _recv method. All samples must
                # be sent to _recv. This is what is available to the model. IT
                # could be a tuple, dictionary, list, etc. The same must be
                # assumed in the model as well
                vec = (False, True)
                self._recv(vec)
                if self.enlog:
                    for x in vec:
                        self.log.debug('soc.imon: '+str(x))
                    

class OMonitor(Monitor):
    """Observes outputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('mkregisterfile', 'register')
    _signals = get_signals('mkTbSoc', 'wire') 
    _signals.update(get_signals('mkregisterfile', 'register',['arr_reg']))
    _signals.update(get_signals('mkTbSoc','wire',['soc_log', 'write_dump']))

   
    def __init__(self, dut, callback=None, event=None, enlog=False):
        """tb must be an instance of the Testbench class."""
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "soc.omon"                                                          
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)
   
    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        arr = []
        
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)
            
            # increment by 5 more nano-second. This is so that the output from the
            # combo is available. Doing it at 1ns (same as inputs) might not
            # work.
            yield Timer(5,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            commitpacket = self.soc_log.value                                 
            commitvalid = commitpacket.integer >> (commitpacket.n_bits-1)
            willfire = self.write_dump.value.integer
            if willfire == 1 and commitvalid == 1:                                      
                # check if the value is not don't care. Then send the value
                # to _recv method. This _recv will act as the actual output for
                # comparison in scoreboard.
                arr.clear()
                for k in range(32):
                    arr.append(self.arr_reg[k].value.integer)
                
                self._recv(arr)                             
                #obj.clear()
                if self.enlog:
                    self.log.debug('soc.omon: ' + str(arr))        
               
  


