import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard

import logging
from utils import *
import random
import sys
global rg_renameid
rg_renameid=0
rg_rf_board={0: {'lock': 0, 'id': 0}, 1: {'lock': 0, 'id': 0}, 2: {'lock': 0, 'id': 0}, 3: {'lock': 0, 'id': 0}, 4: {'lock': 0, 'id': 0}, 5: {'lock': 0, 'id': 0}, 6: {'lock': 0, 'id': 0}, 7: {'lock': 0, 'id': 0}, 8: {'lock': 0, 'id': 0}, 9: {'lock': 0, 'id': 0}, 10: {'lock': 0, 'id': 0}, 11: {'lock': 0, 'id': 0}, 12: {'lock': 0, 'id': 0}, 13: {'lock': 0, 'id': 0}, 14: {'lock': 0, 'id': 0}, 15: {'lock': 0, 'id': 0}, 16: {'lock': 0, 'id': 0}, 17: {'lock': 0, 'id': 0}, 18: {'lock': 0, 'id': 0}, 19: {'lock': 0, 'id': 0}, 20: {'lock': 0, 'id': 0}, 21: {'lock': 0, 'id': 0}, 22: {'lock': 0, 'id': 0}, 23: {'lock': 0, 'id': 0}, 24: {'lock': 0, 'id': 0}, 25: {'lock': 0, 'id': 0}, 26: {'lock': 0, 'id': 0}, 27: {'lock': 0, 'id': 0}, 28: {'lock': 0, 'id': 0}, 29: {'lock': 0, 'id': 0}, 30: {'lock': 0, 'id': 0}, 31: {'lock': 0, 'id': 0}}
class Checker:

    def __init__(self, dut, enlog=False):
        
        self.name = "scoreboard"
        self.log = logging.getLogger("cocotb")

        if enlog:
            self.log.info('Setting up Output Monitor')
        self.output_mon = OMonitor(dut)

        if enlog:
            self.log.info('Setting up Input Monitor')
        self.input_mon = IMonitor(dut, callback=self.model)
        
        self.log.info('Setting up Score-board')
        self.expected_output = []
        self.scoreboard = MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)

    
    def model(self, transaction, flag=False):
        """Model """
        # This function acts as the model for the dut under test
        lock, rls, en_lck, en_rls = transaction
        global rg_renameid
        lock_str=''
        id_str=''
        if rg_renameid==16:
           rg_renameid=0
        for i in range(31,-1, -1):
            lock_str+=str(rg_rf_board[i]['lock'])
            id_str+=str(BinaryValue(value=rg_rf_board[i]['id'], n_bits=5, binaryRepresentation=2, bigEndian=False))[-4:]
        output=int((lock_str+id_str), 2)
        if en_lck:
           index=(lock&0x1f)
           rg_rf_board[index]['lock']=1 if index!=0 else 0
           rg_rf_board[index]['id']=rg_renameid if index!=0 else 0
           rg_renameid=(rg_renameid+1)
        if en_rls and (((rls&0x1E0)>>5)==rg_rf_board[(rls&0x1f)]['id']):
           index=(rls&0x1f)
           rg_rf_board[index]['lock']=0 if index!=0 else rg_rf_board[index]['lock']
           
        if not flag:
           self.expected_output.append( output )
        else:
           return output
        
       
class MyScoreboard(Scoreboard):
    # This is the scoreboard class. You need to define how the comparison needs
    # to be happen between data received from dut and that received from the
    # model
    def compare(self, got, exp, log, **_):
        got_output=got
        exp_output=exp
        if got_output != exp_output:
           log.info(__file__ + ": Output differs Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
           exit(1)
            

class IMonitor(Monitor):
    """Observes inputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('scoreboard','inputs')
    _signals.update(get_signals('mkstage3','wires',['base_arith_fire', 'op1_avail', 'op2_avail']))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))

        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "scoreboard.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)

    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 1 more second
            yield Timer(1,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            
            # sample the inputs and send to _recv method. All samples must
            # be sent to _recv. This is what is available to the model. IT
            # could be a tuple, dictionary, list, etc. The same must be
            # assumed in the model as well
            if self.en_lock.value.integer==1 or self.en_release.value.integer==1:
               vec = (self.lock.value.integer, 
                      self.rls.value.integer,
                      self.en_lock.value.integer,
                      self.en_release.value.integer)
               self._recv(vec)
               if self.enlog:
                  for x in vec:
                     self.log.debug('scoreboard.imon: '+str(x))

class OMonitor(Monitor):
    """Observes outputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('scoreboard','outputs')
    _signals.update(get_signals('mkstage3','wires',['fwding', 'op1_avail', 'op2_avail']))
    _signals.update(get_signals('scoreboard','inputs'))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        """tb must be an instance of the Testbench class."""
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "scoreboard.omon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)
   
    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 5 more nano-second. This is so that the output from the
            # combo is available. Doing it at 1ns (same as inputs) might not
            # work.
            yield Timer(5,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            # check if the value is not don't care. Then send the value
            # to _recv method. This _recv will act as the actual output for
            # comparison in scoreboard.
            if (self.rd.value is not None) and (self.en_lock.value.integer==1 or self.en_release.value.integer==1):
               self._recv(self.board.value.integer)
               if self.enlog:
                  self.log.debug('scoreboard.omon: ' + str(self.board.value.integer))



