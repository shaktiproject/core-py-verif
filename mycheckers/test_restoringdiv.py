import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard

import logging
from utils import *
import random
import sys
import numpy as np
from bitstring import BitArray

class Checker:

    def __init__(self, dut, enlog=False):
        
        self.name = "mkrestoring_div"
        self.log = logging.getLogger("cocotb")

        if enlog:
            self.log.info('Setting up Output Monitor')
        self.output_mon = OMonitor(dut)

        if enlog:
            self.log.info('Setting up Input Monitor')
        self.input_mon = IMonitor(dut, callback=self.model)
        
        self.log.info('Setting up Score-board')
        self.expected_output = []
        self.scoreboard = MyScoreboard(dut)
        self.scoreboard.add_interface(self.output_mon, self.expected_output)

    
    def model(self, transaction, flag=False):
        """Model """
        # This function acts as the model for the dut under test
        in1, in2, funct3, wordop = transaction
        reslt = 0
        result = 0
        if(wordop == 1):
               _in1 = bin(in1)[2:].zfill(64)  
               _in2 = bin(in2)[2:].zfill(64)
               if (funct3 == 4 or funct3 ==6):
                  in1 = "".join([_in1[32]*32, _in1[32:]])
                  in2 = "".join([_in2[32]*32, _in2[32:]])
               else:
                  in1 = (_in1)[32:].zfill(64)
                  in2 = (_in2)[32:].zfill(64)
               in1 = BitArray(bin=in1).int
               in1 = int(np.binary_repr(in1, width=64), 2)
               in2 = BitArray(bin=in2).int
               in2 = int(np.binary_repr(in2, width=64), 2)
        if (funct3 == 4):  
               if in2==0:
                  result=-1
               else:
                  _op1=bin(in1)[2:].zfill(64)
                  op1=BitArray(bin=_op1).int
                  _op2=bin(in2)[2:].zfill(64)
                  op2=BitArray(bin=_op2).int
                  rem=abs(op1)%abs(op2)
                  if op1<0:
                    op1=(abs(op1)-rem)*(-1)
                  else:
                    op1=op1-rem
                  result=int(np.floor_divide(op1, op2))
        elif (funct3 == 5):
               if in2==0:
                  result=-1
               else:
                  result = int(in1)//int(in2)
        elif (funct3 == 7):
               if(in2 == 0):
                  result = int(in1)
               else:  
                  result = int(in1)%int(in2)
        elif (funct3 == 6):
               if in2 == 0:
                  result = int(in1)
               else:
                  _op1=bin(in1)[2:].zfill(64)
                  op1=BitArray(bin=_op1).int
                  _op2=bin(in2)[2:].zfill(64)
                  op2=BitArray(bin=_op2).int
                  result= (np.fmod(op1, op2))
        result=int(np.binary_repr(result, width=64), 2)

        if wordop == 1:
               reslt = bin(result)[2:].zfill(64)
               result = "".join([reslt[32]*32, reslt[32:]])
               result=BitArray(bin=result).int
               result = int(np.binary_repr(result, width=64), 2)
        else:
               result = result
        
        if not flag:
           self.expected_output.append( result )
        else:
           return int(result)
        
    
class MyScoreboard(Scoreboard):
    # This is the scoreboard class. You need to define how the comparison needs
    # to be happen between data received from dut and that received from the
    # model
    def compare(self, got, exp, log, **_):
        got_output=got
        exp_output=exp
        #if got_output == exp_output:
         #   print("Ouput matched Expected")
        if got_output != exp_output:
           log.info(__file__ + ": Output differs Expected: {0!s}. differ Received: {1!s}.".format( hex(exp_output), hex(got_output)))
           exit(1)
            

class IMonitor(Monitor):
    """Observes inputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('mkrestoring_div','inputs')
    #_signals.update(get_signals('mkstage3','wires',['base_arith_fire', 'op1_avail', 'op2_avail']))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))

        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "mkrestoring_div.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)

    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 1 more second
            yield Timer(1,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            if self.input.value.integer == 1:

                # sample the inputs and send to _recv method. All samples must
                # be sent to _recv. This is what is available to the model. IT
                # could be a tuple, dictionary, list, etc. The same must be
                # assumed in the model as well
                vec = (self.in1.value.integer, 
                       self.in2.value.integer,
                       self.funct3.value.integer, 
                       self.wordop.value.integer)
                self._recv(vec)
                if self.enlog:
                    for x in vec:
                        self.log.debug('mkrestoring_div.imon: '+str(x))

class OMonitor(Monitor):
    """Observes outputs of DUT."""
    # utils has loaded the alias_signal.yaml. Use the below function to populate
    # the _signals as a dictionary of all alias-signal mapping. First argument
    # is the sub-module name (without hierarchy) and the next argument is the
    # category : inputs, outputs, registers.
    _signals = get_signals('mkrestoring_div','outputs')
    _signals.update(get_signals('mkrestoring_div','inputs',['output']))
    #_signals.update(get_signals('mkstage3','wires',['base_arith_fire', 'op1_avail', 'op2_avail']))
    
    def __init__(self, dut, callback=None, event=None, enlog=False):
        """tb must be an instance of the Testbench class."""
        # the following will populate the alias as methods of the self object so
        # that you can access the methods as self.op1, etc.
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))
        # set the clock that may or maynot be required
        self.clock = dut.CLK

        # The following 3 lines are necessary, else cocotb 1.5.2 will complaint.
        # You can change the name though. imon= input monitor, omon = output
        # monitor, smon = internal signal monitor, etc.
        self.name = "mkrestoring_div.imon"
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        Monitor.__init__(self, callback, event)
   
    @coroutine
    def _monitor_recv(self):
        # This is going to a forked coroutine which will sample the signals of
        # choice
        while True:
            # wait for posedge of clock
            yield RisingEdge(self.clock)

            # increment by 5 more nano-second. This is so that the output from the
            # combo is available. Doing it at 1ns (same as inputs) might not
            # work.
            yield Timer(5,units="ns")

            # we only trigger sample inputs when the base arithmetic operation
            # is detected in stage3 and that its a logical op
            if self.output.value.integer == 1:

                # check if the value is not don't care. Then send the value
                # to _recv method. This _recv will act as the actual output for
                # comparison in scoreboard.
                if (self.result.value is not None):
                    self._recv(self.result.value.integer) 
                    if self.enlog:
                        self.log.debug('mkrestoring_div.omon: ' + str(self.result.value.integer))

