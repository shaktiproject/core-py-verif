# Creating Checkers
This directory should contain only checker files of modules/submodules of the c-class design. A
checker in this context means: monitors + scoreboards and models. A checker can be defined for any
submodule of the core. Please refer to `test_fn_logic.py` for a quick example of the components of
the checker

## Building Signal Monitors

We typically include two set of monitors : input monitors and output monitors. However, you could
monitor internal signals or registers either within the input monitors or have a separate monitor
for each.

The input/output monitors are inherited from the
[Monitor](https://docs.cocotb.org/en/stable/library_reference.html#monitor) class of cocotb. 

### Capturing signals of interest
To capture the signals that need to be monitored, we use the `alias_signal.yaml` file. The
`get_signals` utility function can be used to capture these signals. It is recommended to use the
aliases for writing your code as it will be agnostic of the design signal name changes that may
happen over time.

The first argument of the `get_signals` function is the name of the verilog module and the second
argument is the category of signals which can be : inputs, outputs, wires and registers. Make sure
to wrap these values in single-quotes. The function will return a dictionary mapping of aliases and
signals. The output is captured in the local variable `_signals`. You can add more mappings to this
dictionary using `_signals.update({'alias_name': 'signal-name'})`. 

### Initializing the Monitors

The `__init__` function is used to initialize any monitor instance. This function requires the `dut`
SimBase handle to be provided as an argument. In case of input monitors, a callback function (which
is typically the function in which the models are implemented) may also be provided as a second
argument. The function definition is provided below for referece

```python
def __init__(self, dut, callback=None, event=None):
```

This function further uses the `_signals` dictionary to create local objects of the alias names
which can then on be referenced using `self.alias-name`. The following code achieves this

```python
  for alias, signal in self._signals.items():
    setattr(self, alias, getattr(dut, signal))
```

Towards the end of the function we assign a logger (which is also inherited from cocotb.log) without
which cocotb 1.5.2 complaints

### Sampling monitored signals

The aim of the monitors is to capture specific samples of the signals (at specific triggering
events) and pass it to the model to obtain the expected output. This functionality has to be
provided in the `_monitor_recv` method of the monitor class. This method will also be a
cocotb-routine which will be internally forked by cocotb.

In this function we first define a triggering condition to sample the monitored signals. Trigger
conditions can be as simple as rising edge of clock or complex conditions of the monitoring signals.
Once the trigger condition is hit, we sample the values of the signals using the `value` methods of
cocotb and dump them into the `self._recv` method of the Monitor class. This value dumps can be a
list, dictionary, tuples or any other structure which is comprehensible by the model.

### Printing/Logging

Often times you want to print certain things to the log/terminal. Use the `self.log` logger to do
so.

## Building Scoreboards

You can build a scoreboard which inherits the
[Scoreboard](https://docs.cocotb.org/en/stable/library_reference.html#module-cocotb_bus.scoreboard)
class of the cocotb. In the scoreboard we typically define the compare function which checks if the
expected output of the model matches the actual value obtained from the output monitors. More
complex scoreboards can be defined here.

At times, some models may have multiple outputs and the checks for each of these output can be
different. This behavior can also be captured in this function.

## Building your model

The model must mimick the output behavior of your dut module/submodule. It takes as input the
transaction sampled by your input monitors and supplies an output which can be consumed by the
scoreboard to compare against the values sampled by the output monitors.

## Building your checker

Once the monitors, scoreboards and models have been defined, you are not ready to build your top
level checker. This checker will be a custom class defined in the same file. 

We first instantiate the input and output monitors (and any other monitors that have been defined by you). While defining the input monitors we will use the model function as the callback argument. The output monitors may not require such a callback argument.

We also create an empty list to capture the expected outputs from the model. Finally, we instantiate
the scoreboard and connect the expected output list to the output monitor for checking.

## Calling the Checker in the `test_cclass.py`

Once your checker is ready, in `test_cclass.py` you should import the checker file and then
instantiate the class after the clock and reset and other routines have been declared.

Finally we instantiate
