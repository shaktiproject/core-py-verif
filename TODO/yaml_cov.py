import ruamel
from ruamel.yaml import YAML
import yaml as pyyaml
import re

yaml = YAML(typ="rt")
yaml.default_flow_style = False
yaml.allow_unicode = True
global isa_yaml, x, cov_str

def print_coverpoints(triggers, specific_trigger):
    cov_pt=''
    for i in range(0, len(triggers)):
        if triggers[i][0] == specific_trigger:
           cov_pt += '''\n        CoverPoint("top.{0}", xf = lambda sample: sample.{1} , bins = {2} ), '''.format(triggers[i][3],triggers[i][1], triggers[i][2])
    return cov_pt

def print_sample(triggers, specific_trigger):
    sample_pt = ''
    _type=''
    for i in range(0, len(triggers)):
        if triggers[i][4]=='dut_signals':
           if isa_yaml[x[0]][triggers[i][4]][triggers[i][1]]['type'] == 'uint':
               _type = 'self.'+triggers[i][1] + '.value.integer'
           elif isa_yaml[x[0]][triggers[i][4]][triggers[i][1]]['type'] == 'int':
               _type = 'self.'+triggers[i][1] +'.value.signed_integer'
        else:
            _type=str(isa_yaml[x[0]][triggers[i][4]][triggers[i][1]]['rel'])
        if triggers[i][0] == specific_trigger:
            sample_pt += '                sample.{0} = {1}\n'.format(triggers[i][1],  _type)
    return sample_pt

def print_trigger(trigger):
 tr_str=''
 for node, val in isa_yaml[x[0]]['triggers'][trigger].items():
    if 'type' in val.keys():
     if  val['type']=='uint':
        _type='.value.integer'
    else:
        _type=''
    symbol='in' if type(val['val'])!=int else '=='
    tr_str+='''              self.{0}{2} {1} and \\'''.format(node, symbol+ str(val['val']),_type) +'\n'
 return tr_str
    
file=open('/home/sneha/python/model_yaml.yaml','r')
isa_yaml = yaml.load(file)
x=list(isa_yaml.keys())
cov_str=''
f = open('cov_'+x[0]+'.py', 'w')
cov_str+='''import cocotb
from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge
from cocotb_bus.monitors import Monitor
from cocotb.binary import BinaryValue
from cocotb.result import TestFailure
from cocotb.log import SimLog
from cocotb_bus.scoreboard import Scoreboard
from cocotb_coverage.coverage import *
from mycoverage.coverage_utils import *
import logging
from utils import *
import random
import sys
from enum import Enum, IntEnum, unique, auto\n\n\n'''


 
dut_signals=list(isa_yaml[x[0]]['dut_signals'].keys())


cov_str+='''class Coverage(object):
    
    _signals = get_signals('module_{0}','inputs') \n'''.format(x[0])
    
for i in range(0, len(dut_signals)):
    cov_str+='''    _signals.update(get_signals({0}))\n'''.format(isa_yaml[x[0]]['dut_signals'][dut_signals[i]]['alias'])

decorator=x[0]
decorator=decorator.replace('_','')
decorator=decorator.replace('fn','Fn')
decorator=decorator.replace(decorator[2],decorator[2].upper())
triggers=[]
cov_str+= ''' 
    def __init__(self, dut, enlog=False):
        self.dut = dut
        self.log = logging.getLogger("cocotb")
        self.enlog = enlog
        for alias, signal in self._signals.items():
            setattr(self, alias, getattr(dut, signal))'''



dut_or_derived=''
for node, val in isa_yaml[x[0]]['coverpoints'].items():
    if val['xf'] in dut_signals:
       dut_or_derived='dut_signals'
    else:
      dut_or_derived='derived_signals'
    triggers.append((val['triggers'][0], val['xf'], val['bins'], node, dut_or_derived))
trigger_list=[]
t_list=[]
for i in range(0, len(triggers)):
    if triggers[i][0] not in t_list:
        cov_str += '''\n        cocotb.fork(self.sample_signals{0}())'''.format('_' + triggers[i][0])
    t_list.append(triggers[i][0])

for i in range(0, len(triggers)):
    if triggers[i][0] not in trigger_list:
        cov_str += '''\n\n    {0}Cov = coverage_section ('''.format(decorator+'_'+triggers[i][0]+'_')
        cov_str+=print_coverpoints(triggers, triggers[i][0])
        cov_str += ')\n'
        cov_str += '''    
            
    @{0}Cov
    def sample_coverage{3}(self, sample):
        pass 

    @coroutine
    def sample_signals{3}(self):
        while True:
            yield {1}
            yield {2} '''.format(decorator+'_'+triggers[i][0]+'_', isa_yaml[x[0]]['triggers']['timing'][0],
                                         isa_yaml[x[0]]['triggers']['timing'][1], '_'+triggers[i][0])
        _type=''

        cov_str+= ''' \n            if '''
        cov_str+=print_trigger(triggers[i][0])
        cov_str=cov_str[:-7]
        cov_str+=''':
                sample = InputSample()\n'''
        trigger_list.append(triggers[i][0])
        cov_str+=print_sample(triggers, triggers[i][0])
        cov_str+='''                if self.enlog:
                          self.log.info('Coverage Sampling : ' +str(sample))
                self.sample_coverage{0}(sample)'''.format('_'+triggers[i][0])
cov_str+='''\n\nclass InputSample:
    def __init__(self):\n'''
for i in range(0, len(triggers)):
    cov_str+='        self.{0}: {1}\n'.format(triggers[i][1], isa_yaml[x[0]][triggers[i][4]][triggers[i][1]]['type'])
f.write(cov_str)
