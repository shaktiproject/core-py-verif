from cocotb_test.simulator import run
import pytest
import os
import shlex
import shutil
from pathlib import Path
import logging
#from utils import *
import utils
from user_config import *
import filecmp
from cocotb_coverage.coverage import *

# here we derive additional constants based on the settings in config.py
commonlib=cclass_path+'/common_verilog/'
hw_verilog=cclass_path+'/build/hw/verilog/'
bscbuild=cclass_path+'/build/hw/intermediate/'

structfile = './struct_info.txt'
if shutil.which('bsc') is None:
    utils.log.error('BSC not available in $PATH')
    raise SystemExit

# compile time args required for verilator
compile_args=shlex.split("-O3 --x-assign fast --x-initial fast --noassert \
        --bbox-sys -Wno-STMTDLY -Wno-UNOPTFLAT -Wno-WIDTH -Wno-lint \
        -Wno-COMBDLY -Wno-TIMESCALEMOD -Wno-INITIALDLY --autoflush \
        -y {0}/Verilog/ -y {1} -y {2} --output-split 20000  --output-split-ctrace 10000 \
        -DBSV_RESET_FIFO_HEAD -DBSV_RESET_FIFO_ARRAY".format(bsclib, commonlib, hw_verilog))

# load the test-list yaml
testyaml = utils.load_yaml(testlist_path)

# we will run the tcl files now to capture the bluespec structs used in the
# design
utils.log.info('Running bluetcl to capture structs used in the design')
utils.shellCommand(shlex.split('BSC_BUILDDIR="{0}" ./tcllibs/genStructInfo.tcl > {1}'.\
            format(bscbuild, 'struct_info.txt'))).run()

# we will convert the bsv struct information to python dataclasses
utils.log.info('Converting BSV structs to Python Dataclasses in : bsvstruct_class.py')
utils.create_dataclass_frm_bsv(structfile)

utils.log.info('Converting riscv opcodes to regex patterns in : decoder_inst_defines.py')
utils.extract_patterns_from_riscv_opcodes()

def check_spike_dump(testnode):
    abi = testnode['mabi']
    arch = testnode['march']
    isa = testnode['isa']
    work_dir = testnode['work_dir']
    if '64' in isa:
        xlen = 64
    else:
        xlen = 32
    spike_isa = 'rv' + str(xlen) + 'imac'
#    spike_isa += 'm' if 'M' in isa.upper() else ''
#    spike_isa += 'a' if 'A' in isa.upper() else ''
#    spike_isa += 'f' if 'F' in isa.upper() else ''
#    spike_isa += 'd' if 'D' in isa.upper() else ''
#    spike_isa += 'c' if 'C' in isa.upper() else ''

    sim_cmd = 'spike --log spike.dump --log-commits --isa={0} dut.elf'

    if shutil.which('spike') is None:
        utils.log.error('Spike not available in $PATH')
        raise SystemExit

    utils.shellCommand(sim_cmd.format(spike_isa)).run(cwd=work_dir)
    utils.shellCommand(shlex.split('head -n -4 rtl.dump > temp')).run(cwd=work_dir)
    result = filecmp.cmp(work_dir+'/temp', work_dir+'/spike.dump')
    return result

def create_code_mem(testnode):
    # collect all information from the test-list node
    abi = testnode['mabi']
    arch = testnode['march']
    isa = testnode['isa']
    work_dir = testnode['work_dir']
    link_args = testnode['linker_args']
    link_file = testnode['linker_file']
    cc = testnode['cc']
    cc_args = testnode['cc_args']
    asm_file = testnode['asm_file']

    cwd = os.getcwd()

    # build the asm compile command
    compile_cmd = '{0} {1} -march={2} -mabi={3} {4} {5} {6}'.format(\
                cc, cc_args, arch, abi, link_args, link_file, asm_file)

    # include any header file directories
    for x in testnode['extra_compile']:
        compile_cmd += ' ' + x
    for x in testnode['include']:
        compile_cmd += ' -I ' + str(x)

    # generate all compile time macros required for the test
    compile_cmd += ' '.join(map(' -D{0}'.format, testnode['compile_macros']))
    compile_cmd += ' -o dut.elf && '

    # add the objdump command
    objdump_cmd = 'riscv64-unknown-elf-objdump -D dut.elf > dut.disass && '

    # command to generate the code.mem file
    elf2hex_cmd = 'elf2hex 8 4194304 dut.elf 2147483648 > code.mem'

    # run the compile, objdump and elf2hex command
    cmd=compile_cmd + objdump_cmd + elf2hex_cmd
    utils.shellCommand(cmd).run(cwd=work_dir)

    # populate the sim_build with boot files
    os.symlink(cclass_path+'bin/boot.LSB', work_dir+'/boot.LSB_temp')
    os.rename(work_dir+'/boot.LSB_temp', work_dir+'/boot.LSB')

    os.symlink(cclass_path+'bin/boot.MSB', work_dir+'/boot.MSB_temp')
    os.rename(work_dir+'/boot.MSB_temp', work_dir+'/boot.MSB')

    os.symlink(cwd+'/sim_build', work_dir+'/sim_build_temp')
    os.rename(work_dir+'/sim_build_temp', work_dir+'/sim_build')

# here we parameterize the test for each node in the test-list dictionary. For
# each parameter we will be calling the test_cclass module
# the env marker is used for selection from the command by passing the value to
# to -E
#@pytest.mark.env("run")
@pytest.mark.parametrize('testname', testyaml)
def test_run(testname):
    
    utils.log.info('Running Testcase: '+str(testname))

    # get all the details of the test that is allocated to this run
    testcase = testyaml[testname]

    # for the selected test generate the code.mem files
    create_code_mem(testcase)

    # change working directory before you run cocotb.
    #os.chdir(testcase['work_dir'])

    # initiate the fun
    run (
        verilog_sources=[hw_verilog+'/mkTbSoc.v'],
        toplevel_lang = 'verilog',
        toplevel='mkTbSoc',
        module='test_cclass',
        compile_args=compile_args,
#        extra_args=['--trace'],
        plus_args=['+rtldump'],
        extra_env=myenv,
        work_dir=testcase['work_dir'],
        sim_build='./sim_build'
        )
    utils.log.info('Running spike for testcase: ' +str(testname))
    assert check_spike_dump(testcase), "Spike and RTL dumps do match"

# function to merge coverage files
def test_merge():
    mergecovfile = str(os.getcwd())+'/merge_coverage.yml'
    files = []
    for test, testnode in testyaml.items():
        files.append(testnode['work_dir']+'/coverage.yml')
    merge_coverage(utils.log.info, mergecovfile, *files)

         
# This function is used to just compile the verilog code using verilator. This
# is done once before you spawn of regression to avoid each thread from building
# it parallely
#@pytest.mark.env("compile_only")
def test_compile():
    
    utils.log.info('Compile the Verilog')

    # initiate the run
    run (
        verilog_sources=[hw_verilog+'/mkTbSoc.v'],
        toplevel_lang = 'verilog',
        toplevel='mkTbSoc',
        module='test_cclass',
        compile_args=compile_args,
        compile_only=True,
        force_compile=True,
#        extra_args=['--trace'],
        plus_args=['+rtldump'],
        extra_env=myenv,
        sim_build='./sim_build'
        )

# This test is used to clean the build environments that were created because of
# the runs
#@pytest.mark.env("clean")
def test_clean():
    utils.log.info('Running Clean')
    test_artifacts = ['boot.MSB',
                'boot.LSB',
                'code.mem',
                'dut.elf',
                'dut.disass',
                'app_log',
                'rtl.dump', 
                'spike.dump',
                'log', 
                'results.xml', 
                'signature',
                'sim.log'
                'sim_build',
                'dump.vcd']
    global_artifacts = ['report.html','bsvstruct_class.py','struct_info.txt']
    for testname, testnode in testyaml.items():
        utils.log.info('Checking Directory: '+testnode['work_dir']) 
        for a in test_artifacts:
            if os.path.exists(testnode['work_dir']+'/'+a):
                os.remove(testnode['work_dir']+'/'+a)
    simbuild = './sim_build/'
    if os.path.exists(simbuild):
        utils.log.info('Removing : ' +str(simbuild))
        shutil.rmtree(simbuild, ignore_errors=True)
    for a in global_artifacts:
        if os.path.exists('./'+a):
            os.remove('./'+a)

